<?php
/**
 * Template Name: Thank You
*/
get_header();
?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page" style="padding-bottom:0px;">
	<div class="container">
		<div class="table">
			<div class="cell"><?php the_content(); ?></div>
		</div>
	</div>
</section>

<section class="generic-page" style="padding-top:0px;">
	<div class="container">
		<div class="table">
			<div class="cell <?php /* if (get_field('gated') && !is_user_logged_in()) echo ' check-auth padded'; */ ?>">
					<?php /* if (get_field('gated') && !is_user_logged_in()): ?>
						<div class="gated">
							<div class="container single_recipes_from">
								<div class="existing-customers">
											<img class="login_left_logo" alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_left_logo.png">
								<div class="title">Please log in to view this recipe</div>
                                    
								<form name="pp_loginform" id="pp_loginform_recipes" action="" method="post">
									<div id="msg"></div>	
																		
								<p class="login-username login_form_group">
									<input type="text" name="log" id="pp_user_login2" class="input" size="20" required>
									<label for="pp_user_login2">Email</label>
								</p>
								<p class="login-password login_form_group">
									<input type="password" name="pwd" id="pp_user_pass2" autocomplete="new-password" class="input" size="20" required>
									<label for="pp_user_pass2">Password</label>
								</p>
								
								<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme2" value="forever"> Remember Me</label></p>
								<p class="login-submit">
									<input type="submit" name="wp-submit" id="pp_wp-submit2" class="button button-primary" value="Log In">
									<input type="hidden" name="redirect_to" value="<?php echo get_permalink(); ?>">
								</p>
								</form>
									<script type="text/javascript">
									$(document).ready(function(){
										$('#pp_loginform_recipes').submit(function(){
											var username = $('#pp_user_login2').val();
											var password = $('#pp_user_pass2').val();
											var redirect_to = $('#redirect_to').val();
											var tok = username + ':' + password;
											var hash = btoa(tok);
											var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
											
											if(username == '')
											{
												$('#msg').html('<span class="warning">Please enter your username/email in the Email Address field below.</span>');
												return false;
											}
											else if(password == '')
											{
												$('#msg').html('<span class="warning">Please enter your password.</span>');
												return false;
											}
											else
											{	
												$.ajax({
													url : ajaxurl,
													type: "POST",
													data : {action: "login_validation",tok : hash},
													success:function(response, textStatus, jqXHR) 
													{
														if(response == "success")
														 {
															// window.location.href= redirect_to;
															location.reload(true);
														 }
														 else
														 {
															$('#msg').html('<span class="warning">'+response+'</span>');
															return false;
														 }
														 
													},
													error: function(jqXHR, textStatus, errorThrown) 
													{
														alert(jqXHR.responseText);
														return false;  												
													}
												});
												return false;											
											}																		
										});
									});
									</script>
                                   
								</div>
								<div class="new-customers">
									<div class="login_right_img">
									<img alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_right_img.png">
									</div>
										<ul>
											<li class="book_i">Free e-recipe book on sign up</li>
											<li class="check_i">Access to exclusive recipes & content</li>
											<li class="sign_i">Exclusive offers on AK products</li>
											<li class="star_i">Great competitions and prizes</li>
											<li class="letter_i">Never miss a thing with our newsletter</li>
										</ul>
										<p>
											<a class="button" href="<?php echo site_url('/register/'); ?>">sign up</a>
										</p>
									</div>
								</div>
						</div>
					<?php endif; */ ?>
				
				<?php get_template_part('parts/flexible-content'); ?>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer();?>
<script>
(function($){
	$("#pp_loginform_recipes .login_form_group input").on("keyup", function(){
		if($(this).val() != "") {
			$(this).siblings("label").addClass("active");
		} else {
			$(this).siblings("label").removeClass("active");
		}
	});
	
	$('#pp_loginform_recipes').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			validating: 'glyphicon glyphicon-refresh',
			invalid: 'glyphicon glyphicon-remove'
		},
		fields: {
			log: {
				group: '.login_form_group',
				validators: {
					emailAddress: {message: 'Please enter a valid Email'},
					notEmpty: {message: 'Email is required'},
					callback: {
                        callback: function(value, validator, $field) {
							var regs = /[ ]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
                            if (value.length >= 7) {
								var atReg = /[*\%|\$|\&|\*|\£|\&|\"|\(]/g;
								
								if(value.search(atReg) > 0) {
									
									return {
										valid: false,
										message: "The Email field only accepts letters (a-z), numbers, and the following special characters + @ . -"
									}	
								} else {
									var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									
									if(value.search(reg) < 0) {
										return {
											valid: false,
											message: 'Please enter a valid Email'
										}
									}									
								}
								
                                
                            }
							
							
							if(value.length == 0){
								return {
									valid: false,
									message: 'The Email field is mandatory – please enter at least seven characters for this field'
								};
							}
                            return true
                        }
                    }
				}
			},
			pwd: {
				group: '.login_form_group',
				validators: {
					notEmpty: {message: 'Password is required'}
				}
			}
		}
	});
	$('#pp_loginform_recipes .login-password.login_form_group .form-control-feedback').click(function(){
	  if ($("#pp_loginform_recipes .login-password.login_form_group .input").attr("type") == "password") {
		$("#pp_loginform_recipes .login-password.login_form_group .input").attr("type", "text");
	  } else {
		$("#pp_loginform_recipes .login-password.login_form_group .input").attr("type", "password");
	  }
	});
})(jQuery);
</script>
<?php
include('auth_api.php');
class API_NOTIFICATION extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	private static $API_SERVER_KEY = 'AAAAyWajSEs:APA91bEcGw1ZfTo69rDpqy7HF4aMxW3Wq67PVOAge2KrvKI8uqWOu87zRMVsm0WDSamK_bbZBYPPfHcg4sCki99RRGVo3ZuTXswQdbR_moz8h9wQM-AFqgGH2yX0sWHTSywVR31u_3H2';
    private static $is_background = "TRUE";
	public function __construct($u,$p)
	{		
		$this->auth_username = $u;
		$this->auth_password = $p;
	}
	public function push_notification($data)
	{
		if(!empty(get_field('push_notification','option')))
		{
			$appdevice_token = unserialize(get_option('app_device'));
			$tokens_array=array();
			if(!empty($appdevice_token))
			{
				foreach($appdevice_token as $key => $value)
				{
					$tokens_array[]	= $value;
				}
			}
				
			$message = get_field('push_notification','option');
			if(!empty(get_option('push_notification_sent')) && get_option('push_notification_sent') != $message)
			{
				if(!empty($tokens_array))
				{
					$jsonString = $this->sendPushNotificationToFCMSever( $tokens_array, $message);
					update_option( 'push_notification_sent', $message);
					$this->resArr =	array('status'=>'success','response_code'=>200,'response_msg'=>'Notification sent', 'fcm_response'=>json_decode($jsonString));
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>400,'response_msg'=>'Token missing.');
					header("HTTP/1.0 400 Bad Request");
				}
			}
			elseif(empty(get_option('push_notification_sent')) && !empty($message))
			{
				if(!empty($tokens_array))
				{
					$jsonString = $this->sendPushNotificationToFCMSever( $tokens_array, $message);
					add_option( 'push_notification_sent', $message, ' ', 'yes');
					$this->resArr =	array('status'=>'success','response_code'=>200,'response_msg'=>'Notification sent', 'fcm_response'=>json_decode($jsonString));
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>400,'response_msg'=>'Token missing.');
					header("HTTP/1.0 400 Bad Request");
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>400,'response_msg'=>'Need updated notification content to send.');
				header("HTTP/1.0 400 Bad Request");
			}
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>400,'response_msg'=>'Notification content missing.');
			header("HTTP/1.0 400 Bad Request");
		}
		$this->response($this->resArr);
	}
	public function add_devicetoken($data)
	{
		if(!empty($data['device_id']) && !empty($data['device_token']))
		{
				$device_key = $data['device_id'];
				$device_token = $data['device_token'];			
				
				if(!empty(get_option('app_device')))
				{
					$app_device = get_option('app_device');
					
					if(!empty($app_device))
					{
						$app_device = unserialize($app_device);
						if (array_key_exists($device_key,$app_device))
						{
							$app_device[$device_key] = $device_token;
						}
						else
						{
							$app_device[$device_key] = $device_token;
						}
					}
					
					update_option( 'app_device', serialize($app_device) );
				}
				else
				{
					$app_device = array($device_key => $device_token);
					
					update_option( 'app_device', serialize($app_device) );
				}
				$this->resArr =	array('status'=>'success','response_code'=>200,'response_msg'=>'Device ID & Token successfully added !');					
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Data missing');
			header("HTTP/1.0 401 Unauthorized");
		}
		$this->response($this->resArr);
	}
	
	public function get_devicetoken()
	{
		$app_device = get_option('app_device');
		$this->resArr =	array('status'=>'success','response_code'=>200,'tokens'=>unserialize($app_device));
		$this->response($this->resArr);
	}
	
	public function sendPushNotificationToFCMSever($token, $message) {
        $url = 'https://fcm.googleapis.com/fcm/send';
		//print_r($todatatokens);
        $fields = array(
			'registration_ids' => $token,
			'priority' => 10,
			'notification' => array('title' => 'AnnabelKarmel', 'body' =>  $message ,'sound'=>'Default','badge' => '1' ),
		);
        $headers = array(
            'Authorization:key=' . self::$API_SERVER_KEY,
            'Content-Type:application/json'
        );  
		
		// Open connection
		$ch = curl_init();

		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Disabling SSL Certificate support temporarily
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		// Execute post
		$result = curl_exec($ch);
		if($result === FALSE){
			die('Curl failed: ' . curl_error($ch));
		}
		// Close connection
		curl_close($ch);
		return $result;
		die;
    }
	
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}
}
?>
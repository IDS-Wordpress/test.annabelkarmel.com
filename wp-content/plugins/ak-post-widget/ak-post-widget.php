<?php
/**
 * Plugin Name: AK Post Widget
 * Description: Embeds a post/page/product widget into the sidebar.
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

namespace Verb {
	
	class AK_Post_Widget extends \WP_Widget {
		function __construct() {
			global $_wp_additional_image_sizes;
			
			add_image_size('verb-ak-post-widget', 300, 275, true);
			add_image_size('verb-ak-post-widget-padded', 303, 278);
			$_wp_additional_image_sizes['verb-ak-post-widget-padded']['padded'] = true;
			
			add_shortcode('ak_post_widget', [
				$this,
				'render_shortcode'
			]);
			
			parent::__construct('verb_ak_post_widget', 'AK Post Widget', [
				'description' => 'Embeds a post/page/product widget into the sidebar.'
			]);
		}
		
		public static function ak_post_widget($post_id = null) {
			global $post;
			
			if ($post_id) {
				if (!($post_id = icl_object_id($post_id, 'post', false, ICL_LANGUAGE_CODE))) {
					return;
				}
				
				$post = get_post($post_id);
				setup_postdata($post);
			}
			
			include (locate_template('ak-post-widget.php') ?: 'templates/ak-post-widget.php');
			
			wp_reset_postdata();
		}
		
		public function render_shortcode($atts) {
			$atts = shortcode_atts([
				'ids' => '',
				'num_per_row' => 3
			], $atts);
			$atts['num_per_row'] = intval($atts['num_per_row']);
			
			ob_start();		
			if ($atts['ids'] && ($ids = array_filter(array_map(function($item) {
					return icl_object_id($item, 'post', false, ICL_LANGUAGE_CODE);
				}, explode(',', $atts['ids']))))) {
				if (!is_integer($atts['num_per_row']) || 1 > $atts['num_per_row'] || 4 < $atts['num_per_row']) {
					$atts['num_per_row'] = 3;
				}
				echo '<div class="ak-post-widgets shortcode-induced" data-num-per-row="' . $atts['num_per_row'] . '">';
				foreach ($ids as $id) {
					if (1 == $atts['num_per_row']) {
						$GLOBALS['Annabel_Karmel']->ak_large_item($id);
					} elseif (2 == $atts['num_per_row']) {
						$GLOBALS['Annabel_Karmel']->ak_video_item($id);
					} else {
						self::ak_post_widget($id);
					}
				}
				
				echo '</div>';
			}
			
			return ob_get_clean();
		}
		
		public function widget($args, $instance) {
			if (!empty($instance['post_ID'])) {
				echo $args['before_widget'];
				
				self::ak_post_widget($instance['post_ID']);
				
				echo $args['after_widget'];
			}
		}
		
		public function form($instance) {
			$post_ID = (!empty($instance['post_ID'])
				? $instance['post_ID']
				: ''
			);
			?>
			<p>
				<label for="<?php echo $this->get_field_id('post_ID'); ?>">Post ID:</label> 
				<input class="widefat" id="<?php echo $this->get_field_id('post_ID'); ?>" name="<?php echo $this->get_field_name('post_ID'); ?>" type="text" value="<?php echo esc_attr($post_ID); ?>">
			</p>
			<?php
		}
		
		public function update($new_instance, $old_instance) {
			return [
				'post_ID' => esc_attr(!empty($new_instance['post_ID'])
					? $new_instance['post_ID']
					: ''
				)
			];
		}
	}
	add_action('widgets_init', function() {
		register_widget('Verb\AK_Post_Widget');
	});
}

namespace {
	
	function ak_post_widget($post_id = null) {
		Verb\AK_Post_Widget::ak_post_widget($post_id = null);
	}
	
}
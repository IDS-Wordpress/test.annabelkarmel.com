<?php
define('WP_USE_THEMES', false);
include('../wp-load.php');
global $wbdb;
$sqlVersionTest  = "select max(version) as version,type,max(from_date) as from_date from wp_api_version GROUP BY type ORDER BY version DESC";
$recordsTest = $wpdb->get_results($sqlVersionTest);
if(isset($recordsTest) && !empty($recordsTest))
{
	$sqlVersion  = "select max(version) as version,type,max(from_date) as from_date,max(to_date) as to_date from wp_api_version GROUP BY type ORDER BY version DESC";
	$records = $wpdb->get_results($sqlVersion);
	foreach($records as $vRecords)
	{
		$verArgs = array('post_type' => $vRecords->type,
			'post_status' => array( 'mobile', 'publish'),
			'meta_query'=> array(
			'relation' => 'OR',
			array(
				'key'     => 'visibility',
				'value'   => 'Both',
				'compare' => '='
			),
			array(
				'key'     => 'visibility',
				'value'   => 'Mobile',
				'compare' => '='
			)),
			'date_query'=>array(
						array('column'  => 'post_modified_gmt',
						'after' => $vRecords->to_date,
						'inclusive' => true),
						)
			);
		$post_query = new WP_Query($verArgs);
		$count = $post_query->post_count;
		if($count > 0)
		{
			$vRecords->type;
			$to_date = $vRecords->to_date;
			$versionNumber = $vRecords->version;
			$versionNumber = ($versionNumber == 0) ? 1 : $versionNumber+0.1;
			$version = $versionNumber;			
			$from_date_by_old_to_date = date("Y-m-d H:i:s", (strtotime($to_date) + 1));
			
			$sqlVersionInsert = "INSERT INTO ".$wpdb->prefix."api_version SET version='".$version."',type='".$vRecords->type."',from_date='".$from_date_by_old_to_date."',to_date='".date("Y-m-d H:i:s")."'";
			$wpdb->query($sqlVersionInsert);
		}
	}
}
else
{
	$arrTypes = array('post','apps-books','recipes','products');
	foreach($arrTypes as $tYpe)
	{
		$sqlInitVersionInsert = "INSERT INTO ".$wpdb->prefix."api_version SET version='0',type='".$tYpe."',to_date='".date('Y-m-d H:i:s')."'";
		$wpdb->query($sqlInitVersionInsert);
	}
}

echo 'Version of contents has modified if any changes has made after respective last version date';
?>
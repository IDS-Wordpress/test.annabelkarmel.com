<?php get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="singular-wrapper">
					<div class="page-header" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/Product">
						<meta content="Annabel Karmel" itemprop="brand">
						<meta content="<?php the_permalink(); ?>" itemprop="url">
						<div class="title-allergens">
							<h1 itemprop="name"><?php the_title(); ?><?php if( get_field('product_title_2') ): ?><br /> <?php the_field('product_title_2'); ?> <?php endif; ?></h1>
							<?php if ($terms = get_the_terms(get_the_ID(), 'allergen')): ?>
							<div class="allergens">
								<?php foreach ($terms as $term): ?>
								<div class="allergen qtip2" title="<?php echo esc_attr($term->name); ?>">
									<img alt="<?php echo esc_attr($term->name); ?>" src="<?php echo get_field('icon', $term->taxonomy . '_' . $term->term_id)['sizes']['icon']; ?>">
									<?php if ($icon = get_field('hover__active_icon', $term->taxonomy . '_' . $term->term_id)): ?>
									<img alt="<?php echo esc_attr($term->name); ?>" src="<?php echo $icon['sizes']['icon']; ?>">
									<?php endif; ?>
								</div>
								<?php endforeach; ?>
							</div>
							<?php endif; ?>
						</div>
						<?php get_template_part('parts/save-share'); ?>
						<div class="images-info table">
							<?php if (has_post_thumbnail()): ?>
							<div class="cell gallery" data-gallery="<?php $GLOBALS['Annabel_Karmel']->generate_gallery_json(array_merge([
									acf_get_attachment(get_post_thumbnail_id())
								], (($back = get_field('back_of_product'))
									? [
										$back
									]
									: []
								))); ?>">
								<a class="open-gallery" data-index="0">
									<?php the_post_thumbnail('single-recipes-padded', [
										'itemprop' => 'image thumbnailUrl'
									]); ?>
								</a>
								<?php if (get_field('video') || get_field('back_of_product')): ?>
								<div class="image-thumbnails">
									<?php if (get_field('video')): ?>
									<a class="popup-youtube" href="<?php echo get_post_meta(get_the_ID(), 'video', true); ?>">
										<img alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
										<?php the_post_thumbnail('single-recipes-small'); ?>
									</a>
									<?php endif; ?>
									<?php if ($back = get_field('back_of_product')): ?>
									<a class="open-gallery" data-index="1">
										<img alt="<?php echo esc_attr($back['alt'] ?: $back['title']); ?>" src="<?php echo $back['sizes']['single-recipes-small']; ?>" title="<?php echo esc_attr($back['title']); ?>">
									</a>
									<?php endif; ?>
								</div>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<div class="cell">
								<?php if (get_field('product_information')): ?>
								<div class="wysiwyg-content">
									<?php the_field('product_information'); ?>
									<div class="clear"></div>
								</div>
								<?php endif; ?>
								<?php if (($icons = (get_field('allergens__icons') ?: [])) || get_field('nutritionally_approved')): ?>
								<div class="icons">
									<?php if (get_field('nutritionally_approved')): ?>
									<img alt="Nutritionally Approved" src="<?php echo get_template_directory_uri(); ?>/images/nutritionally-approved.png" title="Nutritionally Approved">
									<?php endif; ?>
									<?php foreach ($icons as $icon): ?>
									<img alt="<?php echo esc_attr($icon['alt'] ?: $icon['title']); ?>" src="<?php echo $icon['sizes']['product-icon']; ?>" title="<?php echo esc_attr($icon['title']); ?>">
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
								<?php if (have_rows('retailers')): ?>
								<div class="retailers">
									<span>Buy Now:</span>
									<?php while (have_rows('retailers')): the_row(); ?>
									<a href="<?php the_sub_field('link'); ?>" target="_blank">
										<?php if ('image' == get_sub_field('button_type')): ?>
										<img alt="" src="<?php echo get_template_directory_uri(); ?>/images/<?php the_sub_field('retailer'); ?>.png">
										<?php else: ?>
										<?php if ('amazon' == trim(mb_strtolower(get_sub_field('other_retailer')))): ?>
										<img alt="" src="<?php echo get_template_directory_uri(); ?>/images/amazon-logo_transparent.png">
										<?php else: ?>
										<span class="button"><?php echo ('other' != ($retailer = get_sub_field('retailer'))
											? $retailer
											: get_sub_field('other_retailer')
										); ?></span>
										<?php endif; ?>
										<?php endif; ?>
									</a>
									<?php endwhile; ?>
								</div>
								<?php endif; ?>
								<?php if ($tags = get_the_terms(get_the_ID(), 'product-tag')): ?>
								<div class="recipe-tags">
									<img alt="Product Tags" src="<?php echo get_template_directory_uri(); ?>/images/tags.png">
									<?php echo implode(' &middot; ', array_map(function($item) {
										return '<a href="' . get_term_link($item) . '">' . $item->name . '</a>';
									}, $tags)); ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php if (get_the_content()): ?>
					<div class="wysiwyg-content" itemprop="description">
						<?php the_content(); ?>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					<?php if (get_field('lifestyle_image') || get_field('nutritionally_approved')): ?>
					<div class="additional-product-images">
						<?php if ($lifestyle_image = get_field('lifestyle_image')): ?>
						<div class="gallery" data-gallery="<?php $GLOBALS['Annabel_Karmel']->generate_gallery_json([
							$lifestyle_image
						]); ?>">
							<a class="open-gallery" data-index="0">
								<img alt="<?php echo esc_attr($lifestyle_image['alt'] ?: $lifestyle_image['title']); ?>" src="<?php echo $lifestyle_image['sizes']['product-lifestyle']; ?>" title="<?php echo esc_attr($lifestyle_image['title']); ?>">
							</a>
						</div>
						<?php endif; ?>
						<?php if (get_field('nutritionally_approved')): ?>
						<a class="nutritionally-approved-banner" href="<?php echo site_url('/nutritionist-sarah-almond-bushell/'); ?>">
							<img alt="Nutritionall approved by child nutritionist and dietician Sarah Almond Bushell" src="<?php echo get_template_directory_uri(); ?>/images/nutritionally-approved-banner.png">
						</a>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				
				<?php if ($products = get_field('related_products')): ?>
				<div class="related-content">
					<h2>Related Products</h2>
					<a href="<?php echo get_post_type_archive_link('products'); ?>">View All Products &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets fi-panel">
						<?php foreach ($products as $post): setup_postdata($post); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php $args = [
					'orderby' => 'rand',
					'post_type' => 'products',
					'post__not_in' => [
						get_the_ID()
					],
					'posts_per_page' => 6
				]; ?>
				<?php if ($terms = get_the_terms(get_the_ID(), 'product-category')): ?>
				<?php $args['tax_query'] = array_merge([
					'relation' => 'OR'
				], array_map(function($item) {
					return [
						'taxonomy' => $item->taxonomy,
						'terms' => $item->term_id
					];
				}, $terms)); ?>
				<?php endif; ?>
				<?php $the_query = new WP_Query($args); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>You may also like...</h2>
					<a href="<?php echo get_post_type_archive_link('products'); ?>">View All Products &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>
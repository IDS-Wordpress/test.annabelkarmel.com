<div class="download_widzard">
<?php if(get_sub_field('section_heading')): ?><h3><?php echo get_sub_field('section_heading'); ?></h3><?php endif; ?>
<?php if(get_sub_field('download_items')):
$download_items = get_sub_field('download_items');
if(!empty($download_items)):
?>
<ul class="download_items">
<?php

foreach($download_items as $dwld):
$loginRequired = 0;
$filetTitle = ($dwld['download_caption']) ? $dwld['download_caption'] : $dwld['downloadable_file']['title'];

if($dwld['gated'])
{
	$loginRequired = 1;
	if(is_user_logged_in())
	{
	  $loginRequired = 0;
	}
}
$DownloadLink = ($loginRequired)? '#login' : get_template_directory_uri().'/fd.php?token='.base64_encode($dwld['downloadable_file']['url']);
?>
<li><a href="<?php echo $DownloadLink; ?>" title='<?php echo $filetTitle; ?>' ><?php echo $filetTitle; ?></a></li>
<?php endforeach; ?>
</ul>
<?php
endif;
endif;
?>
</div>
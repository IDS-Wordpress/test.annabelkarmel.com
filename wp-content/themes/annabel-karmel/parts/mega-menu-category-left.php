<?php 
global $mega_menu_cat_id;
$term = get_term($mega_menu_cat_id);
$link = get_term_link( $term->term_id, $term->taxonomy );
?>
<div class="mega-menu-category left clearfix">
	<a href="<?php echo $link ?>">
		<div class="text">
			<h4><?php echo $term->name; ?></h4>
			<?php if ($children = get_term_children($term->term_id, $term->taxonomy)): ?>
			<ul>
				<?php foreach ($children as $child): $child = get_term($child, $term->taxonomy); ?>
				<li>
					<a href="<?php echo get_term_link($child->term_id, $child->taxonomy); ?>"><?php echo $child->name; ?></a>
				</li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
	</a>
</div>
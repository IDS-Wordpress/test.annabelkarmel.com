<div class="feel-inspired">
	<h2><?php echo preg_replace('/\*([^*]+)\*/', '<span>$1</span>', (get_sub_field('title') ?: 'Feel *Inspired*')); ?></h2>
	<h3>Popular Recipes &amp; Products</h3>
	<span class="bold">FILTER BY:</span>
	<div class="buttons">
		<button class="selected" data-panel="all">All</button>
		<?php foreach (($terms = get_terms([
			'parent' => 0,
			'taxonomy' => 'recipe-category'
		])) as $term): ?>
		<button data-panel="<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
		<?php endforeach; ?>
	</div>
	<div class="recipes">
		<?php $the_query = new WP_Query([
			'meta_key' => 'force_popular',
			'meta_value' => 1,
			'post_type' => 'recipes',
			'posts_per_page' => 8
		]); ?>
		<?php $second_query = (8 > $the_query->post_count
			? new WP_Query([
				'post_type' => 'recipes',
				'posts_per_page' => 8 - $the_query->post_count
			])
			: false
		); ?>
		<?php if ($the_query->have_posts() || ($second_query && $second_query->have_posts())): ?>
		<div class="recipes-all">
			<h3>Popular Recipes</h3>
			<a href="<?php echo get_post_type_archive_link('recipes'); ?>">View all recipes &raquo;</a>
			<div class="clear"></div>
			<div class="ak-post-widgets fi-panel owl-carousel owl-theme" data-items="3">
				<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php if ($second_query && $second_query->have_posts()): ?>
				<?php while ($second_query->have_posts()): $second_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php foreach ($terms as $term): ?>
		<?php $the_query = new WP_Query([
			'meta_key' => 'force_popular',
			'meta_value' => 1,
			'post_type' => 'recipes',
			'posts_per_page' => 8,
			'tax_query' => [
				[
					'taxonomy' => 'recipe-category',
					'terms' => $term->term_id
				]
			]
		]); ?>
		<?php $second_query = (8 > $the_query->post_count
			? new WP_Query([
				'post_type' => 'recipes',
				'posts_per_page' => 8 - $the_query->post_count,
				'tax_query' => [
					[
						'taxonomy' => 'recipe-category',
						'terms' => $term->term_id
					]
				]
			])
			: false
		); ?>
		<?php if ($the_query->have_posts() || ($second_query && $second_query->have_posts())): ?>
		<div class="recipes-<?php echo $term->slug; ?>">
			<h3>Popular <?php echo $term->name; ?> Recipes</h3>
			<a href="<?php echo get_term_link($term); ?>">View all <?php echo mb_strtolower($term->name); ?> recipes &raquo;</a>
			<div class="clear"></div>
			<div class="ak-post-widgets fi-panel owl-carousel owl-theme" data-items="3">
				<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php if ($second_query && $second_query->have_posts()): ?>
				<?php while ($second_query->have_posts()): $second_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<div class="products">
		<?php $the_query = new WP_Query([
			'meta_key' => 'force_popular',
			'meta_value' => 1,
			'post_type' => [
				'apps-books',
				'products'
			],
			'posts_per_page' => 8
		]); ?>
		<?php $second_query = (8 > $the_query->post_count
			? new WP_Query([
				'post_type' => [
					'apps-books',
					'products'
				],
				'posts_per_page' => 8 - $the_query->post_count
			])
			: false
		); ?>
		<?php if ($the_query->have_posts() || ($second_query && $second_query->have_posts())): ?>
		<div class="products-all">
			<h3>Popular Products</h3>
			<a href="<?php echo get_post_type_archive_link('products'); ?>">View all products &raquo;</a>
			<div class="clear"></div>
			<div class="ak-post-widgets fi-panel owl-carousel owl-theme" data-items="3">
				<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php if ($second_query && $second_query->have_posts()): ?>
				<?php while ($second_query->have_posts()): $second_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php foreach ($terms as $term): ?>
		<?php if ('pregnancy' != $term->slug): ?>
		<?php $the_query = new WP_Query([
			'meta_key' => 'force_popular',
			'meta_value' => 1,
			'post_type' => [
				'apps-books',
				'products'
			],
			'posts_per_page' => 8,
			'tax_query' => [
				'relation' => 'OR',
				[
					'field' => 'slug',
					'taxonomy' => 'app-book-category',
					'terms' => $term->slug
				],
				[
					'field' => 'slug',
					'taxonomy' => 'product-category',
					'terms' => $term->slug
				]
			]
		]); ?>
		<?php $second_query = (8 > $the_query->post_count
			? new WP_Query([
				'post_type' => [
					'apps-books',
					'products'
				],
				'posts_per_page' => 8 - $the_query->post_count,
				'tax_query' => [
					'relation' => 'OR',
					[
						'field' => 'slug',
						'taxonomy' => 'app-book-category',
						'terms' => $term->slug
					],
					[
						'field' => 'slug',
						'taxonomy' => 'product-category',
						'terms' => $term->slug
					]
				]
			])
			: false
		); ?>
		<?php if ($the_query->have_posts() || ($second_query && $second_query->have_posts())): ?>
		<div class="products-<?php echo $term->slug; ?>">
			<h3>Popular <?php echo $term->name; ?> Products</h3>
			<a href="<?php echo get_post_type_archive_link('products'); ?>">View all products &raquo;</a>
			<div class="clear"></div>
			<div class="ak-post-widgets fi-panel owl-carousel owl-theme" data-items="3">
				<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php if ($second_query && $second_query->have_posts()): ?>
				<?php while ($second_query->have_posts()): $second_query->the_post(); ?>
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
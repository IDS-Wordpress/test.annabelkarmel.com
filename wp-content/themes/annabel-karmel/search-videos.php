<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container videos-main cookies-with-water">
		<div class="table">
			<div class="cell">
			<div class="flexible-content videos-top-section" itemprop="articleBody">
					<div class="feature_section">
						<div class="leftpart">
							<?php
						$args = array(
								'post_type'=> 'videos',
								'meta_key' => 'featured_video',
								'meta_value' => 1,
								'orderby'=> 'date',
								'order'>'DESC',
								'posts_per_page'=>1,								
							);
						query_posts( $args );
						if(have_posts()):
							while(have_posts()): the_post();							 
							?>
								<div class="video-wrapper">
									<?php //the_field('video_url');
									$video = get_field( 'video_url' );
									preg_match('/src="(.+?)"/', $video, $matches_url );
									$src = $matches_url[1];
									
									preg_match('/embed(.*?)?feature/', $src, $matches_id );
									$id = $matches_id[1];
									$id = str_replace( str_split( '?/' ), '', $id );

									//var_dump( $video );
																
									?>
									<div id="holder">
										<div id="stock-video"></div>
										<a href="javascript:void(0);" id="stock-play-btn" class="player-icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/play-icon.png" />
										</a>
									</div>
								</div>
								<script>
								//youtube script
								var tag = document.createElement('script');
								tag.src = "//www.youtube.com/iframe_api";
								var firstScriptTag = document.getElementsByTagName('script')[0];
								firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
								var player;
								onYouTubeIframeAPIReady = function () {
									player = new YT.Player('stock-video', {
										width: '100%',
										height: '360',
										videoId: '<?php echo $id; ?>',  // youtube video id
										playerVars: {
											'autoplay': 1,
											'rel': 0,
											'showinfo': 0
										},
										events: {
											'onReady': onPlayerReady,
											'onStateChange': onPlayerStateChange
										}
									});
								}
								onPlayerReady = function (event) {
									$('#stock-play-btn').fadeOut('normal');
								}
								onPlayerStateChange = function (event) {
									if (YT.PlayerState.UNSTARTED || event.data == YT.PlayerState.BUFFERING || event.data == YT.PlayerState.PLAYING) {
										$('#stock-play-btn').fadeOut('normal');
									}
									if (event.data == YT.PlayerState.PAUSED) {
										$('#stock-play-btn').fadeIn('normal');
									}
									if (event.data == YT.PlayerState.ENDED) {
										$('#stock-play-btn').fadeIn('normal');
										//player.playVideo(); 
									}
								}
								$(document).on('click', '#stock-play-btn', function () {
									$(this).fadeOut('normal');
									player.playVideo();
								});
								</script>
							
							<?php
							endwhile;
						endif;
						wp_reset_query();
						?>
						</div>
						<div class="rightpart">
							<div class="advertpart">
							<?php
							if(get_field('adzone_for_video','option')):
							echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(get_field('adzone_for_video','option'), 'post', false, ICL_LANGUAGE_CODE) . ']');
							endif;
							?>
							</div>
							<div class="searchpart">
							<form action="<?php echo home_url( '/' ); ?>" method="get">
							<p><input type="text" name="s" value="<?php echo get_search_query(); ?>" placeholder="Search all videos"><input type="submit"></p>
							<input type="hidden" name="post_type" value="videos">
							</form>
							</div>
						</div>				
					</div>
				</div>
			
			<div class="video-listing">
				<?php if (have_posts()):  ?>
				<div class="recipe-filter">
					<h2>Search results for '<?php echo get_search_query(); ?>'</h2>
				</div>
				<?php while(have_posts()): the_post(); ?>
				<div class="video-list-item">
					<div class="post-widget video-widget" data-post-type="<?php echo esc_attr(get_post_type()); ?>">
						<button type="button" class="mfp-close" data-post-id="<?php echo get_the_ID(); ?>">×</button>
						<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
							<div class="post-widget-header">
								<h4><?php if(get_field('short_title')){echo get_field('short_title');} else { echo get_the_title(); if( get_field('video_title_2') ): echo '<br>'.get_field('video_title_2');  endif; } ?></h4>           
							</div>	
							<div class="post-widget-thumbnail">
								<?php the_post_thumbnail('video-widget'); ?>
							</div>
						</a>
						<?php if (in_array(get_post_type(), ['videos']) && get_the_excerpt()): ?>
							<span class="excerpt"><?php the_excerpt(); ?></span>
						<?php endif; ?>
						<div class="viewlink">
							<a href="<?php the_permalink(); ?>">VIEW</a>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				<?php else: ?>
				<div class="recipe-filter">
					<h2>No search results for '<?php echo get_search_query(); ?>'</h2>
				</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
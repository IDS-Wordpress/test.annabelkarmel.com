<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
?>
<?php
global $post;
$fp_col = false;
if($cards = get_sub_field('cards')): ?>
<div class="front-page-board">
<?php
foreach ($cards as $card):
if ($card['card_type']=='Small')
{
	$content2 = $card['content2'];
	if (!$fp_col)
	{
		$fp_col = true;
		?>
		<div class="fp-col">
		<?php
	}
	if(!empty($content2)):
	?>
		<div class="fp-small-cards <?php if($isLoginRequired) { echo 'loginRequired'; }?>">
        <?php if($isLoginRequired) { ?>
    <div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
    <?php
    }
    ?>
			<?php foreach($content2 as $item): ?>
			<?php $post = $item; setup_postdata($post); ?>
			<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
			<?php endforeach; ?>
		</div>
	<?php
	endif;
}
elseif ($card['card_type']=='Large')
{
	
	$post = $card['content'];
	setup_postdata( $post );
	if (!$fp_col): $fp_col = true; ?>
		<div class="fp-col">
	<?php endif; ?>
		<div class="owl-carousel owl-theme" data-items="1">			
			<div class="item">
				<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false, true); ?>
			</div>
		</div>
		<?php
	wp_reset_postdata();
}
elseif ($card['card_type']=='Break' && $fp_col){
	?>
	</div>
		<div class="fp-col">
	<?php
}
endforeach;
if ($fp_col): $fp_col=false; ?>
</div>
<?php endif; ?>
</div>
<?php endif; ?>
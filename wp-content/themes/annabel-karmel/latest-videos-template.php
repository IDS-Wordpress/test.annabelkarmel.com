<?php
/**
 * Template Name: Latest Video Page
 */
get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container videos-main cookies-with-water">
		<div class="table">
			<div class="cell">
			<div class="flexible-content videos-top-section" itemprop="articleBody">
           <h1> <?php the_title();?></h1>
           <?php the_content();?>
					<div class="feature_section">
						<div class="leftpart">
						<?php
						$args = array(
								'post_type'=> 'videos',
								'meta_key' => 'featured_video',
								'meta_value' => 1,
								'orderby'=> 'date',
								'order'>'DESC',
								'posts_per_page'=>1,								
							);
						query_posts( $args );
						if(have_posts()):
							while(have_posts()): the_post();							 
							?>
								<div class="video-wrapper">
									<?php //the_field('video_url');
									$video = get_field( 'video_url' );
									preg_match('/src="(.+?)"/', $video, $matches_url );
									$src = $matches_url[1];
									
									preg_match('/embed(.*?)?feature/', $src, $matches_id );
									$id = $matches_id[1];
									$id = str_replace( str_split( '?/' ), '', $id );

									//var_dump( $video );
																
									?>
									<div id="holder">
										<div id="stock-video"></div>
										<a href="javascript:void(0);" id="stock-play-btn" class="player-icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/play-icon.png" />
										</a>
									</div>
								</div>
								<script>
								//youtube script
								var tag = document.createElement('script');
								tag.src = "//www.youtube.com/iframe_api";
								var firstScriptTag = document.getElementsByTagName('script')[0];
								firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
								var player;
								onYouTubeIframeAPIReady = function () {
									player = new YT.Player('stock-video', {
										width: '100%',
										height: '360',
										videoId: '<?php echo $id; ?>',  // youtube video id
										playerVars: {
											'autoplay': 1,
											'rel': 0,
											'showinfo': 0
										},
										events: {
											'onReady': onPlayerReady,
											'onStateChange': onPlayerStateChange
										}
									});
								}
								onPlayerReady = function (event) {
									$('#stock-play-btn').fadeOut('normal');
								}
								onPlayerStateChange = function (event) {
									if (YT.PlayerState.UNSTARTED || event.data == YT.PlayerState.BUFFERING || event.data == YT.PlayerState.PLAYING) {
										$('#stock-play-btn').fadeOut('normal');
									}
									if (event.data == YT.PlayerState.PAUSED) {
										$('#stock-play-btn').fadeIn('normal');
									}
									if (event.data == YT.PlayerState.ENDED) {
										$('#stock-play-btn').fadeIn('normal');
										//player.playVideo(); 
									}
								}
								$(document).on('click', '#stock-play-btn', function () {
									$(this).fadeOut('normal');
									player.playVideo();
								});
								</script>
							
							<?php
							endwhile;
						endif;
						wp_reset_query();
						?>
						</div>
						<div class="rightpart">
							<div class="advertpart">
							<?php
							if(get_field('adzone_for_video','option')):
							echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(get_field('adzone_for_video','option'), 'post', false, ICL_LANGUAGE_CODE) . ']');
							endif;
							?>
							</div>
							<div class="searchpart">
							<form action="<?php echo home_url( '/' ); ?>" method="get">
							<p><input type="text" name="s" placeholder="Search all videos"><input type="submit"></p>
							<input type="hidden" name="post_type" value="videos">
							</form>
							</div>
						</div>				
					</div>
				</div>
				
			
			<div class="carousel_heading recipe-filter">
				<h2>Latest Videos</h2>
			</div>
				<?php
				wp_reset_query();
				if (have_posts()): the_post(); ?>
					<div class="video-listing">
						<?php echo do_shortcode('[ajax_load_more id="2197759369" preloaded="true" seo="true" repeater="template_5" post_type="videos" posts_per_page="8" pause="true" scroll="false" transition="fade" images_loaded="true" button_label="Load More" button_loading_label="Loading Videos...."]'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
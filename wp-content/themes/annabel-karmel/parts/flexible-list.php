<?php if ($title = get_sub_field('title')): ?>
<div class="list-items-title"><?php echo $title; ?></div>
<?php endif; ?>
<?php if ($list_items = get_sub_field('list_items')): ?>
<div class="list-items wysiwyg-content">
	<?php if ('ordered' == ($type = get_sub_field('type'))): ?>
	<ol class="column-count-<?php the_sub_field('number_of_columns'); ?>">
	<?php else: ?>
	<ul class="column-count-<?php the_sub_field('number_of_columns'); ?> <?php echo $type; ?>">
	<?php endif; ?>
		<li><?php echo implode('</li><li>', explode('<br>', preg_replace('/<br\s*\/?>/', '<br>', $list_items))); ?></li>
	<?php if ('ordered' == $type): ?>
	</ol>
	<?php else: ?>
	</ul>
	<?php endif; ?>
</div>
<?php endif; ?>
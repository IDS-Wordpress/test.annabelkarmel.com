<?php
/**
 * Plugin Name: Verb Disable Admin Notifications
 * Description: Prevents new user registration and password reset emails being sent to the site administrator (by default, that's Warren).
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

if (!function_exists('wp_new_user_notification')) {
	
	function wp_new_user_notification() { }
	
}

if (!function_exists('wp_password_change_notification')) {
	
	function wp_password_change_notification() { }
	
}
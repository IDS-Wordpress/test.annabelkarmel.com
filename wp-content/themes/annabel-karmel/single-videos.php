<?php get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="singular-wrapper">
					<div class="page-header" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting">
						<meta content="<?php echo the_time('Y-m-d'); ?>" itemprop="datePublished">
						<meta content="<?php the_permalink(); ?>" itemprop="url">
						<h1 itemprop="name"><?php the_title(); ?><?php if( get_field('video_title_2') ): ?><br /><?php the_field('video_title_2');  endif; ?></h1>
						<?php get_template_part('parts/save-share'); ?>

						<div class="video-wrapper">
							<?php //the_field('video_url');
							$video = get_field( 'video_url' );
							preg_match('/src="(.+?)"/', $video, $matches_url );
							$src = $matches_url[1];
							
							preg_match('/embed(.*?)?feature/', $src, $matches_id );
							$id = $matches_id[1];
							$id = str_replace( str_split( '?/' ), '', $id );

							//var_dump( $video );
														
							?>
							<div id="holder">
								<div id="stock-video"></div>
								<a href="javascript:void(0);" id="stock-play-btn" class="player-icon">
									<img src="<?php echo get_template_directory_uri(); ?>/images/play-icon.png" />
								</a>
							</div>
						</div>
						
						<script>
						//youtube script
						var tag = document.createElement('script');
						tag.src = "//www.youtube.com/iframe_api";
						var firstScriptTag = document.getElementsByTagName('script')[0];
						firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
						var player;
						onYouTubeIframeAPIReady = function () {
							player = new YT.Player('stock-video', {
								width: '100%',
								height: '360',
								videoId: '<?php echo $id; ?>',  // youtube video id
								playerVars: {
									'autoplay': 1,
									'rel': 0,
									'showinfo': 0
								},
								events: {
									'onReady': onPlayerReady,
									'onStateChange': onPlayerStateChange
								}
							});
						}
						onPlayerReady = function (event) {
							$('#stock-play-btn').fadeOut('normal');
						}
						onPlayerStateChange = function (event) {
							if (YT.PlayerState.UNSTARTED || event.data == YT.PlayerState.BUFFERING || event.data == YT.PlayerState.PLAYING) {
								$('#stock-play-btn').fadeOut('normal');
							}
							if (event.data == YT.PlayerState.PAUSED) {
								$('#stock-play-btn').fadeIn('normal');
							}
							if (event.data == YT.PlayerState.ENDED) {
								$('#stock-play-btn').fadeIn('normal');
								//player.playVideo(); 
							}
						}
						$(document).on('click', '#stock-play-btn', function () {
							$(this).fadeOut('normal');
							player.playVideo();
						});
						</script>
					</div>
					<?php get_template_part('parts/flexible-content'); ?>
                    
                    <div style="padding:0 2em 2em 2em;"class="wysiwyg-content itemprop="description">
						<?php the_content(); ?>
					</div>
                    
					<div class="page-footer">
						<?php get_template_part('parts/save-share'); ?>
					</div>
				</div>
				
				<?php if ($videos = get_field('related_videos')): ?>
				<div class="related-content">
					<h2>Related Videos</h2>
					<div class="ak-post-widgets fi-panel">
						<?php foreach ($videos as $post): setup_postdata($post); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php $the_query = new WP_Query([
					'orderby' => 'rand',
					'post_type' => 'videos',
					'post__not_in' => [
						get_the_ID()
					],
					'posts_per_page' => 6
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>You may also like...</h2>
					<a href="<?php echo get_post_type_archive_link('videos'); ?>">View All Videos &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?php get_footer(); ?>
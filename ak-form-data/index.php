<?php
error_reporting(E_ALL);
/**
 * XLS parsing uses php-excel-reader from http://code.google.com/p/php-excel-reader/
 */
	$_GET['File'] = '2018-face-of-bibetta-2018-02-01.csv';
	
	if (isset($argv[1]))
	{
		$Filepath = $argv[1];
	}
	elseif (isset($_GET['File']))
	{
		$Filepath = $_GET['File'];
	}
	else
	{
		if (php_sapi_name() == 'cli')
		{
			echo 'Please specify filename as the first argument'.PHP_EOL;
		}
		else
		{
			echo 'Please specify filename as a HTTP GET parameter "File", e.g., "/test.php?File=test.xlsx"';
		}
		exit;
	}

	// Excel reader from http://code.google.com/p/php-excel-reader/
	require('php-excel-reader/excel_reader2.php');
	require('SpreadsheetReader.php');

	date_default_timezone_set('UTC');

	$StartMem = memory_get_usage();
	/*echo '---------------------------------'.PHP_EOL;
	echo 'Starting memory: '.$StartMem.PHP_EOL;
	echo '---------------------------------'.PHP_EOL;*/

	try
	{
		$Spreadsheet = new SpreadsheetReader($Filepath);
		$BaseMem = memory_get_usage();

		$Sheets = $Spreadsheet -> Sheets();
		
		$arraySheet = array();
		foreach ($Sheets as $Index => $Name)
		{
			
			$comArrs = array();			
			$arraySheet[$Name]=array();
			$Spreadsheet -> ChangeSheet($Index);
			$arraySheetTab = array();
			
			foreach ($Spreadsheet as $Key => $Row)
			{				
				array_push($arraySheetTab,$Row);				
			}
			$fields = array_shift($arraySheetTab);
			
			
			$tempArr=array();
			foreach($arraySheetTab as $rec)
			{
				$tempArr = @array_combine($fields,$rec);
				array_push($comArrs,$tempArr);
			}
			
			echo '<style>
				.paginationBox
				{
					margin-top: 20px;
				}
				.paginationBox a
				{
					padding: 2px;
					margin: 2px;
					color: #000;
					text-decoration: none;
					border: 2px solid;
					background: #CCC;
				}
				.paginationBox a.active {
					color: #FF0000;
					background: #fc9f9f;
				}
			</style>';
			$totalItems = count($comArrs);
			echo "<h1> #39 : 2018 Face of Bibetta ( ".$totalItems.")</h1>
			<table width='100%' border='1' cellpadding='5' style='border-collapse: collapse; border-color: #CCC;'>";
			echo "<tr>
			<th align='left' style='background-color: #CCC;'>Parent Full Name</th>
			<th align='center' style='background-color: #CCC;'>Image 1</th>
			<th align='center' style='background-color: #CCC;'>Image 2</th>
			</tr>";
			
			$show_per_page = 100;
			$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
			$pageNav = paganation_nav($comArrs, $page);
            $comArrs = paganation($comArrs, $page);
			foreach($comArrs as $rec)
			{
				//echo "<pre>"; print_r($rec); echo "</pre>";
				echo "<tr>
				<td align='left' valign='top'>".$rec['Your full name (parent or guardian)']."</td>
				<td align='center'>
				<a href='".$rec['Upload first image of your child (mandatory)']."' target='_blank'>
				<img src='".$rec['Upload first image of your child (mandatory)']."' width='100'>
				</a>
				</td>
				<td align='center'>";
				if(isset($rec['Upload second image of your child (optional)']) && !empty($rec['Upload second image of your child (optional)']))
				{
					echo "<a href='".$rec['Upload second image of your child (optional)']."' target='_blank'>
				<img src='".$rec['Upload second image of your child (optional)']."' width='100'>
				</a>";
				}
				echo "</td>
				</tr>";
			}
			echo "</table>";

			echo $pageNav; 
			
		}
		
	}
	catch (Exception $E)
	{
		echo $E -> getMessage();
	}

	function paganation($display_array, $page) {
        global $show_per_page;

        $page = $page < 1 ? 1 : $page;

        // start position in the $display_array
        // +1 is to account for total values.
        $start = ($page - 1) * ($show_per_page + 1);
        $offset = $show_per_page + 1;

        $outArray = array_slice($display_array, $start, $offset);

        return $outArray;
    }
	function paganation_nav($display_array, $page) {
        global $show_per_page;

		$total = count( $display_array );
		$totalPages = ceil( $total/ $show_per_page );
		$actual_link = 'http://'.$_SERVER['HTTP_HOST'].'/ak-form-data';
		
		$opHtml = '';
		$opHtml.='<div class="paginationBox">';
		if($page>1)
		{
			$prevpage = $page-1;
			$opHtml.='<a href="'.$actual_link.'?page='.$prevpage.'">Prev</a>';
		}
		
		for($i=1; $i<=$totalPages; $i++)
		{
			$current = ($page==$i)?'class="active"':'';
			$opHtml.='<a href="'.$actual_link.'?page='.$i.'" '.$current.'>'.$i.'</a>';
		}
		
		if($page < $totalPages)
		{
			$nextpage = $page+1;
			$opHtml.='<a href="'.$actual_link.'?page='.$nextpage.'">Next</a>';
		}
		$opHtml.='</div>';
		
		return $opHtml;
    }
?>
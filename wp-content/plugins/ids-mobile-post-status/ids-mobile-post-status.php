<?php 
/*
Plugin Name: IDS Post Status (Mobile Only)
Plugin URI: https://www.idslogic.com/
Description: Creates an custom post status.
Version: 1.0
Author: WP Team
Author URI: https://www.idslogic.com/
*/

class IDS_mobil_post_status_plugin {

var $newstatusname = 'mobile';
var $newstatuslabel = 'Mobile Only';
var $newstatuslabel_count = 'Mobile Only <span class="count">(%s)</span>';
var $title_label_field_name = 'ids_mobile_post_status-title_label';
var $message_field_name = 'ids_mobile_post_status_message';
var $hidden_field_name = 'ids_mobile_post_status-submit_hidden';
var $publicly_available = 'public';
var $opt_name = 'ids_mobile_post_status_options';
var $namespace = 'ids_mobile_post_status';

var $filename;
var $options;


private function mobile_post_by_id($postid)
{
	$update = array( 'ID' => $postid, 'post_status' => $this->newstatusname );
	wp_update_post($update);
}

public function plugin_menu() {
	add_options_page('Mobile Status Options', 'Mobile Only Status', 'manage_options', $this->filename, array($this,"plugin_options"));
}


function current_user_can_view() {
	/**
	 * Default capability to grant ability to view Archived content (if the status is set to non public)
	 *
	 * @since 0.3.0
	 *
	 * @return string
	 */
	$capability = 'read_private_posts';

	return current_user_can( $capability );
}

function plugin_options() {

	if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}

    // variables for the field and option names 
   

 // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    if( isset($_POST[ $this->hidden_field_name ]) && $_POST[ $this->hidden_field_name ] == 'Y' ) 
	{
		// Read their posted value

		if (($_POST[ $this->publicly_available ] == "0") || ($_POST[ $this->publicly_available ] == "1"))
		{	  

			$options[ $this->publicly_available ] = $_POST[ $this->publicly_available ];

		}

		if( isset($_POST[ $this->title_label_field_name ]))
		{
			$options[$this->title_label_field_name] = sanitize_text_field($_POST[ $this->title_label_field_name ]);
		}

		if( isset($_POST[ $this->message_field_name ]))
		{
			$allowed_tags = wp_kses_allowed_html( 'post' );
			$options[ $this->message_field_name ] = wp_kses($_POST[ $this->message_field_name ], $allowed_tags);
		}

		if (update_option( $this->opt_name, $options ))
		{
			$this->options = get_option($this->opt_name);
			// Put an settings updated message on the screen
			?>
			<div class="updated"><p><strong><?php _e('settings saved.', $this->namespace ); ?></strong></p></div>
			<?php

		}

    } 

    // Now display the settings editing screen
    echo '<div class="wrap">';
    // header
    echo "<h1>" . __( 'IDS Mobile Only Post Status Settings', $this->namespace ) . "</h1>";
    // settings form    
    ?>

<form name="form1" method="post" action="">
<input type="hidden" name="<?php echo $this->hidden_field_name; ?>" value="Y">

<p><label for="<?php echo $this->publicly_available; ?>"><?php _e("Can Archived Posts be read publicly:", $this->namespace); ?></label>
<select name="<?php echo $this->publicly_available; ?>" id="<?php echo $this->publicly_available; ?>">
<option value="1" <?php if($this->options[$this->publicly_available] == 1){ echo 'selected="selected"'; }  ?>>Yes - Available Publicly</option>
<option value="0" <?php if($this->options[$this->publicly_available] == 0){ echo 'selected="selected"';}  ?>>No - Available Privately( Only Administrator and Editor can view posts )</option>
</select>
</p>



<p><strong><label for="<?php echo $this->title_label_field_name; ?>"><?php _e("Title Label:", $this->namespace); ?></label></strong><br/>
<?php _e("This label will appear after the title for mobile content only on the front end of your website", $this->namespace); ?><br/>
<input type="text" name="<?php echo $this->title_label_field_name; ?>" id="<?php echo $this->title_label_field_name; ?>" value="<?php echo $this->options[$this->title_label_field_name] ; ?>" size="20" />
</p>

<p><label for="<?php echo $this->message_field_name; ?>"><?php _e("Message For Mobile Only:", $this->namespace); ?></label><br/>
<?php
$settings = array( 'media_buttons' => false );
wp_editor( $this->options[$this->message_field_name], $this->message_field_name, $settings);
?>
</p>



<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
</p>

</form>
</div>

<?php
}
function create_mobile_custom_post_status()
{

	if ($this->options[ $this->publicly_available])
	{
		$public = $this->options[ $this->publicly_available ];
	}
	else
	{
		$public = $this->current_user_can_view();
	}
	foreach ( get_post_types( array('public'   => true ), 'names' ) as $posttype )
	{		
		$args_post_status = array(
		'label'                     => _x( $this->newstatuslabel, $posttype ),
		'public'                    => $public,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( $this->newstatuslabel_count, $this->newstatuslabel_count) );
		if($public==0)
		{
			$args_post_status['private'] = true;
		}			  
		register_post_status( $this->newstatusname,$args_post_status);
	}

}

function add_mobileonly_message($content)
{
	global $post;
	if (is_singular())
	{
		if ($post->post_status == $this->newstatusname)
		{
			$content = $this->options[$this->message_field_name].$content;
		}
	}
	return $content;
}

function exclude_archive_post_status_from_main_query( $query )
{
	if ( $query->is_main_query() && !is_admin() && !is_singular() && !$query->is_search())
	{
		if ( current_user_can('read_private_posts') )
		{
			$post_status = array( 'publish', 'private' );
		}
		else
		{
			$post_status = array( 'publish');
		}
	$query->set( 'post_status', $post_status );
	}
}

function exclude_archive_post_status_from_feed( $query )
{
	if ($query->is_feed)
	{
		if ( current_user_can('read_private_posts') )
		{
			$post_status = array( 'publish', 'private' );
		}
		else
		{
			$post_status = array( 'publish');
		}
		$query->set( 'post_status', $post_status );
	}
}

function display_archive_state( $states ) {
     global $post;
     $arg = get_query_var( 'post_status' );
     if($arg != $this->newstatusname){
          if($post->post_status == $this->newstatusname){
               return array(ucwords($this->newstatuslabel));
          }
     }
    return $states;
}

function append_post_status_list(){
     global $post;
          if($post->post_status == $this->newstatusname){
          echo '<script>jQuery(document).ready(function($){
				$("#post-status-display" ).text("'.ucwords($this->newstatuslabel).'");
				$("select#post_status").append("<option value=\"'.$this->newstatusname.'\" selected=\"selected\">'.ucwords($this->newstatuslabel).'</option>");
				$(".misc-pub-post-status label").append("<span id=\"post-status-display\"> '.ucwords($this->newstatuslabel).'</span>");
          });
          </script>
          ';
          }
		  else
		  {
			echo '<script>
			  jQuery(document).ready(function($){
					$("select#post_status").append("<option value=\"'.$this->newstatusname.'\" >'.ucwords($this->newstatuslabel).'</option>");
			  });
			  </script>'; 
		  }
} 

function append_post_status_list_on_new()
{
	global $post;

	echo '<script>
		  jQuery(document).ready(function($){
				$("select#post_status").append("<option value=\"'.$this->newstatusname.'\" >'.ucwords($this->newstatuslabel).'</option>");
		  });
		  </script>'; 
	
}

public function append_post_status_bulk_edit() {

echo '<script>
jQuery(document).ready(function($){

$(".inline-edit-status select ").append("<option value=\"'.$this->newstatusname.'\">'.ucwords($this->newstatuslabel).'</option>");

});
</script>';

}


// add a settings link next to deactive / edit
public function add_settings_link( $links, $file ) {

	if( $file == $this->filename ){
		$links[] = '<a href="'. admin_url( 'options-general.php?page=' ).$this->filename.'">Settings</a>';
	}
	return $links;
}

public function modify_title($title,$post_id){


if (in_the_loop() && is_singular()){


$post_object = get_post($post_id);


if ($post_object->post_status == $this->newstatusname){


  if (!empty($this->options[$this->title_label_field_name])){


$title .= ' - '.$this->options[$this->title_label_field_name];
	
  }

}

}

return $title;


}


public function __construct() {

$this->filename = plugin_basename( __FILE__ );
$this->options = get_option($this->opt_name);

add_action( 'init', array($this,"create_mobile_custom_post_status"));
add_filter( 'the_content', array($this,"add_mobileonly_message"));
add_action('admin_menu', array($this,"plugin_menu"));
add_action( 'pre_get_posts', array($this,"exclude_archive_post_status_from_main_query"));
add_action( 'pre_get_posts', array($this,"exclude_archive_post_status_from_feed"));
add_filter( 'display_post_states', array($this,"display_archive_state"));
add_action( 'admin_footer-post.php', array($this,"append_post_status_list"));
add_action( 'admin_footer-post-new.php',array($this,'append_post_status_list_on_new') );
add_filter('plugin_action_links', array($this,"add_settings_link"), 10, 2);
add_filter('the_title', array( $this, 'modify_title' ),10,2);

//Javascript for bulk edit support
add_action( 'admin_footer-edit.php', array( $this, 'append_post_status_bulk_edit' ));
}


}


$ids_mobile_post_status_instance = new IDS_mobil_post_status_plugin();

function ids_mobile_post_status_uninstall(){

$option_name = 'ids_mobile_post_status_options';

delete_option( $option_name );

}


register_uninstall_hook( __FILE__, 'ids_mobile_post_status_uninstall' );


?>
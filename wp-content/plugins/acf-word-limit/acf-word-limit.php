<?php
/**
 * Plugin Name: ACF Word Limit
 * Description: Adds an advanced custom field type (based on textarea) that's limited by words instead of by characters. ACF v5 only.
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

namespace Verb;

class ACF_Word_Limit {
	public function __construct() {
		add_action('acf/include_field_types', [
			$this,
			'include_field_types'
		]);
	}
	
	public function include_field_types() {
		require_once 'acf-field-word-limit.php';
	}
}

$GLOBALS['ACF_Word_Limit'] = new ACF_Word_Limit;
<?php
/**
 * Template Name: New  Register Page Template 
 */
if (is_user_logged_in())
{
	wp_redirect( home_url() );
	exit;
}
get_header('profile');
?>

<?php //wp_head();?>
<!-- Facebook Pixel Code -->
<script>
 !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}
;if(!f._fbq)f._fbq=n;
 n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1337309409664540');
 fbq('track', 'PageView');
 </script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1337309409664540&ev=PageView&noscript=1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<?php if (is_singular('products') && has_term('frozen-meals', 'product-category')): ?>
<body <?php body_class('frozen-meals');?>>
<?php else: ?>
<body <?php body_class();?>>
<?php endif;?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1104764539603478";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
<?php
if (get_field('page_skin_adzone')) {
    echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(get_field('page_skin_adzone'), 'post', false, ICL_LANGUAGE_CODE) . ']');
}
?>
<?php if (ICL_LANGUAGE_CODE == 'nl') {?>
<aside id="responsive-menu">
  <?php if (false === ($header_menu = get_transient('ak_header_menu_' . ICL_LANGUAGE_CODE))) {
    set_transient('ak_header_menu_' . ICL_LANGUAGE_CODE, ($header_menu = wp_nav_menu([
        'container' => '',
        'echo' => false,
        'theme_location' => 'header-menu',
    ])), DAY_IN_SECONDS);
}
    ?>
  <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?> </aside>
<?php } else {?>
<aside id="responsive-menu">
  <form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
    <input class="search-field" name="s" placeholder="Search our website..." type="search">
  </form>
  <div class="top-bar"> <a href="<?php echo get_post_type_archive_link('competitions'); ?>">Competitions</a> <a class="icon-heart" href="<?php echo site_url('/your-saved/'); ?>">Your Saved</a>
    <?php if (is_user_logged_in()): ?>
    <a href="<?php echo site_url('/logout/'); ?>">Logout</a>
    <?php else: ?>
    <a href="#login">Login / Register</a>
    <?php endif;?>
  </div>
  <?php if (false === ($header_menu = get_transient('ak_header_menu_' . ICL_LANGUAGE_CODE))) {
    set_transient('ak_header_menu_' . ICL_LANGUAGE_CODE, ($header_menu = wp_nav_menu([
        'container' => '',
        'echo' => false,
        'theme_location' => 'header-menu',
    ])), DAY_IN_SECONDS);
}
    ?>
  <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?> </aside>
<?php }?>
<div class="wrapper">
<div class="header-container">
  <style>

.contact-us-nl {position:absolute; right:20px; top:4px;}
            </style>
  <div class="app_btn" id="gplay"  style="display:none;"> <a  href="javascript:void();"class="app_close"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" alt="Google Play"></a>
    <div class="app_btn-inner"> <span class="app-logo"><?php if (get_field("logo", 'option')): ?><img src="<?php the_field("logo", 'option');?>" alt=""><?php else: ?><img src="<?php echo get_template_directory_uri(); ?>/images/app-logo.png" alt=""><?php endif;?></span>
      <div class="app_btn-inner-text">
        <p class="app_msg">
          <?php if (get_field("title", 'option')): ?><?php the_field("title", 'option');?><?php endif;?>
        </p>

        <p class="star-main">
        	<?php
if (get_field("google_rating", 'option')):
    $starRating = get_field("google_rating", 'option');
    $lStar = floor($starRating);
    $point = (float) $starRating - (float) $lStar;
    $balanceStar = 5 - (float) $starRating;
    $blankStar = $balanceStar - $point;

    if ($starRating < 5) {
        for ($i = 1; $i <= $lStar; $i++) {
            echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
        }
        if ($point > 0 && $point < 1) {
            echo '<i class="fa fa-star-half-o" aria-hidden="true"></i>&nbsp;';
        }
        for ($j = 1; $j <= $blankStar; $j++) {
            echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
        }
    }
endif;
?>

         <a href="<?php if (get_field("download_rul", 'option')): the_field("download_rul", 'option');endif;?>">
           <?php if (get_field("download_caption_text", 'option')): the_field("download_caption_text", 'option');endif;?>
          </a> </p>

      </div>
    </div>
  </div>

  <div class="app_btn" id="iphone" style="display:none;"> <a  href="javascript:void();"class="app_close"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" alt="Google Play"></a>
    <div class="app_btn-inner"> <span class="app-logo"><?php if (get_field("logo_iphone_logo", 'option')): ?><img src="<?php the_field("logo_iphone_logo", 'option');?>" alt=""><?php else: ?><img src="<?php echo get_template_directory_uri(); ?>/images/app-logo.png" alt=""><?php endif;?></span>
      <div class="app_btn-inner-text">
        <p class="app_msg">
           <?php if (get_field("title_iphone", 'option')): ?><?php the_field("title_iphone", 'option');?><?php endif;?>
        </p>

        <p class="star-main">
			<?php
			if (get_field("google_rating_iphone", 'option')):
				$starRating = get_field("google_rating_iphone", 'option');
				$lStar = floor($starRating);
				$point = (float) $starRating - (float) $lStar;
				$balanceStar = 5 - (float) $starRating;
				$blankStar = $balanceStar - $point;

				if ($starRating < 5) {
					for ($i = 1; $i <= $lStar; $i++) {
						echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
					}
					if ($point > 0 && $point < 1) {
						echo '<i class="fa fa-star-half-o" aria-hidden="true"></i>&nbsp;';
					}
					for ($j = 1; $j <= $blankStar; $j++) {
						echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
					}
				}
			endif;
			?>

        <a href="<?php if (get_field("download_rul_iphone", 'option')): the_field("download_rul_iphone", 'option');endif;?>">
			<?php if (get_field("download_caption_iphone", 'option')): the_field("download_caption_iphone", 'option');endif;?>
        </a>
		</p>
      </div>
    </div>
  </div>
  <header id="header">
    <div class="top-bar skid">
      <div class="container"> <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu(['container' => '', 'echo' => false, 'theme_location' => 'top-bar-menu'])); ?>
        <?php if (ICL_LANGUAGE_CODE == 'nl') {?>
        <?php } else {?>
        <form action="<?php echo home_url(); ?>" class="search-form" id="search-form" method="get">
          <input class="search-field" name="s" id="s" placeholder="Search our website..." type="search">
        </form>
        <?php }?>
      </div>
    </div>
    <?php if (ICL_LANGUAGE_CODE == 'nl') {?>
    <div class="container"> <a href="https://www.annabelkarmel.com/nl/hartelijk-welkom-bij-annabel-karmel/"><img width="390" height="261" src="https://www.annabelkarmel.com/wp-content/uploads/2016/08/logo.png" class="custom-logo" alt="" itemprop="logo"></a>
      <?php if (1 == get_current_user_id()): ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
    'container' => '',
    'echo' => false,
    'theme_location' => 'header-menu',
])); ?>
      <?php else: ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
      <?php endif;?>
      <div id="responsive-menu-icon">
        <button class="lines-button" type="button"> <span class="lines"></span> </button>
      </div>
    </div>
    <?php } else {?>
    <div class="container">
      <?php the_custom_logo();?>
      <?php if (1 == get_current_user_id()): ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
    'container' => '',
    'echo' => false,
    'theme_location' => 'header-menu',
])); ?>
      <?php else: ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
      <?php endif;?>
      <div id="responsive-menu-icon">
        <button class="lines-button" type="button"> <span class="lines"></span> </button>
      </div>
    </div>
    <?php }?>
  </header>
</div>
<div id="outer-content">


<div class="clearfix"></div>

<section class="join_club">
    <div class="container">

        <img class="join_logo" src="<?php echo get_template_directory_uri(); ?>/images/join-club.png" alt="image">

        <div class="intro">Receive emails packed full of food, fun, facts, and freebies, save your favourite recipes and enter competitions. Furthermore, once registered, you will be able to download our free "Welcome to the Club" eRecipe Book containing lovely recipes for baby, toddler and the whole family.</div>
    </div>
</section>

<section class="register_section">
  <form id="securePasswordForm" class="form-horizontal" method="post" name="myform" autocomplete="off">
    <div class="container">
      <div id="reg-step-1" class="toggle-step">
        <div class="title">1. About Me</div>
        <div id="about-me" class="toggle-div">
          <div class="form-group">
            <input type="text" name="full_name" class="input form-control" autocomplete="false" required>
            <label class="control-label">First Name</label>
          </div>
          <div class="form-group">
            <input type="text" name="last_name" class="input form-control" autocomplete="false">
            <label class="control-label">Last Name</label>
          </div>
          <div class="form-group">
            <input type="text" name="about_email" id="about_email" class="input form-control" autocomplete="false" required>
            <label class="control-label">Email</label>
          </div>
        <div class="form-group password-group">
          	<input type="password" class="input form-control" name="pwd" autocomplete="false" required/>
          	<label class="control-label">Password</label>
        </div>
        <div class="form-group">
            <input type="text" name="post_code" class="input form-control" autocomplete="false" required>
            <label class="col-sm-3 control-label">Postcode/Zipcode</label>
        </div>
        <div class="form-group slct-group">
            <select class="input form-control" name="country">
                <option value="">-- Select Country --</option>
				<option value="United Kingdom" selected="selected">United Kingdom</option>
				<option value="Australia">Australia</option>
				<option value="United States of America">United States of America</option>
				<option value="Netherlands">Netherlands</option>
				<option value="United Arab Emirates">United Arab Emirates</option>
				<option value="Afghanistan">Afghanistan</option>
				<option value="Albania">Albania</option>
				<option value="Algeria">Algeria</option>
				<option value="American Samoa">American Samoa</option>
				<option value="Andorra">Andorra</option>
				<option value="Angola">Angola</option>
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>
				<option value="Argentina">Argentina</option>
				<option value="Armenia">Armenia</option>
				<option value="Austria">Austria</option>
				<option value="Azerbaijan">Azerbaijan</option>
				<option value="Bahamas">Bahamas</option>
				<option value="Bahrain">Bahrain</option>
				<option value="Bangladesh">Bangladesh</option>
				<option value="Barbados">Barbados</option>
				<option value="Belarus">Belarus</option>
				<option value="Belgium">Belgium</option>
				<option value="Belize">Belize</option>
				<option value="Benin">Benin</option>
				<option value="Bermuda">Bermuda</option>
				<option value="Bhutan">Bhutan</option>
				<option value="Bolivia">Bolivia</option>
				<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
				<option value="Botswana">Botswana</option>
				<option value="Brazil">Brazil</option>
				<option value="Brunei">Brunei</option>
				<option value="Bulgaria">Bulgaria</option>
				<option value="Burkina Faso">Burkina Faso</option>
				<option value="Burundi">Burundi</option>
				<option value="Cambodia">Cambodia</option>
				<option value="Cameroon">Cameroon</option>
				<option value="Canada">Canada</option>
				<option value="Cape Verde">Cape Verde</option>
				<option value="Cayman Islands">Cayman Islands</option>
				<option value="Central African Republic">Central African Republic</option>
				<option value="Chad">Chad</option>
				<option value="Chile">Chile</option>
				<option value="China">China</option>
				<option value="Colombia">Colombia</option>
				<option value="Comoros">Comoros</option>
				<option value="Congo, Democratic Republic of the">Congo, Democratic Republic of the</option>
				<option value="Congo, Republic of the">Congo, Republic of the</option>
				<option value="Costa Rica">Costa Rica</option>
				<option value="Croatia">Croatia</option>
				<option value="Cuba">Cuba</option>
				<option value="Curaçao">Curaçao</option>
				<option value="Cyprus">Cyprus</option>
				<option value="Czech Republic">Czech Republic</option>
				<option value="Côte d'Ivoire">Côte d'Ivoire</option>
				<option value="Denmark">Denmark</option>
				<option value="Djibouti">Djibouti</option>
				<option value="Dominica">Dominica</option>
				<option value="Dominican Republic">Dominican Republic</option>
				<option value="East Timor">East Timor</option>
				<option value="Ecuador">Ecuador</option>
				<option value="Egypt">Egypt</option>
				<option value="El Salvador">El Salvador</option>
				<option value="Equatorial Guinea">Equatorial Guinea</option>
				<option value="Eritrea">Eritrea</option>
				<option value="Estonia">Estonia</option>
				<option value="Ethiopia">Ethiopia</option>
				<option value="Faroe Islands">Faroe Islands</option>
				<option value="Fiji">Fiji</option>
				<option value="Finland">Finland</option>
				<option value="France">France</option>
				<option value="French Polynesia">French Polynesia</option>
				<option value="Gabon">Gabon</option>
				<option value="Gambia">Gambia</option>
				<option value="Georgia">Georgia</option>
				<option value="Germany">Germany</option>
				<option value="Ghana">Ghana</option>
				<option value="Gibraltar">Gibraltar</option>
				<option value="Greece">Greece</option>
				<option value="Greenland">Greenland</option>
				<option value="Grenada">Grenada</option>
				<option value="Guam">Guam</option>
				<option value="Guatemala">Guatemala</option>
				<option value="Guinea">Guinea</option>
				<option value="Guinea-Bissau">Guinea-Bissau</option>
				<option value="Guyana">Guyana</option>
				<option value="Haiti">Haiti</option>
				<option value="Honduras">Honduras</option>
				<option value="Hong Kong">Hong Kong</option>
				<option value="Hungary">Hungary</option>
				<option value="Iceland">Iceland</option>
				<option value="India">India</option>
				<option value="Indonesia">Indonesia</option>
				<option value="Iran">Iran</option>
				<option value="Iraq">Iraq</option>
				<option value="Ireland">Ireland</option>
				<option value="Israel">Israel</option>
				<option value="Italy">Italy</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Japan">Japan</option>
				<option value="Jordan">Jordan</option>
				<option value="Kazakhstan">Kazakhstan</option>
				<option value="Kenya">Kenya</option>
				<option value="Kiribati">Kiribati</option>
				<option value="Kosovo">Kosovo</option>
				<option value="Kuwait">Kuwait</option>
				<option value="Kyrgyzstan">Kyrgyzstan</option>
				<option value="Laos">Laos</option>
				<option value="Latvia">Latvia</option>
				<option value="Lebanon">Lebanon</option>
				<option value="Lesotho">Lesotho</option>
				<option value="Liberia">Liberia</option>
				<option value="Libya">Libya</option>
				<option value="Liechtenstein">Liechtenstein</option>
				<option value="Lithuania">Lithuania</option>
				<option value="Luxembourg">Luxembourg</option>
				<option value="Macedonia">Macedonia</option>
				<option value="Madagascar">Madagascar</option>
				<option value="Malawi">Malawi</option>
				<option value="Malaysia">Malaysia</option>
				<option value="Maldives">Maldives</option>
				<option value="Mali">Mali</option>
				<option value="Malta">Malta</option>
				<option value="Marshall Islands">Marshall Islands</option>
				<option value="Mauritania">Mauritania</option>
				<option value="Mauritius">Mauritius</option>
				<option value="Mexico">Mexico</option>
				<option value="Micronesia">Micronesia</option>
				<option value="Moldova">Moldova</option>
				<option value="Monaco">Monaco</option>
				<option value="Mongolia">Mongolia</option>
				<option value="Montenegro">Montenegro</option>
				<option value="Morocco">Morocco</option>
				<option value="Mozambique">Mozambique</option>
				<option value="Myanmar">Myanmar</option>
				<option value="Namibia">Namibia</option>
				<option value="Nauru">Nauru</option>
				<option value="Nepal">Nepal</option>
				<option value="New Zealand">New Zealand</option>
				<option value="Nicaragua">Nicaragua</option>
				<option value="Niger">Niger</option>
				<option value="Nigeria">Nigeria</option>
				<option value="North Korea">North Korea</option>
				<option value="Northern Mariana Islands">Northern Mariana Islands</option>
				<option value="Norway">Norway</option>
				<option value="Oman">Oman</option>
				<option value="Pakistan">Pakistan</option>
				<option value="Palau">Palau</option>
				<option value="Palestine, State of">Palestine, State of</option>
				<option value="Panama">Panama</option>
				<option value="Papua New Guinea">Papua New Guinea</option>
				<option value="Paraguay">Paraguay</option>
				<option value="Peru">Peru</option>
				<option value="Philippines">Philippines</option>
				<option value="Poland">Poland</option>
				<option value="Portugal">Portugal</option>
				<option value="Puerto Rico">Puerto Rico</option>
				<option value="Qatar">Qatar</option>
				<option value="Romania">Romania</option>
				<option value="Russia">Russia</option>
				<option value="Rwanda">Rwanda</option>
				<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
				<option value="Saint Lucia">Saint Lucia</option>
				<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
				<option value="Samoa">Samoa</option>
				<option value="San Marino">San Marino</option>
				<option value="Sao Tome and Principe">Sao Tome and Principe</option>
				<option value="Saudi Arabia">Saudi Arabia</option>
				<option value="Senegal">Senegal</option>
				<option value="Serbia">Serbia</option>
				<option value="Seychelles">Seychelles</option>
				<option value="Sierra Leone">Sierra Leone</option>
				<option value="Singapore">Singapore</option>
				<option value="Sint Maarten">Sint Maarten</option>
				<option value="Slovakia">Slovakia</option>
				<option value="Slovenia">Slovenia</option>
				<option value="Solomon Islands">Solomon Islands</option>
				<option value="Somalia">Somalia</option>
				<option value="South Africa">South Africa</option>
				<option value="South Korea">South Korea</option>
				<option value="Spain">Spain</option>
				<option value="Sri Lanka">Sri Lanka</option>
				<option value="Sudan">Sudan</option>
				<option value="Sudan, South">Sudan, South</option>
				<option value="Suriname">Suriname</option>
				<option value="Swaziland">Swaziland</option>
				<option value="Sweden">Sweden</option>
				<option value="Switzerland">Switzerland</option>
				<option value="Syria">Syria</option>
				<option value="Taiwan">Taiwan</option>
				<option value="Tajikistan">Tajikistan</option>
				<option value="Tanzania">Tanzania</option>
				<option value="Thailand">Thailand</option>
				<option value="Togo">Togo</option>
				<option value="Tonga">Tonga</option>
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>
				<option value="Tunisia">Tunisia</option>
				<option value="Turkey">Turkey</option>
				<option value="Turkmenistan">Turkmenistan</option>
				<option value="Tuvalu">Tuvalu</option>
				<option value="Uganda">Uganda</option>
				<option value="Ukraine">Ukraine</option>
				<option value="Uruguay">Uruguay</option>
				<option value="Uzbekistan">Uzbekistan</option>
				<option value="Vanuatu">Vanuatu</option>
				<option value="Vatican City">Vatican City</option>
				<option value="Venezuela">Venezuela</option>
				<option value="Vietnam">Vietnam</option>
				<option value="Virgin Islands, British">Virgin Islands, British</option>
				<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
				<option value="Yemen">Yemen</option>
				<option value="Zambia">Zambia</option>
				<option value="Zimbabwe">Zimbabwe</option>
            </select>
            <label class="control-label active">Country</label>
	    </div>
        <!--<div class="form-group slct-group">
              <select class="input form-control" name="about_iam">
                  <option value="">Please select </option>
                  <option value="a_mam">A Mum</option>
                  <option value="a_dad">A Dad</option>
                  <option value="a_grandparent">A Grandparent</option>
                  <option value="a_grandparent_carer">Nanny, Carer or Childminder</option>
                  <option value="other">Other </option>
              </select>
            <label class="control-label active">I am...</label>
        </div>-->
        <div class="form-group">
             <a href="javascript:void(0)" id="step-1-submit" class="btn btn-default btn-next step-1">Next</a>
        </div>
      </div>
    </div>
       <div id="reg-step-2" class="toggle-step">
        <div class="title little_ones">2. Childs Name</div>
          <div id="my-little-ones" class="step_form toggle-div">
				<div class="helper-text">This helps us provide you with the best, recipes, competitions and information</div>
				
             <div id="add-more-ones">
				<div class="add-little-ones">
					  <div class="form-group">
						  <input type="text" class="input form-control" name="little_name[]" autocomplete="false">
						 <label class="control-label">Name</label>
					  </div>

					  <div class="form-group">
						<input type="text" class="input readonly form-control datepicker date" name="little_dob[]" autocomplete="false">
						<label class="control-label">Date of Birth <i>(dd/mm/yyyy)</i></label>
					  </div>
					  <a href="javascript:void(0);" class="remove_button disabled"><img src="<?php echo get_template_directory_uri(); ?>/images/delete-icon.png" images="icon"></a>
					  <div class="full_width child-error">If enter child name date field mandetory</div>
				  </div>
             	</div>
             	<div class="form-group">
                	<a href="javascript:void(0);" class="add_button" title="Add New Child">Add New Child</a>
              	</div>
              	<div class="form-group checkbox">
	                <div class="form-group">
	                    <label>
	                        <input type="checkbox" name="agree" value="1" /> <span class="rect"></span> I agree to the <a href="https://www.annabelkarmel.com/privacy-policy/" target="_blank">Privacy Policy</a> and <a href="https://www.annabelkarmel.com/terms-conditions/" target="_blank">T&amp;Cs</a>
	                    </label>
	                </div>
	            </div>
	            <div class="checkbox">
	                <label>
	                    <input type="checkbox" name="subscribe" value="1" /> <span class="rect"></span> I would like to receive information, competitions, deals, vouchers and recipes. Unsubscribe at any time.
	                </label>
	            </div>
	            <div class="form-group">
	                <div class="g-recaptcha" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback" data-sitekey="6LfqV2MUAAAAAC1_FwtVtUGtaoyMUzbOZvrIENbq"></div>
	                <input type="hidden" class="form-control" data-recaptcha="true" required>
	                <div class="help-block with-errors captcha-error">The reCAPTCHA was invalid. Please try again.</div>
	            </div>
	            <!--<div class="form-group">
	                 <a href="javascript:void(0)" id="step-2-submit" class="btn btn-default btn-next step-1">Next</a>
	            </div>-->
	            <div class="form-group">
	               <input type="button" id="step-3-submit" class="btn btn-default btn-next reg_btn step-3" value="Join">
	               <span id="response-loading"><i class="fa fa-spinner fa-spin"></i> Please wait Loading</span>
	            </div>
          </div>
      </div>
        
       </div>
    </form>
</section>

<div class="clearfix"></div>

<?php get_footer('profile');?>
<script>
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
window.onload = checkMobile;
function checkMobile (){
var gplay = document.getElementById("gplay");
var iphone = document.getElementById("iphone");
if ( isMobile.iOS() ){
  // iphone.style.display =  "inline-block";
  // gplay.style.display =  "none";
  var popUpdata = localStorage.getItem('popst');
	$('.app_btn .app_close img').click(function () {
		$(".app_btn").fadeOut("fast");
		$("body").removeClass("app-body");
		localStorage.setItem('popst', true);
	});
	if(!popUpdata) {
		$("#iphone").fadeIn("fast");
		//localStorage.setItem('popst', true);
		$("body").addClass("app-body");
	}
}
if ( isMobile.Android() ){
	// iphone.style.display =  "none";
  	// gplay.style.display =  "inline-block";
	var popUpdata = localStorage.getItem('popst');
	$('.app_btn .app_close img').click(function () {
		$(".app_btn").fadeOut("fast");
		$("body").removeClass("app-body");
		localStorage.setItem('popst', true);
	});
	if(!popUpdata) {
		$("#gplay").fadeIn("fast");
		//localStorage.setItem('popst', true);
		$("body").addClass("app-body");
	}
}
}
function isChildValid() {
	var isValid = false;
	var $rowWraper = $("#add-more-ones");
	$rowWraper.find(".add-little-ones").each(function(){
		var $this = $(this);
		var $nameElm = $this.find('input[name="little_name[]"]');
		var $dateElm = $this.find('input[name="little_dob[]"]');
		var nameVal = $nameElm.val();
		var dateVal = $dateElm.val();
		
		if(nameVal.length != 0 && dateVal.length == 0) {
			//$this.find(".child-error").show();
			//$dateElm.closest(".form-group").removeClass("has-success").addClass("has-error");
			$dateElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
			isValid = false;
			return false;
		} else if(dateVal.length != 0 && nameVal.length == 0) {
			//$this.find(".child-error").show();
			//$nameElm.closest(".form-group").removeClass("has-success").addClass("has-error");
			$nameElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
			isValid = false;
			return false;
		} else {
			$dateElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
			$nameElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
			isValid = true;
		}
		
		
	});
	return isValid;
}
$(document).ready(function() {

    $('#securePasswordForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
			validating: 'glyphicon glyphicon-refresh',
            invalid: 'glyphicon glyphicon-remove'
        },
        fields: {
            full_name: {
                validators: {
                    notEmpty: {message: 'The First Name field is mandatory – please enter at least two characters for this field'},
					callback: {
						message: 'The First Name field is mandatory – please enter at least two characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z \-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							if(value.length == 0){return false;}
							return true;
						}
					}
                }
            },

            last_name: {
                validators: {
                    notEmpty: {message: 'The Last Name field is mandatory – please enter at least two characters for this field'},
					callback: {
						message: 'The Last Name field is mandatory – please enter at least two characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z \-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							if(value.length == 0){return false;}
							return true;
						}
					}
                }
            },
            about_email: {
                validators: {
                    notEmpty: {
                        message: 'The Email field is mandatory – please enter at least seven characters for this field'
                    },
                    /*stringLength: {
                        min: 7,
                        max: 255,
                        messase: 'Please enter at least 7 characters for this field.'
                    },*/
                    remote: {
                        enabled: false,
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        type: "POST",
                        message: '<div id="email-exists"><span>Already registered. </span><span class="click-here"><a href="#login">Log in</a></span></div>',
                        delay: 1000,
                        data : {action: "check_email_exists"}
                    },
                    callback: {
                        callback: function(value, validator, $field) {
							//var regs = /[*\`|\~|\!|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>| ]/g;
							var regs = /[ ]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							

                            if(value.length < 7 && value.length != 0) {
                                return {
                                    valid: false,
                                    message: 'The Email field is mandatory – please enter at least seven characters for this field'
                                }
                            }
							
							if(value.length > 255) {
								$field.val(value.substring(0, 255));
							}
							
                            if (value.length >= 7) {
								
								//var atReg = /[*\@]/g;
								var atReg = /[*\%|\$|\&|\*|\£|\&|\"|\(]/g;
								
								if(value.search(atReg) > 0) {
									
									return {
										valid: false,
										message: "The Email field only accepts letters (a-z), numbers, and the following special characters + @ . -"
									}
									
									
									
								} else {
									var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									
									if(value.search(reg) < 0) {
										validator.options.fields.about_email.validators.remote.enabled = false;
										//validator.revalidateField('about_email');
										return {
											valid: false,
											message: 'Please enter a valid Email – it looks like you might be missing an @ sign'
										}
									}									
								}
								
                                
                            }
							
							
							if(value.length == 0){
								return {
									valid: false,
									message: 'The Email field is mandatory – please enter at least seven characters for this field'
								};
							}
									
                            validator.options.fields.about_email.validators.remote.enabled = true;
							validator.options.fields.about_email.validators.remote.data.about_email = value;
                            return true
                        }
                    }
                }
            },
            pwd: {
                validators: {
                    notEmpty: {
                        message: 'The Password field is mandatory – please enter at least six characters for this field'
                    },
                    stringLength: {
                        min: 6,
                        //max: 100,
                        message: 'The Password field is mandatory – please enter at least six characters for this field'
                    },
					callback: {
						message: 'The Password field is mandatory – please enter at least six characters for this field',
                        callback: function(value, validator, $field) {
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if(value.length > 100) {
								$field.val(value.substring(0, 100));
							}
							return true;
						}
					}
                }
            },
            about_iam: {
                validators: {
                    notEmpty: {
                        message: 'This field is mandatory – please select the type of user you are'
                    }
                }
            },
            'little_name[]': {
                validators: {
                    notEmpty: {
						enabled: false,
                        message: "It looks like you've started typing in the child's Name field – please enter at least two characters for this field or leave it empty"
                    },
					callback: {
						message: "It looks like you've started typing in the child's Name field – please enter at least two characters for this field or leave it empty",
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z| |\-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {

								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {
										return {valid: false,}
									}
								}
								if(value.length > 50) {$field.val(value.substring(0, 50));}
							}
														
							isChildValid();
							return {valid: true};
						}
					}
                }
            },
            'little_dob[]': {
                validators: {
                    notEmpty: {
						enabled : false,
						message: "The child's Date of Birth or Due Date field is mandatory – please select a date"
					},
                    date: {
							format: 'DD/MM/YYYY',
							message: "The child's Date of Birth or Due Date field is mandatory – please select a date"
						},
					callback: {
						message: "The child's Date of Birth or Due Date field is mandatory – please select a date",
						callback: function(value, validator, $field) {
							/*
							var nameElmValue = $("input[name='little_name[]']").val();
							var dateElmValue = $("input[name='little_dob[]']").val();
							
							if(nameElmValue.length > 0 && dateElmValue.length > 0) {
								$field.closest(".add-little-ones").find(".child-error").hide();
							} else {
								$field.closest(".add-little-ones").find(".child-error").show();
							}*/
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
							
							isChildValid();
							return {valid: true};
						}
					}
                }
            },
            house_no:{
                validators: {
                    notEmpty: {
                        message: 'The House Name/Number field is mandatory – please enter at least one character for this field'
                    },
					callback: {
						message: 'The House Name/Number field is mandatory – please enter at least one character for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
                            if (value.length > 0) {
																
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 1) {return {valid: false}}
									if(value.length > 100) {$field.val(value.substring(0, 100));}
								}
								
							}
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
					
                    /*regexp: {
                        regexp: /^[a-zA-Z0-9-\s-.]+$/i,
                        message: "The House Name/Number field is mandatory – please enter at least one character for this field."
                    }*/
                }
            },
            street:{
                validators: {
                    notEmpty: {
                        message: 'The Street field is mandatory – please enter at least three characters for this field'
                    },
					callback: {
						message: 'The Street field is mandatory – please enter at least three characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {

								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 3) {return {valid: false}}
									if(value.length > 100) {$field.val(value.substring(0, 100));}
								}
							}
							
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            city:{
                validators: {
                    notEmpty: {
                        message:'The City field is mandatory – please enter at least three characters for this field'
                    },
					callback: {
						message:'The City field is mandatory – please enter at least three characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								
								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 3) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            post_code:{
                validators: {
                    notEmpty: {
                        message:'The Postcode/Zipcode field is mandatory – please enter at least four characters for this field'
                    },
					callback: {
						message: 'The Postcode/Zipcode field is mandatory – please enter at least four characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\.|\'|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								
								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 4) {return {valid: false,}}
									if(value.length > 11) {$field.val(value.substring(0, 11));}
								}
							}
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The Country field is mandatory'
                    },
					callback: {
						message: 'The Country field is mandatory',
						callback: function(value, validator, $field){
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							return true;
						}
					}
                }
            },
            agree: {
                validators: {
                    notEmpty: {
                        message: '	The Terms & Conditions field is mandatory - please select this checkbox to continue'
                    }
                }
            }

        }
    });
});
$(document).ready(function(){

	    var validator = $('#securePasswordForm').data('bootstrapValidator');


       /*$('.register_section .toggle-step .title').click(function(){
		   $(this).parent().find(".toggle-div").slideToggle();
		   $(this).parent().siblings().find(".toggle-div").slideUp();
        });*/

		$("#reg-step-1 .title, #reg-step-2 .title").click(function(){
			validator.validate();
			var secodV = isChildValid();
			if (validator.isValid() && secodV) {
				$(this).parent().find(".toggle-div").slideToggle();
				$(this).parent().siblings().find(".toggle-div").slideUp();
			}
		});

		$("#reg-step-3 .title").click(function(){
			//var validator = $('#securePasswordForm').data('bootstrapValidator');
			validator.validate();
			if (validator.isValid()) {
				var isSecondValidate = isChildValid();

				if(isSecondValidate) {
					$(this).parent().find(".toggle-div").slideToggle();
					$(this).parent().siblings().find(".toggle-div").slideUp();
				} else {
					$("#reg-step-2").find(".toggle-div").slideDown();
				}
			}
		});


	$('#step-1-submit').click(function() {
		validator.validate();
		if (validator.isValid() && !$(this).hasClass("disabled")) {
		  $('#about-me').hide();
		  $('#my-little-ones').show();
		}
	});
	

	$('#step-2-submit').click(function() {
		var isChildv = isChildValid();
		if(isChildv) {
			validator.validate();
			if (validator.isValid()) {
				$('#about-me').hide();
				$('#my-little-ones').hide();
				$('#my-address').css('display','block');
				$('#reg-step-2').addClass('active-form');
				//$("#step-3-submit").attr( "disabled","disabled");
			}
		}
	});

	//$('form#securePasswordForm').on("submit",function(e){
	$('#step-3-submit').click(function(e){
		e.preventDefault();

		//e.stopImmediatePropagation();
		var goRes = grecaptcha.getResponse();
		validator.validate();
		if(!$(this).hasClass("disabled") && goRes != "" && goRes && validator.isValid()) {
			$(".captcha-error").hide();
			$("#step-3-submit").addClass("disabled");
			var form_data = $("#securePasswordForm").serialize();
			$('#response-loading').show();
			var url = '<?php echo admin_url('admin-ajax.php'); ?>';
			  $.ajax({
				type:'POST',
				url: url,
				data: {
					  action: 'register_template_post_data',
					  formData: form_data
					},
				dataType: "json",
				success:function(response) {
				  //$('#step-3-submit').unbind('submit');
					console.log(response);
					if(response.status==1){
					  //alert(response.message);
					  //location.reload();
					   window.location.href= '<?php echo site_url(); ?>/thank-you';
					  //$("#securePasswordForm")[0].reset();
					}
					else{
					  alert(response.message);
					}
					$("#step-3-submit").removeClass("disabled");
					$('#response-loading').hide();
				},
				error: function(errorThrown){
					console.error(errorThrown);
					$("#step-3-submit").removeClass("disabled");
					$('#response-loading').hide();
				}
			});

		} else {
			$(".captcha-error").show();
		}

		return false;
	});


	  $("#email-exists a").on("click", function(){
		  var aboutEmail = $("#about_email").val();
		  if(aboutEmail){
			  $("#pp_user_login").val(aboutEmail);
		  }
	  });
});
$(document).ready(function(){

	$( ".datepicker" ).datepicker({
        maxDate: '0',
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy',
	    onSelect: function(dateText) {
		var textBox = this;
		setTimeout(function(){
			$(textBox).trigger('change');
			$(textBox).trigger('input');
		},100);
	  }
	});


    var maxField = 10;
    var addButton = $('.add_button');
    var wrapper = $('#add-more-ones');
	//var wrapper = $('#my-little-ones');
    var x = 1;
	var akId = 1;
	
	

    $(addButton).click(function(){
        if(!$(this).hasClass("disabled") && x < maxField){
            
            var fieldHTML = '<div class="add-little-ones"><div class="form-group"><input data-akid="name_'+akId+'" type="text" class="input form-control " name="little_name[]" required="" data-bv-field="little_name[]"><label class="control-label">Name</label></div><div class="form-group"><input readonly data-akid="akid_'+ akId +'" type="text" class="input form-control datepicker" name="little_dob[]" data-bv-field="little_dob[]"><label class="control-label">Date of Birth <i>(dd/mm/yyyy)</i></label></div><a href="javascript:void(0);" class="remove_button"><img src="<?php echo get_template_directory_uri(); ?>/images/delete-icon.png" images="icon"></a></div>';

			$(addButton).addClass("disabled");
			$('.remove_button').removeClass("disabled");
            $(wrapper).append(fieldHTML);
			
            setTimeout(function() {
              $('.datepicker[data-akid="akid_'+ akId +'"]').datepicker({
                    maxDate: '0',
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd/mm/yy',
				  onSelect: function(dateText) {
					  var textBox = this;
					setTimeout(function(){
						$(textBox).trigger('change');
						$(textBox).trigger('input');
					},100);
				  }
				});

              var validator = $('#securePasswordForm').data('bootstrapValidator');
              validator.addField($('input.form-control[data-akid="name_'+akId+'"]'));
              validator.addField($( '.datepicker[data-akid="akid_'+ akId +'"]' ));

			  $(addButton).removeClass("disabled");
			  
				x++;
				akId +=1;

            },300);
			
			if(maxField == x) {
				$(this).hide();
			}

        }
    });

    // $('body').on('focus',".datepicker", function(){
    //     $(this).datepicker();
    // });

    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
		if(x != 1) {
			$(this).parent('div').remove();
			x--;
		} else {
			//$(this).parent('div').remove();
			$(this).parent('div').find("input").val("");
			var isChilda = isChildValid();
		}
		
		if(x < maxField) {
			$(addButton).show();
		}
    });

});
$(document).ready(function(){

    $('.register_section .form-group.password-group .form-control-feedback').click(function(){

	 //$(this).toggleClass("in");

	  if ($(".register_section .form-group.password-group .input").attr("type") == "password") {
		$(".register_section .form-group.password-group .input").attr("type", "text");
	  } else {
		$(".register_section .form-group.password-group .input").attr("type", "password");
	  }

	});
	
	$("#securePasswordForm .form-group").each(function(){
		if($(this).find('input').val() != ""){
			$(this).find("label").addClass("active");
		}
	});
	
});
</script>
<?php get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="singular-wrapper">
					<div class="page-header" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting">
						<meta content="<?php echo the_time('Y-m-d'); ?>" itemprop="datePublished">
						<meta content="<?php the_permalink(); ?>" itemprop="url">
						<h1 itemprop="name"><?php the_title(); ?></h1>
						<?php get_template_part('parts/save-share'); ?>
						<?php if (has_post_thumbnail()): ?>
						<?php the_post_thumbnail('single-header', [
							'itemprop' => 'image thumbnailUrl'
						]); ?>
						<?php endif; ?>
					</div>
					<?php if (get_the_content()): ?>
					<div class="flexible-content">
						<div class="wysiwyg-content">
							<?php the_content(); ?>
						</div>
					</div>
					<?php endif; ?>
					<?php if (($close = get_field('competition_close')) && time() >= DateTime::createFromFormat('d/m/Y g:i a', $close)->getTimestamp()): ?>
					<div class="survey-closed">
						<h3>Sorry &mdash; this offer has now ended.</h3>
						Why not check our <a href="<?php echo get_post_type_archive_link('offers'); ?>">offers page</a> to see the latest?
					</div>
					<?php elseif (get_field('survey_embed_code')): ?>
					<?php if (is_user_logged_in()): ?>
					<div class="survey-embed">
						<?php the_field('survey_embed_code'); ?>
					</div>
					<?php else: ?>
					<div class="ingredients-method-container">
						<div class="gated">
							<div class="container skid">
								<div class="existing-customers">
									<img alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/ak-club.png">
									<h2>
										Please <span>log in</span> to enter this competition
									</h2>
									<?php $GLOBALS['Annabel_Karmel']->wp_login_form([
										'form_id' => 'pp_loginform',
										'id_password' => 'pp_user_pass',
										'id_submit' => 'pp_wp-submit',
										'id_username' => 'pp_user_login',
										'remember' => true
									]); ?>
								</div>
								<div class="new-customers">
									<h2>Don't have an account?</h2>
									<p>Join for these great benefits</p>
									<ul>
										<li class="check">Exclusive Recipes and Content</li>
										<li class="check">Insider News</li>
										<li class="check">Competitions</li>
										<li class="check">Vouchers and Offers</li>
									</ul>
									<p>
										<a class="button" href="<?php echo site_url('/register/'); ?>">Join Now &gt;</a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?><style type="text/css">img.attachment-single-header.size-single-header.wp-post-image {
    width: 100%;
}</style>
<?php get_footer(); ?>
<?php if ($title = get_sub_field('title')): ?>
<div class="wysiwyg-content">
	<h2><?php echo $title; ?></h2>
</div>
<?php endif; ?>
<?php if (have_rows('gallery')): ?>
<div class="gallery-grid">
	<?php while (have_rows('gallery')): the_row(); ?>
	<?php if ($link = get_sub_field('link_to_url')): ?>
	<a href="<?php echo esc_url($link); ?>">
	<?php else: ?>
	<div>
	<?php endif; ?>
		<?php $image = get_sub_field('image'); ?>
		<?php if ($image['caption'] || $link): ?>
		<span><?php echo ($image['caption'] ?: 'Read More'); ?></span>
		<?php endif; ?>
		<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['gallery-grid']; ?>" title="<?php echo esc_attr($image['title']); ?>">
	<?php if ($link): ?>
	</a>
	<?php else: ?>
	</div>
	<?php endif; ?>
	<?php endwhile; ?>
</div>
<?php endif; ?>
��          �      �       H     I     _  4   w     �  
   �     �     �     �          (  :   7     r  ;   �     �     �  '   �  9   
     D     T     a     �  !   �     �     �     �     �  ;        
                            	                                Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2015-10-15 18:38:31+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Stable (latest release)
 Kon niet uitschakelen. Kon niet omschakelen tussen gebruikers. Direct omschakelen tussen gebruikersaccounts in WordPress John Blackbourn Uitschakelen Terugschakelen naar %1$s (%2$s) Naar&nbsp;omschakelen Teruggeschakeld naar %1$s (%2$s). Omgeschakeld naar %1$s (%2$s). User Switching Omschakelen gebruiker https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ 
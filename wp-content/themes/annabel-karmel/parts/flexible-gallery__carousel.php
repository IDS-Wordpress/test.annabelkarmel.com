<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
?>
 <?php if($isLoginRequired) { ?>
<div class="loginRequired"> 
<div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
     <?php
    }
    ?>
<?php if ($gallery = get_sub_field('gallery')): ?>
<div class="owl-carousel owl-theme" data-items="<?php echo esc_attr($num_items = get_sub_field('number_of_items')); ?>">
    
     <?php foreach ($gallery as $image): ?>
	<div class="item">

   
		<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['gallery-carousel']; ?>">
	</div>
	<?php endforeach; ?>
</div>

<?php endif; ?>
 <?php if($isLoginRequired) { ?>
</div>
     <?php
    }
    ?>
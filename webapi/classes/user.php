<?php
define('WP_USE_THEMES', false);
include('../wp-load.php');
include('classes/user_api.php');

/****** Auth Username/Password for Authorization***************/
$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$obj = new API_USER($username,$password);

/****** Parameters for routing on base of action***************/
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

/****** Start Data Recieving from Json Raw Data OR Form Post Data***************/
$params = (array) json_decode(file_get_contents('php://input'), TRUE);
if(!empty($params)) {	$data = $params; }
else {	$data = isset($_POST)?$_POST : array(); }
/****** End Data Recieving from Json Raw Data OR Form Post Data***************/

switch ($action)
{
	case 'authorisation_register':				
		$obj->register_user($data);
		break;
	case 'authorisation_login':
		$obj->authorize_user($data);
		break;
	case 'authorisation_forgotpassword':
		$obj->user_forgotpassword($data);
		break;
	case 'authorisation_updateprofile':
		$obj->update_user($data);
		break;
	case 'authorisation_getprofile':
		$obj->user_information();
		break;
	default:
		$obj->response(array('status'=>'fail','error_code'=>400,'response_msg'=>'Bad Request'));
		header("HTTP/1.0 400 Bad Request");	
		break;			
}
?>	 
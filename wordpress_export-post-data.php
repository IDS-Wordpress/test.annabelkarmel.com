<?php
/**
 * Export WordPress post data to CSV
 * Based on <http://stackoverflow.com/a/3474698> and <http://ran.ge/2009/10/27/howto-create-stream-csv-php/>
 */

/**
 *********************************************************************
 * Configuration
 *********************************************************************
 */

/**
 * Path to WordPress installation root
 * Default path './'
 */
// define( 'PATH_TO_WORDPRESS', './' ); # Default WordPress install location
define( 'PATH_TO_WORDPRESS', '' ); # Default WordPress sub-directory install location


/**
 * Export fields: array of strings representing $post properties to output to CSV.
 */
$export_fields = array(
	'Id',
	'post_title',
	'image_url',
	'size',
	'Category'
);


/**
 * Export query parameters for WP_Query
 * @link http://codex.wordpress.org/Function_Reference/WP_Query WP_Query parameter reference
 */
$export_query = array(
	'posts_per_page' => -1,
	'post_status' => array( 'mobile', 'publish'),
	'post_type' => 'recipes',
	'meta_query'=> array(
			'relation' => 'OR',
			array(
				'key'     => 'visibility',
				'value'   => 'Both',
				'compare' => '='
			),
			array(
				'key'     => 'visibility',
				'value'   => 'Mobile',
				'compare' => '='
			)),
);


/**
 *********************************************************************
 * Data export
 *********************************************************************
 */

// Require WordPress environment
require PATH_TO_WORDPRESS . 'wp-load.php';

// Posts query
$posts = new WP_Query( $export_query );
$posts = $posts->posts;



// Output file stream
$output_filename = 'export_' . strftime( '%Y-%m-%d' )  . '.csv';
$output_handle = @fopen( 'php://output', 'w' );

header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
header( 'Content-Description: File Transfer' );
header( 'Content-type: text/csv' );
header( 'Content-Disposition: attachment; filename=' . $output_filename );
header( 'Expires: 0' );
header( 'Pragma: public' );

// Output data to file
foreach ( $posts as $post ) {

	// Get post permalink
	
	// Build export array
	$post_export = array( $permalink );
	foreach ( $export_fields as $export_field ) {
		if($export_field=='Category'){
			$namesset='';
			$dbs=wp_get_post_terms($post->ID, 'mobile-recipe-category', array("fields" => "all"));
			foreach ($dbs as $ctlue) {
				$namesset .=$ctlue->name.' ,';
			}

			$post_export[] = rtrim($namesset,",");
		}else
		if($export_field=='Id'){
			$post_export[] = $post->ID;
		}else
		if($export_field=='image_url'){
			
			$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );		
			$post_export[] = $feat_image_url;
		}else if($export_field=='size'){
			$post_export[] = formatSizeUnits(filesize( get_attached_file( get_post_thumbnail_id($post->ID)  ) ));
		}else{
			$post_export[] = $post->$export_field;
		}
		
	}

	// Add row to file
	fputcsv( $output_handle, $post_export );

}

// Close output file stream
fclose( $output_handle );
  function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}
// We're done!
exit;
?>
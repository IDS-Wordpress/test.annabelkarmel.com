<?php if ($title = get_sub_field('title')): ?>
<div class="wysiwyg-content">
	<h2><?php echo $title; ?></h2>
</div>
<?php endif; ?>
<?php if ($gallery = get_sub_field('gallery')): ?>
<div class="flexslider">
	<ul class="slides">
		<?php foreach ($gallery as $image): ?>
		<li style="background-image: url('<?php echo $image['sizes']['gallery-slide']; ?>');"></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>
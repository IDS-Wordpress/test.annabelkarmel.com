<?php
/**
 * Template Name: Saved Content
 */
if (!is_user_logged_in()) {
	wp_redirect(site_url('/'), 301);
	exit;
}

get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="skid welcome-banner">
					<div class="container">
						<div>
							<h1>
								<span>Welcome</span>
								<span><?php echo wp_get_current_user()->user_firstname; ?> <?php echo wp_get_current_user()->user_lastname; ?></span>
							</h1>
							<div>
								<div>
									<img alt="" src="<?php echo get_template_directory_uri(); ?>/images/saved-content-save.png">
								</div>
								<div class="wysiwyg-content">
									<p>
										Here you will find all the content you saved throughout the site. You can also create and organise your collections as you please.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $collections = $GLOBALS['AK_Saved_Content']->list_collections(); ?>
				<?php $count = (isset($_GET['collection_id']) && isset($collections[$_GET['collection_id']])
					? count($collections[$_GET['collection_id']]['content'])
					: array_sum(array_map(function($item) {
						return count($item['content']);
					}, $collections))
				); ?>
				<div class="search-title">
					<h1><?php the_title(); ?></h1>
					<?php if (!$collections): ?>
						<p style="font-size: 22px; line-height: 37px; margin-top: 20px;">You currently don't have any saved content. To add content simply click on the blue heart under your favourite recipes, products and articles.</p>
					<?php endif; ?>
					<?php if ($collections): ?>
					<div class="bootstrap-namespaced">
						<select class="selectpicker">
							<option<?php if (!isset($_GET['collection_id'])) echo' selected';?> value="<?php echo remove_query_arg('collection_id'); ?>">All</option>
							<?php foreach ($collections as $k => $collection): ?>
							<option<?php if (isset($_GET['collection_id']) && $k == $_GET['collection_id']) echo' selected';?> value="<?php echo esc_url(add_query_arg('collection_id', $k)); ?>"><?php echo $collection['title']; ?></option>
							<?php endforeach; ?>
						</select>
						<?php if (isset($_GET['collection_id'])): ?>
						<div id="alter-existing-collection">
							<a data-collection-id="<?php echo $_GET['collection_id']; ?>" id="edit-existing-collection">Edit this Collection</a><br>
							<a data-collection-id="<?php echo $_GET['collection_id']; ?>" id="delete-existing-collection">Delete this Collection</a>
						</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php if ($count): ?>
				<div class="ak-post-widgets">
					<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="12" seo="true" post_type="post, page, recipes, products, apps-books, competitions" orderby="post__in" posts_per_page="12" post__in="' . implode(',', (isset($_GET['collection_id']) && isset($collections[$_GET['collection_id']])
						? $collections[$_GET['collection_id']]['content']
						: array_map(function($item) {
							return implode(',', $item['content']);
						}, $collections)
					)) . '" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<?php
define('WP_USE_THEMES', false);
require('../../wp-load.php');
include('classes/recipes_api.php');

/****** Auth Username/Password for Authorization***************/
$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$obj = new API_RECIPES($username,$password);

/****** Parameters for routing on base of action***************/
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

/****** Start Data Recieving from Json Raw Data OR Form Post Data***************/
$params = (array) json_decode(file_get_contents('php://input'), TRUE);
if(!empty($params)) { $data = $params; }else{ $data = isset($_REQUEST)?$_REQUEST : array(); }
/****** End Data Recieving from Json Raw Data OR Form Post Data***************/

switch ($action)
{
	case 'Categories':
		$obj->get_categories();
		break;
	case 'category':
		$obj->get_category($id);
		break;
	case 'recentcat':
		$obj->latest_category();
		break;
	case 'Filters':
		$obj->get_filters();
		break;
	case 'detail':
		$obj->get_detail($id);
		break;
	default:
		$obj->get_all($data);
		break;
}
?>
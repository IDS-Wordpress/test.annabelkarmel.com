<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php single_term_title(); ?></h1>
				<?php if ($description = category_description()): ?>
				<div class="flexible-content">
					<div class="wysiwyg-content">
						<?php echo $description; ?>
					</div>
				</div>
				<?php endif; ?>
				<?php $the_query = new WP_Query([
					'meta_key' => 'mark_this_as_featured',
					'meta_value' => 1,
					'post_type' => 'products',
					'posts_per_page' => 3,
					'tax_query' => [
						[
							'taxonomy' => get_queried_object()->taxonomy,
							'terms' => get_queried_object()->term_id
						]
					]
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<h2>Featured Products</h2>
				<div class="ak-post-widgets">
					<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
					<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php elseif (have_posts()): ?>
				<h2>Featured Products</h2>
				<div class="ak-post-widgets">
					<?php while (have_posts()): the_post(); ?>
					<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					<?php if (1 < $GLOBALS['wp_query']->current_post) break; ?>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				<?php if (3 < $GLOBALS['wp_query']->found_posts): ?>
				<h2>All Other Products</h2>
				<div class="ak-post-widgets">
					<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="products" taxonomy="product-category" taxonomy_terms="' . get_queried_object()->slug . '" taxonomy_operator="IN" offset="3" posts_per_page="6" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
				</div>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
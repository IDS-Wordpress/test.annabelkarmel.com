<?php
/**
 * Template Name: Edit Profile template
 */
if (!is_user_logged_in())
{
	wp_redirect( home_url() );
	exit;
}
get_header('profile');
 ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
			 <?php //if (get_the_content()): ?>
				<div class="flexible-content" itemprop="articleBody" >			
					<div class="edit-profile-title">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
				<div class="clear"></div>
				<?php //endif; ?>
				<?php //get_template_part('parts/flexible-content'); ?>
				<?php  if ( is_user_logged_in() ) { 
				   global $current_user;
			      get_currentuserinfo();
			      $user_id =  $current_user->ID;
			      $is_pushed_to_mailchimp =  get_user_meta($user_id, 'is_pushed_to_mailchimp',true);
				?>
				<style>div#my-little-ones { display: block;}</style>
				<section class="register_section edit_section" style="background-color: #ffffff;">
				<form id="securePasswordForm" class="form-horizontal" method="post" name="myform" autocomplete="off">
					    <div id="reg-step-1" class="toggle-step">
					        <div id="about-me" class="toggle-div">
					        	<input type="hidden" name="user_id" value="<?php echo $user_id;?>">
					        	<div class="form-group half">
					            	<input type="text" name="full_name" class="input form-control" autocomplete="false" required value="<?php echo  $current_user->user_firstname; ?>">
					            	<label class="control-label active">First Name</label>
					          	</div>
					          <div class="form-group half">
				            	<input type="text" name="last_name" class="input form-control" autocomplete="false" required value="<?php echo $current_user->user_lastname; ?>">
				            	<label class="control-label active">Last Name</label>
				          </div>
				          <div class="form-group edit-user-email">
				            <input type="text" name="about_email" id="about_email" class="input form-control" autocomplete="false" required value="<?php echo $current_user->user_email;?>" readonly>
				            <label class="control-label active">Email</label>
				          </div>
						  
					        <div class="form-group password-group half">
					          	<input type="password" class="input form-control" name="pwd" autocomplete="false" />
					          	<label class="control-label">Password</label>
					        </div>
					        <div class="form-group password-group half">
					          	<input type="password" class="input form-control" name="con_pwd" autocomplete="false" />
					          	<label class="control-label">Confirm Password</label>
					        </div>
				          	<div id="my-little-ones" class="step_form toggle-div">
				             	<div id="add-more-ones">
										<?php 
											$num = 0;
											foreach($little_dob as $little_dob1){
												if(!empty($little_dob1)){
													$little_date_check = date('Ymd', strtotime($little_dob1));
												}else{
													$little_date_check = ""; 
												}
												$num++;
											}
										?>
										<?php
											$num = 0;
										    for($num= 0; $num <= 9; $num++){
										    	?>
										    	<div class="add-little-ones">
										    	<?php 
										      	$little_name = get_user_meta($user_id, 'children_'.$num.'_childs_name',true); 
										      
										      	$little_dob = get_user_meta($user_id, 'children_'.$num.'_childs_birthday',true); 
										     

										      	if(!empty($little_name)){
										      		?>
										      			<div class="form-group">
															<input type="text" class="input form-control" name="little_name[]" autocomplete="false" value="<?php echo $little_name;?>">
															<label class="control-label active">Name</label>
														</div>
										      		<?php
										      	}else{
										      		break;
										      	}
										      	if(!empty($little_dob)){
										      		?>
														<div class="form-group">
															<input type="text" class="input readonly form-control datepicker" name="little_dob[]" autocomplete="false" value="<?php echo date('d/m/Y',strtotime($little_dob));?>">
															<label class="control-label active">Date of Birth <i>(dd/mm/yyyy)</i></label>
														  </div>
										      		<?php
										      	}else{
										      		break;
										      	}
										      	?>
										      	<a href="javascript:void(0);" class="remove_button">
										      		<img src="<?php echo get_template_directory_uri(); ?>/images/delete-icon.png" images="icon">
										      	</a>
										      </div>
										      	<?php 
										    }
											?>
																	
								  <div class="form-group child-error">
							  		<a href="javascript:void(0);" class="remove_button disabled"><img src="<?php echo get_template_directory_uri(); ?>/images/delete-icon.png" images="icon"></a>

									  <div class="full_width child-error">If enter child name date field mandetory</div>
								  </div>

					            
				          		</div>
								<div id="little-once-wrapper">
									</div>

				          		<div class="form-group">
				                	<a href="javascript:void(0);" class="add_button" title="Add New Child">Add New Child</a>
				              	</div>
								<div class="form-group slct-group half">
						            <select class="input form-control" name="country">
						                <option value="">-- Select Country --</option>
										<option value="United Kingdom">United Kingdom</option>
										<option value="Australia">Australia</option>
										<option value="United States of America">United States of America</option>
										<option value="Netherlands">Netherlands</option>
										<option value="United Arab Emirates">United Arab Emirates</option>
										<option value="Afghanistan">Afghanistan</option>
										<option value="Albania">Albania</option>
										<option value="Algeria">Algeria</option>
										<option value="American Samoa">American Samoa</option>
										<option value="Andorra">Andorra</option>
										<option value="Angola">Angola</option>
										<option value="Antigua and Barbuda">Antigua and Barbuda</option>
										<option value="Argentina">Argentina</option>
										<option value="Armenia">Armenia</option>
										<option value="Austria">Austria</option>
										<option value="Azerbaijan">Azerbaijan</option>
										<option value="Bahamas">Bahamas</option>
										<option value="Bahrain">Bahrain</option>
										<option value="Bangladesh">Bangladesh</option>
										<option value="Barbados">Barbados</option>
										<option value="Belarus">Belarus</option>
										<option value="Belgium">Belgium</option>
										<option value="Belize">Belize</option>
										<option value="Benin">Benin</option>
										<option value="Bermuda">Bermuda</option>
										<option value="Bhutan">Bhutan</option>
										<option value="Bolivia">Bolivia</option>
										<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
										<option value="Botswana">Botswana</option>
										<option value="Brazil">Brazil</option>
										<option value="Brunei">Brunei</option>
										<option value="Bulgaria">Bulgaria</option>
										<option value="Burkina Faso">Burkina Faso</option>
										<option value="Burundi">Burundi</option>
										<option value="Cambodia">Cambodia</option>
										<option value="Cameroon">Cameroon</option>
										<option value="Canada">Canada</option>
										<option value="Cape Verde">Cape Verde</option>
										<option value="Cayman Islands">Cayman Islands</option>
										<option value="Central African Republic">Central African Republic</option>
										<option value="Chad">Chad</option>
										<option value="Chile">Chile</option>
										<option value="China">China</option>
										<option value="Colombia">Colombia</option>
										<option value="Comoros">Comoros</option>
										<option value="Congo, Democratic Republic of the">Congo, Democratic Republic of the</option>
										<option value="Congo, Republic of the">Congo, Republic of the</option>
										<option value="Costa Rica">Costa Rica</option>
										<option value="Croatia">Croatia</option>
										<option value="Cuba">Cuba</option>
										<option value="Curaçao">Curaçao</option>
										<option value="Cyprus">Cyprus</option>
										<option value="Czech Republic">Czech Republic</option>
										<option value="Côte d'Ivoire">Côte d'Ivoire</option>
										<option value="Denmark">Denmark</option>
										<option value="Djibouti">Djibouti</option>
										<option value="Dominica">Dominica</option>
										<option value="Dominican Republic">Dominican Republic</option>
										<option value="East Timor">East Timor</option>
										<option value="Ecuador">Ecuador</option>
										<option value="Egypt">Egypt</option>
										<option value="El Salvador">El Salvador</option>
										<option value="Equatorial Guinea">Equatorial Guinea</option>
										<option value="Eritrea">Eritrea</option>
										<option value="Estonia">Estonia</option>
										<option value="Ethiopia">Ethiopia</option>
										<option value="Faroe Islands">Faroe Islands</option>
										<option value="Fiji">Fiji</option>
										<option value="Finland">Finland</option>
										<option value="France">France</option>
										<option value="French Polynesia">French Polynesia</option>
										<option value="Gabon">Gabon</option>
										<option value="Gambia">Gambia</option>
										<option value="Georgia">Georgia</option>
										<option value="Germany">Germany</option>
										<option value="Ghana">Ghana</option>
										<option value="Gibraltar">Gibraltar</option>
										<option value="Greece">Greece</option>
										<option value="Greenland">Greenland</option>
										<option value="Grenada">Grenada</option>
										<option value="Guam">Guam</option>
										<option value="Guatemala">Guatemala</option>
										<option value="Guinea">Guinea</option>
										<option value="Guinea-Bissau">Guinea-Bissau</option>
										<option value="Guyana">Guyana</option>
										<option value="Haiti">Haiti</option>
										<option value="Honduras">Honduras</option>
										<option value="Hong Kong">Hong Kong</option>
										<option value="Hungary">Hungary</option>
										<option value="Iceland">Iceland</option>
										<option value="India">India</option>
										<option value="Indonesia">Indonesia</option>
										<option value="Iran">Iran</option>
										<option value="Iraq">Iraq</option>
										<option value="Ireland">Ireland</option>
										<option value="Israel">Israel</option>
										<option value="Italy">Italy</option>
										<option value="Jamaica">Jamaica</option>
										<option value="Japan">Japan</option>
										<option value="Jordan">Jordan</option>
										<option value="Kazakhstan">Kazakhstan</option>
										<option value="Kenya">Kenya</option>
										<option value="Kiribati">Kiribati</option>
										<option value="Kosovo">Kosovo</option>
										<option value="Kuwait">Kuwait</option>
										<option value="Kyrgyzstan">Kyrgyzstan</option>
										<option value="Laos">Laos</option>
										<option value="Latvia">Latvia</option>
										<option value="Lebanon">Lebanon</option>
										<option value="Lesotho">Lesotho</option>
										<option value="Liberia">Liberia</option>
										<option value="Libya">Libya</option>
										<option value="Liechtenstein">Liechtenstein</option>
										<option value="Lithuania">Lithuania</option>
										<option value="Luxembourg">Luxembourg</option>
										<option value="Macedonia">Macedonia</option>
										<option value="Madagascar">Madagascar</option>
										<option value="Malawi">Malawi</option>
										<option value="Malaysia">Malaysia</option>
										<option value="Maldives">Maldives</option>
										<option value="Mali">Mali</option>
										<option value="Malta">Malta</option>
										<option value="Marshall Islands">Marshall Islands</option>
										<option value="Mauritania">Mauritania</option>
										<option value="Mauritius">Mauritius</option>
										<option value="Mexico">Mexico</option>
										<option value="Micronesia">Micronesia</option>
										<option value="Moldova">Moldova</option>
										<option value="Monaco">Monaco</option>
										<option value="Mongolia">Mongolia</option>
										<option value="Montenegro">Montenegro</option>
										<option value="Morocco">Morocco</option>
										<option value="Mozambique">Mozambique</option>
										<option value="Myanmar">Myanmar</option>
										<option value="Namibia">Namibia</option>
										<option value="Nauru">Nauru</option>
										<option value="Nepal">Nepal</option>
										<option value="New Zealand">New Zealand</option>
										<option value="Nicaragua">Nicaragua</option>
										<option value="Niger">Niger</option>
										<option value="Nigeria">Nigeria</option>
										<option value="North Korea">North Korea</option>
										<option value="Northern Mariana Islands">Northern Mariana Islands</option>
										<option value="Norway">Norway</option>
										<option value="Oman">Oman</option>
										<option value="Pakistan">Pakistan</option>
										<option value="Palau">Palau</option>
										<option value="Palestine, State of">Palestine, State of</option>
										<option value="Panama">Panama</option>
										<option value="Papua New Guinea">Papua New Guinea</option>
										<option value="Paraguay">Paraguay</option>
										<option value="Peru">Peru</option>
										<option value="Philippines">Philippines</option>
										<option value="Poland">Poland</option>
										<option value="Portugal">Portugal</option>
										<option value="Puerto Rico">Puerto Rico</option>
										<option value="Qatar">Qatar</option>
										<option value="Romania">Romania</option>
										<option value="Russia">Russia</option>
										<option value="Rwanda">Rwanda</option>
										<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
										<option value="Saint Lucia">Saint Lucia</option>
										<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
										<option value="Samoa">Samoa</option>
										<option value="San Marino">San Marino</option>
										<option value="Sao Tome and Principe">Sao Tome and Principe</option>
										<option value="Saudi Arabia">Saudi Arabia</option>
										<option value="Senegal">Senegal</option>
										<option value="Serbia">Serbia</option>
										<option value="Seychelles">Seychelles</option>
										<option value="Sierra Leone">Sierra Leone</option>
										<option value="Singapore">Singapore</option>
										<option value="Sint Maarten">Sint Maarten</option>
										<option value="Slovakia">Slovakia</option>
										<option value="Slovenia">Slovenia</option>
										<option value="Solomon Islands">Solomon Islands</option>
										<option value="Somalia">Somalia</option>
										<option value="South Africa">South Africa</option>
										<option value="South Korea">South Korea</option>
										<option value="Spain">Spain</option>
										<option value="Sri Lanka">Sri Lanka</option>
										<option value="Sudan">Sudan</option>
										<option value="Sudan, South">Sudan, South</option>
										<option value="Suriname">Suriname</option>
										<option value="Swaziland">Swaziland</option>
										<option value="Sweden">Sweden</option>
										<option value="Switzerland">Switzerland</option>
										<option value="Syria">Syria</option>
										<option value="Taiwan">Taiwan</option>
										<option value="Tajikistan">Tajikistan</option>
										<option value="Tanzania">Tanzania</option>
										<option value="Thailand">Thailand</option>
										<option value="Togo">Togo</option>
										<option value="Tonga">Tonga</option>
										<option value="Trinidad and Tobago">Trinidad and Tobago</option>
										<option value="Tunisia">Tunisia</option>
										<option value="Turkey">Turkey</option>
										<option value="Turkmenistan">Turkmenistan</option>
										<option value="Tuvalu">Tuvalu</option>
										<option value="Uganda">Uganda</option>
										<option value="Ukraine">Ukraine</option>
										<option value="Uruguay">Uruguay</option>
										<option value="Uzbekistan">Uzbekistan</option>
										<option value="Vanuatu">Vanuatu</option>
										<option value="Vatican City">Vatican City</option>
										<option value="Venezuela">Venezuela</option>
										<option value="Vietnam">Vietnam</option>
										<option value="Virgin Islands, British">Virgin Islands, British</option>
										<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
										<option value="Yemen">Yemen</option>
										<option value="Zambia">Zambia</option>
										<option value="Zimbabwe">Zimbabwe</option>
						            </select>
									<?php $country = get_user_meta($user_id,'country',true);?>
									
						            <label class="control-label active">Country</label>
							    </div>
						        <div class="form-group half">
						            <input type="text" name="post_code" class="input form-control" autocomplete="false" required value="<?php echo $current_user->postcode;?>">
						            <label class="col-sm-3 control-label active">Postcode/Zipcode</label>
						        </div>
						        
							    <div class="checkbox">
					                <label>
					                    <input type="checkbox" name="subscribe" value="1" <?php if($is_pushed_to_mailchimp==1){echo 'checked';}?> /> <span class="rect"></span> I would like to receive information, competitions, deals, vouchers and recipes. Unsubscribe at any time.
					                </label>
					            </div>
						        <div class="form-group">
						            <!--<input type="button" id="step-1-submit" class="btn btn-default btn-next reg_btn step-3" value="SAVE">-->
									<input type="submit" id="updateBtn" class="btn btn-default btn-next reg_btn step-3" value="SAVE">
					               <span id="response-loading"><i class="fa fa-spinner fa-spin"></i> Please wait Loading</span>
					               <div id="update-response"></div>
						        </div>

				      		</div>
				    	</div>
				    </form>
				</section>
			<?php } else{
					?>
					<div class="wysiwyg-content itemprop=" description="" style="background-color: #ffffff;">
						Oops! You need to be logged in to use this form.
						<div class="clear"></div>
					</div>
				<?php }
				?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer('profile'); ?>
<script>
jQuery('[name=country] option').filter(function() { 
	return (jQuery(this).text() == '<?php echo $country; ?>'); 
}).prop('selected', true);
</script>
<script>
	$(document).ready(function(){
		$('#securePasswordForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
			validating: 'glyphicon glyphicon-refresh',
            invalid: 'glyphicon glyphicon-remove'
        },
        fields: {
            full_name: {
                validators: {
                    notEmpty: {message: 'The First Name field is mandatory – please enter at least two characters for this field'},
					callback: {
						message: 'The First Name field is mandatory – please enter at least two characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z \-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							if(value.length == 0){return false;}
							return true;
						}
					}
                }
            },

            last_name: {
                validators: {
                    notEmpty: {message: 'The Last Name field is mandatory – please enter at least two characters for this field'},
					callback: {
						message: 'The Last Name field is mandatory – please enter at least two characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z \-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							if(value.length == 0){return false;}
							return true;
						}
					}
                }
            },
            about_email: {
                validators: {
                    notEmpty: {
                        message: 'The Email field is mandatory – please enter at least seven characters for this field'
                    },
                    remote: {
                        enabled: false,
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        type: "POST",
                        message: '<div id="email-exists"><span>Already registered. </span><span class="click-here"><a href="#login">Log in</a></span></div>',
                        delay: 1000,
                        data : {action: "check_email_exists"}
                    },
                    callback: {
                        callback: function(value, validator, $field) {
							//var regs = /[*\`|\~|\!|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>| ]/g;
							var regs = /[ ]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							

                            if(value.length < 7 && value.length != 0) {
                                return {
                                    valid: false,
                                    message: 'The Email field is mandatory – please enter at least seven characters for this field'
                                }
                            }
							
							if(value.length > 255) {
								$field.val(value.substring(0, 255));
							}
							
                            if (value.length >= 7) {
								
								//var atReg = /[*\@]/g;
								var atReg = /[*\%|\$|\&|\*|\£|\&|\"|\(]/g;
								
								if(value.search(atReg) > 0) {
									
									return {
										valid: false,
										message: "The Email field only accepts letters (a-z), numbers, and the following special characters + @ . -"
									}
									
									
									
								} else {
									var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									
									if(value.search(reg) < 0) {
										validator.options.fields.about_email.validators.remote.enabled = false;
										//validator.revalidateField('about_email');
										return {
											valid: false,
											message: 'Please enter a valid Email – it looks like you might be missing an @ sign'
										}
									}									
								}
								
                                
                            }
							
							if(value.length == 0){
								return {
									valid: false,
									message: 'The Email field is mandatory – please enter at least seven characters for this field'
								};
							}
									
                            validator.options.fields.about_email.validators.remote.enabled = true;
							validator.options.fields.about_email.validators.remote.data.about_email = value;
                            return true
                        }
                    }
                }
            },
            pwd: {
                validators: {                   
                    stringLength: {
                        min: 6,
                        //max: 100,
                        message: 'The Password field is mandatory – please enter at least six characters for this field'
                    },
                    identical: {
                        field: 'con_pwd',
                        message: 'The password and its confirm are not the same'
                    },
					callback: {
						message: 'The Password field is mandatory – please enter at least six characters for this field',
                        callback: function(value, validator, $field) {
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if(value.length > 100) {
								$field.val(value.substring(0, 100));
							}
							return true;
						}
					}
                }
            },
            con_pwd: {
                validators: {                    
                    stringLength: {
                        min: 6,
                        //max: 100,
                        message: 'The Password field is mandatory – please enter at least six characters for this field'
                    },
                    identical: {
                        field: 'pwd',
                        message: 'The password and its confirm are not the same'
                    },
					callback: {
						message: 'The Password field is mandatory – please enter at least six characters for this field',
                        callback: function(value, validator, $field) {
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if(value.length > 100) {
								$field.val(value.substring(0, 100));
							}
							return true;
						}
					}
                }
            },
            about_iam: {
                validators: {
                    notEmpty: {
                        message: 'This field is mandatory – please select the type of user you are'
                    }
                }
            },
            'little_name[]': {
                validators: {
                    notEmpty: {
						enabled: false,
                        message: "It looks like you've started typing in the child's Name field – please enter at least two characters for this field or leave it empty"
                    },
					callback: {
						message: "It looks like you've started typing in the child's Name field – please enter at least two characters for this field or leave it empty",
                        callback: function(value, validator, $field) {
							var regs = /[^a-zA-Z| |\-\'\.]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {

								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 2) {
										return {valid: false,}
									}
								}
								if(value.length > 50) {$field.val(value.substring(0, 50));}
							}
														
							isChildValid();
							return {valid: true};
						}
					}
                }
            },
            'little_dob[]': {
                validators: {
                    notEmpty: {
						enabled : false,
						message: "The child's Date of Birth or Due Date field is mandatory – please select a date"
					},
                    date: {
						format: 'DD/MM/YYYY',
						message: "The child's Date of Birth or Due Date field is mandatory – please select a date"},
					callback: {
						message: "The child's Date of Birth or Due Date field is mandatory – please select a date",
						callback: function(value, validator, $field) {
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
							
							isChildValid();
							return {valid: true};
						}
					}
                }
            },
            house_no:{
                validators: {
                    notEmpty: {
                        message: 'The House Name/Number field is mandatory – please enter at least one character for this field'
                    },
					callback: {
						message: 'The House Name/Number field is mandatory – please enter at least one character for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
                            if (value.length > 0) {
																
								if (value.length > 0 && value != " " && value != "  ") {
									if(value.length < 1) {return {valid: false}}
									if(value.length > 100) {$field.val(value.substring(0, 100));}
								}
								
							}
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            street:{
                validators: {
                    notEmpty: {
                        message: 'The Street field is mandatory – please enter at least three characters for this field'
                    },
					callback: {
						message: 'The Street field is mandatory – please enter at least three characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {

								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 3) {return {valid: false}}
									if(value.length > 100) {$field.val(value.substring(0, 100));}
								}
							}
							
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            city:{
                validators: {
                    notEmpty: {
                        message:'The City field is mandatory – please enter at least three characters for this field'
                    },
					callback: {
						message:'The City field is mandatory – please enter at least three characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								
								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 3) {return {valid: false}}
									if(value.length > 50) {$field.val(value.substring(0, 50));}
								}
							}
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            post_code:{
                validators: {
                    notEmpty: {
                        message:'The Postcode/Zipcode field is mandatory – please enter at least four characters for this field'
                    },
					callback: {
						message: 'The Postcode/Zipcode field is mandatory – please enter at least four characters for this field',
                        callback: function(value, validator, $field) {
							var regs = /[*\`|\~|\!|\@|\#|\%|\^|\$|\&|\*|\||\(|\||\)|\_|\+|\=|\\|\[|\]|\{|\}|\"|\;|\:|\/|\?|\,|\.|\'|\<|\>]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							
                            if (value.length > 0) {
								
								if (value.length > 0 && value != " " && value != "  ") {
									
									if(value.length < 4) {return {valid: false,}}
									if(value.length > 11) {$field.val(value.substring(0, 11));}
								}
							}
							
							if(value.length == 0){return {valid: false};}
							return {valid: true};
						}
					}
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The Country field is mandatory'
                    },
					callback: {
						message: 'The Country field is mandatory',
						callback: function(value, validator, $field){
							if(value.length > 0) {
								$field.siblings(".control-label").addClass("active");
							} else {
								$field.siblings(".control-label").removeClass("active");
							}
							return true;
						}
					}
                }
            },
            agree: {
                validators: {
                    notEmpty: {
                        message: '	The Terms & Conditions field is mandatory - please select this checkbox to continue'
                    }
                }
            }
        }
    });
	var validator = $('#securePasswordForm').data('bootstrapValidator');
	$('#securePasswordForm').on("submit",function(e) { 
			e.preventDefault();
			
			var pwd = $("input[name=pwd]").val();
			var con_pwd = $("input[name=con_pwd]").val();
			
			if((pwd=="")||(con_pwd=="")){
				$('.form-control-feedback').removeClass('glyphicon glyphicon-ok');
			}
		
			
			
			var isValid = isChildValid();
			if(isValid && !$(this).hasClass("disabled"))
			{
				var form_data = $("#securePasswordForm").serialize();
				//console.log(form_data);
				$('#response-loading').show();
				var url = '<?php echo admin_url('admin-ajax.php'); ?>';
				  $.ajax({
					type:'POST',
					url: url,
					data: {
						  action: 'update_template_post_data',
						  formData: form_data
						},
					dataType: "json",
					success:function(response) { 
						if(response.status==1){
							$('#update-response').slideDown();
						   $('#update-response').html('<div class="success">Record update successfully!</div>');
						   setTimeout(function(){
							   $('#update-response').slideUp('slow', window.location.reload());
						   },5000);
						}
						$('#response-loading').hide();
					},
					error: function(errorThrown){
						$("#step-3-submit").removeClass("disabled");
						$('#response-loading').hide();
					}
				});
			} 
			return false;
		});
		
		$( ".datepicker" ).datepicker({
        maxDate: '0',
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy',
	    onSelect: function(dateText) {
		  var textBox = this;
		  setTimeout(function(){
			$(textBox).trigger('change');
			$(textBox).trigger('input');
		},100);
	  }
	});
		
	var maxField = 10;
    var addButton = $('.add_button');
    var wrapper = $('#little-once-wrapper');
	//var wrapper = $('#my-little-ones');
    var x = 1;
	var akId = 1;
	
	

    $(addButton).click(function(){
        if(!$(this).hasClass("disabled") && x < maxField){
            
            var fieldHTML = '<div class="add-little-ones"><div class="form-group"><input data-akid="name_'+akId+'" type="text" class="input form-control " name="little_name[]" required data-bv-field="little_name[]"><label class="control-label">Name</label></div><div class="form-group"><input readonly data-akid="akid_'+ akId +'" type="text" class="input form-control datepicker" required name="little_dob[]" data-bv-field="little_dob[]"><label class="control-label">Date of Birth <i>(dd/mm/yyyy)</i></label></div><a href="javascript:void(0);" class="remove_button"><img src="<?php echo get_template_directory_uri(); ?>/images/delete-icon.png" images="icon"></a></div>';

			$(addButton).addClass("disabled");
			$('.remove_button').removeClass("disabled");
            $(wrapper).append(fieldHTML);


            setTimeout(function() {
              $('.datepicker[data-akid="akid_'+ akId +'"]').datepicker({
                    maxDate: '0',
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd/mm/yy',
				  onSelect: function(dateText) {
					  var textBox = this;
					setTimeout(function(){
						$(textBox).trigger('change');
						$(textBox).trigger('input');
					},100);
				  }
				});

              var validator = $('#securePasswordForm').data('bootstrapValidator');
              validator.addField($('input.form-control[data-akid="name_'+akId+'"]'));
              validator.addField($( '.datepicker[data-akid="akid_'+ akId +'"]' ));

			  $(addButton).removeClass("disabled");
			  
				x++;
				akId +=1;

            },300);
			
			if(maxField == x) {
				$(this).hide();
			}

        }
    });

    $(wrapper).on('click', '.remove_button', function(e){ 
        e.preventDefault();
		
		if(x != 1) {
			$(this).parent('div').remove();
			x--;
		} else {
			//$(this).parent('div').remove();
			$(this).parent('div').find("input").val("");
			var isChilda = isChildValid();
		}
		
		if(x < maxField) {
			$(addButton).show();
		}
    });
	
	function isChildValid() {
		var isValid = true;
		var $rowWraper = $("#little-once-wrapper");
		
		$rowWraper.find(".add-little-ones").each(function(){
			var $this = $(this);
			var $nameElm = $this.find('input[name="little_name[]"]');
			var $dateElm = $this.find('input[name="little_dob[]"]');
			if($nameElm.length > 0) {
				var nameVal = $nameElm.val();
				var dateVal = $dateElm.val();
				if(nameVal.length != 0 && dateVal.length == 0) {
					$dateElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
					$("#updateBtn").attr( "disabled",true);
					isValid = false;
					return false;
				} else if(dateVal.length != 0 && nameVal.length == 0) {
					$nameElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
					//$("#updateBtn").attr( "disabled",true);
					isValid = false;
					return false;
				} else {
					$dateElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
					$nameElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
					//$("#updateBtn").attr( "disabled",false);
					isValid = true;
				}
			}
		});
		
		var $rowWraper2 = $("#add-more-ones");
		$rowWraper2.find(".add-little-ones").each(function(){
			var $this = $(this);
			var $nameElm = $this.find('input[name="little_name[]"]');
			var $dateElm = $this.find('input[name="little_dob[]"]');
			if($nameElm.length > 0) {
				var nameVal = $nameElm.val();
				var dateVal = $dateElm.val();
				if(nameVal.length != 0 && dateVal.length == 0) {
					$dateElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
					$("#updateBtn").attr( "disabled",true);
					isValid = false;
					return false;
				} else if(dateVal.length != 0 && nameVal.length == 0) {
					$nameElm.siblings("i.form-control-feedback").addClass("ap-glyphicon-remove");
					//$("#updateBtn").attr( "disabled",true);
					isValid = false;
					return false;
				} else {
					$dateElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
					$nameElm.siblings("i.form-control-feedback").removeClass("ap-glyphicon-remove");
					//$("#updateBtn").attr( "disabled",false);
					isValid = true;
				}
			}
		});
		
		return isValid;
	}
}); 
	$(document).ready(function(){
		
		var pwd = $("input[name=pwd]").val();
		var con_pwd = $("input[name=con_pwd]").val();
		
		
		
		/*if((pwd=="")||(con_pwd=="")){
			$('.input form-control').removeClass('glyphicon glyphicon-ok');
		}*/
		
		$('.register_section .form-group.password-group .form-control-feedback').click(function(){
			  if ($(".register_section .form-group.password-group .input").attr("type") == "password") {
				$(".register_section .form-group.password-group .input").attr("type", "text");
			  } else {
				$(".register_section .form-group.password-group .input").attr("type", "password");
			  }
		});
	});
</script>
<div class="feel-inspired">
	<h2><?php echo get_sub_field('heading'); ?></h2>
	<h3><?php echo get_sub_field('subheading'); ?></h3>
	<span class="bold">FILTER BY:</span>
	<div class="buttons">
		<button class="selected" data-panel="all">All</button>
		<?php 
		if($filters = get_sub_field('filter_items')):		
			foreach ($filters as $term):
			//print_r($filters);
			?>
			<button data-panel="<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
			<?php
			endforeach;
		endif;
		?>
	</div>

	<?php $count = (get_sub_field('first_load_count'))?get_sub_field('first_load_count'):6; ?>
	<div class="recipes">
		<div class="recipes-all">
			<div class="ak-post-widgets fi-panel">
				<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="'.$count.'" seo="true" post_type="apps-books" posts_per_page="'.$count.'" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
			</div>
		</div>
		<?php 
		if($filters = get_sub_field('filter_items')):
		foreach ($filters as $term): ?>
		<div class="recipes-<?php echo $term->slug; ?>">
			<div class="ak-post-widgets fi-panel">
				<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="'.$count.'" seo="true" post_type="apps-books" taxonomy="app-book-category" taxonomy_terms="' . $term->slug . '" posts_per_page="'.$count.'" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
			</div>
		</div>
		<?php
		endforeach;
		endif;
		?>
	</div>
</div>
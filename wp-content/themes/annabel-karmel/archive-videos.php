<?php
get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container videos-main">
		<div class="table">
			<div class="cell">
				<div class="flexible-content videos-top-section" itemprop="articleBody">
               
             <?php if( get_field("h1_title", 'option') ): ?><h1><?php the_field("h1_title", 'option'); ?></h1><?php endif; ?>
              <?php if( get_field("videos_descriptions", 'option') ): ?><?php the_field("videos_descriptions", 'option'); ?><?php endif; ?>
           
               		<div class="feature_section">
						<div class="leftpart">
						<?php
						if(get_field('feature_video_home','option')):
							$args = array(
									'post_type'=> 'videos',
									'p'=> get_field('feature_video_home','option'),
								);
						else:
							$args = array(
									'post_type'=> 'videos',
									'orderby'=> 'date',
									'order'>'DESC',
									'posts_per_page'=>1,								
								);
						endif;
						
						query_posts( $args );
						if(have_posts()):
							while(have_posts()): the_post();							 
							?>
								<div class="video-wrapper">
									<?php //the_field('video_url');
									$video = get_field( 'video_url' );
									preg_match('/src="(.+?)"/', $video, $matches_url );
									$src = $matches_url[1];
									
									preg_match('/embed(.*?)?feature/', $src, $matches_id );
									$id = $matches_id[1];
									$id = str_replace( str_split( '?/' ), '', $id );

									//var_dump( $video );
																
									?>
									<div id="holder">
										<div id="stock-video"></div>
										<a href="javascript:void(0);" id="stock-play-btn" class="player-icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/play-icon.png" />
										</a>
									</div>
								</div>
								<script>
								//youtube script
								var tag = document.createElement('script');
								tag.src = "//www.youtube.com/iframe_api";
								var firstScriptTag = document.getElementsByTagName('script')[0];
								firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
								var player;
								onYouTubeIframeAPIReady = function () {
									player = new YT.Player('stock-video', {
										width: '100%',
										height: '360',
										videoId: '<?php echo $id; ?>',  // youtube video id
										playerVars: {
											'autoplay': 1,
											'rel': 0,
											'showinfo': 0
										},
										events: {
											'onReady': onPlayerReady,
											'onStateChange': onPlayerStateChange
										}
									});
								}
								onPlayerReady = function (event) {
									$('#stock-play-btn').fadeOut('normal');
								}
								onPlayerStateChange = function (event) {
									if (YT.PlayerState.UNSTARTED || event.data == YT.PlayerState.BUFFERING || event.data == YT.PlayerState.PLAYING) {
										$('#stock-play-btn').fadeOut('normal');
									}
									if (event.data == YT.PlayerState.PAUSED) {
										$('#stock-play-btn').fadeIn('normal');
									}
									if (event.data == YT.PlayerState.ENDED) {
										$('#stock-play-btn').fadeIn('normal');
										//player.playVideo(); 
									}
								}
								$(document).on('click', '#stock-play-btn', function () {
									$(this).fadeOut('normal');
									player.playVideo();
								});
								</script>
							
							<?php
							endwhile;
						endif;
						wp_reset_query();
						?>
						</div>
						<div class="rightpart">
							<div class="advertpart">
							<?php
							if(get_field('adzone_for_video','option')):
							echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(get_field('adzone_for_video','option'), 'post', false, ICL_LANGUAGE_CODE) . ']');
							endif;
							?>
							</div>
							<div class="searchpart">
							<form action="<?php echo home_url( '/' ); ?>" method="get">
							<p><input type="text" name="s" placeholder="Search all videos"><input type="submit"></p>
							<input type="hidden" name="post_type" value="videos">
							</form>
							</div>
						</div>				
					</div>
				</div>
				
				<?php //get_template_part('parts/flexible-content'); ?>
				<div class="flexible-content videos-middle-section" itemprop="articleBody">
					<?php
					$args = array(
							'post_type'=> 'videos',
							'orderby'=>'date',
							'order'=>'DESC',
							'posts_per_page'=>20,
						);
					query_posts( $args );
					if(have_posts()):
					?>
					<div class="carousel_heading recipe-filter">
						<h2><?php _e('Latest Videos','ak-label'); ?></h2>
					</div>
					
					<div class="owl-carousel owl-theme" data-items="4">
					<?php
						while(have_posts()): the_post();							
						?>
						<div class="item">
								<?php if (has_post_thumbnail()): ?>
								<a href="<?php the_permalink(); ?>">
									<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-carousel')[0]; ?>">
								</a>
								<?php endif; ?>
								<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								<p class="viewlink"><a href="<?php the_permalink(); ?>"><?php _e('View','ak-label'); ?></a></p>						
							</div>
						<?php
						endwhile;					
						?>
					</div>	
					<?php
					$latest_page_link = get_field('latest_video_page','option') ? get_permalink(get_field('latest_video_page','option')) : home_url().'/latest-videos'; ?>
					<div class="video-more"><a href="<?php echo $latest_page_link; ?>"><?php _e('View More','ak-label'); ?></a></div>
					<?php
					endif;
					wp_reset_query();
					?>
					
					 
				
				<?php
				wp_reset_query();
				$feature_category = array();
				$videos_categories = get_terms( 'video-category', array('meta_key'=>'featured_category','meta_value'=>1,'number'=>5,'hide_empty' => true,));
				
				if($videos_categories):
				foreach($videos_categories as $vCat):
				array_push($feature_category,$vCat->term_id);
				?>				
					<div class="carousel_heading recipe-filter">
						<h2><?php echo $vCat->name.' Videos'; ?></a></h2>
					</div>
					<div class="owl-carousel owl-theme" data-items="4">
					<?php
					$args = array(
							'post_type'=> 'videos',
							'tax_query' => array(
												array(
													'taxonomy' => 'video-category',
													'field'    => 'slug',
													'terms'    => $vCat->slug,
												),
											),
						);
					query_posts( $args );
					if(have_posts()):
						while(have_posts()): the_post();							
						?>
						<div class="item">
								<?php if (has_post_thumbnail()): ?>
								<a href="<?php the_permalink(); ?>">
									<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-carousel')[0]; ?>">
								</a>
								<?php endif; ?>
								<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								<p class="viewlink"><a href="<?php the_permalink(); ?>"><?php _e('View','ak-label'); ?></a></p>						
							</div>
						<?php
						endwhile;
					endif;
					?>
					
					
					</div>
					<div class="video-more"><a href="<?php echo get_term_link( $vCat ); ?>">View More</a></div>				
		
				<?php				
				endforeach;
				endif;
				
				$videos_other_categories = get_terms( 'video-category', array('exclude'=>$feature_category,'hide_empty' => false,));
				if($videos_other_categories):
				?>
				<div class="carousel_heading recipe-filter">
						<h2>Other Categories</h2>
				</div>
				<div class="buttons other-videos-cats">
                <ul>
					<?php
					foreach($videos_other_categories as $vOthCat):
					?>
                    <li>
						<a href="<?php echo get_term_link( $vOthCat ); ?>"><?php echo $vOthCat->name; ?></a>
                    </li>                       
					<?php
					endforeach;
					?>
                     </ul>
					</div>
				<?php
				endif;
				?>
				</div>
			</div>
		</div>
	</div>
</section>

<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function(){
	$(".videos-main .videos-middle-section").each(function () {
  $(".owl-carousel").owlCarousel({
                margin: 20,
				dots:false,
				nav:true,
				loop:true,
				autoplay: true,
				responsiveClass: true,
				autoplaySpeed: 1000,				
				responsive: {
                    0: {
                        items: 1
                    },
                    500: {
                        items: 2
                    },
                    768: {
                        items: 3
					},
					 992: {
                        items: 4
					}
				}
  });
});

});

</script>
<?php get_footer(); ?>
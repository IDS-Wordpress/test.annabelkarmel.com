<!doctype html>
<html <?php language_attributes();  ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">		
		<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<link href="<?php echo site_url('/favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon">
		<link href="http://gmpg.org/xfn/11" rel="profile">
		<link href="<?php bloginfo('pingback_url'); ?>" rel="pingback">
		<?php wp_head(); ?>
	</head>
	<?php if (is_singular('products') && has_term('frozen-meals', 'product-category')): ?>
	<body <?php body_class('frozen-meals'); ?>>
	<?php else: ?>
	<body <?php body_class(); ?>>
	<?php endif; ?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1104764539603478";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('page_skin_adzone') ?: 12603), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
		<aside id="responsive-menu">
			<form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
				<input class="search-field" name="s" placeholder="Search our website..." type="search">
			</form>
			<div class="top-bar">
				<a href="<?php echo get_post_type_archive_link('competitions'); ?>">Competitions</a>
				<a class="icon-heart" href="<?php echo site_url('/your-saved-content/'); ?>">Your Saved</a>
				<?php if (is_user_logged_in()): ?>
				<a href="<?php echo site_url('/logout/'); ?>">Logout</a>
				<?php else: ?>
				<a href="#login">Login / Register</a>
				<?php endif; ?>
			</div>
			<?php if (false === ($header_menu = get_transient('ak_header_menu_' . ICL_LANGUAGE_CODE))) set_transient('ak_header_menu_' . ICL_LANGUAGE_CODE, ($header_menu = wp_nav_menu([
				'container' => '',
				'echo' => false,
				'theme_location' => 'header-menu'
			])), DAY_IN_SECONDS); ?>
			<?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
		</aside>
		<div class="wrapper">
			<div class="header-container">
				<header id="header">
					<div class="top-bar skid">
						<div class="container">
							<?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
								'container' => '',
								'echo' => false,
								'theme_location' => 'top-bar-menu'
							])); ?>
							<form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
								<input class="search-field" name="s" placeholder="Search our website..." type="search">
							</form>
						</div>
					</div>
					<div class="container">
						<?php the_custom_logo(); ?>
						<?php if (1 == get_current_user_id()): ?>
						<?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
							'container' => '',
							'echo' => false,
							'theme_location' => 'header-menu'
						])); ?>
						<?php else: ?>
						<?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
						<?php endif; ?>
						<div id="responsive-menu-icon">
							<button class="lines-button" type="button">
								<span class="lines"></span>
							</button>
			    		</div>
					</div>
				</header>
			</div>
			<div id="outer-content">
				<?php if (!is_user_logged_in() && ((is_string($enable_in = get_field('subscribe_banner_enable_in', 'option')) && 'header' == $enable_in) || (is_array($enable_in) && in_array('header', $enable_in)))): ?>
				<div class="subscribe skid">
					<div class="container">
						<a href="<?php echo site_url('/register/'); ?>">
							<img alt="Join the AK Club" src="<?php echo get_template_directory_uri(); ?>/images/join-the-ak-club.png">
						</a>
					</div>
				</div>
				<?php endif; ?>
<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php echo get_post_type_object('competitions')->labels->name; ?></h1>
				<?php if (get_field('competitions_description', 'option')): ?>
				<div class="flexible-content">
					<?php the_field('competitions_description', 'option'); ?>
				</div>
				<?php endif; ?>
				<?php if (have_posts()): ?>
				<div class="competitions-widgets">
					<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="competitions" repeater="template_1" posts_per_page="6" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
				</div>
				<?php endif; ?>
				
				<?php $the_query = new WP_Query([
					'orderby' => 'rand',
					'post_type' => 'products',
					'posts_per_page' => 6
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>Products You'll Love</h2>
					<div class="clear"></div>
					<div class="ak-post-widgets owl-carousel owl-theme">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
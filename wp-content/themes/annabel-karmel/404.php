<?php get_header(); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="http-error">
					<div class="container">
						<h1>
							4<img alt="" src="<?php echo get_template_directory_uri(); ?>/images/404-apple.png" />4
						</h1>
						<h2>PAGE NOT FOUND</h2>
						<h3>Sorry, but the delicious recipe or article you were looking for doesn't exist.</h3>
						Please click <a href="<?php echo site_url('/'); ?>">here</a> to go back to the homepage.
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
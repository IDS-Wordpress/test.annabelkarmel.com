<?php if ('straight' == get_sub_field('type')): ?>
<div class="wysiwyg-content">
	<hr style="border-top-color: <?php the_sub_field('colour'); ?>; border-top-style: <?php the_sub_field('style'); ?>; border-top-width: <?php the_sub_field('width'); ?>px;">
</div>
<?php endif; ?>
<?php
require '../wp-load.php';
/*https://graph.facebook.com/oauth/access_token?client_id=260997754864&client_secret=9612f63443dfc77568efeea82f07cd82&grant_type=client_credentials
*/

$fb = json_decode(file_get_contents('https://graph.facebook.com/annabelkarmeluk/posts?access_token=1104764539603478|XrNiA8LTqTK3WffTJhnBIHK24vY'));
if(!empty($fb->data)){
	foreach ($fb->data as $post) {
		$the_query = new WP_Query([
			'meta_key' => 'id_str',
			'meta_value' => $post->id,
			'post_status' => 'publish',
			'post_type' => 'fb-posts',
			'posts_per_page' => -1
		]);
		
		if (!$the_query->have_posts()) {
			$id = wp_insert_post([
				'post_title' => $post->id,
				'post_content' => $post->message,
				'post_date' => date('Y-m-d H:i:s', strtotime($post->created_time)),
				'post_status' => 'publish',
				'post_type' => 'fb-posts'
			]);
			
			add_post_meta($id, 'id_str', $post->id);
		}
	}
	wp_reset_postdata();
}
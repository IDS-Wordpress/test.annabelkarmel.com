<?php
/**
 * Template Name: No Sidebar
 */
get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="singular-wrapper">
					<div class="page-header" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting">
						<meta content="<?php echo the_time('Y-m-d'); ?>" itemprop="datePublished">
						<meta content="<?php the_permalink(); ?>" itemprop="url">
						<h1 itemprop="name"><?php the_title(); ?></h1>
						<?php get_template_part('parts/save-share'); ?>
						<?php if (has_post_thumbnail()): ?>
						<?php the_post_thumbnail('original', [
							'itemprop' => 'image thumbnailUrl'
						]); ?>
						<?php endif; ?>
					</div>
					<?php if (get_the_content()): ?>
						<div class="flexible-content" itemprop="articleBody">			
							<div class="wysiwyg-content wysin-content ">
								<?php the_content(); ?>
							</div>
						</div>
					<div class="clear"></div>
					<?php endif; ?>
					<?php get_template_part('parts/flexible-content'); ?>
					<div class="page-footer">
						<?php get_template_part('parts/save-share'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>
<?php
require '../wp-load.php';
require ABSPATH . '/vendor/autoload.php';

$twitter = new TwitterAPIExchange([
	'consumer_key' => 'eV8zIs3np8oQqjsCjEUBvpDlN',
	'consumer_secret' => 'gSs0iQxSlVD342SsA7xKyzanzOxPFfbVI8ya2FfzX6GXc8eNio',
	'oauth_access_token' => '392817221-LSIseM2BKo5K7w5TXilc9rR2cbncPTp1CsaOC7Vr',
	'oauth_access_token_secret' => 'DKmO4D5eU5QOHzmVe4nn5HGB6KsGdZ1KuGPlqdLeGCU0r'
]);
$the_query = new WP_Query([
	'post_status' => 'publish',
	'post_type' => 'tweet',
	'posts_per_page' => 1
]);
$since = '';
if ($the_query->have_posts()) {
	$the_query->the_post();
	$since = '&since_id=' . get_field('id_str');
}
$tweets = json_decode($twitter->setGetField('?exclude_replies=1&include_rts=1&include_entities=1&screen_name=annabelkarmel' . $since)->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')->performRequest());
//print_r($tweets); die;

foreach ($tweets as $tweet) {
	$the_query = new WP_Query([
		'meta_key' => 'id_str',
		'meta_value' => $tweet->id_str,
		'post_status' => 'publish',
		'post_type' => 'tweet',
		'posts_per_page' => -1
	]);
	if (!$the_query->have_posts()) {
		$text = $tweet->text;
		foreach ($tweet->entities->hashtags as $hashtag) {
			$text = str_ireplace('#' . $hashtag->text, '<a href="https://twitter.com/search/%23' . $hashtag->text . '" target="_blank">#' . $hashtag->text . '</a>', $text);
		}
		foreach ($tweet->entities->urls as $url) {
			$text = str_ireplace($url->url, '<a href="' . $url->url . '" target="_blank">' . $url->url . '</a>', $text);
		}
		foreach ($tweet->entities->user_mentions as $user_mention) {
			$text = str_ireplace('@' . $user_mention->screen_name, '<a href="https://twitter.com/' . $user_mention->screen_name . '" target="_blank">@' . $user_mention->screen_name . '</a>', $text);
		}
		
		$id = wp_insert_post([
			'post_title' => $tweet->id_str,
			'post_content' => $text,
			'post_date' => date('Y-m-d H:i:s', strtotime($tweet->created_at)),
			'post_status' => 'publish',
			'post_type' => 'tweet'
		]);
		add_post_meta($id, 'id_str', $tweet->id_str);
		add_post_meta($id, 'url', 'https://twitter.com/tosseduae/status/' . $tweet->id_str);
	}
}
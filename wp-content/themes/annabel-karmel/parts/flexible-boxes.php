<?php 

$isLoginRequired = 0;

if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }

?>

<?php if ('type_2' == ($parent_type = get_sub_field('type'))): ?>

<div class="recipe-categories <?php if($isLoginRequired) { echo 'loginRequired'; }?>" data-num-per-row="<?php echo (count(get_sub_field('boxes')) % 4	? 3	: 4); ?>">

<?php else: ?>

<?php
if (is_page('global')) { ?>
<div class="boxes boxesnew <?php if($isLoginRequired) { echo 'loginRequired'; }?>" data-num-per-row="<?php echo (count(get_sub_field('boxes')) % 4 	? 3 : 4); ?>">
<?php
}
else { 
?>
<div class="boxes <?php if($isLoginRequired) { echo 'loginRequired'; }?>" data-num-per-row="<?php echo (count(get_sub_field('boxes')) % 4 	? 3 : 4); ?>">
<?php } ?>
<?php endif; ?>



<?php if($isLoginRequired) { ?>

<div class="child_bg" >

	<div class="child_overlay">

    <a href="#login">Please login to access content</a>

    </div>

</div>

<?php

}

?>



	<?php while (have_rows('boxes')): the_row(); ?>

	<?php if ('type_2' != $parent_type): ?>

	<div class="box box-global">

	<?php endif; ?>

		<?php if ('existing' == ($type = get_sub_field('type'))): ?>

		<a href="<?php echo get_permalink($link = get_sub_field('link')); ?>">

			<?php if ('type_2' != $parent_type): ?>

			<h3><?php echo ($title = get_the_title($link)); ?></h3>

			<?php endif; ?>

			<?php if (has_post_thumbnail($link)): ?>

			<div class="box-image">

				<img alt="<?php echo esc_attr($title); ?>" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($link), ('type_2' != $parent_type

					? 'boxes'

					: 'recipe-category-mediums'

				))[0]; ?>" title="<?php echo esc_attr($title); ?>">

			</div>
            <div class="excerpt">
                      <?php the_sub_field('excerpt'); ?>
                      
                      </div>
			<?php endif; ?>

			<?php if ('type_2' == $parent_type): ?>

			<h4 class="skid">

				<span><?php echo ($title = get_the_title($link)); ?></span>

			</h4>

			<?php endif; ?>

			<?php if ('type_2' != $parent_type && ($excerpt = apply_filters('the_excerpt', get_post_field('post_excerpt', $link)))): ?>

			<div class="excerpt">

				<?php echo $excerpt; ?>

			</div>

			<?php endif; ?>

		</a>

		<?php else: ?>

		<a<?php if ($link = get_sub_field('other_link')) echo ' href="' . $link . '"'; ?>>

			<?php if ('type_2' != $parent_type): ?>

				
            <h4 class="skid">

				<span><?php the_sub_field('title'); ?></span>

			</h4>
			<?php endif; ?>

			<?php $image = get_sub_field('image'); ?>

			<div class="box-image">

				<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['type_2' != $parent_type

					? 'boxes'

					: 'recipe-category-mediums'

				]; ?>" title="<?php echo esc_attr($image['title']); ?>">

			</div>

			<?php if ('type_2' == $parent_type): ?>

			<h4 class="skid">

				<span><?php the_sub_field('title'); ?></span>

			</h4>

			<?php endif; ?>

			<?php if ('type_2' != $parent_type && ($excerpt = get_sub_field('excerpt'))): ?>

			<div class="excerpt">

				<?php echo $excerpt; ?>

			</div>

			<?php endif; ?>

		</a>

		<?php endif; ?>

	<?php if ('type_2' != $parent_type): ?>

	</div>

	<?php endif; ?>

	<?php endwhile; ?>

</div>




<?php

namespace Verb;

if (class_exists('\acf_field')) {
	class acf_field_word_limit extends \acf_field {
		public function __construct() {
			$this->category = 'basic';
			$this->label = 'Word Limit';
			$this->name = 'word_limit';
			
			add_action('acf/validate_value/type=word_limit', [
				$this,
				'validate_value'
			], 10, 3);
			
			parent::__construct();
		}
		
		public function input_admin_enqueue_scripts() {
			wp_enqueue_style('acf-word-limit', plugin_dir_url(__FILE__) . 'css/acf-word-limit.css', [], '4.5.2.1');
			
			wp_enqueue_script('acf-word-limit', plugin_dir_url(__FILE__) . 'js/acf-word-limit.js', [
				'jquery'
			], '4.5.2.2');
		}
		
		public function render_field($field) {
			echo '<textarea class="' . $field['class'] . '" id="' . $field['id'] . '"' . (isset($field['word_limit']) && 0 < ($word_limit = (int) $field['word_limit'])
				? ' data-word-limit="' . $word_limit .'"'
				: ''
			) . ' name="' . $field['name'] . '" rows="4">' . $field['value'] . '</textarea><span' . ($word_limit < ($word_count = count(array_filter(explode(' ', preg_replace('/\s+/', ' ', $field['value'])))))
				? ' class="over"'
				: ''
			) . '>' . $word_count . ' word' . (1 == $word_count
				? ''
				: 's'
			) . '</span>';
		}
		
		public function render_field_settings($field) {
			acf_render_field_setting($field, [
				'label' => 'Word Limit',
				'name' => 'word_limit',
				'type' => 'number'
			]);
		}
		
		public function validate_value($valid, $value, $field) {
			if (isset($field['word_limit']) && 0 < ($word_limit = (int) $field['word_limit']) && $word_limit < ($word_count = count(array_filter(explode(' ', preg_replace('/\s+/', ' ', $value)))))) {
				$valid = 'Submitted value (' . $word_count . ' word' . (1 < $word_count
					? 's'
					: ''
				) . ') exceeds limit of ' . $word_limit . ' word' . (1 < $word_limit
					? 's'
					: ''
				) . '.';
			}
			
			return $valid;
		}
	}
	
	$GLOBALS['acf_field_word_limit'] = new acf_field_word_limit;
}
<?php if ($banner_image = get_sub_field('banner_image')): ?>
<div class="hero-banner-image">
<?php if ($banner_link = get_sub_field('banner_link')): ?>
	<a href="<?php echo $banner_link; ?>" target="_blank"><img alt="" src="<?php echo $banner_image; ?>"></a>
<?php else: ?>
	<img alt="" src="<?php echo $banner_image; ?>">
<?php endif; ?>
</div>
<?php endif; ?>
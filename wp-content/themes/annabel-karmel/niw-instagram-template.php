<?php 
/*
 * Form can be overwritten by placing file with same name in active theme file
 */
?>

<?php if ($niw_instagram_images->have_posts()): ?>
	<section class="niw-instagram">
		<div class="niw-instagram-wrapper">
			<?php while ($niw_instagram_images->have_posts()): $niw_instagram_images->the_post(); ?>

				<?php
					if(get_option('niw_instagram_resolution', '0') == '1') {
						$instagram_image_url = get_post_meta(get_the_id(), 'image_large', true);
					} else {
						$instagram_image_url = get_post_meta(get_the_id(), 'image', true);
					}
				?>
				<a href="<?php echo get_post_meta(get_the_id(), 'link', true); ?>" target="_blank">
					<img alt="" src="<?php echo $instagram_image_url; ?>">
				</a>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>
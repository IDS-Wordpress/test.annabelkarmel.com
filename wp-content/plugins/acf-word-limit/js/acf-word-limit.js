if (window.jQuery) {
	jQuery(function($) {
		var textareas = $('textarea[data-word-limit]');
		textareas.each(function() {
			$(this).keydown(function(e) {
				// First check limits
				if ((word_limit = (that = $(this)).attr('data-word-limit')) <= (word_count = that.val().replace(/\s+/g, ' ').split(' ').filter(function(obj) {
					return obj;
				}).length) && /^\s$/.test(String.fromCharCode(e.which))) {
					e.preventDefault();
					
					return false;
				}
			}).keyup(function() {
				(span = $('+ span', (that = $(this)))).html((word_count = that.val().replace(/\s+/g, ' ').split(' ').filter(function(obj) {
					return obj;
				}).length) + ' word' + (1 == word_count
					? ''
					: 's'
				));
				
				if (that.attr('data-word-limit') < word_count) {
					span.addClass('over');
				} else {
					span.removeClass('over');
				}
			});
		});
	});
}
<div class="post-widget video-widget" data-post-type="<?php echo esc_attr(get_post_type()); ?>">
	<button type="button" class="mfp-close" data-post-id="<?php echo get_the_ID(); ?>">×</button>
	<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
		<div class="post-widget-header">
			<h4><?php echo (get_field('short_title') ?: get_the_title()); ?></h4>
            <span><?php echo ($top_cat = $GLOBALS['Annabel_Karmel']->top_cat($GLOBALS['post'])); ?></span>
		</div>	
		<div class="post-widget-thumbnail">
        	<?php the_post_thumbnail('video-widget'); ?>
    	</div>
	</a>
	<?php if (($bottom_cat = $GLOBALS['Annabel_Karmel']->bottom_cat($GLOBALS['post'])) && $bottom_cat != $top_cat): ?>
	<div class="bottom-cat">
		<?php echo $bottom_cat; ?>
	</div>
	<?php endif; ?>
	<?php if (!in_array(get_post_type(), [
		'apps-books',
		'products',
		'recipes'
	]) && get_the_excerpt()): ?>
	<a class="excerpt-link" href="<?php the_permalink(); ?>">
		<div class="excerpt">
			<?php the_excerpt(); ?>
		</div>
	</a>
	<?php endif; ?>
	<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
		<a>SAVE</a>
	</div>
</div>
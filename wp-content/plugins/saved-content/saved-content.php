<?php
/**
 * Plugin Name: AK Saved Content
 * Description: Allows logged-in users to save content in collections.
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

namespace Verb;
	
class AK_Saved_Content {
	public function __construct() {
		// Register (de)activation hooks
		register_activation_hook(__FILE__, [
			$this,
			'activate'
		]);
		register_deactivation_hook(__FILE__, [
			$this,
			'deactivate'
		]);
		
		// Register WP hooks and filters
		add_filter('rewrite_rules_array', [
			$this,
			'rewrite_rules_array'
		]);
		add_filter('query_vars', [
			$this,
			'query_vars'
		], 0);
		add_action('parse_request', [
			$this,
			'parse_request'
		]);
	}
	
	public function activate() {
		// Recipes
		add_rewrite_rule('^create-collection/?$', 'index.php?__save_f=create_collection', 'top');
		add_rewrite_rule('^delete-collection/?$', 'index.php?__save_f=delete_collection', 'top');
		add_rewrite_rule('^list-collections/?$', 'index.php?__save_f=list_collections', 'top');
		add_rewrite_rule('^save/?$', 'index.php?__save_f=save', 'top');
		add_rewrite_rule('^unsave/?$', 'index.php?__save_f=unsave', 'top');
		
		// Flush
		flush_rewrite_rules();
	}
	
	public function create_collection($name, $checked = false) {
		if (!$checked && !is_user_logged_in()) {
			return false;
		}
		
		if (!($collections = get_user_meta(get_current_user_id(), '_collections', true))) {
			$collections = [];
		}
		
		$collections[] = [
			'title' => $name,
			'content' => []
		];
		
		update_user_meta(get_current_user_id(), '_collections', $collections);
		
		return $collections;
	}
	
	public function deactivate() {
		flush_rewrite_rules();
	}
	
	public function delete_collection($collection_id, $checked = false) {
		if (!$checked && (!is_user_logged_in() || !($collections = get_user_meta(get_current_user_id(), '_collections', true)) || !array_key_exists((int) $collection_id, $collections))) {
			return false;
		}
		
		unset($collections[$collection_id]);
		
		update_user_meta(get_current_user_id(), '_collections', $collections);
		
		return $collections;
	}
	
	public function edit_collection($collection_id, $new_name, $checked = false) {
		if (!$checked && (!is_user_logged_in() || !($collections = get_user_meta(get_current_user_id(), '_collections', true)) || !array_key_exists((int) $collection_id, $collections))) {
			return false;
		}
		
		$collections[$collection_id]['title'] = $new_name;
		
		update_user_meta(get_current_user_id(), '_collections', $collections);
		
		return $collections;
	}
	
	public function list_collections($user_id = 0) {
		if (!$user_id) {
			$user_id = get_current_user_id();
		}
		
		if (!$user_id) {
			return false;
		}
		
		if (!($collections = get_user_meta(get_current_user_id(), '_collections', true))) {
			return [];
		}
		
		return $collections;
	}
	
	public function rewrite_rules_array($rules) {
		return [
			'^create-collection/?$' => 'index.php?__save_f=create_collection',
			'^delete-collection/?$' => 'index.php?__save_f=delete_collection',
			'^edit-collection/?$' => 'index.php?__save_f=edit_collection',
			'^list-collections/?$' => 'index.php?__save_f=list_collections',
			'^save/?$' => 'index.php?__save_f=save',
			'^unsave/?$' => 'index.php?__save_f=unsave'
		] + $rules;
	}
	
	public function query_vars($query_vars) {
		return array_merge($query_vars, [
			'__collection_id',
			'__save_f',
			'__save_id'
		]);
	}
	
	public function parse_request() {
		global $wp;
		
		if (isset($wp->query_vars['__save_f'])) {
			$this->__handle_request($wp->query_vars['__save_f']);
		}
	}
	
	public function save($collection_id, $post_id, $checked = false) {
		if (!$checked && (!is_user_logged_in() || !($collections = get_user_meta(get_current_user_id(), '_collections', true)) || !array_key_exists($collection_id, $collections) || !get_post($post_id) || in_array($post_id, $collections[$collection_id]['content']))) {
			return false;
		}
		
		$collections[$collection_id]['content'][] = $post_id;
		
		update_user_meta(get_current_user_id(), '_collections', $collections);
		
		return true;
	}
	
	public function unsave($post_id, $collection_id = false, $checked = false) {
		if (!$checked && (!is_user_logged_in() || !($collections = get_user_meta(get_current_user_id(), '_collections', true)) || (false !== $collection_id && !array_key_exists($collection_id, $collections)) || (false !== $collection_id && false !== ($key = array_search($post_id, $collections[$collection_id]['content']))))) {
			return false;
		}
		
		if (false !== $collection_id) {
			unset($collections[$collection_id]['content'][$key]);
		} else {
			foreach ($collections as $collection_id => $collection) {
				if (false !== ($key = array_search($post_id, $collection['content']))) {
					unset($collections[$collection_id]['content'][$key]);
				}
			}
		}
		
		update_user_meta(get_current_user_id(), '_collections', $collections);
		
		return true;
	}
	
	protected function __create_collection() {
		if ('POST' !== $_SERVER['REQUEST_METHOD']) {
			wp_die('Only data presented via POST is allowed.', 'Method Not Allowed', [
				'response' => 405
			]);
		}
		
		if (!isset($_POST['collection_name'])) {
			wp_die('Collection name required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		header('Content-Type: text/json');
		
		echo json_encode($this->create_collection($_POST['collection_name']), JSON_FORCE_OBJECT);
		
		exit;
	}
	
	protected function __delete_collection() {
		if ('POST' !== $_SERVER['REQUEST_METHOD']) {
			wp_die('Only data presented via POST is allowed.', 'Method Not Allowed', [
				'response' => 405
			]);
		}
		
		if (!isset($_POST['collection_id'])) {
			wp_die('Collection ID required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		header('Content-Type: text/json');
		
		echo json_encode($this->delete_collection($_POST['collection_id']), JSON_FORCE_OBJECT);
		
		exit;
	}
	
	protected function __edit_collection() {
		if ('POST' !== $_SERVER['REQUEST_METHOD']) {
			wp_die('Only data presented via POST is allowed.', 'Method Not Allowed', [
				'response' => 405
			]);
		}
		
		if (!isset($_POST['collection_id'])) {
			wp_die('Collection ID required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		if (!isset($_POST['collection_name'])) {
			wp_die('Collection name required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		header('Content-Type: text/json');
		
		echo json_encode($this->edit_collection($_POST['collection_id'], $_POST['collection_name']), JSON_FORCE_OBJECT);
		
		exit;
	}
	
	protected function __handle_request($f = null) {
		if (method_exists($this, '__' . $f)) {
			call_user_func([
				$this,
				'__' . $f
			]);
		} else {
			wp_die('The capability to handle your request has not been implemented yet &mdash; watch this space...', 'Not Implemented', [
				'response' => 501
			]);
		}
	}
	
	protected function __list_collections() {
		header('Content-Type: text/json');
		
		echo json_encode($this->list_collections(), JSON_FORCE_OBJECT);
		
		exit;
	}
	
	protected function __save() {
		if ('POST' !== $_SERVER['REQUEST_METHOD']) {
			wp_die('Only data presented via POST is allowed.', 'Method Not Allowed', [
				'response' => 405
			]);
		}
		
		if (!isset($_POST['collection_id'])) {
			wp_die('Collection ID required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		if (!isset($_POST['save_id'])) {
			wp_die('Post ID required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		$this->save($_POST['collection_id'], $_POST['save_id']);
		
		wp_redirect(site_url('/your-saved/'), 302);
		
		exit;
	}
	
	protected function __unsave() {		
		if (!isset($_REQUEST['save_id'])) {
			wp_die('Post ID required.', 'Bad Request', [
				'response' => 400
			]);
		}
		
		$this->unsave($_REQUEST['save_id']);
		
		wp_redirect(site_url('/your-saved/'), 302);
		
		exit;
	}
}

$GLOBALS['AK_Saved_Content'] = new AK_Saved_Content;
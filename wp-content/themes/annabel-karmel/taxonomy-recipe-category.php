<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php echo get_queried_object()->name; ?> <?php echo get_post_type_object('recipes')->labels->name; ?></h1>
				<?php if ($description = category_description()): ?>
				<div class="flexible-content">
					<?php echo $description; ?>
				</div>
				<?php endif; ?>
				<?php if (!isset($_POST['rf'])): ?>
				<div class="recipe-categories-search table">
					<div class="cell">
						<span>Search recipes by keyword...</span>
						<form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
							<input class="search-field" name="s" placeholder="Enter search..." type="search"><input type="submit" value="SEARCH">
							<input name="s_pt" type="hidden" value="recipes">
						</form>
						<a href="#recipe-filter">
							Or filter to find your ideal recipe<i class="fa fa-angle-down"></i>
						</a>
					</div>
				</div>
				<?php if (have_posts()): ?>
				<h2>Popular <?php echo get_post_type_object('recipes')->labels->name; ?></h2>
				<div class="ak-post-widgets owl-carousel owl-theme" data-items="3">
					<?php $the_query = new WP_Query([
						'meta_key' => 'force_popular',
						'meta_value' => 1,
						'post_type' => 'recipes',
						'posts_per_page' => 6,
						'tax_query' => [
							[
								'taxonomy' => 'recipe-category',
								'terms' => get_queried_object()->term_id
							]
						]
					]); ?>
					<?php if ($the_query->have_posts()): ?>
					<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
					<div class="item">
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					</div>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php $counter = 6; ?>
					<?php while (have_posts()): the_post(); ?>
					<?php if ($counter-- > $the_query->post_count): ?>
					<div class="item">
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					</div>
					<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<?php get_template_part('parts/recipe-finder'); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<?php
require '../wp-load.php';

/*http://instagram.pixelunion.net/ */

$result = json_decode(file_get_contents('https://api.instagram.com/v1/users/212133815/media/recent/?access_token=212133815.1677ed0.2a15cb9d096f4bb8802ee6e49659c161'));
if(!empty($result->data)){
	foreach ($result->data as $post) {
		$the_query = new WP_Query([
			'meta_key' => 'id_str',
			'meta_value' => $post->id,
			'post_status' => 'publish',
			'post_type' => 'ig-posts',
			'posts_per_page' => -1
		]);			
		if (!$the_query->have_posts()) {
			$save_args = [
				'post_title' => $post->id,
				'post_content' => $post->caption->text,
				'post_date' => date('Y-m-d H:i:s', $post->created_time),
				'post_status' => 'publish',
				'post_type' => 'ig-posts'
			];
			$id = wp_insert_post($save_args);
			add_post_meta($id, 'id_str', $post->id);
					 $image_url        = $post->images->low_resolution->url;
					$mainurl=explode('?', $image_url);

					 $image_name       = basename($mainurl[0]); 
					$upload_dir       = wp_upload_dir(); // Set upload folder
					$image_data       = file_get_contents($image_url); // Get image data
					$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
					$filename         = basename( $unique_file_name ); // Create image file name

					// Check folder permission and define file location
					if( wp_mkdir_p( $upload_dir['path'] ) ) {
					$file = $upload_dir['path'] . '/' . $filename;
					} else {
					$file = $upload_dir['basedir'] . '/' . $filename;
					}

					// Create the image  file on the server
					file_put_contents( $file, $image_data );

					// Check image file type
					$wp_filetype = wp_check_filetype( $filename, null );

					// Set attachment data
					$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_title'     => sanitize_file_name( $filename ),
					'post_content'   => '',
					'post_status'    => 'inherit'
					);

					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
			add_post_meta($id, 'ig_image', $attach_id);
			add_post_meta($id, 'ig_link', $post->link);
			add_post_meta($id, 'ig_type', $post->type);			
		}
	}
	wp_reset_postdata();
}
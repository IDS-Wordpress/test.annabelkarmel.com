<?php
include('simple_html_dom.php');
class API_AUTH EXTENDS simple_html_dom
{
	public $auth_user;
	public $responseData;
	public function authorize($username,$password)
	{		
		if(isset($username) && isset($password))
		{
			$this->auth_user = wp_authenticate($username,$password);
			if(is_wp_error($this->auth_user))					
			{
				$this->responseData = array('status'=>'fail','response_code'=>401,'response_msg'=>'Unauthorised');
			}
		}
		else
		{
			$this->responseData = array('status'=>'fail','response_code'=>401,'response_msg'=>'Unauthorised');
		}		
		return $this->responseData;
	}
	
	public function getversionData($vType,$vNum)
	{
		global $wpdb;
		
		$vNum = substr($vNum,1);
		$vNum = (float)$vNum;
		
		$sqlRequested = "SELECT version,type,from_date from wp_api_version where type = '".$vType."' AND version='".$vNum."'";
		$rowR = $wpdb->get_row($sqlRequested,ARRAY_A);
		
		$sqlLast = "SELECT * FROM `".$wpdb->prefix."api_version` where type='".$vType."' ORDER BY id desc";
		$rowL = $wpdb->get_row($sqlLast,ARRAY_A);
		
		$arrayVersion = array();
		if(!empty($rowR) && !empty($rowL))
		{
			$arrayVersion['current_version'] = $vNum;
			$arrayVersion['from_date'] = $rowR['from_date'];
			$arrayVersion['last_version'] = $rowL['version'];
			$arrayVersion['to_date'] = $rowL['to_date'];			
			return $arrayVersion;
		}
		else
		{
			header('Content-Type: application/json');
			$arrResponse = array('status'=>'fail','response_code'=>802,'response_msg'=>'Content Version requested does not exist');
			header("HTTP/1.0 400 Bad Request");
			echo json_encode($arrResponse);
			die;
		}
	}

}
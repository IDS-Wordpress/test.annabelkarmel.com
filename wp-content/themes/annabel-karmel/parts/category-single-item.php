<div class="post-widget<?php if (get_field('sponsored_content')) echo ' sponsored'; ?>" data-post-type="<?php echo esc_attr(get_post_type()); ?>">
	<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
		<div class="post-widget-header">
			<div class="rec_tit">
				<h4><?php echo (get_field('short_title') ?: get_the_title()); ?></h4>
				
            <?php if( get_field('blog_post_title') ): ?>
				<h4><?php the_field('blog_post_title'); ?></h4>
            <?php endif; ?>
			
			<?php if( get_field('apps_&_books_title_2') ): ?>
				<h4><?php the_field('apps_&_books_title_2'); ?></h4>
            <?php endif; ?>
			
           <?php if( get_field('recipes_title_2') ): ?>
				<h4><?php the_field('recipes_title_2'); ?></h4>
            <?php endif; ?>
			
            <?php if( get_field('video_title_2') ): ?>
				<h4><?php the_field('video_title_2'); ?></h4>
            <?php endif; ?>
			
            <?php if( get_field('product_title_2') ): ?>
				<h4><?php the_field('product_title_2'); ?></h4>
            <?php endif; ?>
			</div>
			<?php if (is_array($top_cats = $GLOBALS['Annabel_Karmel']->top_cat($GLOBALS['post'], true))): $top_cat = $top_cats[0]->name; ?>
			<span><?php echo implode(' / ', array_map(function($item) {
				return $item->name;
			}, $top_cats)); ?></span>
			<?php elseif ($top_cats): ?>
			<span><?php echo $top_cats; ?></span>
			<?php endif; ?>
		</div>	
		<div class="post-widget-thumbnail">
			<?php the_post_thumbnail(in_array(get_post_type(), [
				'apps-books',
				'products'
			])
				? 'verb-ak-post-widget-padded'
				: 'verb-ak-post-widget'
			); ?>
		</div>
	</a>
	<?php if (($bottom_cat = (get_sub_field('tagline') ?: $GLOBALS['Annabel_Karmel']->bottom_cat($GLOBALS['post']))) && $bottom_cat != $top_cat): ?>
	<div class="bottom-cat">
		<?php echo $bottom_cat; ?>
	</div>
	<?php endif; ?>
	<?php if ((!in_array(get_post_type(), [
		'apps-books',
		'post',
		'products',
		'recipes'
	]) || $force_recipe_excerpt) && get_the_excerpt() && (isset($excerpt)
		? $excerpt
		: true
	)): ?>
	<a class="excerpt-link" href="<?php the_permalink(); ?>">
		<div class="excerpt">
			<?php the_excerpt(); ?>
		</div>
	</a>
	<?php endif; ?>
	<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
		<a>SAVE</a>
	</div>
</div>
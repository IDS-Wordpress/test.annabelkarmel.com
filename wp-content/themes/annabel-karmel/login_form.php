<div class="login_popup" id="login" autocomplete="off">
	<div class="existing-customers">
		<img class="login_left_logo" alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_left_logo.png">
		<div class="title">Please log in to view this recipe</div>
		<div id="msg"></div>
		
		
		
		<form name="pp_loginform" id="pp_loginform" action="<?php echo esc_url( wp_login_url() ); ?>" method="post">
			
			<p class="login-username login_form_group">
				<input type="text" name="log" id="pp_user_login" class="input" size="20" required>
				<label for="pp_user_login">Email</label>
			</p>
			<p class="login-password login_form_group">
				<input type="password" name="pwd" id="pp_user_pass" class="input" size="20" required>
				<label for="pp_user_pass">Password</label>
			</p>
			
			<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"> Remember Me</label></p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="pp_wp-submit" class="button button-primary" value="Log In">
				<input type="hidden" name="redirect_to" value="<?php echo get_permalink(); ?>">
			</p>
			
		</form>
		<?php/*  $GLOBALS['Annabel_Karmel']->wp_login_form([
			'form_id' => 'pp_loginform',
			'id_password' => 'pp_user_pass',
			'id_submit' => 'pp_wp-submit',
			'id_username' => 'pp_user_login',
			'remember' => true
		]);  */?>
		<a class="forget_text" href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a>
	</div>
	<div class="new-customers">
	<div class="login_right_img">
	<img alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_right_img.png">
	</div>
		<ul>
			<li class="book_i">Free e-recipe book on sign up</li>
			<li class="check_i">Access to exclusive recipes & content</li>
			<li class="sign_i">Exclusive offers on AK products</li>
			<li class="star_i">Great competitions and prizes</li>
			<li class="letter_i">Never miss a thing with our newsletter</li>
		</ul>
		<p>
			<a class="button" href="<?php echo site_url('/register/'); ?>">sign up</a>
		</p>
	</div>
</div>
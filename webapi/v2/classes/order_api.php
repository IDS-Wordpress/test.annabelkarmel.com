<?php
include('auth_api.php');
class API_ORDER extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	public function __construct($u,$p)
	{		
		$this->auth_username = $u;
		$this->auth_password = $p;
	}
	public function order_checker($data)
	{		
		$data['user_email'] = $this->auth_username;
		$data['user_password'] = $this->auth_password;
		if(!empty($data['user_email']))
		{
			if ( email_exists( $data['user_email']) )
			{
				$user_wp = wp_authenticate( $data['user_email'], $data['user_password'] );
				
				if(!is_wp_error( $user_wp ))
				{
					$ordered_cats = array();
					$user_order_cats = (!empty(get_user_meta($user_wp->ID,'user_order_cats',true)))?get_user_meta($user_wp->ID,'user_order_cats',true):array();
					foreach($user_order_cats as $catId)
					{
						$catdetails = array();
						$term = get_term( $catId, 'mobile-recipe-category' );
						$catdetails['cat_id']  = $catId;
						$catdetails['cat_name']  = $term->name;
						array_push($ordered_cats,$catdetails);
					}
					$this->resArr =	$ordered_cats;
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid Password');
					header("HTTP/1.0 401 Unauthorized");
					print_r($this->resArr);
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'Invalid Email');
				header("HTTP/1.0 401 Unauthorized");
			}
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid email and password combination');
			header("HTTP/1.0 401 Unauthorized");
		}
		
		$this->response($this->resArr);
	}
	public function capture_order($data)
	{		
		$data['user_email'] = $this->auth_username;
		$data['user_password'] = $this->auth_password;
		if(!empty($data['user_email']))
		{
			if ( email_exists( $data['user_email']) )
			{
				$user_wp = wp_authenticate( $data['user_email'], $data['user_password'] );
				
				if(!is_wp_error( $user_wp ))
				{
					if(!empty($data['transaction_id']) && !empty($data['purchase_token']) && !empty($data['cat_id']))
					{
						if(metadata_exists('user',$user_wp->ID,'user_order') && metadata_exists('user',$user_wp->ID,'user_order_cats'))
						{
							$user_order = get_user_meta($user_wp->ID, 'user_order', true);
							if(!empty($user_order))
							{
								$user_order = maybe_unserialize($user_order);
								$transaction_id = $data['transaction_id'];
								$purchase_token = $data['purchase_token'];
								
								$purchasedCats = array();
								foreach($user_order as $uOrd)
								{
									$cats = $uOrd['cat_id'];
									if(!empty($cats))
									{
										foreach($cats as $catId)
										{
											$purchasedCats[] = $catId;
										}
									}
								}
								if(!in_array($data['cat_id'],$purchasedCats))
								{
									$orderedcat[] = $data['cat_id'];
									$user_order[] = array('transaction_date'=>date('d-m-Y H:i:s'),'purchase_token'=>$purchase_token,'transaction_id'=>$transaction_id,'cat_id'=>$orderedcat);
									update_user_meta( $user_wp->ID, 'user_order', $user_order );
								}								
							}
							$user_order_cats = get_user_meta($user_wp->ID,'user_order_cats',true);
							if(!empty($user_order) && !empty($user_order_cats))
							{
								$user_order_cats = maybe_unserialize($user_order_cats);						
								if(!in_array($data['cat_id'],$user_order_cats))
								{
									$user_order_cats[] = $data['cat_id'];
									update_user_meta( $user_wp->ID, 'user_order_cats', $user_order_cats );
								}
							}
							
						}
						else
						{
							
							$transaction_id = $data['transaction_id'];
							$purchase_token = $data['purchase_token'];
							$cats = array($data['cat_id']);
							
							$user_ordered_details = array(array('transaction_date'=>date('d-m-Y H:i:s'),'transaction_id'=>$transaction_id,'purchase_token'=>$purchase_token,'cat_id'=>$cats));
							add_user_meta( $user_wp->ID, 'user_order', $user_ordered_details);
							add_user_meta( $user_wp->ID, 'user_order_cats', $cats);
						}
						$this->resArr =	array('status'=>'success','response_code'=>200,'response_msg'=>'Order Captured successfully','ordered_cats'=>get_user_meta($user_wp->ID,'user_order_cats',true));
					}
					else
					{
						$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Data missing');
						header("HTTP/1.0 401 Unauthorized");
					}					
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid Password');
					header("HTTP/1.0 401 Unauthorized");
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'Invalid Email');
				header("HTTP/1.0 401 Unauthorized");
			}
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid email and password combination');
			header("HTTP/1.0 401 Unauthorized");
		}
		
		$this->response($this->resArr);
	}
	
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}
}
?>
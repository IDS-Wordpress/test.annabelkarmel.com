<?php
/**
 * Plugin Name: AK Post Types
 * Description: Setup Custom Post Types and Taxonomies
 * Version: 1.0
 * Author: Under2
 * Author URI: https://under2.agency/
 */

add_action('init', function () {
	// Advice
	register_post_type('advice', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Advice Post',
			'all_items' => 'All Advice Posts',
			'edit_item' => 'Edit Advice Post',
			'name' => 'Advice',
			'new_item' => 'New Advice Post',
			'not_found' => 'No advice posts found',
			'not_found_in_trash' => 'No advice posts found in Trash',
			'search_item' => 'Search Advice Posts',
			'singular_name' => 'Advice Post',
			'view_item' => 'View Advice Post'
		],
		'menu_icon' => 'dashicons-welcome-learn-more',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		],
		'taxonomies' => ['advice-category']
	]);

	register_taxonomy('advice-category', 'advice', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Advice Category',
			'all_itmes' => 'All Advice Categories',
			'edit_item' => 'Edit Advice Category',
			'name' => 'Advice Categories',
			'new_item_name' => 'New Advice Category Name',
			'parent_item' => 'Parent Advice Category',
			'parent_item_colon' => 'Parent Advice Category:',
			'search_items' => 'Search Advice Categories',
			'singular_name' => 'Advice Category',
			'view_item' => 'View Advice Category'
		]
	]);
	register_taxonomy_for_object_type('advice-category', 'advice');

	// Recipes
	register_post_type('recipes', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Recipe',
			'all_items' => 'All Recipes',
			'edit_item' => 'Edit Recipe',
			'name' => 'Recipes',
			'new_item' => 'New Recipe',
			'not_found' => 'No recipes found',
			'not_found_in_trash' => 'No recipes found in Trash',
			'search_item' => 'Search Recipes',
			'singular_name' => 'Recipe',
			'view_item' => 'View Recipe'
		],
		'menu_icon' => 'dashicons-carrot',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	register_taxonomy('recipe-category', 'recipes', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Category',
			'all_itmes' => 'All Categories',
			'edit_item' => 'Edit Category',
			'name' => 'Recipe Categories',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'search_items' => 'Search Categories',
			'singular_name' => 'Category',
			'view_item' => 'View Category'
		]
	]);

	register_taxonomy('mobile-recipe-category', 'recipes', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Category',
			'all_itmes' => 'All Categories',
			'edit_item' => 'Edit Category',
			'name' => 'Mobile Recipe Categories',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'search_items' => 'Search Categories',
			'singular_name' => 'Category',
			'view_item' => 'View Category'
		]
	]);
	register_taxonomy_for_object_type('recipe-category', 'recipes');

	register_taxonomy('recipe-tag', 'recipes', [
		'labels' => [
			'add_new_item' => 'Add New Tag',
			'all_itmes' => 'All Tags',
			'edit_item' => 'Edit Tag',
			'name' => 'Recipe Tags',
			'new_item_name' => 'New Tag Name',
			'search_items' => 'Search Tags',
			'singular_name' => 'Tag',
			'view_item' => 'View Tag'
		]
	]);
	register_taxonomy_for_object_type('recipe-tag', 'recipes');

	register_taxonomy('recipe-filter', 'recipes', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Filter',
			'all_itmes' => 'All Filters',
			'edit_item' => 'Edit Filter',
			'name' => 'Filters',
			'new_item_name' => 'New Filter Name',
			'parent_item' => 'Parent Filter',
			'parent_item_colon' => 'Parent Filter:',
			'search_items' => 'Search Filters',
			'singular_name' => 'Filter',
			'view_item' => 'View Filter'
		]
	]);
	register_taxonomy_for_object_type('recipe-filter', 'recipes');

	register_taxonomy('prod-allergen', 'products', [
		'labels' => [
			'add_new_item' => 'Add New Allergen / Dietary',
			'all_itmes' => 'All Allergens & Dietary',
			'edit_item' => 'Edit Allergen / Dietary',
			'name' => 'Allergens & Dietary',
			'new_item_name' => 'New Allergen / Dietary',
			'search_items' => 'Search Allergens & Dietary',
			'singular_name' => 'Allergen / Dietary',
			'view_item' => 'View Allergen / Dietary'
		],
		'meta_box_cb' => false
	]);
	register_taxonomy_for_object_type('prod-allergen', 'products');

	register_taxonomy('allergen', 'recipes', [
		'labels' => [
			'add_new_item' => 'Add New Allergen / Dietary Requirement',
			'all_itmes' => 'All Allergens & Dietary Requirements',
			'edit_item' => 'Edit Allergen / Dietary Requirement',
			'name' => 'Allergens & Dietary Requirements',
			'new_item_name' => 'New Allergen / Dietary Requirement',
			'search_items' => 'Search Allergens & Dietary Requirements',
			'singular_name' => 'Allergen / Dietary Requirement',
			'view_item' => 'View Allergen / Dietary Requirement'
		],
		'meta_box_cb' => false
	]);
	register_taxonomy_for_object_type('allergen', 'recipes');
	register_taxonomy_for_object_type('allergen', 'products');

	// Videos
	register_post_type('videos', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Video',
			'all_items' => 'All Videos',
			'edit_item' => 'Edit Video',
			'name' => 'Videos',
			'new_item' => 'New Video',
			'not_found' => 'No videos found',
			'not_found_in_trash' => 'No videos found in Trash',
			'search_item' => 'Search Videos',
			'singular_name' => 'Video',
			'view_item' => 'View Video'
		],
		'menu_icon' => 'dashicons-video-alt3',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	register_taxonomy('video-category', 'videos', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Category',
			'all_itmes' => 'All Categories',
			'edit_item' => 'Edit Category',
			'name' => 'Video Categories',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'search_items' => 'Search Categories',
			'singular_name' => 'Category',
			'view_item' => 'View Category'
		]
	]);
	register_taxonomy_for_object_type('video-category', 'videos');

	// Products
	register_post_type('products', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Product',
			'all_items' => 'All Products',
			'edit_item' => 'Edit Product',
			'name' => 'Products',
			'new_item' => 'New Product',
			'not_found' => 'No products found',
			'not_found_in_trash' => 'No products found in Trash',
			'search_item' => 'Search Products',
			'singular_name' => 'Product',
			'view_item' => 'View Product'
		],
		'menu_icon' => 'dashicons-products',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	register_taxonomy('product-category', 'products', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Category',
			'all_itmes' => 'All Categories',
			'edit_item' => 'Edit Category',
			'name' => 'Product Categories',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'search_items' => 'Search Categories',
			'singular_name' => 'Category',
			'view_item' => 'View Category'
		]
	]);
	register_taxonomy_for_object_type('product-category', 'products');

	register_taxonomy('product-tag', 'products', [
		'labels' => [
			'add_new_item' => 'Add New Tag',
			'all_itmes' => 'All Tags',
			'edit_item' => 'Edit Tag',
			'name' => 'Product Tags',
			'new_item_name' => 'New Tag Name',
			'search_items' => 'Search Tags',
			'singular_name' => 'Tag',
			'view_item' => 'View Tag'
		]
	]);
	register_taxonomy_for_object_type('product-tag', 'products');

	register_taxonomy('product-ingredient', 'products', [
		'hierarchical' => false,
		'labels' => [
			'add_new_item' => 'Add New Product Ingredient',
			'all_itmes' => 'All Product Ingredients',
			'edit_item' => 'Edit Product Ingredient',
			'name' => 'Product Ingredients',
			'new_item_name' => 'New Product Ingredient Name',
			'parent_item' => 'Parent Product Ingredient',
			'parent_item_colon' => 'Parent Product Ingredient:',
			'search_items' => 'Search Product Ingredients',
			'singular_name' => 'Product Ingredient',
			'view_item' => 'View Product Ingredient'
		]
	]);
	register_taxonomy_for_object_type('product-ingredient', 'products');

	// Apps & Books
	register_post_type('apps-books', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New App / Book',
			'all_items' => 'All Apps & Books',
			'edit_item' => 'Edit App / Book',
			'name' => 'Apps & Books',
			'new_item' => 'New App / Book',
			'not_found' => 'No apps / books found',
			'not_found_in_trash' => 'No apps / bounds found in Trash',
			'search_item' => 'Search Apps & Books',
			'singular_name' => 'App / Book',
			'view_item' => 'View App / Book'
		],
		'menu_icon' => 'dashicons-book-alt',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	register_taxonomy('app-book-category', 'apps-books', [
		'hierarchical' => true,
		'labels' => [
			'add_new_item' => 'Add New Category',
			'all_itmes' => 'All Categories',
			'edit_item' => 'Edit Category',
			'name' => 'App / Book Categories',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'search_items' => 'Search Categories',
			'singular_name' => 'Category',
			'view_item' => 'View Category'
		]
	]);
	register_taxonomy_for_object_type('app-book-category', 'apps-books');

	register_taxonomy('app-book-tag', 'apps-books', [
		'labels' => [
			'add_new_item' => 'Add New Tag',
			'all_itmes' => 'All Tags',
			'edit_item' => 'Edit Tag',
			'name' => 'App / Book Tags',
			'new_item_name' => 'New Tag Name',
			'search_items' => 'Search Tags',
			'singular_name' => 'Tag',
			'view_item' => 'View Tag'
		]
	]);
	register_taxonomy_for_object_type('app-book-tag', 'apps-books');

	// Competitions
	register_post_type('competitions', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Competition',
			'all_items' => 'All Competitions',
			'edit_item' => 'Edit Competition',
			'name' => 'Competitions',
			'new_item' => 'New Competition',
			'not_found' => 'No competitions found',
			'not_found_in_trash' => 'No competitions found in Trash',
			'search_item' => 'Search Competitions',
			'singular_name' => 'Competition',
			'view_item' => 'View Competition'
		],
		'menu_icon' => 'dashicons-awards',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	// Offers
	register_post_type('offers', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Offer',
			'all_items' => 'All Offers',
			'edit_item' => 'Edit Offer',
			'name' => 'Offers',
			'new_item' => 'New Offer',
			'not_found' => 'No offers found',
			'not_found_in_trash' => 'No offers found in Trash',
			'search_item' => 'Search Offers',
			'singular_name' => 'AK Offer',
			'view_item' => 'View offers'
		],
		'menu_icon' => 'dashicons-awards',
		'menu_position' => 7,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	// Discount Offers
	register_post_type('discount_offers', [
		'has_archive' => true,
		'labels' => [
			'add_new_item' => 'Add New Discount Offer',
			'all_items' => 'All Offers',
			'edit_item' => 'Edit Offer',
			'name' => 'Discount Offers',
			'new_item' => 'New Offer',
			'not_found' => 'No offers found',
			'not_found_in_trash' => 'No offers found in Trash',
			'search_item' => 'Search Offers',
			'singular_name' => 'Discount Offer',
			'view_item' => 'View offers'
		],
		'menu_icon' => 'dashicons-awards',
		'menu_position' => 7,
		'public' => true,
		'supports' => [
			'editor',
			'thumbnail',
			'title'
		]
	]);

	// Galleries
	register_post_type('galleries', [
		'labels' => [
			'add_new_item' => 'Add New Gallery',
			'all_items' => 'All Galleries',
			'edit_item' => 'Edit Gallery',
			'name' => 'Galleries',
			'new_item' => 'New Gallery',
			'not_found' => 'No galleries found',
			'not_found_in_trash' => 'No galleries found in Trash',
			'search_item' => 'Search Galleries',
			'singular_name' => 'Gallery',
			'view_item' => 'View Gallery'
		],
		'menu_icon' => 'dashicons-format-gallery',
		'menu_position' => 5,
		'public' => true,
		'supports' => [
			'title'
		]
	]);

	register_post_type('ig-posts', [
		'description' => 'Internal post type to keep track of AK\'s latest posts on Instgram.',
		'exclude_from_search' => true,
		'labels' => [
			'name' => 'Instagram Posts',
			'singular_name' => 'Instagram Post',
		],
		'public' => WP_DEBUG
	]);

	register_post_type('tweet', [
		'description' => 'Internal post type to keep track of AK\'s latest tweets on Twitter.',
		'exclude_from_search' => true,
		'labels' => [
			'name' => 'Tweets',
			'singular_name' => 'Tweet',
		],
		'public' => WP_DEBUG
	]);

	register_post_type('fb-posts', [
		'description' => 'Internal post type to keep track of AK\'s latest posts on Facebook.',
		'exclude_from_search' => true,
		'labels' => [
			'name' => 'Facebook Posts',
			'singular_name' => 'Facebook Post',
		],
		'public' => WP_DEBUG
	]);
});

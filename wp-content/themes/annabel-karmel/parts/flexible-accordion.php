<div class="accordion accordion-<?php the_sub_field('style'); ?>">
	<?php if ($title = get_sub_field('title')): ?>
	<h2><?php echo $title; ?></h2>
	<?php endif; ?>
	<?php if (have_rows('accordion')): ?>
	<div class="accordion-container">
		<?php while (have_rows('accordion')): the_row(); ?>
		<div>
			<a><?php the_sub_field('title'); ?></a>
			<div>
				<div class="wysiwyg-content">
					<?php the_sub_field('content'); ?>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</div>
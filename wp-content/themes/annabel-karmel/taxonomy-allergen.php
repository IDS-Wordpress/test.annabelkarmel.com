<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php echo get_queried_object()->name; ?> <?php echo get_post_type_object('recipes')->labels->name; ?></h1>
				<?php if ($description = category_description()): ?>
				<div class="flexible-content">
					<?php echo $description; ?>
				</div>
				<?php endif; ?>
				<h2>Latest</h2>
				<?php $offset = 8; ?>
				<?php 
				$args = array(
				'post_type'=>'recipes',
				'orderby'=>'date',
				'order'=>'DESC',
				'tax_query' => array(
								array(
								'taxonomy' => 'allergen',
								'field' => 'slug',
								'terms' => get_queried_object()->slug),
								),
						);
				query_posts($args);
				?>
				<?php if (have_posts()): ?>
				<div class="ak-post-widgets owl-carousel owl-theme" data-items="3">
               
					<?php while (have_posts()): the_post(); ?>
                    
                     <?php //the_title();?>
					<div class="item">
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					</div>
					<?php if (8 < $GLOBALS['wp_query']->found_posts && 11 > $GLOBALS['wp_query']->found_posts && 4 == $GLOBALS['wp_query']->found_posts - $GLOBALS['wp_query']->current_post): ?>
					<?php $offset = $GLOBALS['wp_query']->current_post + 1; break; ?>
					<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				
				<?php //echo $offset; ?>
				
				<?php if (8 < $GLOBALS['wp_query']->found_posts): ?>
				<div class="ak-post-widgets">
					<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="recipes" taxonomy="allergen" taxonomy_terms="' . get_queried_object()->slug . '" taxonomy_operator="IN" offset="' . $offset . '" posts_per_page="6" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
				</div>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
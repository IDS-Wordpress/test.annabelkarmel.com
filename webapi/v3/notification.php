<?php
define('WP_USE_THEMES', false);
require('../../wp-load.php');
include('classes/notification_api.php');

/****** Auth Username/Password for Authorization***************/
$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$obj = new API_NOTIFICATION($username,$password);

/****** Parameters for routing on base of action***************/
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

/****** Start Data Recieving from Json Raw Data OR Form Post Data***************/
$params = (array) json_decode(file_get_contents('php://input'), TRUE);
if(!empty($params)) {	$data = $params; }
else {	$data = isset($_POST)?$_POST : array(); }
/****** End Data Recieving from Json Raw Data OR Form Post Data***************/

switch ($action)
{
	case 'addToken':				
		$obj->add_devicetoken($data);
		break;
	case 'getToken':				
		$obj->get_devicetoken();
		break;	
	case 'notify':				
		$obj->push_notification($data);
		break;		
	default:
		$obj->response(array('status'=>'fail','error_code'=>400,'response_msg'=>'Bad Request'));
		header("HTTP/1.0 400 Bad Request");	
		break;			
}
?>
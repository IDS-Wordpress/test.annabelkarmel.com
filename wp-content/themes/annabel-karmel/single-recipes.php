<?php get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php 
if(ICL_LANGUAGE_CODE=='au'){
	$adid=17886;
echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(($adid ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']');	
}else{
echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']');	
}
 ?>



<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<?php get_template_part('parts/topflexible-content'); ?>
				<div class="singular-wrapper">
					<div class="page-header<?php if (get_field('sponsored_content')) echo ' sponsored'; ?>" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/Recipe">
					
					
					<?php /*****************schema data***********start*******/?>
					
					
					<?php
					$terms = get_the_terms(get_the_ID(), 'recipe-category');
					$post_category = "";
					foreach($terms as $term_data){
						$post_category .= $term_data->name.', ';
					}
					
					?>
						<meta itemprop="recipeCategory" content="<?php echo $post_category; ?>">
						<meta itemprop="author" content="Annabel Karmel">
						<meta content="<?php echo the_time('Y-m-d'); ?>" itemprop="datePublished">
						<meta content="<?php the_permalink(); ?>" itemprop="url">	
						<meta itemprop="prepTime" content="<?php echo get_field( "preparation_time"); ?>">
						<meta itemprop="cookTime" content="<?php echo get_field( "cooking_time"); ?>">
						<meta itemprop="keywords" content="<?php echo get_field( "keywords"); ?>">
						
						<meta itemprop="recipeCuisine" content="<?php echo get_field( "recipe_cuisine"); ?>">						
						<div style="display:none;">
							<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
								<span itemprop="ratingValue"><?php echo get_field( "aggregate_rating"); ?></span>
								<span itemprop="reviewCount">5</span>
							</div>
							<div itemprop="nutrition" itemscope itemtype="http://schema.org/NutritionInformation">
								<span itemprop="calories"><?php echo get_field( "nutrition"); ?></span>
							</div>
							<?php 
								$ingredients = get_field( "ingredient_sections");
								$ingredient_list = "";
								foreach($ingredients as $ingredient){
									$ingredient_list .= $ingredient['ingredients'];
								}
								echo '<span itemprop="recipeIngredient" >'.$ingredient_list.'</span>';
								echo '<span itemprop="recipeInstructions">'.get_field( "method").'</span>';
							?>
							<div itemscope itemtype="http://schema.org/VideoObject" > 
								<h1 itemprop="name"><?php the_title(); ?></h1>
								<p itemprop="description"><?php the_title(); ?></p>
								<span itemprop="uploadDate"><?php the_date('Y-m-dTH:i:s'); ?></span>
								<span itemprop="thumbnailUrl" itemscope itemtype="<?php echo get_template_directory_uri(); ?>/images/play.png"> 
									<img itemprop="contentUrl" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
								</span>
							</div>
						</div>
					<?php/*****************schema data************end******/?>
						
						<div class="title-allergens">
							<h1 itemprop="name"><?php the_title(); ?></h1>
                           <?php if(get_field( "recipes_title_2", get_the_ID())){ ?>
						   <h1> <?php  echo get_field( "recipes_title_2", get_the_ID() ); ?></h1>
						   <?php } ?>
                           <?php if(get_field('sponsor_logo')): ?>
                            <a href="<?php  echo get_field( "sponsor_url", get_the_ID() ); ?>" target="_blank"><img src="<?php the_field('sponsor_logo' ) ?>"></a>
                            <?php else: ?>
                            <?php endif; ?>  
                          
							<?php if (($terms = get_the_terms(get_the_ID(), 'allergen')) || get_field('apps_&_books')): ?>
							<div class="allergens">
								<?php if ($terms): ?>
								<?php foreach ($terms as $term): ?>
								<div class="allergen qtip2" title="<?php echo esc_attr($term->name); ?>">
									<img alt="<?php echo esc_attr($term->name); ?>" src="<?php echo get_field('icon', $term->taxonomy . '_' . $term->term_id)['sizes']['icon']; ?>">
									<?php if ($icon = get_field('hover__active_icon', $term->taxonomy . '_' . $term->term_id)): ?>
									<img alt="<?php echo esc_attr($term->name); ?>" src="<?php echo $icon['sizes']['icon']; ?>">
									<?php endif; ?>
								</div>
								<?php endforeach; ?>
								<?php endif; ?>
								<?php if ($apps_books = get_field('apps_&_books')): ?>
								<?php foreach ($apps_books as $app_book): ?>
								<?php if (has_term('our-books', 'app-book-category', $app_book->ID)): ?>
								<div class="allergen qtip2" title="<?php echo esc_attr($app_book->post_title); ?>">
									<a href="<?php echo get_the_permalink($app_book->ID); ?>">
										<!--<img alt="<?php //echo esc_attr($app_book->post_title); ?>" src="<?php //echo get_template_directory_uri(); ?>/images/book.png">-->
										<!--<img alt="<?php //echo esc_attr($app_book->post_title); ?>" src="<?php //echo get_template_directory_uri(); ?>/images/book-hover.png">-->
									</a>
								</div>
								<?php elseif (has_term('apps', 'app-book-category', $app_book->ID)): ?>
								<div class="allergen qtip2" title="<?php echo esc_attr($app_book->post_title); ?>">
									<a href="<?php echo get_the_permalink($app_book->ID); ?>">
										<img alt="<?php echo esc_attr($app_book->post_title); ?>" src="<?php echo get_template_directory_uri(); ?>/images/app.png">
										<img alt="<?php echo esc_attr($app_book->post_title); ?>" src="<?php echo get_template_directory_uri(); ?>/images/app-hover.png">
									</a>
								</div>
								<?php endif; ?>
								<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<?php endif; ?>
						</div>
					
						<?php get_template_part('parts/save-share'); ?>
						<?php if (get_the_content()): ?>
					<div class="wysiwyg-content  no-side-padding" itemprop="description">
						<?php the_content(); ?>
					</div>
					<div class="clear"></div>
					<?php endif; ?>
						<div class="images-info table">
							<?php if (has_post_thumbnail()): ?>
							<?php if ('owl' == get_field('gallery_type')): ?>
							<div class="cell carousel">
								<div class="owl-carousel owl-theme" data-items="1">
									<?php the_post_thumbnail('single-recipes', [
										'class' => 'item',
										'itemprop' => 'image thumbnailUrl'
									]); ?>
									<?php if (get_field('video') || get_field('additional_images')): ?>
									<?php if (get_field('video')): ?>
									<a class="item popup-youtube" href="<?php echo get_post_meta(get_the_ID(), 'video', true); ?>">
										<img alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
										<?php the_post_thumbnail('single-recipes'); ?>
									</a>
									<?php endif; ?>
									<?php foreach ((get_field('additional_images') ?: []) as $image): ?>
									<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" class="item" src="<?php echo $image['sizes']['single-recipes']; ?>" title="<?php echo esc_attr($image['title']); ?>">
									<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
							<?php else: ?>
							<div class="cell gallery" data-gallery="<?php $GLOBALS['Annabel_Karmel']->generate_gallery_json(array_merge([
									acf_get_attachment(get_post_thumbnail_id())
								], (get_field('additional_images') ?: []))); ?>">
                                <?php if ( get_field('mark_this_as_featured_image') == false ) { ?>
								<a class="open-gallery" data-index="<?php echo ($index = 0); ?>">
									<?php the_post_thumbnail('single-recipes', [
										'itemprop' => 'image thumbnailUrl'
									]); ?>
								</a>
                                 <?php } else { ?>
<?php } ?>
								<?php if (get_field('video') || get_field('additional_images')): ?>
								<div class="image-thumbnails">
									<?php if (get_field('video')): ?>
									<a class="popup-youtube" href="<?php echo get_post_meta(get_the_ID(), 'video', true); ?>">
										<img alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
										<?php the_post_thumbnail('single-recipes-small'); ?>
									</a>
									<?php endif; ?>
									<?php foreach ((get_field('additional_images') ?: []) as $image): ?>
									<a class="open-gallery" data-index="<?php echo ++$index; ?>">
										<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['single-recipes-small']; ?>" title="<?php echo esc_attr($image['title']); ?>">
									</a>
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<?php endif; ?>
							<div class="cell">
								<?php if (get_field('prep_time') || get_field('cook_time') || get_field('serves__makes') || get_field('suitable_for_freezing')): ?>
								<div class="recipe-info">
									<div class="recipe-info-container">
										<div class="table">
											<?php if ($prep_time = get_field('prep_time')): ?>
											<div class="row">
												<div class="cell prep-time">
													<img alt="Prep Time" src="<?php echo get_template_directory_uri(); ?>/images/prep-time.png">
												</div>
												<div class="cell">
													<h5>Prep Time:</h5>
													<?php echo $prep_time; ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if ($cook_time = get_field('cook_time')): ?>
											<div class="row">
												<div class="cell cook-time">
													<img alt="Cook Time" src="<?php echo get_template_directory_uri(); ?>/images/cook-time.png">
												</div>
												<div class="cell">
													<h5>Cook Time:</h5>
													<?php echo $cook_time; ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if ($serves_makes = get_field('serves__makes')): ?>
											<div class="row">
												<div class="cell yield">
													<img alt="Yield" src="<?php echo get_template_directory_uri(); ?>/images/yield.png">
												</div>
												<div class="cell" itemprop="yield">
													<h5><?php echo ucwords($serves_makes); ?>:</h5>
													<span itemprop="yield"><?php the_field('yield'); ?></span>
												</div>
											</div>
											<?php endif; ?>
											<?php if ($great_for = array_filter((get_the_terms(get_the_ID(), 'recipe-category') ?: []), function($item) {
												return 0 != $item->parent;
											})): ?>
											<div class="row">
												<div class="cell great-for">
													<img alt="Great For" src="<?php echo get_template_directory_uri(); ?>/images/great-for.png">
												</div>
												<div class="cell">
													<h5>Great For:</h5>
													<?php echo implode(', ', array_map(function($item) {
														return '<a href="' . get_term_link($item) . '">' . $item->name . '</a>';
													}, $great_for)); ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if (get_field('suitable_for_freezing')): ?>
											<div class="row">
												<div class="cell suitable-for-freezing">
													<img alt="Suitable for Freezing" src="<?php echo get_template_directory_uri(); ?>/images/suitable-for-freezing.png">
												</div>
												<div class="cell">
													<h5>Suitable for Freezing</h5>
												</div>
											</div>
											<?php else: ?>
											<div class="row">
												<div class="cell suitable-for-freezing">
													<img alt="Not Suitable for Freezing" src="<?php echo get_template_directory_uri(); ?>/images/not-suitable-for-freezing.png">
												</div>
												<div class="cell">
													<h5>Not Suitable for Freezing</h5>
												</div>
											</div>
											<?php endif; ?>
										</div>
										<?php if ($tags = get_the_terms(get_the_ID(), 'recipe-tag')): ?>
										<div class="recipe-tags">
											<img alt="Recipe Tags" src="<?php echo get_template_directory_uri(); ?>/images/tags.png">
											<?php echo implode(' &middot; ', array_map(function($item) {
												return '<a href="' . get_term_link($item) . '">' . $item->name . '</a>';
											}, $tags)); ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					
                    
					<div class="ingredients-method-container<?php if (get_field('gated') && !is_user_logged_in()) echo ' padded'; ?>">
						<?php if (get_field('gated') && !is_user_logged_in()): ?>
						<div class="gated">
							<div class="container single_recipes_from">
								<div class="existing-customers">
											<img class="login_left_logo" alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_left_logo.png">
		                    <div class="title">Please log in to view this recipe</div>
                                    
									<?php /*$GLOBALS['Annabel_Karmel']->wp_login_form([
										'form_id' => 'pp_loginform',
										'id_username' => 'pp_user_login',
										'id_password' => 'pp_user_pass',
										'id_submit' => 'pp_wp-submit',										
										'remember' => false
									]);*/ ?>
									<form name="pp_loginform" id="pp_loginform_recipes" action="" method="post">
									<div id="msg"></div>	
																		
								<p class="login-username login_form_group">
				<input type="text" name="log" id="pp_user_login2" class="input" size="20" required>
				<label for="pp_user_login2">Email</label>
			</p>
			<p class="login-password login_form_group">
				<input type="password" name="pwd" id="pp_user_pass2" autocomplete="new-password" class="input" size="20" required>
				<label for="pp_user_pass2">Password</label>
			</p>
			
			<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme2" value="forever"> Remember Me</label></p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="pp_wp-submit2" class="button button-primary" value="Log In">
				<input type="hidden" name="redirect_to" value="<?php echo get_permalink(); ?>">
			</p>
								</form>
									<script type="text/javascript">
									$(document).ready(function(){
										$('#pp_loginform_recipes').submit(function(){
											var username = $('#pp_user_login2').val();
											var password = $('#pp_user_pass2').val();
											var redirect_to = $('#redirect_to').val();
											var tok = username + ':' + password;
											var hash = btoa(tok);
											var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
											
											if(username == '')
											{
												$('#msg').html('<span class="warning">Please enter your username/email in the Email Address field below.</span>');
												return false;
											}
											else if(password == '')
											{
												$('#msg').html('<span class="warning">Please enter your password.</span>');
												return false;
											}
											else
											{	
												$.ajax({
													url : ajaxurl,
													type: "POST",
													data : {action: "login_validation",tok : hash},
													success:function(response, textStatus, jqXHR) 
													{
														if(response == "success")
														 {
															// window.location.href= redirect_to;
															location.reload(true);
														 }
														 else
														 {
															$('#msg').html('<span class="warning">'+response+'</span>');
															return false;
														 }
														 
													},
													error: function(jqXHR, textStatus, errorThrown) 
													{
														alert(jqXHR.responseText);
														return false;  												
													}
												});
												return false;											
											}																		
										});
									});
									</script>
                                   
								</div>
									<div class="new-customers">
	<div class="login_right_img">
	<img alt="AK Club" src="<?php echo get_template_directory_uri(); ?>/images/login_right_img.png">
	</div>
		<ul>
			<li class="book_i">Free e-recipe book on sign up</li>
			<li class="check_i">Access to exclusive recipes & content</li>
			<li class="sign_i">Exclusive offers on AK products</li>
			<li class="star_i">Great competitions and prizes</li>
			<li class="letter_i">Never miss a thing with our newsletter</li>
		</ul>
		<p>
			<a class="button" href="<?php echo site_url('/register/'); ?>">sign up</a>
		</p>
	</div>
							</div>
						</div>
						<?php endif; ?>
						<?php if (have_rows('ingredient_sections') || get_field('method')): ?>
						<div class="ingredients-method table">
							<?php if (have_rows('ingredient_sections')): ?>
							<div class="cell">
								<div class="ingredient-sections">
									<h2>Ingredients</h2>
									<?php while (have_rows('ingredient_sections')): the_row(); ?>
									<?php if ($title = get_sub_field('title')): ?>
									<h4><?php echo $title; ?></h4>
									<?php endif; ?>
									<ul>
										<li itemprop="recipeIngredient"><?php echo implode('</li><li itemprop="recipeIngredient">', explode('<br>', preg_replace('/<br\s*\/?>/', '<br>', get_sub_field('ingredients')))); ?></li>
									</ul>
									<?php endwhile; ?>
								</div>
							</div>
							<?php endif; ?>
							<?php if (get_field('method')): ?>
							<div class="cell">
								<div class="method">
									<h2>Method</h2>
									<div class="wysiwyg-content" itemprop="recipeInstructions">
										<?php the_field('method'); ?>
									</div>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
					<div class="page-footer">
						<?php get_template_part('parts/save-share'); ?>
					</div>
				</div>
				<?php get_template_part('parts/flexible-content'); ?>
				<?php if ($recipes = get_field('related_recipes')): ?>
				<div class="related-content">
					<h2>Related Recipes</h2>
					<a href="<?php echo get_post_type_archive_link('recipes'); ?>">View All Recipes &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets fi-panel">
						<?php foreach ($recipes as $post): setup_postdata($post); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php $args = [
					'orderby' => 'rand',
					'post_type' => 'recipes',
					'post__not_in' => [
						get_the_ID()
					],
					'posts_per_page' => 6
				]; ?>
				<?php if ($terms = get_the_terms(get_the_ID(), 'recipe-category')): ?>
				<?php $args['tax_query'] = array_merge([
					'relation' => 'OR'
				], array_map(function($item) {
					return [
						'taxonomy' => $item->taxonomy,
						'terms' => $item->term_id
					];
				}, $terms)); ?>
				<?php endif; ?>
				<?php $the_query = new WP_Query($args); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>You may also like...</h2>
					<a href="<?php echo get_post_type_archive_link('recipes'); ?>">View All Recipes &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php //$GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<div class="post-widget<?php if (get_field('sponsored_content')) echo ' sponsored'; ?>" data-post-type="<?php echo esc_attr(get_post_type()); ?>">
						<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
							<div class="post-widget-header">
								<div class="rec_tit">
								<h4><?php echo (get_field('short_title') ?: get_the_title()); ?></h4>
								<?php if( get_field('blog_post_title') ): ?><h4><?php the_field('blog_post_title'); ?></h4><?php endif; ?>            
								<?php if( get_field('recipes_title_2') ): ?><h4><?php the_field('recipes_title_2'); ?></h4><?php endif; ?>
								<?php if( get_field('video_title_2') ): ?><h4><?php the_field('video_title_2'); ?></h4><?php endif; ?>
								<?php if( get_field('product_title_2') ): ?><h4><?php the_field('product_title_2'); ?></h4><?php endif; ?>
								</div>
								<span><?php echo $terms[0]->name; ?></span>
							</div>	
							<div class="post-widget-thumbnail">
								<?php the_post_thumbnail(in_array(get_post_type(), [
									'apps-books',
									'products'
								])
									? 'verb-ak-post-widget-padded'
									: 'verb-ak-post-widget'
								); ?>
							</div>
						</a>
						<?php if (($bottom_cat = (get_sub_field('tagline') ?: $GLOBALS['Annabel_Karmel']->bottom_cat($GLOBALS['post']))) && $bottom_cat != $top_cat): ?>
						<div class="bottom-cat">
							<?php echo $bottom_cat; ?>
						</div>
						<?php endif; ?>
						<?php if ((!in_array(get_post_type(), [
							'apps-books',
							'post',
							'products',
							'recipes'
						]) || $force_recipe_excerpt) && get_the_excerpt() && (isset($excerpt)
							? $excerpt
							: true
						)): ?>
						<a class="excerpt-link" href="<?php the_permalink(); ?>">
							<div class="excerpt">
								<?php the_excerpt(); ?>
							</div>
						</a>
						<?php endif; ?>
						<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
							<a>SAVE</a>
						</div>
					</div>

						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<script>
(function($){
	$("#pp_loginform_recipes .login_form_group input").on("keyup", function(){
		if($(this).val() != "") {
			$(this).siblings("label").addClass("active");
		} else {
			$(this).siblings("label").removeClass("active");
		}
	});
	
	$('#pp_loginform_recipes').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			validating: 'glyphicon glyphicon-refresh',
			invalid: 'glyphicon glyphicon-remove'
		},
		fields: {
			log: {
				group: '.login_form_group',
				validators: {
					emailAddress: {message: 'Please enter a valid Email'},
					notEmpty: {message: 'Email is required'},
					callback: {
                        callback: function(value, validator, $field) {
							var regs = /[ ]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
                            if (value.length >= 7) {
								var atReg = /[*\%|\$|\&|\*|\£|\&|\"|\(]/g;
								
								if(value.search(atReg) > 0) {
									
									return {
										valid: false,
										message: "The Email field only accepts letters (a-z), numbers, and the following special characters + @ . -"
									}	
								} else {
									var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									
									if(value.search(reg) < 0) {
										return {
											valid: false,
											message: 'Please enter a valid Email'
										}
									}									
								}
								
                                
                            }
							
							
							if(value.length == 0){
								return {
									valid: false,
									message: 'The Email field is mandatory – please enter at least seven characters for this field'
								};
							}
                            return true
                        }
                    }
				}
			},
			pwd: {
				group: '.login_form_group',
				validators: {
					notEmpty: {message: 'Password is required'}
				}
			}
		}
	});
	$('#pp_loginform_recipes .login-password.login_form_group .form-control-feedback').click(function(){
	  if ($("#pp_loginform_recipes .login-password.login_form_group .input").attr("type") == "password") {
		$("#pp_loginform_recipes .login-password.login_form_group .input").attr("type", "text");
	  } else {
		$("#pp_loginform_recipes .login-password.login_form_group .input").attr("type", "password");
	  }
	});
})(jQuery);
</script>
<?php get_footer(); ?>
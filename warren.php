<?php

header('Content-Type: text/plain');

if ($f = fopen('small_images.csv', 'r')) {
	$output = fopen('php://output', 'w');
	
	while (false !== ($row = fgetcsv($f))) {
		list($width, $height) = getimagesize($row[2]);
		
		if ($width < 380 || $height < 315) {
			fputcsv($output, array_merge($row, [
				$width,
				$height
			]));
		}
	}
}
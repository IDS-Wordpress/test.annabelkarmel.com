<?php
/**
 * Plugin Name: Recipe Finder Widget
 * Description: Embeds the recipe finder form into the sidebar.
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

namespace Verb {
	
	class Recipe_Finder_Widget extends \WP_Widget {
		function __construct() {
			parent::__construct('verb_recipe_finder_widget', 'Recipe Finder Widget', [
				'description' => 'Embeds the recipe finder into the sidebar.'
			]);
		}
		
		public function widget($args, $instance) {
			echo $args['before_widget'];
			?>
			<div class="recipe-filter">
				<h2>Recipe Finder</h2>
				<form action="<?php echo site_url('/recipe-results/'); ?>" method="post">
					<div class="accordion">
						<div class="accordion-container">
							<?php if ($categories = get_terms([
								'orderby' => 'term_order',
								'taxonomy' => 'recipe-category'
							])): ?>
							<div>
								<a>Age</a>
								<div>
									<button class="all<?php if (!isset($_POST['rf_category'])) echo ' selected'; ?>">All</button>
									<?php foreach ($categories as $category): ?>
									<?php if (isset($_POST['rf_category']) && in_array($category->slug, $_POST['rf_category'])): ?>
									<button class="selected"><?php echo $category->name; ?></button>
									<input checked name="rf_category[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php else: ?>
									<button><?php echo $category->name; ?></button>
									<input name="rf_category[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
							<?php endif; ?>
							<?php foreach (get_terms([
								'parent' => 0,
								'taxonomy' => 'recipe-filter'
							]) as $filter): ?>
							<?php if ($categories = get_terms([
								'child_of' => $filter->term_id,
								'taxonomy' => 'recipe-filter'
							])): usort($categories, function($a, $b) {
								return strcmp($a->name, $b->name);
							}); ?>
							<div>
								<a><?php echo $filter->name; ?></a>
								<div>
									<button class="all<?php if (!isset($_POST['rf_' . $filter->slug])) echo ' selected'; ?>">All</button>
									<?php foreach ($categories as $category): ?>
									<?php if (isset($_POST['rf_' . $filter->slug]) && in_array($category->slug, $_POST['rf_' . $filter->slug])): ?>
									<button class="selected"><?php echo $category->name; ?></button>
									<input checked name="rf_<?php echo $filter->slug; ?>[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php else: ?>
									<button><?php echo $category->name; ?></button>
									<input name="rf_<?php echo $filter->slug; ?>[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
							<?php endif; ?>
							<?php endforeach; ?>
							<?php if ($categories = get_terms([
								'taxonomy' => 'allergen'
							])): ?>
							<div>
								<a>Preferences &amp; Allergies</a>
								<div>
									<button class="all<?php if (!isset($_POST['rf_allergen'])) echo ' selected'; ?>">All</button>
									<?php foreach ($categories as $category): ?>
									<?php if (isset($_POST['rf_allerge']) && in_array($category->slug, $_POST['rf_category'])): ?>
									<button class="selected"><?php echo $category->name; ?></button>
									<input checked name="rf_allergen[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php else: ?>
									<button><?php echo $category->name; ?></button>
									<input name="rf_allergen[]" type="checkbox" value="<?php echo $category->slug; ?>">
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<input name="rf" type="submit" value="FILTER RECIPES">
				</form>
			</div>
			<?php
			echo $args['after_widget'];
		}
		
		public function form($instance) { }
		
		public function update($new_instance, $old_instance) {
			return [];
		}
	}
	add_action('widgets_init', function() {
		register_widget('Verb\Recipe_Finder_Widget');
	});
}
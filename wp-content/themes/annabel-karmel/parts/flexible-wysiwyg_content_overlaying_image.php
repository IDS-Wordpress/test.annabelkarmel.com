<?php $image = get_sub_field('header_image'); ?>
<div class="overlaying-image">
	<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['header-for-overlaid-wysiwyg']; ?>" title="<?php echo esc_attr($image['title']); ?>">
</div>
<div class="wysiwyg-content">
	<?php the_sub_field('wysiwyg_content'); ?>
	<div class="clear"></div>
</div>
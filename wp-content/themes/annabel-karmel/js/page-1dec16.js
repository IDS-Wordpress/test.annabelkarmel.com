var app = app || {};

app.menu = new function() {
	this.okay = true;
	this.toggle = function() {
		if (app.menu.okay) {
			app.menu.okay = false;
			var wrapper = jQuery('.wrapper');
			if (wrapper.hasClass('show_menu')) {
				wrapper.removeClass('show_menu').addClass('hide_menu');
				jQuery('#responsive-menu-icon').removeClass('close');
				jQuery('#responsive-menu').fadeOut(300, function() {
					wrapper.removeClass('hide_menu');
					jQuery('html, body').css('overflow', '').removeClass('menu');
					setTimeout(function() {
						app.menu.okay = true;
					}, 300);
				});
			} else {
				jQuery('html, body').css('overflow', 'hidden').addClass('menu');
				wrapper.addClass('show_menu');
				jQuery('#responsive-menu-icon').addClass('close');
				setTimeout(function() {
					jQuery('#responsive-menu').fadeIn(300, function() {
						app.menu.okay = true;
					});
				}, 300);
			}
		}
		return false;
	};
};

app.pswp = null;
app.pswp_counter = 0;

// Prototype Extensions
if ('function' !== typeof String.prototype.endsWith) {
	String.prototype.endsWith = function(suffix) {
		return -1 !== this.indexOf(suffix, this.length - suffix.length);
	};
}

jQuery(function($) {
	$('.accordion > .accordion-container > div > a').click(function() {
		$('+ div', $(this).toggleClass('open')).slideToggle();
		
		return false;
	});
	
	$('.flexslider').flexslider({
		controlNav: false
	});
	
	// Lightbox
	$('a.open-gallery').click(function() {
		app.pswp = new PhotoSwipe($('.pswp')[0], PhotoSwipeUI_Default, (that = $(this)).closest('.gallery').data('gallery'), {
			history: false,
			index: (that.data('index')
				? that.data('index')
				: 0
			)
		});
		app.pswp.listen('close', function() {
			if (window.location.href.endsWith('gallery/')) {
				history.back();
			}
		});
		app.pswp.init();
		
		if ('undefined' !== typeof history) {
			that.attr('data-pswp_counter', app.pswp_counter);
			history.pushState(app.pswp_counter++, null, window.location.href + 'gallery/');
		}
		
		return false;
	});
	
	window.addEventListener('popstate', function(e) {
		if (window.location.href.endsWith('gallery/') && 'undefined' !== e.state) {
			app.pswp = new PhotoSwipe($('.pswp')[0], PhotoSwipeUI_Default, (that = $('[data-pswp_counter="' + e.state + '"]')).closest('.gallery').data('gallery'), {
				history: false,
				index: (that.data('index')
					? that.data('index')
					: 0
				)
			});
			app.pswp.listen('close', function() {
				if (window.location.href.endsWith('gallery/')) {
					history.back();
				}
			});
			app.pswp.init();
		} else if (app.pswp) {
			app.pswp.close();
		}
	});
	
	// Owl Carousel
	$('body:not(.single-apps-books) .generic-page :not(.carousel) > .owl-carousel:not(.ak-post-widgets)').each(function() {
		(that = $(this)).owlCarousel({
			dots: false,
			loop: true,
			margin: 10,
			nav: true,
			responsive: {
				0: {
					items: 1
				},
				425: {
					items: 2
				},
				768: {
					items: that.data('items')
				}
			},
			responsiveClass: true
		});
		
		if (!(that = $(this)).parent().hasClass('flexible-content')) {
			$('.owl-nav', that).appendTo($('.owl-stage-outer', that));
		}
	});
		
	$('.recipe-categories-search ~ .owl-carousel, .ak-post-widgets.owl-carousel').each(function() {
		(that = $(this)).owlCarousel({
			dots: false,
			center: true,
			loop: true,
			margin: 10,
			nav: true,
			responsive: {
				0: {
					center: true,
					items: 1
				},
				500: {
					center: false,
					items: 2
				},
				768: {
					items: that.data('items')
				}
			},
			responsiveClass: true
		});
	});
	
	$('body.single-apps-books .generic-page .owl-carousel:not(.ak-post-widgets)').each(function() {
		if (3 <= $(this).children().length) {
			(that = $(this)).owlCarousel({
				loop: true,
				margin: 10,
				nav: true,
				responsiveClass: true
			});
			
			$('.owl-nav', (that = $(this))).appendTo($('.owl-stage-outer', that));
		}
	});
	
	$('body .generic-page .carousel > .owl-carousel').each(function() {
		(that = $(this)).owlCarousel({
			items: 1,
			loop: true,
			margin: 10,
			nav: true,
			responsiveClass: true
		});
		
		$('.owl-nav', (that = $(this))).appendTo($('.owl-stage-outer', that));
	});
	
	// Recipe Video Popup
	$('.popup-youtube').magnificPopup({
		type: 'iframe'
	});
	
	// Recipe Filter
	$('.recipe-filter .accordion button').click(function() {
		if ((that = $(this)).hasClass('all') && !that.hasClass('selected')) {
			$('> button.selected', (parent = that.parent())).removeClass('selected');
			$('> input[type="checkbox"]', parent).attr('checked', false);
			that.addClass('selected');
		} else if (!that.hasClass('all') && that.hasClass('selected')) {
			that.removeClass('selected');
			$('+ input[type="checkbox"]', that).attr('checked', false);
			
			if (!$('> input[type="checkbox"]:checked', (parent = that.parent())).length) {
				$('> button.all', parent).addClass('selected');
			}
		} else if (!that.hasClass('all') && !that.hasClass('selected')) {
			that.addClass('selected');
			$('+ input[type="checkbox"]', that).attr('checked', true);
			$('> button.all', that.parent()).removeClass('selected');
		}
		
		return false;
	});
	
	// Title Tooltips
	$('.qtip2[title!=""]').qtip({
		position: {
	    	my: 'bottom center',
			at: 'top center'
		}
	});
	
	// Book Type
	$('.book-types select').change(function() {
		$('> .button', (parent = (that = $(this)).closest('.book-types'))).remove();
		
		if (link = (option = $('option:selected', that)).data('link')) {
			$('<a class="button" href="' + link + '">' + option.data('text') + '</a>').appendTo(parent);
		}
	});
	
	// Change post type on search
	$('.search-title select').change(function() {
		window.location.href = $(this).val();
	});
	
	// Gravity Forms Select Picker
	$('select.gfield_select, .address_country > select').wrap('<div class="bootstrap-namespaced"></div>').selectpicker();
	
	// Save
	$('.save > a').click(function() {
		$('#save-content-to-id').val($(this).parent().data('save-id'));
		
		if (1 < $('#save-content-to select option').length) {
			$.magnificPopup.open({
				items: {
					src: $('#save-content-to').css('display', 'inline-block')
				}
			});
		} else if ($('body').hasClass('logged-in')) {
			$.magnificPopup.open({
				items: {
					src: $('#create-new-collection').css('display', 'inline-block')
				}
			});
		} else {
			$.magnificPopup.open({
				items: {
					src: $('#login').css('display', 'flex')
				}
			});
		}
		
		return false;
	});
	
	$('#create-new-collection form').submit(function() {
		$.ajax((that = $(this)).attr('action'), {
			data: {
				collection_name: $('input[type="text"]', that).val()
			},
			dataType: 'json',
			method: 'POST',
			success: function(data, textStatus, jqXHR) {
				$.magnificPopup.close();
				
				if ((keys = Object.keys(data)).length) {
					$('select > option:not([disabled])', (that = $('#save-content-to'))).remove();
					
					for (i = 0; i < keys.length; i++) {
						$('select', that).append('<option value="' + keys[i] + '">' + data[keys[i]].title + '</option>');
					}
					
					$('select', that).selectpicker('refresh');
					
					$.magnificPopup.close();
					$.magnificPopup.open({
						items: {
							src: that.css('display', 'inline-block')
						}
					});
				}
			}
		});
		
		return false;
	});
	
	$('a[href="#create-new-collection"]').click(function() {
		$.magnificPopup.close();
		$.magnificPopup.open({
			items: {
				src: $('#create-new-collection').css('display', 'inline-block')
			}
		});
		
		return false;
	});
	
	$('#delete-existing-collection').click(function() {
		$.ajax('/delete-collection/', {
			data: {
				collection_id: $(this).data('collection-id')
			},
			dataType: 'json',
			method: 'POST',
			success: function(data, textStatus, jqXHR) {
				window.location.href = '/your-saved/';
			}
		});
		
		return false;
	});
	
	$('#edit-collection form').submit(function() {
		$.ajax((that = $(this)).attr('action'), {
			data: {
				collection_id: $('input[type="hidden"]', that).val(),
				collection_name: $('input[type="text"]', that).val()
			},
			dataType: 'json',
			method: 'POST',
			success: function(data, textStatus, jqXHR) {
				window.location.reload();
			}
		});
		
		return false;
	});
	
	$('#edit-existing-collection').click(function() {
		var form = $('#edit-collection');
		$('input[name="collection_id"]', form).val($(this).data('collection-id'));
		
		$.magnificPopup.close();
		$.magnificPopup.open({
			items: {
				src: form.css('display', 'inline-block')
			}
		});
		
		return false;
	});
	
	// Article Jump
	if ((article_jumps = $('.article-jump')).length) {
		var counter = 1;
		var jumps = [];
		
		$('.flexible-content > .wysiwyg-content > h2').each(function() {
			(that = $(this)).attr('id', 'aj-h2-' + counter);
			jumps.push(that.html());
			counter += 1;
		});
		
		if (!jumps.length) {
			article_jumps.remove();
		} else {
			for (i = 0; i < jumps.length; i++) {
				article_jumps.append('<a class="button" href="#aj-h2-' + (i + 1) + '">' + jumps[i] + '</a>');
			}
		}
	}
	
	// FitVids
	$('.video-wrapper').fitVids();
	
	// Login
	$('a[href$="#login"]').click(function() {
		if ($('.wrapper').hasClass('show_menu')) {
			app.menu.toggle();
		}
		
		$.magnificPopup.open({
			items: {
				src: $('#login').css('display', 'flex')
			}
		});
		
		return false;
	});
	
	// Try to fit text on cards by resizing
	(resize_cards = function() {
		$('.post-widget:not(.large-post-widget) h4').each(function() {
			var that = $(this);
			var font_size = parseInt(that.css('font-size'));
			
			while (60 < that.outerHeight()) {
				that.css('font-size', --font_size + 'px');
				
				if (12 >= font_size) {
					break;
				}
			}
			
			that.outerHeight(60);
		});
		
		$('.post-widget:not(.large-post-widget) .bottom-cat').each(function() {
			var that = $(this);
			var font_size = parseInt(that.css('font-size'));
			
			while (36 < that.outerHeight()) {
				that.css('font-size', --font_size + 'px');
				
				if (10 >= font_size) {
					break;
				}
			}
			
			that.outerHeight(36);
		});
	})();
	
	$.fn.almComplete = function() {
		resize_cards();
	};
	
	// Clamping
	// $('.recipe-categories > a h4 > span').each(function() {
		// $clamp($(this)[0], {
			// clamp: 1
		// });
	// });
	
	// Feel Inspired
	$('.feel-inspired > .buttons > button').click(function() {
		if (!(that = $(this)).hasClass('selected')) {			
			$('> .selected', (parent = that.parent())).removeClass('selected');
			that.addClass('selected');
			
			// Recipes
			$('> :visible', (recipes = parent.next())).hide();
			$('> .recipes-' + that.data('panel'), recipes).show();
			
			// Products
			$('> :visible', (products = recipes.next())).hide();
			$('> .products-' + that.data('panel'), products).show();
		}
	});
	
	// Product Carousel Clamping
	$('.flexible-content > .owl-carousel:not(.ak-post-widgets) .item h4').each(function() {
		$clamp($(this)[0], {
			clamp: 2
		});
	});
	
	// Unsave
	$('body.page-template-saved-content .post-widget > .mfp-close').click(function() {
		window.location.href = '/unsave/?save_id=' + $(this).data('post-id');
		
		return false;
	});
	
	// Responsive Menu Sliding
	$('#responsive-menu .ubermenu-item-has-children > a').click(function() {
		$(this).parent().addClass('active-panel').closest('.ubermenu-nav, .ubermenu-submenu').addClass('slide-away');
		
		return false;
	});
	
	// Responsive Menu Back Button
	$('#responsive-menu .ubermenu-submenu').each(function() {
		if ((that = $(this)).is('ul')) {
			that.prepend('<li class="ubermenu-item"><a class="go-back ubermenu-target"><span class="ubermenu-target-text">&laquo; Back</span></a></li>');
		} else {
			that.prepend('<a class="go-back ubermenu-target"><span class="ubermenu-target-text">&laquo; Back</span></a>');
		}
	});
	$('#responsive-menu .go-back').click(function() {
		$(this).closest('.active-panel').removeClass('active-panel').closest('.ubermenu-nav, .ubermenu-submenu').removeClass('slide-away');
	});
	
	// Resp. Menu
	$('#responsive-menu-icon').click(function() {
		return app.menu.toggle();
	});
	
	// Hide / show with repeater
	$('#choice_2_10_0').click(function() {
		$('body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield').show();
	});
	$('#choice_2_10_1').click(function() {
		$('body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield').hide();
	});
	
	$('#choice_3_10_0').click(function() {
		$('body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield').show();
	});
	$('#choice_3_10_1').click(function() {
		$('body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield').hide();
	});	
	
	// Convert gform radio buttons to toggles...
	$('.ginput_container_radio').each(function() {
		// Hide the radio buttons that are there
		(ul = $('> ul.gfield_radio', (that = $(this)))).hide();
		
		// Create <button> for each option
		$('> li', ul).each(function() {
			if ((input = $('> input', $(this))).is(':checked')) {
				that.append('<button class="selected" type="button">' + $('> label', $(this)).html() + '</button>');
				input[0].click();
			} else {
				that.append('<button type="button">' + $('> label', $(this)).html() + '</button>');
			}
		});
	});
	
	// ...and bind a click event on them
	$('.ginput_container_radio > button').click(function() {
		if (!(that = $(this)).hasClass('selected')) {
			(buttons = $('> button', (par = that.parent()))).removeClass('selected');
			that.addClass('selected');
			
			$('li:eq(' + buttons.index(that) + ') > input', par)[0].click();
		}
	});
	
	// Children?
	if ((that = $('.my-children')).length) {
		var children = that.data('children');
		
		if (children.length > 1) {
			for (i = 0; i < children.length - 1; i++) {
				gfRepeater_repeatRepeater(3, 1);
			}
		}
		
		for (i = 1; i <= children.length; i++) {
			$('input[name="input_12-1-' + i + '"]').val(children[i - 1]);
		}
	}
});
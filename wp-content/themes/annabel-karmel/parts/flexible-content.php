<?php if (have_rows('flexible_content')): ?>
<div class="flexible-content" itemprop="articleBody">
	<?php while (have_rows('flexible_content')): the_row(); ?>
	<?php
	//echo get_row_layout();
	if ('recipe_finder' == ($row_layout = get_row_layout())): ?>
	<?php get_template_part('parts/recipe-finder'); ?>
	<?php else: ?>
	<?php get_template_part('parts/flexible', get_row_layout()); ?>
	<?php endif; ?>
	<?php endwhile; ?>
</div>
<?php endif; ?>
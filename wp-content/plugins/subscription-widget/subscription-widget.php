<?php
/**
 * Plugin Name: Subscription Widget
 * Description: Adds an subscription signup form to the sidebar
 * Version: 1.0
 * Author: Verb Brands Limited
 * Author URI: http://verbbrands.com/
 */

namespace Verb;

class AK_Subscription_Widget extends \WP_Widget {
	function __construct() {
		parent::__construct('ak_subscription_widget', 'Subscribe', [
			'description' => 'Adds an subscription signup form to the sidebar.'
		]);
	}
	
	public function widget($args, $instance) {
		global $wpdb;
		
		echo $args['before_widget'];
		
		if (!empty($instance['title'])) {
			echo $args['before_title'] . preg_replace('/\*([^\*]+)\*/', '<span class="bold">$1</span>', apply_filters( 'widget_title', $instance['title'] )) . $args['after_title'];
		}
		
		if ($instance['description']) {
			echo '<p class="description">' . $instance['description'] . '</p>';
		}
		
		echo '<form action="//annabelkarmel.us5.list-manage.com/subscribe/post?u=4df7dde41f4f1ee0758ae38a1&amp;id=42be5cf88c" method="post"><input type="email" name="EMAIL" required placeholder="Enter your email address..."><div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4df7dde41f4f1ee0758ae38a1_42be5cf88c" tabindex="-1" value=""></div><input type="submit" value="Submit" name="subscribe"></form>';
		
		echo $args['after_widget'];
	}
	
	public function form($instance) {
		$description = (!empty($instance['description'])
			? $instance['description']
			: ''
		);
		$title = (!empty($instance['title'])
			? $instance['title']
			: ''
		);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('description'); ?>">Description:</label><br />
			<textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $description; ?></textarea>
		</p>
		<?php
	}
	
	public function update($new_instance, $old_instance) {
		return [
			'description' => esc_textarea(!empty($new_instance['description'])
				? $new_instance['description']
				: ''
			),
			'title' => esc_attr(!empty($new_instance['title'])
				? $new_instance['title']
				: ''
			)
		];
	}
}
add_action('widgets_init', function() {
	register_widget('Verb\AK_Subscription_Widget');
});
<?php if (is_active_sidebar('sidebar')): ?>
<div class="cell">
	<div class="primary-sidebar widget-area" id="sidebar" role="complementary">
		<?php dynamic_sidebar('sidebar'); ?>
	</div>
</div>
<?php endif; ?>
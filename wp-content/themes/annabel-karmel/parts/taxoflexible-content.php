<?php
//$term = get_term_by( 'id', get_queried_object_id(),'category' ); 
$queried_object = get_queried_object();
//echo "<pre>"; print_r($queried_object); echo "</pre>"; 
if ( have_rows('flexible_content', $queried_object) ):
?>
<div class="generic-page">
<div class="flexible-content" itemprop="articleBody">
<?php
	while ( have_rows('flexible_content', $queried_object) ) : the_row(); //echo get_row_layout();
		if ('recipe_finder' == ($row_layout = get_row_layout())): 
			get_template_part('parts/recipe-finder');
		else:
			get_template_part('parts/flexible', get_row_layout());
		endif;	
	endwhile;
	?>
</div>
</div>
<?php
endif;
?>

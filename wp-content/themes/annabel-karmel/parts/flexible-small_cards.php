
<div class="recipe_box small_card_wrapper">
<div class="card_box">
<div class="small_card_area small-main-card">
<?php while (have_rows('fp_content')): the_row(); ?>
<?php $post = get_sub_field('content'); setup_postdata($post); ?>
<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
<?php endwhile; ?>
</div>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
</div>
</div>

<div class="video-list-item">
<div class="post-widget video-widget" data-post-type="<?php echo esc_attr(get_post_type()); ?>">
	<button type="button" class="mfp-close" data-post-id="<?php echo get_the_ID(); ?>">×</button>
	<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
		<div class="post-widget-header">
			<h4><?php if(get_field('short_title')){echo get_field('short_title');} else { echo get_the_title(); if( get_field('video_title_2') ): echo '<br>'.get_field('video_title_2');  endif; } ?></h4>           
		</div>	
		<div class="post-widget-thumbnail">
        	<?php the_post_thumbnail('video-widget'); ?>
    	</div>
	</a>
	<?php if (in_array(get_post_type(), ['videos']) && get_the_excerpt()): ?>
		<span class="excerpt"><?php the_excerpt(); ?></span>
	<?php endif; ?>
	<div class="viewlink">
		<a href="<?php the_permalink(); ?>">VIEW</a>
	</div>
</div>
</div>
<?php
include('auth_api.php');
class API_RECIPES extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	public function __construct($u,$p)
	{
		$this->auth_username = $u;
		$this->auth_password = $p;	
	}
	
	public function authRequest()
	{
		$this->resArr = $this->authorize($this->auth_username,$this->auth_password);
		if(!empty($this->resArr))
		{
			$this->response($this->resArr);
			header("HTTP/1.0 401 Unauthorized");
			die;
		}
		else
		{
			$userInfo['username'] = $this->auth_username;
			$userInfo['password'] = $this->auth_password;
			return $userInfo;
		}
	}
	
	public function deleted_items($post_type,$version)
	{
		global $wpdb;
		$deletedSql = "SELECT * FROM ".$wpdb->prefix."posts_deleted WHERE post_type='".$post_type."'";
		if($version!='V0')
		{
			$vData = $this->getversionData($post_type,$version);
			$deletedSql.= " AND deleted_date BETWEEN '".$vData['from_date']."' AND '".$vData['to_date']."'";
		}
		$results = $wpdb->get_results($deletedSql);
		if(!empty($results))
		{
			$arrDel = array();
			foreach($results as $di)
			{					
				$arrDel[] = (int)$di->post_id;					
			}
		}
		return (!empty($arrDel))?$arrDel:array();
	}
	
	public function get_all($data)
	{
		$records = array();
		$version = $data['version'];
		$post_type = 'recipes';
		
		$args = array('post_type' => $post_type,
			'post_status' => array( 'mobile', 'publish'),
			'meta_query'=> array(
			'relation' => 'OR',
			array(
				'key'     => 'visibility',
				'value'   => 'Both',
				'compare' => '='
			),
			array(
				'key'     => 'visibility',
				'value'   => 'Mobile',
				'compare' => '='
			)),
			'posts_per_page' => -1);
			if(isset($version) && !empty($version))
			{
				$vData = $this->getversionData($post_type,$version);
				if(isset($vData) && !empty($vData))
				{
					if($version == 'V0')
					{
						$conPart = array(array('column'  => 'post_modified_gmt','before' => $vData['to_date'],'inclusive' => true));
					}
					else
					{
						$conPart = array(array('column'  => 'post_modified_gmt','after' => $vData['from_date'],'before' => $vData['to_date'],'inclusive' => true));
					}
					
					if(isset($conPart))
					{
						$args['date_query'] = $conPart;
					}
				}
			}
			
		wp_reset_query();
		if(isset($args) && !empty($args))
		{
			$recArr = array();
			query_posts($args);			
			if (have_posts()):
				while (have_posts()): the_post();
					$recItem = array();
					$recItem['recipe_id'] = get_the_ID();		
					$catIds = wp_get_post_terms(get_the_ID(), 'mobile-recipe-category', array("fields" => "ids"));
					$filterMIterms = get_term_children(385,'recipe-filter');
					$filterIds = wp_get_post_terms(get_the_ID(), 'recipe-filter', array("fields" => "ids"));
					$newfilterIds = array();
					foreach($filterIds as $fid)
					{
						if(@in_array($fid,$filterMIterms))
						{
							array_push($newfilterIds,$fid);
						}
					}
					$recItem['recipe_category_id'] = $catIds;
					$recItem['recipe_cat_id'] = $catIds[0];
					$rCat = get_term_by('id', $catIds[0], 'mobile-recipe-category');
					$recItem['recipe_cat_name'] = !empty($rCat->name)?$rCat->name:null;
					$recItem['gated'] = (!empty(get_field('gated')))?1:0;
					$recItem['recipe_filter_id'] = $newfilterIds;
					$recItem['recipe_title'] = html_entity_decode(get_the_title());
					//$recItem['recipe_desc'] = (!empty(get_the_content()))?get_the_content():null;
					$recItem['recipe_desc'] = (!empty(get_the_content()))?html_entity_decode(wp_strip_all_tags(get_the_content())):null;
					if ( has_post_thumbnail())
					{
						//$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),'original');
						$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),'boxes-with-image');
						$image_url = $large_image_url[0];
					}
					$recItem['recipe_image'] = (!empty($image_url))?$image_url:null;
					$recAdditionalInfo = array();
					$recAdditionalInfo['prep_time']=(!empty(get_field('prep_time')))?get_field('prep_time'):null;
					$recAdditionalInfo['cook_time']=(!empty(get_field('cook_time')))?get_field('cook_time'):null;
					$recAdditionalInfo['serves_makes']=(!empty(get_field('serves__makes')))?get_field('serves__makes'):null;
					$recAdditionalInfo['yield']=(!empty(get_field('yield')))?get_field('yield'):null;
					$recAdditionalInfo['suitable_freezing']= (!empty(get_field('suitable_for_freezing')))?true:false;
					$recItem['recipe_additional_info'] = $recAdditionalInfo;													
					$recItem['recipe_allergens_dietreq'] = $this->recItemAlg(get_the_terms(get_the_ID(), 'allergen'));
					
					$recipe_video="";
					$videoData = get_field('video');
					if(isset($videoData) && !empty($videoData))
					{
							$recipe_video = $this->getVideo_Url($videoData);	
					}
					
					$recItem['recipe_video'] = (!empty($recipe_video))?$recipe_video : null;				
					$recItem['recipe_related_content'] = $this->recipesRelatedItems(get_field('apps_&_books'));
					
					$ingrArr=array();
					if (have_rows('ingredient_sections')):		
						while (have_rows('ingredient_sections')): the_row();
							$lines['title'] = (!empty(get_sub_field('title')))?get_sub_field('title'):null;
							$itemsIngre = explode("\n", get_sub_field('ingredients'));
							$itemsIngre = preg_replace("/<br\W*?\/>/", "",$itemsIngre);
							$itemsIngre = implode("<br/>",$itemsIngre);
							$lines['items'] =  html_entity_decode($itemsIngre);
						$ingrArr[] = $lines;
						endwhile;
					$recItem['recipe_ingredients'] = $ingrArr;
					endif;
					$recipe_method="";
					if (get_field('method'))
					{					
						$recipe_method = $this->li2br(get_field('method'));					
					}
					$recItem['recipe_method'] = (!empty($recipe_method))?html_entity_decode($recipe_method):null;
					$recItem['recipe_featured'] = (get_field('recipe_featured'))? true:false;
					$recItem['recipe_complexity_rating'] = (get_field('complexity_rating'))? (int)get_field('complexity_rating'):null;
					array_push($records,$recItem);	
				endwhile;
				endif;
				$recArr['records'] = $records;
				if(isset($version) && $version!='V0'):
					$deletedItems = $this->deleted_items($post_type,$version);
					$recArr['deleted_items'] = $deletedItems;
				endif;		
	}
	else
	{
		$recArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
		header("HTTP/1.0 400 Bad Request");	
	}
	$this->response($recArr);		
	}

	public function getVideo_Url($videoContent)
	{
		if(isset($videoContent) && !empty($videoContent))
		{
			$videoHtml = $this->load($videoContent);				
			if(isset($videoHtml->find('a',0)->href))
			{
				$video_id = url_to_postid($videoHtml->find('a',0)->href);
				$videoIframe = get_field('video_url',$video_id);
				$videoHtml2 = $this->load($videoIframe);
				$video_embedded_url = $videoHtml2->find('iframe',0)->src;
				$video_url = preg_replace('/\?.*/', '', $video_embedded_url);
				return $video_url;
			}
		}	
	}
	
	public function recItemAlg($selectedTerms)
	{
		$recItemArr =array();
		$algArr = array();
		if (isset($selectedTerms) && !empty($selectedTerms))
		{
			foreach ($selectedTerms as $term)
			{
				$algArr[]= $term->term_id;
			}
		}
		$AllAlgTerms = get_terms(['taxonomy' => 'allergen','hide_empty' => false]);
		foreach($AllAlgTerms as $termItem)
		{
			if(in_array($termItem->term_id,$algArr))
			{
				$recItemArr[$termItem->name] = true;
			}
			else
			{
				$recItemArr[$termItem->name] = false;
			}
		}
		
		return $recItemArr;
					
	}
	
	public function recipesRelatedItems($ids)
	{
		$idsWithPostType = $this->getIdsByObject($ids);
		$recipesRelatedItems = array();
		$keyReplacement = array('apps-books'=>"books",'post'=>'articles','page'=>'page','recipes'=>'recipes','products'=>'products','videos'=>'videos');
		foreach($keyReplacement as $key=>$val)
		{
			$recipesRelatedItems[$val] = (!empty($idsWithPostType[$key]))?$idsWithPostType[$key]:array();
		}
		return $recipesRelatedItems;
	}
	
	public function getIdsByObject($objects)
	{
		$resArrData = array();
		if(isset($objects) && !empty($objects))
		{
			foreach($objects as $rr)
			{
				$resArrData[$rr->post_type][] =$rr->ID;
			}
			
		}
		return $resArrData;
	}
	
	public function li2br($content)
	{
		//$content = preg_replace('/[^(\x20-\x7F)]*/','', $content); //preg_replace( "~\x{00a0}~siu", " ",$content);
		$content =  preg_replace('#<(/?(?:ul|ol))(?: +[a-z]+="[^"]*")*>#is', '$2', $content);
		$content =  preg_replace('#<li(.*?)>(.*?)</li>#is', '$2<br/>', $content);
		//return html_entity_decode($content);
		return $content;
	}
	
	public function get_detail($id)
	{
		$recArr = array();
		if(isset($id) && !empty($id))
		{
			wp_reset_query();
			$args = array('post_type' => 'recipes', 'p' => $id, 'post_status' => array( 'mobile', 'publish'),
			'meta_query'=> array(
				'relation' => 'OR',
				array(
					'key'     => 'visibility',
					'value'   => 'Both',
					'compare' => '='
				),
				array(
					'key'     => 'visibility',
					'value'   => 'Mobile',
					'compare' => '='
				),
			),
			);
			query_posts( $args );
			if(have_posts()):
				while(have_posts()): the_post();
					$recItem = array();
					$recItem['recipe_id'] = get_the_ID();		
					$catIds = wp_get_post_terms(get_the_ID(), 'mobile-recipe-category', array("fields" => "ids"));
					$recItem['recipe_category_id'] = $catIds;
					$recItem['recipe_cat_id'] = $catIds[0];
					$rCat = get_term_by('id', $catIds[0], 'mobile-recipe-category');
					$recItem['recipe_cat_name'] = !empty($rCat->name)?$rCat->name:null;
					$recItem['gated'] = (!empty(get_field('gated')))?1:0;
					$recItem['recipe_title'] = html_entity_decode(get_the_title());
					//$recItem['recipe_desc'] = (!empty(get_the_content()))?get_the_content():null;
					$recItem['recipe_desc'] = (!empty(get_the_content()))?html_entity_decode(wp_strip_all_tags(get_the_content())):null;
					if ( has_post_thumbnail())
					{
						$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),'boxes-with-image');
						$recItem['recipe_image'] = $large_image_url[0];
					}		
					$recAdditionalInfo = array();
					if(get_field('prep_time')) { $recAdditionalInfo['prep_time']=get_field('prep_time'); }
					if(get_field('cook_time')) { $recAdditionalInfo['cook_time']=get_field('cook_time'); }
					if(get_field('serves__makes')) { $recAdditionalInfo['serves_makes']=get_field('serves__makes'); }
					if(get_field('yield')) { $recAdditionalInfo['yield']=get_field('yield'); }
					if(get_field('suitable_for_freezing')) { $sf = 'true'; } else { $sf = 'false'; }
					$recAdditionalInfo['suitable_freezing']= $sf;
					$recItem['recipe_additional_info'] = $recAdditionalInfo;
					
					$recItemAlg =array();
					if ($terms = get_the_terms(get_the_ID(), 'allergen')): 
						foreach ($terms as $term):
						$algArr = array();
						$algArr['adr_name']=$term->name;
						$algArr['adr_icon']=get_field('icon', $term)['sizes']['icon'];
						$algArr['adr_icon_hover']=get_field('hover__active_icon', $term)['sizes']['icon'];
						array_push($recItemAlg,$algArr);
						endforeach;
					endif;										
					$recItem['recipe_allergens_dietreq'] = $recItemAlg;
					
					$recipe_video="";
					$videoData = get_field('video');
					if(isset($videoData) && !empty($videoData))
					{
							$recipe_video = $this->getVideo_Url($videoData);	
					}
					$recItem['recipe_video'] = $recipe_video;
					$recipesRelatedItems = array();
					if(get_field('apps_&_books'))
					{
						$recipes_related = get_field('apps_&_books');
						foreach($recipes_related as $rr)
						{
							array_push($recipesRelatedItems,$rr->ID);
						}
					}
					$recItem['recipe_related_content'] = $recipesRelatedItems;
					
					$ingrArr=array();
					if (have_rows('ingredient_sections')):		
						while (have_rows('ingredient_sections')): the_row();
							if (get_sub_field('title')):
								$lines = explode("\n", get_sub_field('ingredients'));
								$lines = preg_replace("/<br\W*?\/>/", "",$lines);
								$lines = implode("<br/>",$lines);
								$ingrArr[get_sub_field('title')] = $lines;
							endif;		
						endwhile;
					$recItem['recipe_ingredients'] = html_entity_decode($ingrArr);
					endif;
					$recipe_method="";
					if (get_field('method'))
					{	
						
						$recipe_method = $this->li2br(get_field('method'));					
					}
					$recItem['recipe_method'] = html_entity_decode($recipe_method);
					array_push($recArr,$recItem);
				endwhile;
				endif;
		}
		else
		{
			$recArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'Invalid Request');
			header("HTTP/1.0 400 Bad Request");	
		}
		
		$this->response($recArr);
		
	}	
	
	public function get_categories()
	{		
		$args = array(
			'taxonomy' => 'mobile-recipe-category',
			'hide_empty' => false,
		);
		$terms = get_terms($args);
		if(isset($terms) && !empty($terms))
		{
			$catColl = array();
			foreach($terms as $term)
			{
					
				//$recipe_count = $query->post_count;				
				$catItem = array();
				$imgArr = get_field('image', $term);
				$catItem['recipe_category_id'] = $term->term_id;
				$catItem['recipe_category_title'] = html_entity_decode($term->name);
				$catItem['recipe_category_image'] = $imgArr['url'];
				//$catItem['recipe_category_content'] = $term->description;
				$catItem['recipe_category_order'] = (int)$term->term_order;
				$catItem['recipe_category_price'] = (!empty(get_field('price', $term)))?(string)get_field('price',$term):'0';
				$catItem['recipe_category_feature_content'] = (!empty(get_field('feature_content1', $term)))?strtoupper(get_field('feature_content1',$term)):'';
				$catItem['recipe_category_feature_content2'] = (!empty(get_field('feature_content2', $term)))?get_field('feature_content2',$term):'';
				
				wp_reset_query();				
				$args = array('post_type' => 'recipes',
				'posts_per_page' => -1,
				'post_status' => array( 'mobile', 'publish'),
					'tax_query' => array(
						array(
							'taxonomy' => 'mobile-recipe-category',
							'terms' => $term->slug,
							'field' => 'slug'
						),
					),
					'meta_query'=> array(
					'relation' => 'OR',
					array(
						'key'     => 'visibility',
						'value'   => 'Both',
						'compare' => '='
					),
					array(
						'key'     => 'visibility',
						'value'   => 'Mobile',
						'compare' => '='
					)),				
					);
				// The Query
				$recipes_query = new WP_Query( $args );
				$catItem['recipe_count'] = $recipes_query->post_count;
				array_push($catColl,$catItem);
				
			}
				
		}
		//print_r($catColl);		
		$this->response($catColl);
	}
	
	public function get_filters()
	{	
		$taxonomy = 'recipe-filter';
		$terms = get_term_children(385,$taxonomy);
		if(isset($terms) && !empty($terms))
		{
			$filterColl = array();
			foreach($terms as $term_id)
			{
				$term = get_term( $term_id, $taxonomy );
				$filterItem = array();
				$filterItem['recipe_filter_id'] = $term->term_id;
				$filterItem['recipe_filter_title'] = html_entity_decode($term->name);
				$filterItem['recipe_filter_id_parent'] = $term->parent;				
				array_push($filterColl,$filterItem);
			}				
		}	
		$this->response($filterColl);
	}

	public function get_category($id)
	{
		$term = get_term( $id, 'mobile-recipe-category' );
		if(isset($term) && !empty($term))
		{		
				$catItem = array();
				$imgArr = get_field('image', $term);
				$catItem['recipe_category_id'] = $term->term_id;
				$catItem['recipe_category_title'] = html_entity_decode($term->name);
				//$catItem['recipe_category_content'] = $term->description;
				$catItem['recipe_category_image'] = $imgArr['url'];
				$catItem['parent_recipe_category_id'] = $term->parent;
				$catItem['recipe_category_price'] = (!empty(get_field('price', $term)))?(string)get_field('price',$term):'0';
				$catItem['recipe_category_feature_content'] = (!empty(get_field('feature_content1', $term)))?strtoupper(get_field('feature_content1',$term)):'';
				$catItem['recipe_category_feature_content2'] = (!empty(get_field('feature_content2', $term)))?get_field('feature_content2',$term):'';
				$catCollectionItems =array();
				wp_reset_query(); 
				
				$args = array('post_type' => 'recipes',
				'posts_per_page' => -1,
				'post_status' => array( 'mobile', 'publish'),
					'tax_query' => array(
						array(
							'taxonomy' => 'mobile-recipe-category',
							'terms' => $term->slug,
							'field' => 'slug'
						),
					),
					'meta_query'=> array(
					'relation' => 'OR',
					array(
						'key'     => 'visibility',
						'value'   => 'Both',
						'compare' => '='
					),
					array(
						'key'     => 'visibility',
						'value'   => 'Mobile',
						'compare' => '='
					)),				
					);
				wp_reset_query();
				// The Query
				$recipes_query = new WP_Query( $args );
				
				$catItem['recipe_count'] = $recipes_query->post_count;
				// The Loop
				if ( $recipes_query->have_posts() ):				
					while ($recipes_query->have_posts()): $recipes_query->the_post();
						$recItem = array();
						$recItem['recipe_id'] = get_the_ID();		
						$catIds = wp_get_post_terms(get_the_ID(), 'mobile-recipe-category', array("fields" => "ids"));
						$recItem['recipe_category_id'] = $catIds;
						$recItem['recipe_cat_id'] = $catIds[0];
						$rCat = get_term_by('id', $catIds[0], 'mobile-recipe-category');
						$recItem['recipe_cat_name'] = !empty($rCat->name)?$rCat->name:null;
						$recItem['gated'] = (!empty(get_field('gated')))?1:0;
						$recItem['recipe_title'] = html_entity_decode(get_the_title());
						$recItem['recipe_desc'] = html_entity_decode(get_the_content());
						if ( has_post_thumbnail())
						{
							$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id());
							$recItem['recipe_image'] = $large_image_url[0];
						}		
						$recAdditionalInfo = array();
						if(get_field('prep_time')) { $recAdditionalInfo['prep_time']= get_field('prep_time'); }
						if(get_field('cook_time')) { $recAdditionalInfo['cook_time']= get_field('cook_time'); }

						if(get_field('serves__makes')) { $recAdditionalInfo['serves_makes']= ucfirst(get_field('serves__makes')); }
						if(get_field('yield')) { $recAdditionalInfo['yield']=get_field('yield'); }
						if(get_field('suitable_for_freezing')) { $sf = 'true'; } else { $sf = 'false'; }
						$recAdditionalInfo['suitable_freezing']= $sf;
						$recItem['recipe_additional_info'] = $recAdditionalInfo;
						
						$recItemAlg =array();
						if ($terms = get_the_terms(get_the_ID(), 'allergen')): 
							foreach ($terms as $term):
							$algArr = array();
							$algArr['adr_name']=$term->name;
							$algArr['adr_icon']=get_field('icon', $term)['sizes']['icon'];
							$algArr['adr_icon_hover']=get_field('hover__active_icon', $term)['sizes']['icon'];
							array_push($recItemAlg,$algArr);
							endforeach;
						endif;										
						$recItem['recipe_allergens_dietreq'] = $recItemAlg;
						
						$recipe_video="";
						if(get_field('video'))
						{
							$videoHtml = $this->load(get_field('video'));					
							if($videoHtml->find('iframe',0))
							{
								$recipe_video = $videoHtml->find('iframe',0)->src;
							}
							elseif($videoHtml->find('a',0))
							{
								$recipe_video = $videoHtml->find('a',0)->href;
							}
							else{
								$recipe_video = get_field('video');
							}					
						}
						$recItem['recipe_video'] = $recipe_video;
						$recipesRelatedItems = array();
						if(get_field('apps_&_books'))
						{
							$recipes_related = get_field('apps_&_books');
							foreach($recipes_related as $rr)
							{
								array_push($recipesRelatedItems,$rr->ID);
							}
						}
						$recItem['recipe_related_content'] = $recipesRelatedItems;
						
						$ingrArr=array();
						if (have_rows('ingredient_sections')):		
							while (have_rows('ingredient_sections')): the_row();
								if (get_sub_field('title')):
									$lines = explode("\n", get_sub_field('ingredients'));
									$lines = preg_replace("/<br\W*?\/>/", "",$lines);
									$lines = implode("<br/>",$lines);
									$ingrArr[get_sub_field('title')] = html_entity_decode($lines);
								endif;		
							endwhile;
						$recItem['recipe_ingredients'] = $ingrArr;
						endif;
						$recipe_method="";
						if (get_field('method'))
						{					
							$recipe_method = $this->li2br(get_field('method'));					
						}
						$recItem['recipe_method'] = html_entity_decode($recipe_method);						
						array_push($catCollectionItems,$recItem);
					endwhile;
				endif;
				wp_reset_query(); 
				$catItem['recipies'] = $catCollectionItems;
		}
		$this->response($catItem);
	}
	
	public function latest_category()
	{
		$args = array(
			'taxonomy' => 'mobile-recipe-category',
			'hide_empty' => false,
			'orderby'=>'id',
			'order'=>'DESC'
		);
		$terms = get_terms($args);
		
		
		$catItem = array();
		if(isset($terms) && !empty($terms))
		{
			$newterms = array();
			foreach($terms as $term)
			{
				$newterms[$term->term_id] = $term;
			}
			
				rsort($newterms);
				$term = $newterms[0];				
				$imgArr = get_field('image', $term);
				$catItem['recipe_category_id'] = $term->term_id;
				$catItem['recipe_category_title'] = html_entity_decode($term->name);
				$catItem['recipe_category_image'] = $imgArr['url'];
				//$catItem['recipe_category_content'] = $term->description;
				$catItem['recipe_category_order'] = (int)$term->term_order;
				$catItem['recipe_category_price'] = (!empty(get_field('price', $term)))?(float)get_field('price',$term):0;
				$catItem['recipe_category_feature_content'] = (!empty(get_field('feature_content', $term)))?get_field('feature_content',$term):'';
		}
		$this->response($catItem);
	}
	
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}
}
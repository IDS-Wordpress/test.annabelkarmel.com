<?php
include('auth_api.php');
class API_USER extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	public function __construct($u,$p)
	{		
		$this->auth_username = $u;
		$this->auth_password = $p;	
	}
	
	public function authRequest()
	{
		$this->resArr = $this->authorize($this->auth_username,$this->auth_password);
		if(!empty($this->resArr))
		{
			$this->response($this->resArr);
			header("HTTP/1.0 401 Unauthorized");
			die;
		}
		else
		{
			$userInfo['username'] = $this->auth_username;
			$userInfo['password'] = $this->auth_password;
			return $userInfo;
		}
	}
	
	public function validate_userdata($data)
	{
		$flag = 1;
		if(!is_email($data['user_email']))
		{
			$flag = 0;
		}
		elseif (!empty($data['user_dob']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['user_dob']))
		{
			$flag = 0;
		}
		elseif ($data['user_pregnant']==true && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['user_duedate']))
		{
			$flag = 0;
		}
		elseif ($data['user_children']==true)
		{
			$child_dob = explode(",",$data['user_child_dob']);
			foreach($child_dob as $dob)
			{
				if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dob))
				{
					$flag = 0;
				}
			}
		}
		else{
			$flag = 1;
		}
		
		if($flag)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public function register_user($data)
	{	
		if($this->validate_userdata($data))
		{
			if ( !email_exists( $data['user_email'] ) )
			{
				$userdata = array(
				'user_login' =>  $data['user_email'],
				'user_email' =>  $data['user_email'],
				'user_pass'  =>  $data['user_password'],
				'first_name' => $data['user_forename'],
				'last_name' => $data['user_surname']
				);
				$user_id = wp_insert_user( $userdata );
				// On success.
				if ( !is_wp_error( $user_id ) )
				{
					$this->add_usermeta($user_id,$data);						
					$this->resArr = array('status'=>'success','response_code'=>200,'response_msg'=>'User created successfully');
					header("HTTP/1.0 201 Created");
				}
				else
				{
					$error_string = $user_id->get_error_message();
					$this->resArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
					header("HTTP/1.0 400 Bad Request");					
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>600,'response_msg'=>'User already exists');
				header("HTTP/1.0 400 Bad Request");
			}
			
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
			header("HTTP/1.0 400 Bad Request");
		}
		$this->response($this->resArr);
	}
	public function authorize_user()
	{
		//$userInfo = $this->authRequest();
		//echo $this->auth_username.'------'.$this->auth_password; die;
		
		$data['user_email'] = $this->auth_username;  //$userInfo['username'];
		$data['user_password'] = $this->auth_password;  //$userInfo['password'];
		
		if(!empty($data['user_email']))
		{
			if ( email_exists( $data['user_email']) )
			{
				$user_wp = wp_authenticate( $data['user_email'], $data['user_password'] );
				if(!is_wp_error( $user_wp ))
				{
					$this->resArr =	array('status'=>'success','response_code'=>200,'response_msg'=>'User logged in successfully','userdata'=>$this->get_userinfo($user_wp->ID));	
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid Password');
					header("HTTP/1.0 401 Unauthorized");
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'Invalid Email');
				header("HTTP/1.0 401 Unauthorized");
			}
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>603,'response_msg'=>'Invalid email and password combination');
			header("HTTP/1.0 401 Unauthorized");
		}
		$this->response($this->resArr);
	}
	public function user_forgotpassword($data)
	{
		//$userInfo = $this->authRequest();		
		//$data['user_email'] = $userInfo['username'];
		
		if(!empty($data['user_email']))
		{
			$user_email = $data['user_email'];
			if ( email_exists( $user_email ) )
			{
				$user = get_user_by( 'email', $user_email );
				if(!empty($user))
				{
					$user_login = $user->user_login;
					global $wp_hasher, $wpdb;
		
					if (!$wp_hasher) {
						require_once ABSPATH . WPINC . '/class-phpass.php';
						
						$wp_hasher = new \PasswordHash(8, true);
					}
					
					$wpdb->update($wpdb->users, [
						'user_activation_key' => time() . ':' . $wp_hasher->HashPassword($key = wp_generate_password(20, false))
					], [
						'user_login' => $user->user_login
					]);
					
					$message = __( 'Someone requested that the password be reset for the following account:' ) . "\r\n\r\n";
					$message .= sprintf( __( 'Username: %s' ), $user->user_login ) . "\r\n\r\n";
					$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
					$message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
					$message .= '<' . network_site_url( "/forgot-password/reset-password/?action=rp&method=gf&key=$key&login=" . rawurlencode( $user->user_login ), 'login' ) . ">\r\n";	
					
					if ( is_multisite() ) {
						$blogname = $GLOBALS['current_site']->site_name;
					} else {
						$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
					}					
					$title = sprintf( __( '[%s] Password Reset' ), $blogname );					
					$title = apply_filters( 'retrieve_password_title', $title );
					$message = apply_filters( 'retrieve_password_message', $message, $key, $user->user_login, $user );
					if ($message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ))
					{
						$this->resArr = array('status'=>'fail','response_code'=>604,'response_msg'=>'Password reset email failed to send');
						header("HTTP/1.0 503 Service Unavailable");	
					}
					else					
					{
						$this->resArr = array('status'=>'success','response_code'=>200,'response_msg'=>'Forgotten password request sent successfully');
					}
				}
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'User email does not exist');
				header("HTTP/1.0 400 Bad Request");	
			}
		}
		else
		{
			$this->resArr = array('status'=>'error','response_code'=>400,'response_msg'=>'Email ID required');
			header("HTTP/1.0 400 Bad Request");	
		}
		$this->response($this->resArr);
	}
	
	public function get_userinfo($id)
	{
		$user = get_user_by( 'id', $id);
		if(!empty($user))
		{
			$userdata = array();
			$userdata['user_id'] = $user->id;
			$userdata['user_email'] = ($user->user_email)?$user->user_email:null;
			//$userdata['user_login'] = $user->user_login;
			$userdata['user_title'] = (get_user_meta( $user->id, 'title', true))?get_user_meta( $user->id, 'title', true):null;
			$userdata['user_forename'] = ($user->first_name)?$user->first_name:null;
			$userdata['user_surname'] = ($user->last_name)?$user->last_name:null;
			$dob = date('Y-m-d',strtotime(get_user_meta( $user->id, 'date_of_birth', true)));
			$userdata['user_dob'] = ($dob!='1970-01-01')?$dob:null;
			$userdata['user_pregnant'] = get_user_meta( $user->id, 'are_you_pregnant', true)?true:false;
			if(get_user_meta( $user->id, 'are_you_pregnant', true))
			{
				$userdata['user_duedate'] = date('Y-m-d',strtotime(get_user_meta( $user->id, 'when_are_you_expecting', true)));
			}			
			$userdata['user_children'] = get_user_meta( $user->id, 'do_you_have_any_children', true)?true:false;
			
			if(get_user_meta( $user->id, 'do_you_have_any_children', true))
			{
				$childCount = get_user_meta( $user->id, 'children', true);
				//$userdata['children'] = $childCount;
				$arrChilDOB = array();
				if($childCount>0)
				{
					for($c=0; $c<$childCount; $c++)
					{
						array_push($arrChilDOB,date('Y-m-d',strtotime(get_user_meta( $user->id, 'children_'.$c.'_childs_birthday',true))));
					}
				}
				$userdata['user_child_dob'] = implode(",",$arrChilDOB);	
			}					
			$userdata['user_address1'] = (get_user_meta( $user->id, 'address_line_1', true))?get_user_meta( $user->id, 'address_line_1', true):null;
			$userdata['user_address2'] = (get_user_meta( $user->id, 'address_line_2', true))?get_user_meta( $user->id, 'address_line_2', true):null;
			$userdata['user_city'] = (get_user_meta( $user->id, 'city', true))?get_user_meta( $user->id, 'city', true):null;
			$userdata['user_postcode'] = (get_user_meta( $user->id, 'postcode', true))?get_user_meta( $user->id, 'postcode', true):null;
			$userdata['user_country'] = (get_user_meta( $user->id, 'country', true))?get_user_meta( $user->id, 'country', true):null;
		}
		return $userdata;
		
	}
	public function user_information()
	{
		$userInfo = $this->authRequest();
		$data['user_email'] = $userInfo['username'];
		if(!empty($data['user_email']))
		{
			if ( email_exists( $data['user_email'] ) )
			{
				$userbyemail = get_user_by( 'email', $data['user_email'] );
				$id = $userbyemail->ID;
				if($this->get_userinfo($id))
				{
					$this->resArr = $this->get_userinfo($id);
				}
				else
				{
					$this->resArr = array('status'=>'error','response_code'=>601,'response_msg'=>'Invalid data format');
					header("HTTP/1.0 400 Bad Request");	
				}				
			}
			else
			{
				$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'User email does not exist');
				header("HTTP/1.0 400 Bad Request");	
			}
		}
		else
		{
			$this->resArr = array('status'=>'error','response_code'=>601,'response_msg'=>'Invalid data format');
			header("HTTP/1.0 400 Bad Request");	
		}	
		$this->response($this->resArr);
	}
	public function update_user($data)
	{
		$userInfo = $this->authRequest();		
		$data['user_email'] = $userInfo['username'];
		if($this->validate_userdata($data))
		{
			if(!empty($data['user_email']))
			{
				if ( email_exists( $data['user_email'] ) )
			    {
					$userbyemail = get_user_by( 'email', $data['user_email'] );
					$userdata['ID'] = $userbyemail->ID;
					if(isset($data['user_forename']) && !empty($data['user_forename']))
					{
						$userdata['first_name'] = $data['user_forename'];
					}
					if(isset($data['user_surname']) && !empty($data['user_surname']))
					{
						$userdata['last_name'] = $data['user_surname'];
					}					
					$user_id = wp_update_user($userdata);
					
					if ( !is_wp_error( $user_id ) )
					{
						if(isset($data['user_password']) && !empty($data['user_password']))
						{
							wp_set_password( $data['user_password'], $user_id );
						}	
						$this->update_usermeta($user_id,$data);
						$this->resArr = array('status'=>'success','response_code'=>200,'response_msg'=>'Profile updated successfully');
					}
					else
					{
						$error_string = $user_id->get_error_message();
						$this->resArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
						header("HTTP/1.0 400 Bad Request");	
					}
				}
				else
				{
					$this->resArr = array('status'=>'fail','response_code'=>602,'response_msg'=>'User email does not exist');
					header("HTTP/1.0 400 Bad Request");	
				}
			}
			else
			{
				$this->resArr = array('status'=>'error','response_code'=>601,'response_msg'=>'Invalid data format');
				header("HTTP/1.0 400 Bad Request");	
			}
		}
		else
		{
			$this->resArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
			header("HTTP/1.0 400 Bad Request");	
		}
		$this->response($this->resArr);
	}
	public function add_usermeta($user_id,$data)
	{
		add_user_meta( $user_id, 'title', $data['user_title']);
		add_user_meta( $user_id, 'date_of_birth', date('Ymd',strtotime($data['user_dob'])));
		$isPregnant = ($data['user_pregnant']==true || $data['user_pregnant']=='true')?1:0;
		add_user_meta( $user_id, 'are_you_pregnant',$isPregnant);
		if($isPregnant)	{ add_user_meta( $user_id, 'when_are_you_expecting', date('Ymd',strtotime($data['user_duedate'])));	}
		$hasChildren = ($data['user_children']==true || $data['user_children']=='true')?1:0;
		add_user_meta( $user_id, 'do_you_have_any_children', $hasChildren);
		if($hasChildren)
		{
			if(isset($data['user_child_dob']) && !empty($data['user_child_dob']))
			{
				$children = explode(",",$data['user_child_dob']);
			}
			$childCount = count($children);
			add_user_meta( $user_id, 'children', $childCount);
			if($childCount>0)
			{
				for($c=0; $c<$childCount; $c++)
				{
					add_user_meta( $user_id, 'children_'.$c.'_childs_birthday', date('Ymd',strtotime($children[$c])));
				}
			}
		}		
		add_user_meta( $user_id, 'address_line_1', $data['user_address1']);
		add_user_meta( $user_id, 'address_line_2', $data['user_address2']);
		add_user_meta( $user_id, 'city', $data['user_city']);
		add_user_meta( $user_id, 'postcode', $data['user_postcode']);
		add_user_meta( $user_id, 'country', $data['user_country']);
		add_user_meta( $user_id, 'source', 'Mobile');
	}
	public function update_usermeta($user_id,$data)
	{
		if (!empty($data['user_title']))
		{
			update_user_meta( $user_id, 'title', $data['user_title']);
		}
		if (!empty($data['user_dob']))
		{
			update_user_meta( $user_id, 'date_of_birth', date('Ymd',strtotime($data['user_dob'])));
		}
		if (isset($data['user_pregnant']) && $data['user_pregnant']==true)
		{
			update_user_meta( $user_id, 'are_you_pregnant',1);
			if (!empty($data['user_duedate']))
			{
				update_user_meta( $user_id, 'when_are_you_expecting', date('Ymd',strtotime($data['user_duedate'])));
			}
		}
		else
		{
			update_user_meta( $user_id, 'are_you_pregnant',0);
		}
		
		if (isset($data['user_children']) && $data['user_children']==true)
		{
			update_user_meta( $user_id, 'do_you_have_any_children', 1);
			if(isset($data['user_child_dob']) && !empty($data['user_child_dob']))
			{
				$children = explode(",",$data['user_child_dob']);
			}
			$childCount = count($children);
			if($childCount>0)
			{
				update_user_meta( $user_id, 'children', $childCount);
				for($c=0; $c<$childCount; $c++)
				{
					
					if (!empty($children[$c]))
					{
						update_user_meta( $user_id, 'children_'.$c.'_childs_birthday', date('Ymd',strtotime($children[$c])));
					}
				}
			}
		}
		else
		{
			update_user_meta( $user_id, 'do_you_have_any_children', 0);
		}

		if (!empty($data['user_address1']))
		{
			update_user_meta( $user_id, 'address_line_1', $data['user_address1']);
		}
		if (!empty($data['user_address2']))
		{
			update_user_meta( $user_id, 'address_line_2', $data['user_address2']);
		}
		if (!empty($data['user_city']))
		{
			update_user_meta( $user_id, 'city', $data['user_city']);
		}
		if (!empty($data['user_postcode']))
		{
			update_user_meta( $user_id, 'postcode', $data['user_postcode']);
		}
		if (!empty($data['user_country']))
		{
			update_user_meta( $user_id, 'country', $data['user_country']);
		}
	}
	
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}

}
?>
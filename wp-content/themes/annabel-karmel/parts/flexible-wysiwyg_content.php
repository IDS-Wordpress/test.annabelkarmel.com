
<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
?>
<div class="wysiwyg-content itemprop="description <?php if($isLoginRequired) { echo 'loginRequired'; }?>"<?php if ($background_colour = get_sub_field('background_colour')) echo ' style="background-color: ' . $background_colour . ';"'; ?>>
<?php if($isLoginRequired) { ?>
    <div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
    <?php
    }
    ?>

	<?php the_sub_field('wysiwyg_content'); ?>
	<div class="clear"></div>
</div>

<?php if (!$background_colour): ?>
<div class="clear"></div>
<?php endif; ?>
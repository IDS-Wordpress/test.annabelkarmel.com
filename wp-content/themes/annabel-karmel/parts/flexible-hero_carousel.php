<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
if($isLoginRequired) { ?>
<div class="loginRequired"> 
<div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
     <?php
    }
    ?>
<?php if ($slides = get_sub_field('slides')): ?>
<div class="owl-carousel owl-theme hero-carousel" data-items="1" data-interval="<?php echo get_sub_field('slide_interval'); ?>">
	<?php foreach ($slides as $image): ?>
	<div class="item">    
		<a href="<?php echo $image['slide_url']; ?>"><img alt="" src="<?php echo $image['slide_image']; ?>"></a>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>
 <?php if($isLoginRequired) { ?>
</div>
     <?php
    }
    ?>
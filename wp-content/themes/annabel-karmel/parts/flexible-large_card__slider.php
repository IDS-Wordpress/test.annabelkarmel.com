

<div class="recipe_box slider_large_wapper">
  
  
   <?php if(get_sub_field('heading')){ ?><div class="recipe-filter"><h2><?php echo get_sub_field('heading'); ?></h2></div> <?php } ?>
  
  <div class="owl-carousel recipes_slide">
<?php while (have_rows('fp_content')): the_row(); ?>
						<?php $post = get_sub_field('content'); setup_postdata($post); ?>
    <div class="item">
     <?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false, true); ?>
      
      </div>
      
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
  </div>
  
<script>
$(document).ready(function(){
	
$('.recipes_slide').owlCarousel({
loop: true,      
nav: true, 
items: 1,        
responsiveClass: true,

});	
});
</script>  
</div>


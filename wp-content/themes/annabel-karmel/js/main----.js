! function (e, n, t) {
    function r(e, n) {
        return typeof e === n
    }

    function s() {
        var e, n, t, s, o, i, l;
        for (var a in C)
            if (C.hasOwnProperty(a)) {
                if (e = [], n = C[a], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))
                    for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase());
                for (s = r(n.fn, "function") ? n.fn() : n.fn, o = 0; o < e.length; o++) i = e[o], l = i.split("."), 1 === l.length ? w[l[0]] = s : (!w[l[0]] || w[l[0]] instanceof Boolean || (w[l[0]] = new Boolean(w[l[0]])), w[l[0]][l[1]] = s), g.push((s ? "" : "no-") + l.join("-"))
            }
    }

    function o(e) {
        var n = x.className,
            t = w._config.classPrefix || "";
        if (_ && (n = n.baseVal), w._config.enableJSClass) {
            var r = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
            n = n.replace(r, "$1" + t + "js$2")
        }
        w._config.enableClasses && (n += " " + t + e.join(" " + t), _ ? x.className.baseVal = n : x.className = n)
    }

    function i() {
        return "function" != typeof n.createElement ? n.createElement(arguments[0]) : _ ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments)
    }

    function l(e, n) {
        return !!~("" + e).indexOf(n)
    }

    function a(e) {
        return e.replace(/([a-z])-([a-z])/g, function (e, n, t) {
            return n + t.toUpperCase()
        }).replace(/^-/, "")
    }

    function f(e, n) {
        return function () {
            return e.apply(n, arguments)
        }
    }

    function u(e, n, t) {
        var s;
        for (var o in e)
            if (e[o] in n) return t === !1 ? e[o] : (s = n[e[o]], r(s, "function") ? f(s, t || n) : s);
        return !1
    }

    function p(e) {
        return e.replace(/([A-Z])/g, function (e, n) {
            return "-" + n.toLowerCase()
        }).replace(/^ms-/, "-ms-")
    }

    function d() {
        var e = n.body;
        return e || (e = i(_ ? "svg" : "body"), e.fake = !0), e
    }

    function c(e, t, r, s) {
        var o, l, a, f, u = "modernizr",
            p = i("div"),
            c = d();
        if (parseInt(r, 10))
            for (; r--;) a = i("div"), a.id = s ? s[r] : u + (r + 1), p.appendChild(a);
        return o = i("style"), o.type = "text/css", o.id = "s" + u, (c.fake ? c : p).appendChild(o), c.appendChild(p), o.styleSheet ? o.styleSheet.cssText = e : o.appendChild(n.createTextNode(e)), p.id = u, c.fake && (c.style.background = "", c.style.overflow = "hidden", f = x.style.overflow, x.style.overflow = "hidden", x.appendChild(c)), l = t(p, e), c.fake ? (c.parentNode.removeChild(c), x.style.overflow = f, x.offsetHeight) : p.parentNode.removeChild(p), !!l
    }

    function m(n, r) {
        var s = n.length;
        if ("CSS" in e && "supports" in e.CSS) {
            for (; s--;)
                if (e.CSS.supports(p(n[s]), r)) return !0;
            return !1
        }
        if ("CSSSupportsRule" in e) {
            for (var o = []; s--;) o.push("(" + p(n[s]) + ":" + r + ")");
            return o = o.join(" or "), c("@supports (" + o + ") { #modernizr { position: absolute; } }", function (e) {
                return "absolute" == getComputedStyle(e, null).position
            })
        }
        return t
    }

    function h(e, n, s, o) {
        function f() {
            p && (delete k.style, delete k.modElem)
        }
        if (o = !r(o, "undefined") && o, !r(s, "undefined")) {
            var u = m(e, s);
            if (!r(u, "undefined")) return u
        }
        for (var p, d, c, h, v, y = ["modernizr", "tspan", "samp"]; !k.style && y.length;) p = !0, k.modElem = i(y.shift()), k.style = k.modElem.style;
        for (c = e.length, d = 0; c > d; d++)
            if (h = e[d], v = k.style[h], l(h, "-") && (h = a(h)), k.style[h] !== t) {
                if (o || r(s, "undefined")) return f(), "pfx" != n || h;
                try {
                    k.style[h] = s
                } catch (e) {}
                if (k.style[h] != v) return f(), "pfx" != n || h
            }
        return f(), !1
    }

    function v(e, n, t, s, o) {
        var i = e.charAt(0).toUpperCase() + e.slice(1),
            l = (e + " " + E.join(i + " ") + i).split(" ");
        return r(n, "string") || r(n, "undefined") ? h(l, n, s, o) : (l = (e + " " + j.join(i + " ") + i).split(" "), u(l, n, t))
    }

    function y(e, n, r) {
        return v(e, t, t, n, r)
    }
    var g = [],
        C = [],
        S = {
            _version: "3.3.1",
            _config: {
                classPrefix: "",
                enableClasses: !0,
                enableJSClass: !0,
                usePrefixes: !0
            },
            _q: [],
            on: function (e, n) {
                var t = this;
                setTimeout(function () {
                    n(t[e])
                }, 0)
            },
            addTest: function (e, n, t) {
                C.push({
                    name: e,
                    fn: n,
                    options: t
                })
            },
            addAsyncTest: function (e) {
                C.push({
                    name: null,
                    fn: e
                })
            }
        },
        w = function () {};
    w.prototype = S, w = new w;
    var x = n.documentElement,
        _ = "svg" === x.nodeName.toLowerCase(),
        b = S._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
    S._prefixes = b;
    var P = "CSS" in e && "supports" in e.CSS,
        T = "supportsCSS" in e;
    w.addTest("supports", P || T);
    var z = "Moz O ms Webkit",
        E = S._config.usePrefixes ? z.split(" ") : [];
    S._cssomPrefixes = E;
    var j = S._config.usePrefixes ? z.toLowerCase().split(" ") : [];
    S._domPrefixes = j;
    var N = {
        elem: i("modernizr")
    };
    w._q.push(function () {
        delete N.elem
    });
    var k = {
        style: N.elem.style
    };
    w._q.unshift(function () {
        delete k.style
    }), S.testAllProps = v, S.testAllProps = y, w.addTest("cssfilters", function () {
        if (w.supports) return y("filter", "blur(2px)");
        var e = i("a");
        return e.style.cssText = b.join("filter:blur(2px); "), !!e.style.length && (n.documentMode === t || n.documentMode > 9)
    }), s(), o(g), delete S.addTest, delete S.addAsyncTest;
    for (var A = 0; A < w._q.length; A++) w._q[A]();
    e.Modernizr = w
}(window, document); +

function (t) {
    "use strict";

    function e(e) {
        var o = e.attr("data-target");
        o || (o = e.attr("href"), o = o && /#[A-Za-z]/.test(o) && o.replace(/.*(?=#[^\s]*$)/, ""));
        var n = o && t(o);
        return n && n.length ? n : e.parent()
    }

    function o(o) {
        o && 3 === o.which || (t(r).remove(), t(a).each(function () {
            var n = t(this),
                r = e(n),
                a = {
                    relatedTarget: this
                };
            r.hasClass("open") && (o && "click" == o.type && /input|textarea/i.test(o.target.tagName) && t.contains(r[0], o.target) || (r.trigger(o = t.Event("hide.bs.dropdown", a)), o.isDefaultPrevented() || (n.attr("aria-expanded", "false"), r.removeClass("open").trigger(t.Event("hidden.bs.dropdown", a)))))
        }))
    }

    function n(e) {
        return this.each(function () {
            var o = t(this),
                n = o.data("bs.dropdown");
            n || o.data("bs.dropdown", n = new d(this)), "string" == typeof e && n[e].call(o)
        })
    }
    var r = ".dropdown-backdrop",
        a = '[data-toggle="dropdown"]',
        d = function (e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    d.VERSION = "3.3.7", d.prototype.toggle = function (n) {
        var r = t(this);
        if (!r.is(".disabled, :disabled")) {
            var a = e(r),
                d = a.hasClass("open");
            if (o(), !d) {
                "ontouchstart" in document.documentElement && !a.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", o);
                var i = {
                    relatedTarget: this
                };
                if (a.trigger(n = t.Event("show.bs.dropdown", i)), n.isDefaultPrevented()) return;
                r.trigger("focus").attr("aria-expanded", "true"), a.toggleClass("open").trigger(t.Event("shown.bs.dropdown", i))
            }
            return !1
        }
    }, d.prototype.keydown = function (o) {
        if (/(38|40|27|32)/.test(o.which) && !/input|textarea/i.test(o.target.tagName)) {
            var n = t(this);
            if (o.preventDefault(), o.stopPropagation(), !n.is(".disabled, :disabled")) {
                var r = e(n),
                    d = r.hasClass("open");
                if (!d && 27 != o.which || d && 27 == o.which) return 27 == o.which && r.find(a).trigger("focus"), n.trigger("click");
                var i = " li:not(.disabled):visible a",
                    s = r.find(".dropdown-menu" + i);
                if (s.length) {
                    var p = s.index(o.target);
                    38 == o.which && p > 0 && p--, 40 == o.which && p < s.length - 1 && p++, ~p || (p = 0), s.eq(p).trigger("focus")
                }
            }
        }
    };
    var i = t.fn.dropdown;
    t.fn.dropdown = n, t.fn.dropdown.Constructor = d, t.fn.dropdown.noConflict = function () {
        return t.fn.dropdown = i, this
    }, t(document).on("click.bs.dropdown.data-api", o).on("click.bs.dropdown.data-api", ".dropdown form", function (t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", a, d.prototype.toggle).on("keydown.bs.dropdown.data-api", a, d.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", d.prototype.keydown)
}(jQuery);
! function (e, t) {
    "function" == typeof define && define.amd ? define(["jquery"], function (e) {
        return t(e)
    }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
}(this, function (e) {
    ! function (e) {
        "use strict";

        function t(t) {
            var i = [{
                re: /[\xC0-\xC6]/g,
                ch: "A"
            }, {
                re: /[\xE0-\xE6]/g,
                ch: "a"
            }, {
                re: /[\xC8-\xCB]/g,
                ch: "E"
            }, {
                re: /[\xE8-\xEB]/g,
                ch: "e"
            }, {
                re: /[\xCC-\xCF]/g,
                ch: "I"
            }, {
                re: /[\xEC-\xEF]/g,
                ch: "i"
            }, {
                re: /[\xD2-\xD6]/g,
                ch: "O"
            }, {
                re: /[\xF2-\xF6]/g,
                ch: "o"
            }, {
                re: /[\xD9-\xDC]/g,
                ch: "U"
            }, {
                re: /[\xF9-\xFC]/g,
                ch: "u"
            }, {
                re: /[\xC7-\xE7]/g,
                ch: "c"
            }, {
                re: /[\xD1]/g,
                ch: "N"
            }, {
                re: /[\xF1]/g,
                ch: "n"
            }];
            return e.each(i, function () {
                t = t.replace(this.re, this.ch)
            }), t
        }

        function i(e) {
            var t = {
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                },
                i = "(?:" + Object.keys(t).join("|") + ")",
                n = new RegExp(i),
                s = new RegExp(i, "g"),
                o = null == e ? "" : "" + e;
            return n.test(o) ? o.replace(s, function (e) {
                return t[e]
            }) : o
        }

        function n(t, i) {
            var n = arguments,
                o = t,
                a = i;
            [].shift.apply(n);
            var l, r = this.each(function () {
                var t = e(this);
                if (t.is("select")) {
                    var i = t.data("selectpicker"),
                        r = "object" == typeof o && o;
                    if (i) {
                        if (r)
                            for (var d in r) r.hasOwnProperty(d) && (i.options[d] = r[d])
                    } else {
                        var h = e.extend({}, s.DEFAULTS, e.fn.selectpicker.defaults || {}, t.data(), r);
                        h.template = e.extend({}, s.DEFAULTS.template, e.fn.selectpicker.defaults ? e.fn.selectpicker.defaults.template : {}, t.data().template, r.template), t.data("selectpicker", i = new s(this, h, a))
                    }
                    "string" == typeof o && (l = i[o] instanceof Function ? i[o].apply(i, n) : i.options[o])
                }
            });
            return "undefined" != typeof l ? l : r
        }
        String.prototype.includes || ! function () {
            var e = {}.toString,
                t = function () {
                    try {
                        var e = {},
                            t = Object.defineProperty,
                            i = t(e, e, e) && t
                    } catch (e) {}
                    return i
                }(),
                i = "".indexOf,
                n = function (t) {
                    if (null == this) throw new TypeError;
                    var n = String(this);
                    if (t && "[object RegExp]" == e.call(t)) throw new TypeError;
                    var s = n.length,
                        o = String(t),
                        a = o.length,
                        l = arguments.length > 1 ? arguments[1] : void 0,
                        r = l ? Number(l) : 0;
                    r != r && (r = 0);
                    var d = Math.min(Math.max(r, 0), s);
                    return !(a + d > s) && -1 != i.call(n, o, r)
                };
            t ? t(String.prototype, "includes", {
                value: n,
                configurable: !0,
                writable: !0
            }) : String.prototype.includes = n
        }(), String.prototype.startsWith || ! function () {
            var e = function () {
                    try {
                        var e = {},
                            t = Object.defineProperty,
                            i = t(e, e, e) && t
                    } catch (e) {}
                    return i
                }(),
                t = {}.toString,
                i = function (e) {
                    if (null == this) throw new TypeError;
                    var i = String(this);
                    if (e && "[object RegExp]" == t.call(e)) throw new TypeError;
                    var n = i.length,
                        s = String(e),
                        o = s.length,
                        a = arguments.length > 1 ? arguments[1] : void 0,
                        l = a ? Number(a) : 0;
                    l != l && (l = 0);
                    var r = Math.min(Math.max(l, 0), n);
                    if (o + r > n) return !1;
                    for (var d = -1; ++d < o;)
                        if (i.charCodeAt(r + d) != s.charCodeAt(d)) return !1;
                    return !0
                };
            e ? e(String.prototype, "startsWith", {
                value: i,
                configurable: !0,
                writable: !0
            }) : String.prototype.startsWith = i
        }(), Object.keys || (Object.keys = function (e, t, i) {
            i = [];
            for (t in e) i.hasOwnProperty.call(e, t) && i.push(t);
            return i
        }), e.fn.triggerNative = function (e) {
            var t, i = this[0];
            i.dispatchEvent ? ("function" == typeof Event ? t = new Event(e, {
                bubbles: !0
            }) : (t = document.createEvent("Event"), t.initEvent(e, !0, !1)), i.dispatchEvent(t)) : (i.fireEvent && (t = document.createEventObject(), t.eventType = e, i.fireEvent("on" + e, t)), this.trigger(e))
        }, e.expr[":"].icontains = function (t, i, n) {
            var s = e(t),
                o = (s.data("tokens") || s.text()).toUpperCase();
            return o.includes(n[3].toUpperCase())
        }, e.expr[":"].ibegins = function (t, i, n) {
            var s = e(t),
                o = (s.data("tokens") || s.text()).toUpperCase();
            return o.startsWith(n[3].toUpperCase())
        }, e.expr[":"].aicontains = function (t, i, n) {
            var s = e(t),
                o = (s.data("tokens") || s.data("normalizedText") || s.text()).toUpperCase();
            return o.includes(n[3].toUpperCase())
        }, e.expr[":"].aibegins = function (t, i, n) {
            var s = e(t),
                o = (s.data("tokens") || s.data("normalizedText") || s.text()).toUpperCase();
            return o.startsWith(n[3].toUpperCase())
        };
        var s = function (t, i, n) {
            n && (n.stopPropagation(), n.preventDefault()), this.$element = e(t), this.$newElement = null, this.$button = null, this.$menu = null, this.$lis = null, this.options = i, null === this.options.title && (this.options.title = this.$element.attr("title")), this.val = s.prototype.val, this.render = s.prototype.render, this.refresh = s.prototype.refresh, this.setStyle = s.prototype.setStyle, this.selectAll = s.prototype.selectAll, this.deselectAll = s.prototype.deselectAll, this.destroy = s.prototype.destroy, this.remove = s.prototype.remove, this.show = s.prototype.show, this.hide = s.prototype.hide, this.init()
        };
        s.VERSION = "1.10.0", s.DEFAULTS = {
            noneSelectedText: "Nothing selected",
            noneResultsText: "No results matched {0}",
            countSelectedText: function (e, t) {
                return 1 == e ? "{0} item selected" : "{0} items selected"
            },
            maxOptionsText: function (e, t) {
                return [1 == e ? "Limit reached ({n} item max)" : "Limit reached ({n} items max)", 1 == t ? "Group limit reached ({n} item max)" : "Group limit reached ({n} items max)"]
            },
            selectAllText: "Select All",
            deselectAllText: "Deselect All",
            doneButton: !1,
            doneButtonText: "Close",
            multipleSeparator: ", ",
            styleBase: "btn",
            style: "btn-default",
            size: "auto",
            title: null,
            selectedTextFormat: "values",
            width: !1,
            container: !1,
            hideDisabled: !1,
            showSubtext: !1,
            showIcon: !0,
            showContent: !0,
            dropupAuto: !0,
            header: !1,
            liveSearch: !1,
            liveSearchPlaceholder: null,
            liveSearchNormalize: !1,
            liveSearchStyle: "contains",
            actionsBox: !1,
            iconBase: "glyphicon",
            tickIcon: "glyphicon-ok",
            showTick: !1,
            template: {
                caret: '<span class="caret"></span>'
            },
            maxOptions: !1,
            mobile: !1,
            selectOnTab: !1,
            dropdownAlignRight: !1
        }, s.prototype = {
            constructor: s,
            init: function () {
                var t = this,
                    i = this.$element.attr("id");
                this.$element.addClass("bs-select-hidden"), this.liObj = {}, this.multiple = this.$element.prop("multiple"), this.autofocus = this.$element.prop("autofocus"), this.$newElement = this.createView(), this.$element.after(this.$newElement).appendTo(this.$newElement), this.$button = this.$newElement.children("button"), this.$menu = this.$newElement.children(".dropdown-menu"), this.$menuInner = this.$menu.children(".inner"), this.$searchbox = this.$menu.find("input"), this.$element.removeClass("bs-select-hidden"), this.options.dropdownAlignRight && this.$menu.addClass("dropdown-menu-right"), "undefined" != typeof i && (this.$button.attr("data-id", i), e('label[for="' + i + '"]').click(function (e) {
                    e.preventDefault(), t.$button.focus()
                })), this.checkDisabled(), this.clickListener(), this.options.liveSearch && this.liveSearchListener(), this.render(), this.setStyle(), this.setWidth(), this.options.container && this.selectPosition(), this.$menu.data("this", this), this.$newElement.data("this", this), this.options.mobile && this.mobile(), this.$newElement.on({
                    "hide.bs.dropdown": function (e) {
                        t.$element.trigger("hide.bs.select", e)
                    },
                    "hidden.bs.dropdown": function (e) {
                        t.$element.trigger("hidden.bs.select", e)
                    },
                    "show.bs.dropdown": function (e) {
                        t.$element.trigger("show.bs.select", e)
                    },
                    "shown.bs.dropdown": function (e) {
                        t.$element.trigger("shown.bs.select", e)
                    }
                }), t.$element[0].hasAttribute("required") && this.$element.on("invalid", function () {
                    t.$button.addClass("bs-invalid").focus(), t.$element.on({
                        "focus.bs.select": function () {
                            t.$button.focus(), t.$element.off("focus.bs.select")
                        },
                        "shown.bs.select": function () {
                            t.$element.val(t.$element.val()).off("shown.bs.select")
                        },
                        "rendered.bs.select": function () {
                            this.validity.valid && t.$button.removeClass("bs-invalid"), t.$element.off("rendered.bs.select")
                        }
                    })
                }), setTimeout(function () {
                    t.$element.trigger("loaded.bs.select")
                })
            },
            createDropdown: function () {
                var t = this.multiple || this.options.showTick ? " show-tick" : "",
                    n = this.$element.parent().hasClass("input-group") ? " input-group-btn" : "",
                    s = this.autofocus ? " autofocus" : "",
                    o = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + "</div>" : "",
                    a = this.options.liveSearch ? '<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"' + (null === this.options.liveSearchPlaceholder ? "" : ' placeholder="' + i(this.options.liveSearchPlaceholder) + '"') + "></div>" : "",
                    l = this.multiple && this.options.actionsBox ? '<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">' + this.options.selectAllText + '</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">' + this.options.deselectAllText + "</button></div></div>" : "",
                    r = this.multiple && this.options.doneButton ? '<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">' + this.options.doneButtonText + "</button></div></div>" : "",
                    d = '<div class="btn-group bootstrap-select' + t + n + '"><button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + s + '><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">' + this.options.template.caret + '</span></button><div class="dropdown-menu open">' + o + a + l + '<ul class="dropdown-menu inner" role="menu"></ul>' + r + "</div></div>";
                return e(d)
            },
            createView: function () {
                var e = this.createDropdown(),
                    t = this.createLi();
                return e.find("ul")[0].innerHTML = t, e
            },
            reloadLi: function () {
                this.destroyLi();
                var e = this.createLi();
                this.$menuInner[0].innerHTML = e
            },
            destroyLi: function () {
                this.$menu.find("li").remove()
            },
            createLi: function () {
                var n = this,
                    s = [],
                    o = 0,
                    a = document.createElement("option"),
                    l = -1,
                    r = function (e, t, i, n) {
                        return "<li" + ("undefined" != typeof i & "" !== i ? ' class="' + i + '"' : "") + ("undefined" != typeof t & null !== t ? ' data-original-index="' + t + '"' : "") + ("undefined" != typeof n & null !== n ? 'data-optgroup="' + n + '"' : "") + ">" + e + "</li>"
                    },
                    d = function (e, s, o, a) {
                        return '<a tabindex="0"' + ("undefined" != typeof s ? ' class="' + s + '"' : "") + ("undefined" != typeof o ? ' style="' + o + '"' : "") + (n.options.liveSearchNormalize ? ' data-normalized-text="' + t(i(e)) + '"' : "") + ("undefined" != typeof a || null !== a ? ' data-tokens="' + a + '"' : "") + ">" + e + '<span class="' + n.options.iconBase + " " + n.options.tickIcon + ' check-mark"></span></a>'
                    };
                if (this.options.title && !this.multiple && (l--, !this.$element.find(".bs-title-option").length)) {
                    var h = this.$element[0];
                    a.className = "bs-title-option", a.appendChild(document.createTextNode(this.options.title)), a.value = "", h.insertBefore(a, h.firstChild), void 0 === e(h.options[h.selectedIndex]).attr("selected") && (a.selected = !0)
                }
                return this.$element.find("option").each(function (t) {
                    var i = e(this);
                    if (l++, !i.hasClass("bs-title-option")) {
                        var a = this.className || "",
                            h = this.style.cssText,
                            c = i.data("content") ? i.data("content") : i.html(),
                            p = i.data("tokens") ? i.data("tokens") : null,
                            u = "undefined" != typeof i.data("subtext") ? '<small class="text-muted">' + i.data("subtext") + "</small>" : "",
                            f = "undefined" != typeof i.data("icon") ? '<span class="' + n.options.iconBase + " " + i.data("icon") + '"></span> ' : "",
                            m = "OPTGROUP" === this.parentNode.tagName,
                            b = this.disabled || m && this.parentNode.disabled;
                        if ("" !== f && b && (f = "<span>" + f + "</span>"), n.options.hideDisabled && b && !m) return void l--;
                        if (i.data("content") || (c = f + '<span class="text">' + c + u + "</span>"), m && i.data("divider") !== !0) {
                            var v = " " + this.parentNode.className || "";
                            if (0 === i.index()) {
                                o += 1;
                                var g = this.parentNode.label,
                                    $ = "undefined" != typeof i.parent().data("subtext") ? '<small class="text-muted">' + i.parent().data("subtext") + "</small>" : "",
                                    x = i.parent().data("icon") ? '<span class="' + n.options.iconBase + " " + i.parent().data("icon") + '"></span> ' : "";
                                g = x + '<span class="text">' + g + $ + "</span>", 0 !== t && s.length > 0 && (l++, s.push(r("", null, "divider", o + "div"))), l++, s.push(r(g, null, "dropdown-header" + v, o))
                            }
                            if (n.options.hideDisabled && b) return void l--;
                            s.push(r(d(c, "opt " + a + v, h, p), t, "", o))
                        } else i.data("divider") === !0 ? s.push(r("", t, "divider")) : i.data("hidden") === !0 ? s.push(r(d(c, a, h, p), t, "hidden is-hidden")) : (this.previousElementSibling && "OPTGROUP" === this.previousElementSibling.tagName && (l++, s.push(r("", null, "divider", o + "div"))), s.push(r(d(c, a, h, p), t)));
                        n.liObj[t] = l
                    }
                }), this.multiple || 0 !== this.$element.find("option:selected").length || this.options.title || this.$element.find("option").eq(0).prop("selected", !0).attr("selected", "selected"), s.join("")
            },
            findLis: function () {
                return null == this.$lis && (this.$lis = this.$menu.find("li")), this.$lis
            },
            render: function (t) {
                var i, n = this;
                t !== !1 && this.$element.find("option").each(function (e) {
                    var t = n.findLis().eq(n.liObj[e]);
                    n.setDisabled(e, this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled, t), n.setSelected(e, this.selected, t)
                }), this.tabIndex();
                var s = this.$element.find("option").map(function () {
                        if (this.selected) {
                            if (n.options.hideDisabled && (this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled)) return;
                            var t, i = e(this),
                                s = i.data("icon") && n.options.showIcon ? '<i class="' + n.options.iconBase + " " + i.data("icon") + '"></i> ' : "";
                            return t = n.options.showSubtext && i.data("subtext") && !n.multiple ? ' <small class="text-muted">' + i.data("subtext") + "</small>" : "", "undefined" != typeof i.attr("title") ? i.attr("title") : i.data("content") && n.options.showContent ? i.data("content") : s + i.html() + t
                        }
                    }).toArray(),
                    o = this.multiple ? s.join(this.options.multipleSeparator) : s[0];
                if (this.multiple && this.options.selectedTextFormat.indexOf("count") > -1) {
                    var a = this.options.selectedTextFormat.split(">");
                    if (a.length > 1 && s.length > a[1] || 1 == a.length && s.length >= 2) {
                        i = this.options.hideDisabled ? ", [disabled]" : "";
                        var l = this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]' + i).length,
                            r = "function" == typeof this.options.countSelectedText ? this.options.countSelectedText(s.length, l) : this.options.countSelectedText;
                        o = r.replace("{0}", s.length.toString()).replace("{1}", l.toString())
                    }
                }
                void 0 == this.options.title && (this.options.title = this.$element.attr("title")), "static" == this.options.selectedTextFormat && (o = this.options.title), o || (o = "undefined" != typeof this.options.title ? this.options.title : this.options.noneSelectedText), this.$button.attr("title", e.trim(o.replace(/<[^>]*>?/g, ""))), this.$button.children(".filter-option").html(o), this.$element.trigger("rendered.bs.select")
            },
            setStyle: function (e, t) {
                this.$element.attr("class") && this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ""));
                var i = e ? e : this.options.style;
                "add" == t ? this.$button.addClass(i) : "remove" == t ? this.$button.removeClass(i) : (this.$button.removeClass(this.options.style), this.$button.addClass(i))
            },
            liHeight: function (t) {
                if (t || this.options.size !== !1 && !this.sizeInfo) {
                    var i = document.createElement("div"),
                        n = document.createElement("div"),
                        s = document.createElement("ul"),
                        o = document.createElement("li"),
                        a = document.createElement("li"),
                        l = document.createElement("a"),
                        r = document.createElement("span"),
                        d = this.options.header && this.$menu.find(".popover-title").length > 0 ? this.$menu.find(".popover-title")[0].cloneNode(!0) : null,
                        h = this.options.liveSearch ? document.createElement("div") : null,
                        c = this.options.actionsBox && this.multiple && this.$menu.find(".bs-actionsbox").length > 0 ? this.$menu.find(".bs-actionsbox")[0].cloneNode(!0) : null,
                        p = this.options.doneButton && this.multiple && this.$menu.find(".bs-donebutton").length > 0 ? this.$menu.find(".bs-donebutton")[0].cloneNode(!0) : null;
                    if (r.className = "text", i.className = this.$menu[0].parentNode.className + " open", n.className = "dropdown-menu open", s.className = "dropdown-menu inner", o.className = "divider", r.appendChild(document.createTextNode("Inner text")), l.appendChild(r), a.appendChild(l), s.appendChild(a), s.appendChild(o), d && n.appendChild(d), h) {
                        var u = document.createElement("span");
                        h.className = "bs-searchbox", u.className = "form-control", h.appendChild(u), n.appendChild(h)
                    }
                    c && n.appendChild(c), n.appendChild(s), p && n.appendChild(p), i.appendChild(n), document.body.appendChild(i);
                    var f = l.offsetHeight,
                        m = d ? d.offsetHeight : 0,
                        b = h ? h.offsetHeight : 0,
                        v = c ? c.offsetHeight : 0,
                        g = p ? p.offsetHeight : 0,
                        $ = e(o).outerHeight(!0),
                        x = "function" == typeof getComputedStyle && getComputedStyle(n),
                        y = x ? null : e(n),
                        w = parseInt(x ? x.paddingTop : y.css("paddingTop")) + parseInt(x ? x.paddingBottom : y.css("paddingBottom")) + parseInt(x ? x.borderTopWidth : y.css("borderTopWidth")) + parseInt(x ? x.borderBottomWidth : y.css("borderBottomWidth")),
                        C = w + parseInt(x ? x.marginTop : y.css("marginTop")) + parseInt(x ? x.marginBottom : y.css("marginBottom")) + 2;
                    document.body.removeChild(i), this.sizeInfo = {
                        liHeight: f,
                        headerHeight: m,
                        searchHeight: b,
                        actionsHeight: v,
                        doneButtonHeight: g,
                        dividerHeight: $,
                        menuPadding: w,
                        menuExtras: C
                    }
                }
            },
            setSize: function () {
                if (this.findLis(), this.liHeight(), this.options.header && this.$menu.css("padding-top", 0), this.options.size !== !1) {
                    var t, i, n, s, o = this,
                        a = this.$menu,
                        l = this.$menuInner,
                        r = e(window),
                        d = this.$newElement[0].offsetHeight,
                        h = this.sizeInfo.liHeight,
                        c = this.sizeInfo.headerHeight,
                        p = this.sizeInfo.searchHeight,
                        u = this.sizeInfo.actionsHeight,
                        f = this.sizeInfo.doneButtonHeight,
                        m = this.sizeInfo.dividerHeight,
                        b = this.sizeInfo.menuPadding,
                        v = this.sizeInfo.menuExtras,
                        g = this.options.hideDisabled ? ".disabled" : "",
                        $ = function () {
                            n = o.$newElement.offset().top - r.scrollTop(), s = r.height() - n - d
                        };
                    if ($(), "auto" === this.options.size) {
                        var x = function () {
                            var r, d = function (t, i) {
                                    return function (n) {
                                        return i ? n.classList ? n.classList.contains(t) : e(n).hasClass(t) : !(n.classList ? n.classList.contains(t) : e(n).hasClass(t))
                                    }
                                },
                                m = o.$menuInner[0].getElementsByTagName("li"),
                                g = Array.prototype.filter ? Array.prototype.filter.call(m, d("hidden", !1)) : o.$lis.not(".hidden"),
                                x = Array.prototype.filter ? Array.prototype.filter.call(g, d("dropdown-header", !0)) : g.filter(".dropdown-header");
                            $(), t = s - v, o.options.container ? (a.data("height") || a.data("height", a.height()), i = a.data("height")) : i = a.height(), o.options.dropupAuto && o.$newElement.toggleClass("dropup", n > s && i > t - v), o.$newElement.hasClass("dropup") && (t = n - v), r = g.length + x.length > 3 ? 3 * h + v - 2 : 0, a.css({
                                "max-height": t + "px",
                                overflow: "hidden",
                                "min-height": r + c + p + u + f + "px"
                            }), l.css({
                                "max-height": t - c - p - u - f - b + "px",
                                "overflow-y": "auto",
                                "min-height": Math.max(r - b, 0) + "px"
                            })
                        };
                        x(), this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize", x), r.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize", x)
                    } else if (this.options.size && "auto" != this.options.size && this.$lis.not(g).length > this.options.size) {
                        var y = this.$lis.not(".divider").not(g).children().slice(0, this.options.size).last().parent().index(),
                            w = this.$lis.slice(0, y + 1).filter(".divider").length;
                        t = h * this.options.size + w * m + b, o.options.container ? (a.data("height") || a.data("height", a.height()), i = a.data("height")) : i = a.height(), o.options.dropupAuto && this.$newElement.toggleClass("dropup", n > s && i > t - v), a.css({
                            "max-height": t + c + p + u + f + "px",
                            overflow: "hidden",
                            "min-height": ""
                        }), l.css({
                            "max-height": t - b + "px",
                            "overflow-y": "auto",
                            "min-height": ""
                        })
                    }
                }
            },
            setWidth: function () {
                if ("auto" === this.options.width) {
                    this.$menu.css("min-width", "0");
                    var e = this.$menu.parent().clone().appendTo("body"),
                        t = this.options.container ? this.$newElement.clone().appendTo("body") : e,
                        i = e.children(".dropdown-menu").outerWidth(),
                        n = t.css("width", "auto").children("button").outerWidth();
                    e.remove(), t.remove(), this.$newElement.css("width", Math.max(i, n) + "px")
                } else "fit" === this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", "").addClass("fit-width")) : this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", this.options.width)) : (this.$menu.css("min-width", ""), this.$newElement.css("width", ""));
                this.$newElement.hasClass("fit-width") && "fit" !== this.options.width && this.$newElement.removeClass("fit-width")
            },
            selectPosition: function () {
                this.$bsContainer = e('<div class="bs-container" />');
                var t, i, n = this,
                    s = function (e) {
                        n.$bsContainer.addClass(e.attr("class").replace(/form-control|fit-width/gi, "")).toggleClass("dropup", e.hasClass("dropup")), t = e.offset(), i = e.hasClass("dropup") ? 0 : e[0].offsetHeight, n.$bsContainer.css({
                            top: t.top + i,
                            left: t.left,
                            width: e[0].offsetWidth
                        })
                    };
                this.$button.on("click", function () {
                    var t = e(this);
                    n.isDisabled() || (s(n.$newElement), n.$bsContainer.appendTo(n.options.container).toggleClass("open", !t.hasClass("open")).append(n.$menu))
                }), e(window).on("resize scroll", function () {
                    s(n.$newElement)
                }), this.$element.on("hide.bs.select", function () {
                    n.$menu.data("height", n.$menu.height()), n.$bsContainer.detach()
                })
            },
            setSelected: function (e, t, i) {
                i || (i = this.findLis().eq(this.liObj[e])), i.toggleClass("selected", t)
            },
            setDisabled: function (e, t, i) {
                i || (i = this.findLis().eq(this.liObj[e])), t ? i.addClass("disabled").children("a").attr("href", "#").attr("tabindex", -1) : i.removeClass("disabled").children("a").removeAttr("href").attr("tabindex", 0)
            },
            isDisabled: function () {
                return this.$element[0].disabled
            },
            checkDisabled: function () {
                var e = this;
                this.isDisabled() ? (this.$newElement.addClass("disabled"), this.$button.addClass("disabled").attr("tabindex", -1)) : (this.$button.hasClass("disabled") && (this.$newElement.removeClass("disabled"), this.$button.removeClass("disabled")), -1 != this.$button.attr("tabindex") || this.$element.data("tabindex") || this.$button.removeAttr("tabindex")), this.$button.click(function () {
                    return !e.isDisabled()
                })
            },
            tabIndex: function () {
                this.$element.data("tabindex") !== this.$element.attr("tabindex") && -98 !== this.$element.attr("tabindex") && "-98" !== this.$element.attr("tabindex") && (this.$element.data("tabindex", this.$element.attr("tabindex")), this.$button.attr("tabindex", this.$element.data("tabindex"))), this.$element.attr("tabindex", -98)
            },
            clickListener: function () {
                var t = this,
                    i = e(document);
                this.$newElement.on("touchstart.dropdown", ".dropdown-menu", function (e) {
                    e.stopPropagation()
                }), i.data("spaceSelect", !1), this.$button.on("keyup", function (e) {
                    /(32)/.test(e.keyCode.toString(10)) && i.data("spaceSelect") && (e.preventDefault(), i.data("spaceSelect", !1))
                }), this.$button.on("click", function () {
                    t.setSize()
                }), this.$element.on("shown.bs.select", function () {
                    if (t.options.liveSearch || t.multiple) {
                        if (!t.multiple) {
                            var e = t.liObj[t.$element[0].selectedIndex];
                            if ("number" != typeof e || t.options.size === !1) return;
                            var i = t.$lis.eq(e)[0].offsetTop - t.$menuInner[0].offsetTop;
                            i = i - t.$menuInner[0].offsetHeight / 2 + t.sizeInfo.liHeight / 2, t.$menuInner[0].scrollTop = i
                        }
                    } else t.$menuInner.find(".selected a").focus()
                }), this.$menuInner.on("click", "li a", function (i) {
                    var n = e(this),
                        s = n.parent().data("originalIndex"),
                        o = t.$element.val(),
                        a = t.$element.prop("selectedIndex");
                    if (t.multiple && i.stopPropagation(), i.preventDefault(), !t.isDisabled() && !n.parent().hasClass("disabled")) {
                        var l = t.$element.find("option"),
                            r = l.eq(s),
                            d = r.prop("selected"),
                            h = r.parent("optgroup"),
                            c = t.options.maxOptions,
                            p = h.data("maxOptions") || !1;
                        if (t.multiple) {
                            if (r.prop("selected", !d), t.setSelected(s, !d), n.blur(), c !== !1 || p !== !1) {
                                var u = c < l.filter(":selected").length,
                                    f = p < h.find("option:selected").length;
                                if (c && u || p && f)
                                    if (c && 1 == c) l.prop("selected", !1), r.prop("selected", !0), t.$menuInner.find(".selected").removeClass("selected"), t.setSelected(s, !0);
                                    else if (p && 1 == p) {
                                    h.find("option:selected").prop("selected", !1), r.prop("selected", !0);
                                    var m = n.parent().data("optgroup");
                                    t.$menuInner.find('[data-optgroup="' + m + '"]').removeClass("selected"), t.setSelected(s, !0)
                                } else {
                                    var b = "function" == typeof t.options.maxOptionsText ? t.options.maxOptionsText(c, p) : t.options.maxOptionsText,
                                        v = b[0].replace("{n}", c),
                                        g = b[1].replace("{n}", p),
                                        $ = e('<div class="notify"></div>');
                                    b[2] && (v = v.replace("{var}", b[2][c > 1 ? 0 : 1]), g = g.replace("{var}", b[2][p > 1 ? 0 : 1])), r.prop("selected", !1), t.$menu.append($), c && u && ($.append(e("<div>" + v + "</div>")), t.$element.trigger("maxReached.bs.select")), p && f && ($.append(e("<div>" + g + "</div>")), t.$element.trigger("maxReachedGrp.bs.select")), setTimeout(function () {
                                        t.setSelected(s, !1)
                                    }, 10), $.delay(750).fadeOut(300, function () {
                                        e(this).remove()
                                    })
                                }
                            }
                        } else l.prop("selected", !1), r.prop("selected", !0), t.$menuInner.find(".selected").removeClass("selected"), t.setSelected(s, !0);
                        t.multiple ? t.options.liveSearch && t.$searchbox.focus() : t.$button.focus(), (o != t.$element.val() && t.multiple || a != t.$element.prop("selectedIndex") && !t.multiple) && t.$element.trigger("changed.bs.select", [s, r.prop("selected"), d]).triggerNative("change")
                    }
                }), this.$menu.on("click", "li.disabled a, .popover-title, .popover-title :not(.close)", function (i) {
                    i.currentTarget == this && (i.preventDefault(), i.stopPropagation(), t.options.liveSearch && !e(i.target).hasClass("close") ? t.$searchbox.focus() : t.$button.focus())
                }), this.$menuInner.on("click", ".divider, .dropdown-header", function (e) {
                    e.preventDefault(), e.stopPropagation(), t.options.liveSearch ? t.$searchbox.focus() : t.$button.focus()
                }), this.$menu.on("click", ".popover-title .close", function () {
                    t.$button.click()
                }), this.$searchbox.on("click", function (e) {
                    e.stopPropagation()
                }), this.$menu.on("click", ".actions-btn", function (i) {
                    t.options.liveSearch ? t.$searchbox.focus() : t.$button.focus(), i.preventDefault(), i.stopPropagation(), e(this).hasClass("bs-select-all") ? t.selectAll() : t.deselectAll()
                }), this.$element.change(function () {
                    t.render(!1)
                })
            },
            liveSearchListener: function () {
                var n = this,
                    s = e('<li class="no-results"></li>');
                this.$button.on("click.dropdown.data-api touchstart.dropdown.data-api", function () {
                    n.$menuInner.find(".active").removeClass("active"), n.$searchbox.val() && (n.$searchbox.val(""), n.$lis.not(".is-hidden").removeClass("hidden"), s.parent().length && s.remove()), n.multiple || n.$menuInner.find(".selected").addClass("active"), setTimeout(function () {
                        n.$searchbox.focus()
                    }, 10)
                }), this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api", function (e) {
                    e.stopPropagation()
                }), this.$searchbox.on("input propertychange", function () {
                    if (n.$searchbox.val()) {
                        var o = n.$lis.not(".is-hidden").removeClass("hidden").children("a");
                        o = n.options.liveSearchNormalize ? o.not(":a" + n._searchStyle() + '("' + t(n.$searchbox.val()) + '")') : o.not(":" + n._searchStyle() + '("' + n.$searchbox.val() + '")'), o.parent().addClass("hidden"), n.$lis.filter(".dropdown-header").each(function () {
                            var t = e(this),
                                i = t.data("optgroup");
                            0 === n.$lis.filter("[data-optgroup=" + i + "]").not(t).not(".hidden").length && (t.addClass("hidden"), n.$lis.filter("[data-optgroup=" + i + "div]").addClass("hidden"))
                        });
                        var a = n.$lis.not(".hidden");
                        a.each(function (t) {
                            var i = e(this);
                            i.hasClass("divider") && (i.index() === a.first().index() || i.index() === a.last().index() || a.eq(t + 1).hasClass("divider")) && i.addClass("hidden")
                        }), n.$lis.not(".hidden, .no-results").length ? s.parent().length && s.remove() : (s.parent().length && s.remove(), s.html(n.options.noneResultsText.replace("{0}", '"' + i(n.$searchbox.val()) + '"')).show(), n.$menuInner.append(s))
                    } else n.$lis.not(".is-hidden").removeClass("hidden"), s.parent().length && s.remove();
                    n.$lis.filter(".active").removeClass("active"), n.$searchbox.val() && n.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(), e(this).focus()
                })
            },
            _searchStyle: function () {
                var e = {
                    begins: "ibegins",
                    startsWith: "ibegins"
                };
                return e[this.options.liveSearchStyle] || "icontains"
            },
            val: function (e) {
                return "undefined" != typeof e ? (this.$element.val(e), this.render(), this.$element) : this.$element.val()
            },
            changeAll: function (t) {
                "undefined" == typeof t && (t = !0), this.findLis();
                for (var i = this.$element.find("option"), n = this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").toggleClass("selected", t), s = n.length, o = [], a = 0; s > a; a++) {
                    var l = n[a].getAttribute("data-original-index");
                    o[o.length] = i.eq(l)[0]
                }
                e(o).prop("selected", t), this.render(!1), this.$element.trigger("changed.bs.select").triggerNative("change")
            },
            selectAll: function () {
                return this.changeAll(!0)
            },
            deselectAll: function () {
                return this.changeAll(!1)
            },
            toggle: function (e) {
                e = e || window.event, e && e.stopPropagation(), this.$button.trigger("click")
            },
            keydown: function (i) {
                var n, s, o, a, l, r, d, h, c, p = e(this),
                    u = p.is("input") ? p.parent().parent() : p.parent(),
                    f = u.data("this"),
                    m = ":not(.disabled, .hidden, .dropdown-header, .divider)",
                    b = {
                        32: " ",
                        48: "0",
                        49: "1",
                        50: "2",
                        51: "3",
                        52: "4",
                        53: "5",
                        54: "6",
                        55: "7",
                        56: "8",
                        57: "9",
                        59: ";",
                        65: "a",
                        66: "b",
                        67: "c",
                        68: "d",
                        69: "e",
                        70: "f",
                        71: "g",
                        72: "h",
                        73: "i",
                        74: "j",
                        75: "k",
                        76: "l",
                        77: "m",
                        78: "n",
                        79: "o",
                        80: "p",
                        81: "q",
                        82: "r",
                        83: "s",
                        84: "t",
                        85: "u",
                        86: "v",
                        87: "w",
                        88: "x",
                        89: "y",
                        90: "z",
                        96: "0",
                        97: "1",
                        98: "2",
                        99: "3",
                        100: "4",
                        101: "5",
                        102: "6",
                        103: "7",
                        104: "8",
                        105: "9"
                    };
                if (f.options.liveSearch && (u = p.parent().parent()), f.options.container && (u = f.$menu), n = e("[role=menu] li", u), c = f.$newElement.hasClass("open"), !c && (i.keyCode >= 48 && i.keyCode <= 57 || i.keyCode >= 96 && i.keyCode <= 105 || i.keyCode >= 65 && i.keyCode <= 90) && (f.options.container ? f.$button.trigger("click") : (f.setSize(), f.$menu.parent().addClass("open"), c = !0), f.$searchbox.focus()), f.options.liveSearch && (/(^9$|27)/.test(i.keyCode.toString(10)) && c && 0 === f.$menu.find(".active").length && (i.preventDefault(), f.$menu.parent().removeClass("open"), f.options.container && f.$newElement.removeClass("open"), f.$button.focus()), n = e("[role=menu] li" + m, u), p.val() || /(38|40)/.test(i.keyCode.toString(10)) || 0 === n.filter(".active").length && (n = f.$menuInner.find("li"), n = f.options.liveSearchNormalize ? n.filter(":a" + f._searchStyle() + "(" + t(b[i.keyCode]) + ")") : n.filter(":" + f._searchStyle() + "(" + b[i.keyCode] + ")"))), n.length) {
                    if (/(38|40)/.test(i.keyCode.toString(10))) s = n.index(n.find("a").filter(":focus").parent()), a = n.filter(m).first().index(), l = n.filter(m).last().index(), o = n.eq(s).nextAll(m).eq(0).index(), r = n.eq(s).prevAll(m).eq(0).index(), d = n.eq(o).prevAll(m).eq(0).index(), f.options.liveSearch && (n.each(function (t) {
                        e(this).hasClass("disabled") || e(this).data("index", t)
                    }), s = n.index(n.filter(".active")), a = n.first().data("index"), l = n.last().data("index"), o = n.eq(s).nextAll().eq(0).data("index"), r = n.eq(s).prevAll().eq(0).data("index"), d = n.eq(o).prevAll().eq(0).data("index")), h = p.data("prevIndex"), 38 == i.keyCode ? (f.options.liveSearch && s--, s != d && s > r && (s = r), a > s && (s = a), s == h && (s = l)) : 40 == i.keyCode && (f.options.liveSearch && s++, -1 == s && (s = 0), s != d && o > s && (s = o), s > l && (s = l), s == h && (s = a)), p.data("prevIndex", s), f.options.liveSearch ? (i.preventDefault(), p.hasClass("dropdown-toggle") || (n.removeClass("active").eq(s).addClass("active").children("a").focus(), p.focus())) : n.eq(s).children("a").focus();
                    else if (!p.is("input")) {
                        var v, g, $ = [];
                        n.each(function () {
                            e(this).hasClass("disabled") || e.trim(e(this).children("a").text().toLowerCase()).substring(0, 1) == b[i.keyCode] && $.push(e(this).index())
                        }), v = e(document).data("keycount"), v++, e(document).data("keycount", v), g = e.trim(e(":focus").text().toLowerCase()).substring(0, 1), g != b[i.keyCode] ? (v = 1, e(document).data("keycount", v)) : v >= $.length && (e(document).data("keycount", 0), v > $.length && (v = 1)), n.eq($[v - 1]).children("a").focus()
                    }
                    if ((/(13|32)/.test(i.keyCode.toString(10)) || /(^9$)/.test(i.keyCode.toString(10)) && f.options.selectOnTab) && c) {
                        if (/(32)/.test(i.keyCode.toString(10)) || i.preventDefault(), f.options.liveSearch) /(32)/.test(i.keyCode.toString(10)) || (f.$menuInner.find(".active a").click(), p.focus());
                        else {
                            var x = e(":focus");
                            x.click(), x.focus(), i.preventDefault(), e(document).data("spaceSelect", !0)
                        }
                        e(document).data("keycount", 0)
                    }(/(^9$|27)/.test(i.keyCode.toString(10)) && c && (f.multiple || f.options.liveSearch) || /(27)/.test(i.keyCode.toString(10)) && !c) && (f.$menu.parent().removeClass("open"), f.options.container && f.$newElement.removeClass("open"), f.$button.focus())
                }
            },
            mobile: function () {
                this.$element.addClass("mobile-device")
            },
            refresh: function () {
                this.$lis = null, this.liObj = {}, this.reloadLi(), this.render(), this.checkDisabled(), this.liHeight(!0), this.setStyle(), this.setWidth(), this.$lis && this.$searchbox.trigger("propertychange"), this.$element.trigger("refreshed.bs.select")
            },
            hide: function () {
                this.$newElement.hide()
            },
            show: function () {
                this.$newElement.show()
            },
            remove: function () {
                this.$newElement.remove(), this.$element.remove()
            },
            destroy: function () {
                this.$newElement.before(this.$element).remove(), this.$bsContainer ? this.$bsContainer.remove() : this.$menu.remove(), this.$element.off(".bs.select").removeData("selectpicker").removeClass("bs-select-hidden selectpicker")
            }
        };
        var o = e.fn.selectpicker;
        e.fn.selectpicker = n, e.fn.selectpicker.Constructor = s, e.fn.selectpicker.noConflict = function () {
            return e.fn.selectpicker = o, this
        }, e(document).data("keycount", 0).on("keydown.bs.select", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input', s.prototype.keydown).on("focusin.modal", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input', function (e) {
            e.stopPropagation()
        }), e(window).on("load.bs.select.data-api", function () {
            e(".selectpicker").each(function () {
                var t = e(this);
                n.call(t, t.data())
            })
        })
    }(e)
});
! function (e) {
    var t = !0;
    e.flexslider = function (a, n) {
        var i = e(a);
        i.vars = e.extend({}, e.flexslider.defaults, n);
        var s, r = i.vars.namespace,
            o = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
            l = ("ontouchstart" in window || o || window.DocumentTouch && document instanceof DocumentTouch) && i.vars.touch,
            c = "click touchend MSPointerUp keyup",
            d = "",
            u = "vertical" === i.vars.direction,
            v = i.vars.reverse,
            p = i.vars.itemWidth > 0,
            m = "fade" === i.vars.animation,
            f = "" !== i.vars.asNavFor,
            g = {};
        e.data(a, "flexslider", i), g = {
            init: function () {
                i.animating = !1, i.currentSlide = parseInt(i.vars.startAt ? i.vars.startAt : 0, 10), isNaN(i.currentSlide) && (i.currentSlide = 0), i.animatingTo = i.currentSlide, i.atEnd = 0 === i.currentSlide || i.currentSlide === i.last, i.containerSelector = i.vars.selector.substr(0, i.vars.selector.search(" ")), i.slides = e(i.vars.selector, i), i.container = e(i.containerSelector, i), i.count = i.slides.length, i.syncExists = e(i.vars.sync).length > 0, "slide" === i.vars.animation && (i.vars.animation = "swing"), i.prop = u ? "top" : "marginLeft", i.args = {}, i.manualPause = !1, i.stopped = !1, i.started = !1, i.startTimeout = null, i.transitions = !i.vars.video && !m && i.vars.useCSS && function () {
                    var e = document.createElement("div"),
                        t = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                    for (var a in t)
                        if (void 0 !== e.style[t[a]]) return i.pfx = t[a].replace("Perspective", "").toLowerCase(), i.prop = "-" + i.pfx + "-transform", !0;
                    return !1
                }(), i.ensureAnimationEnd = "", "" !== i.vars.controlsContainer && (i.controlsContainer = e(i.vars.controlsContainer).length > 0 && e(i.vars.controlsContainer)), "" !== i.vars.manualControls && (i.manualControls = e(i.vars.manualControls).length > 0 && e(i.vars.manualControls)), "" !== i.vars.customDirectionNav && (i.customDirectionNav = 2 === e(i.vars.customDirectionNav).length && e(i.vars.customDirectionNav)), i.vars.randomize && (i.slides.sort(function () {
                    return Math.round(Math.random()) - .5
                }), i.container.empty().append(i.slides)), i.doMath(), i.setup("init"), i.vars.controlNav && g.controlNav.setup(), i.vars.directionNav && g.directionNav.setup(), i.vars.keyboard && (1 === e(i.containerSelector).length || i.vars.multipleKeyboard) && e(document).bind("keyup", function (e) {
                    var t = e.keyCode;
                    if (!i.animating && (39 === t || 37 === t)) {
                        var a = 39 === t ? i.getTarget("next") : 37 === t && i.getTarget("prev");
                        i.flexAnimate(a, i.vars.pauseOnAction)
                    }
                }), i.vars.mousewheel && i.bind("mousewheel", function (e, t, a, n) {
                    e.preventDefault();
                    var s = 0 > t ? i.getTarget("next") : i.getTarget("prev");
                    i.flexAnimate(s, i.vars.pauseOnAction)
                }), i.vars.pausePlay && g.pausePlay.setup(), i.vars.slideshow && i.vars.pauseInvisible && g.pauseInvisible.init(), i.vars.slideshow && (i.vars.pauseOnHover && i.hover(function () {
                    i.manualPlay || i.manualPause || i.pause()
                }, function () {
                    i.manualPause || i.manualPlay || i.stopped || i.play()
                }), i.vars.pauseInvisible && g.pauseInvisible.isHidden() || (i.vars.initDelay > 0 ? i.startTimeout = setTimeout(i.play, i.vars.initDelay) : i.play())), f && g.asNav.setup(), l && i.vars.touch && g.touch(), (!m || m && i.vars.smoothHeight) && e(window).bind("resize orientationchange focus", g.resize), i.find("img").attr("draggable", "false"), setTimeout(function () {
                    i.vars.start(i)
                }, 200)
            },
            asNav: {
                setup: function () {
                    i.asNav = !0, i.animatingTo = Math.floor(i.currentSlide / i.move), i.currentItem = i.currentSlide, i.slides.removeClass(r + "active-slide").eq(i.currentItem).addClass(r + "active-slide"), o ? (a._slider = i, i.slides.each(function () {
                        var t = this;
                        t._gesture = new MSGesture, t._gesture.target = t, t.addEventListener("MSPointerDown", function (e) {
                            e.preventDefault(), e.currentTarget._gesture && e.currentTarget._gesture.addPointer(e.pointerId)
                        }, !1), t.addEventListener("MSGestureTap", function (t) {
                            t.preventDefault();
                            var a = e(this),
                                n = a.index();
                            e(i.vars.asNavFor).data("flexslider").animating || a.hasClass("active") || (i.direction = i.currentItem < n ? "next" : "prev", i.flexAnimate(n, i.vars.pauseOnAction, !1, !0, !0))
                        })
                    })) : i.slides.on(c, function (t) {
                        t.preventDefault();
                        var a = e(this),
                            n = a.index(),
                            s = a.offset().left - e(i).scrollLeft();
                        0 >= s && a.hasClass(r + "active-slide") ? i.flexAnimate(i.getTarget("prev"), !0) : e(i.vars.asNavFor).data("flexslider").animating || a.hasClass(r + "active-slide") || (i.direction = i.currentItem < n ? "next" : "prev", i.flexAnimate(n, i.vars.pauseOnAction, !1, !0, !0))
                    })
                }
            },
            controlNav: {
                setup: function () {
                    i.manualControls ? g.controlNav.setupManual() : g.controlNav.setupPaging()
                },
                setupPaging: function () {
                    var t, a, n = "thumbnails" === i.vars.controlNav ? "control-thumbs" : "control-paging",
                        s = 1;
                    if (i.controlNavScaffold = e('<ol class="' + r + "control-nav " + r + n + '"></ol>'), i.pagingCount > 1)
                        for (var o = 0; o < i.pagingCount; o++) {
                            a = i.slides.eq(o), void 0 === a.attr("data-thumb-alt") && a.attr("data-thumb-alt", "");
                            var l = "" !== a.attr("data-thumb-alt") ? l = ' alt="' + a.attr("data-thumb-alt") + '"' : "";
                            if (t = "thumbnails" === i.vars.controlNav ? '<img src="' + a.attr("data-thumb") + '"' + l + "/>" : '<a href="#">' + s + "</a>", "thumbnails" === i.vars.controlNav && !0 === i.vars.thumbCaptions) {
                                var u = a.attr("data-thumbcaption");
                                "" !== u && void 0 !== u && (t += '<span class="' + r + 'caption">' + u + "</span>")
                            }
                            i.controlNavScaffold.append("<li>" + t + "</li>"), s++
                        }
                    i.controlsContainer ? e(i.controlsContainer).append(i.controlNavScaffold) : i.append(i.controlNavScaffold), g.controlNav.set(), g.controlNav.active(), i.controlNavScaffold.delegate("a, img", c, function (t) {
                        if (t.preventDefault(), "" === d || d === t.type) {
                            var a = e(this),
                                n = i.controlNav.index(a);
                            a.hasClass(r + "active") || (i.direction = n > i.currentSlide ? "next" : "prev", i.flexAnimate(n, i.vars.pauseOnAction))
                        }
                        "" === d && (d = t.type), g.setToClearWatchedEvent()
                    })
                },
                setupManual: function () {
                    i.controlNav = i.manualControls, g.controlNav.active(), i.controlNav.bind(c, function (t) {
                        if (t.preventDefault(), "" === d || d === t.type) {
                            var a = e(this),
                                n = i.controlNav.index(a);
                            a.hasClass(r + "active") || (n > i.currentSlide ? i.direction = "next" : i.direction = "prev", i.flexAnimate(n, i.vars.pauseOnAction))
                        }
                        "" === d && (d = t.type), g.setToClearWatchedEvent()
                    })
                },
                set: function () {
                    var t = "thumbnails" === i.vars.controlNav ? "img" : "a";
                    i.controlNav = e("." + r + "control-nav li " + t, i.controlsContainer ? i.controlsContainer : i)
                },
                active: function () {
                    i.controlNav.removeClass(r + "active").eq(i.animatingTo).addClass(r + "active")
                },
                update: function (t, a) {
                    i.pagingCount > 1 && "add" === t ? i.controlNavScaffold.append(e('<li><a href="#">' + i.count + "</a></li>")) : 1 === i.pagingCount ? i.controlNavScaffold.find("li").remove() : i.controlNav.eq(a).closest("li").remove(), g.controlNav.set(), i.pagingCount > 1 && i.pagingCount !== i.controlNav.length ? i.update(a, t) : g.controlNav.active()
                }
            },
            directionNav: {
                setup: function () {
                    var t = e('<ul class="' + r + 'direction-nav"><li class="' + r + 'nav-prev"><a class="' + r + 'prev" href="#">' + i.vars.prevText + '</a></li><li class="' + r + 'nav-next"><a class="' + r + 'next" href="#">' + i.vars.nextText + "</a></li></ul>");
                    i.customDirectionNav ? i.directionNav = i.customDirectionNav : i.controlsContainer ? (e(i.controlsContainer).append(t), i.directionNav = e("." + r + "direction-nav li a", i.controlsContainer)) : (i.append(t), i.directionNav = e("." + r + "direction-nav li a", i)), g.directionNav.update(), i.directionNav.bind(c, function (t) {
                        t.preventDefault();
                        var a;
                        ("" === d || d === t.type) && (a = e(this).hasClass(r + "next") ? i.getTarget("next") : i.getTarget("prev"), i.flexAnimate(a, i.vars.pauseOnAction)), "" === d && (d = t.type), g.setToClearWatchedEvent()
                    })
                },
                update: function () {
                    var e = r + "disabled";
                    1 === i.pagingCount ? i.directionNav.addClass(e).attr("tabindex", "-1") : i.vars.animationLoop ? i.directionNav.removeClass(e).removeAttr("tabindex") : 0 === i.animatingTo ? i.directionNav.removeClass(e).filter("." + r + "prev").addClass(e).attr("tabindex", "-1") : i.animatingTo === i.last ? i.directionNav.removeClass(e).filter("." + r + "next").addClass(e).attr("tabindex", "-1") : i.directionNav.removeClass(e).removeAttr("tabindex")
                }
            },
            pausePlay: {
                setup: function () {
                    var t = e('<div class="' + r + 'pauseplay"><a href="#"></a></div>');
                    i.controlsContainer ? (i.controlsContainer.append(t), i.pausePlay = e("." + r + "pauseplay a", i.controlsContainer)) : (i.append(t), i.pausePlay = e("." + r + "pauseplay a", i)), g.pausePlay.update(i.vars.slideshow ? r + "pause" : r + "play"), i.pausePlay.bind(c, function (t) {
                        t.preventDefault(), ("" === d || d === t.type) && (e(this).hasClass(r + "pause") ? (i.manualPause = !0, i.manualPlay = !1, i.pause()) : (i.manualPause = !1, i.manualPlay = !0, i.play())), "" === d && (d = t.type), g.setToClearWatchedEvent()
                    })
                },
                update: function (e) {
                    "play" === e ? i.pausePlay.removeClass(r + "pause").addClass(r + "play").html(i.vars.playText) : i.pausePlay.removeClass(r + "play").addClass(r + "pause").html(i.vars.pauseText)
                }
            },
            touch: function () {
                function e(e) {
                    e.stopPropagation(), i.animating ? e.preventDefault() : (i.pause(), a._gesture.addPointer(e.pointerId), T = 0, c = u ? i.h : i.w, f = Number(new Date), l = p && v && i.animatingTo === i.last ? 0 : p && v ? i.limit - (i.itemW + i.vars.itemMargin) * i.move * i.animatingTo : p && i.currentSlide === i.last ? i.limit : p ? (i.itemW + i.vars.itemMargin) * i.move * i.currentSlide : v ? (i.last - i.currentSlide + i.cloneOffset) * c : (i.currentSlide + i.cloneOffset) * c)
                }

                function t(e) {
                    e.stopPropagation();
                    var t = e.target._slider;
                    if (t) {
                        var n = -e.translationX,
                            i = -e.translationY;
                        return T += u ? i : n, d = T, y = u ? Math.abs(T) < Math.abs(-n) : Math.abs(T) < Math.abs(-i), e.detail === e.MSGESTURE_FLAG_INERTIA ? void setImmediate(function () {
                            a._gesture.stop()
                        }) : void((!y || Number(new Date) - f > 500) && (e.preventDefault(), !m && t.transitions && (t.vars.animationLoop || (d = T / (0 === t.currentSlide && 0 > T || t.currentSlide === t.last && T > 0 ? Math.abs(T) / c + 2 : 1)), t.setProps(l + d, "setTouch"))))
                    }
                }

                function n(e) {
                    e.stopPropagation();
                    var t = e.target._slider;
                    if (t) {
                        if (t.animatingTo === t.currentSlide && !y && null !== d) {
                            var a = v ? -d : d,
                                n = a > 0 ? t.getTarget("next") : t.getTarget("prev");
                            t.canAdvance(n) && (Number(new Date) - f < 550 && Math.abs(a) > 50 || Math.abs(a) > c / 2) ? t.flexAnimate(n, t.vars.pauseOnAction) : m || t.flexAnimate(t.currentSlide, t.vars.pauseOnAction, !0)
                        }
                        s = null, r = null, d = null, l = null, T = 0
                    }
                }
                var s, r, l, c, d, f, g, h, S, y = !1,
                    x = 0,
                    b = 0,
                    T = 0;
                o ? (a.style.msTouchAction = "none", a._gesture = new MSGesture, a._gesture.target = a, a.addEventListener("MSPointerDown", e, !1), a._slider = i, a.addEventListener("MSGestureChange", t, !1), a.addEventListener("MSGestureEnd", n, !1)) : (g = function (e) {
                    i.animating ? e.preventDefault() : (window.navigator.msPointerEnabled || 1 === e.touches.length) && (i.pause(), c = u ? i.h : i.w, f = Number(new Date), x = e.touches[0].pageX, b = e.touches[0].pageY, l = p && v && i.animatingTo === i.last ? 0 : p && v ? i.limit - (i.itemW + i.vars.itemMargin) * i.move * i.animatingTo : p && i.currentSlide === i.last ? i.limit : p ? (i.itemW + i.vars.itemMargin) * i.move * i.currentSlide : v ? (i.last - i.currentSlide + i.cloneOffset) * c : (i.currentSlide + i.cloneOffset) * c, s = u ? b : x, r = u ? x : b, a.addEventListener("touchmove", h, !1), a.addEventListener("touchend", S, !1))
                }, h = function (e) {
                    x = e.touches[0].pageX, b = e.touches[0].pageY, d = u ? s - b : s - x, y = u ? Math.abs(d) < Math.abs(x - r) : Math.abs(d) < Math.abs(b - r);
                    var t = 500;
                    (!y || Number(new Date) - f > t) && (e.preventDefault(), !m && i.transitions && (i.vars.animationLoop || (d /= 0 === i.currentSlide && 0 > d || i.currentSlide === i.last && d > 0 ? Math.abs(d) / c + 2 : 1), i.setProps(l + d, "setTouch")))
                }, S = function (e) {
                    if (a.removeEventListener("touchmove", h, !1), i.animatingTo === i.currentSlide && !y && null !== d) {
                        var t = v ? -d : d,
                            n = t > 0 ? i.getTarget("next") : i.getTarget("prev");
                        i.canAdvance(n) && (Number(new Date) - f < 550 && Math.abs(t) > 50 || Math.abs(t) > c / 2) ? i.flexAnimate(n, i.vars.pauseOnAction) : m || i.flexAnimate(i.currentSlide, i.vars.pauseOnAction, !0)
                    }
                    a.removeEventListener("touchend", S, !1), s = null, r = null, d = null, l = null
                }, a.addEventListener("touchstart", g, !1))
            },
            resize: function () {
                !i.animating && i.is(":visible") && (p || i.doMath(), m ? g.smoothHeight() : p ? (i.slides.width(i.computedW), i.update(i.pagingCount), i.setProps()) : u ? (i.viewport.height(i.h), i.setProps(i.h, "setTotal")) : (i.vars.smoothHeight && g.smoothHeight(), i.newSlides.width(i.computedW), i.setProps(i.computedW, "setTotal")))
            },
            smoothHeight: function (e) {
                if (!u || m) {
                    var t = m ? i : i.viewport;
                    e ? t.animate({
                        height: i.slides.eq(i.animatingTo).innerHeight()
                    }, e) : t.innerHeight(i.slides.eq(i.animatingTo).innerHeight())
                }
            },
            sync: function (t) {
                var a = e(i.vars.sync).data("flexslider"),
                    n = i.animatingTo;
                switch (t) {
                    case "animate":
                        a.flexAnimate(n, i.vars.pauseOnAction, !1, !0);
                        break;
                    case "play":
                        a.playing || a.asNav || a.play();
                        break;
                    case "pause":
                        a.pause()
                }
            },
            uniqueID: function (t) {
                return t.filter("[id]").add(t.find("[id]")).each(function () {
                    var t = e(this);
                    t.attr("id", t.attr("id") + "_clone")
                }), t
            },
            pauseInvisible: {
                visProp: null,
                init: function () {
                    var e = g.pauseInvisible.getHiddenProp();
                    if (e) {
                        var t = e.replace(/[H|h]idden/, "") + "visibilitychange";
                        document.addEventListener(t, function () {
                            g.pauseInvisible.isHidden() ? i.startTimeout ? clearTimeout(i.startTimeout) : i.pause() : i.started ? i.play() : i.vars.initDelay > 0 ? setTimeout(i.play, i.vars.initDelay) : i.play()
                        })
                    }
                },
                isHidden: function () {
                    var e = g.pauseInvisible.getHiddenProp();
                    return !!e && document[e]
                },
                getHiddenProp: function () {
                    var e = ["webkit", "moz", "ms", "o"];
                    if ("hidden" in document) return "hidden";
                    for (var t = 0; t < e.length; t++)
                        if (e[t] + "Hidden" in document) return e[t] + "Hidden";
                    return null
                }
            },
            setToClearWatchedEvent: function () {
                clearTimeout(s), s = setTimeout(function () {
                    d = ""
                }, 3e3)
            }
        }, i.flexAnimate = function (t, a, n, s, o) {
            if (i.vars.animationLoop || t === i.currentSlide || (i.direction = t > i.currentSlide ? "next" : "prev"), f && 1 === i.pagingCount && (i.direction = i.currentItem < t ? "next" : "prev"), !i.animating && (i.canAdvance(t, o) || n) && i.is(":visible")) {
                if (f && s) {
                    var c = e(i.vars.asNavFor).data("flexslider");
                    if (i.atEnd = 0 === t || t === i.count - 1, c.flexAnimate(t, !0, !1, !0, o), i.direction = i.currentItem < t ? "next" : "prev", c.direction = i.direction, Math.ceil((t + 1) / i.visible) - 1 === i.currentSlide || 0 === t) return i.currentItem = t, i.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), !1;
                    i.currentItem = t, i.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), t = Math.floor(t / i.visible)
                }
                if (i.animating = !0, i.animatingTo = t, a && i.pause(), i.vars.before(i), i.syncExists && !o && g.sync("animate"), i.vars.controlNav && g.controlNav.active(), p || i.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), i.atEnd = 0 === t || t === i.last, i.vars.directionNav && g.directionNav.update(), t === i.last && (i.vars.end(i), i.vars.animationLoop || i.pause()), m) l ? (i.slides.eq(i.currentSlide).css({
                    opacity: 0,
                    zIndex: 1
                }), i.slides.eq(t).css({
                    opacity: 1,
                    zIndex: 2
                }), i.wrapup(y)) : (i.slides.eq(i.currentSlide).css({
                    zIndex: 1
                }).animate({
                    opacity: 0
                }, i.vars.animationSpeed, i.vars.easing), i.slides.eq(t).css({
                    zIndex: 2
                }).animate({
                    opacity: 1
                }, i.vars.animationSpeed, i.vars.easing, i.wrapup));
                else {
                    var d, h, S, y = u ? i.slides.filter(":first").height() : i.computedW;
                    p ? (d = i.vars.itemMargin, S = (i.itemW + d) * i.move * i.animatingTo, h = S > i.limit && 1 !== i.visible ? i.limit : S) : h = 0 === i.currentSlide && t === i.count - 1 && i.vars.animationLoop && "next" !== i.direction ? v ? (i.count + i.cloneOffset) * y : 0 : i.currentSlide === i.last && 0 === t && i.vars.animationLoop && "prev" !== i.direction ? v ? 0 : (i.count + 1) * y : v ? (i.count - 1 - t + i.cloneOffset) * y : (t + i.cloneOffset) * y, i.setProps(h, "", i.vars.animationSpeed), i.transitions ? (i.vars.animationLoop && i.atEnd || (i.animating = !1, i.currentSlide = i.animatingTo), i.container.unbind("webkitTransitionEnd transitionend"), i.container.bind("webkitTransitionEnd transitionend", function () {
                        clearTimeout(i.ensureAnimationEnd), i.wrapup(y)
                    }), clearTimeout(i.ensureAnimationEnd), i.ensureAnimationEnd = setTimeout(function () {
                        i.wrapup(y)
                    }, i.vars.animationSpeed + 100)) : i.container.animate(i.args, i.vars.animationSpeed, i.vars.easing, function () {
                        i.wrapup(y)
                    })
                }
                i.vars.smoothHeight && g.smoothHeight(i.vars.animationSpeed)
            }
        }, i.wrapup = function (e) {
            m || p || (0 === i.currentSlide && i.animatingTo === i.last && i.vars.animationLoop ? i.setProps(e, "jumpEnd") : i.currentSlide === i.last && 0 === i.animatingTo && i.vars.animationLoop && i.setProps(e, "jumpStart")), i.animating = !1, i.currentSlide = i.animatingTo, i.vars.after(i)
        }, i.animateSlides = function () {
            !i.animating && t && i.flexAnimate(i.getTarget("next"))
        }, i.pause = function () {
            clearInterval(i.animatedSlides), i.animatedSlides = null, i.playing = !1, i.vars.pausePlay && g.pausePlay.update("play"), i.syncExists && g.sync("pause")
        }, i.play = function () {
            i.playing && clearInterval(i.animatedSlides), i.animatedSlides = i.animatedSlides || setInterval(i.animateSlides, i.vars.slideshowSpeed), i.started = i.playing = !0, i.vars.pausePlay && g.pausePlay.update("pause"), i.syncExists && g.sync("play")
        }, i.stop = function () {
            i.pause(), i.stopped = !0
        }, i.canAdvance = function (e, t) {
            var a = f ? i.pagingCount - 1 : i.last;
            return !!t || (!(!f || i.currentItem !== i.count - 1 || 0 !== e || "prev" !== i.direction) || (!f || 0 !== i.currentItem || e !== i.pagingCount - 1 || "next" === i.direction) && (!(e === i.currentSlide && !f) && (!!i.vars.animationLoop || (!i.atEnd || 0 !== i.currentSlide || e !== a || "next" === i.direction) && (!i.atEnd || i.currentSlide !== a || 0 !== e || "next" !== i.direction))))
        }, i.getTarget = function (e) {
            return i.direction = e, "next" === e ? i.currentSlide === i.last ? 0 : i.currentSlide + 1 : 0 === i.currentSlide ? i.last : i.currentSlide - 1
        }, i.setProps = function (e, t, a) {
            var n = function () {
                var a = e ? e : (i.itemW + i.vars.itemMargin) * i.move * i.animatingTo,
                    n = function () {
                        if (p) return "setTouch" === t ? e : v && i.animatingTo === i.last ? 0 : v ? i.limit - (i.itemW + i.vars.itemMargin) * i.move * i.animatingTo : i.animatingTo === i.last ? i.limit : a;
                        switch (t) {
                            case "setTotal":
                                return v ? (i.count - 1 - i.currentSlide + i.cloneOffset) * e : (i.currentSlide + i.cloneOffset) * e;
                            case "setTouch":
                                return v ? e : e;
                            case "jumpEnd":
                                return v ? e : i.count * e;
                            case "jumpStart":
                                return v ? i.count * e : e;
                            default:
                                return e
                        }
                    }();
                return -1 * n + "px"
            }();
            i.transitions && (n = u ? "translate3d(0," + n + ",0)" : "translate3d(" + n + ",0,0)", a = void 0 !== a ? a / 1e3 + "s" : "0s", i.container.css("-" + i.pfx + "-transition-duration", a), i.container.css("transition-duration", a)), i.args[i.prop] = n, (i.transitions || void 0 === a) && i.container.css(i.args), i.container.css("transform", n)
        }, i.setup = function (t) {
            if (m) i.slides.css({
                width: "100%",
                float: "left",
                marginRight: "-100%",
                position: "relative"
            }), "init" === t && (l ? i.slides.css({
                opacity: 0,
                display: "block",
                webkitTransition: "opacity " + i.vars.animationSpeed / 1e3 + "s ease",
                zIndex: 1
            }).eq(i.currentSlide).css({
                opacity: 1,
                zIndex: 2
            }) : 0 == i.vars.fadeFirstSlide ? i.slides.css({
                opacity: 0,
                display: "block",
                zIndex: 1
            }).eq(i.currentSlide).css({
                zIndex: 2
            }).css({
                opacity: 1
            }) : i.slides.css({
                opacity: 0,
                display: "block",
                zIndex: 1
            }).eq(i.currentSlide).css({
                zIndex: 2
            }).animate({
                opacity: 1
            }, i.vars.animationSpeed, i.vars.easing)), i.vars.smoothHeight && g.smoothHeight();
            else {
                var a, n;
                "init" === t && (i.viewport = e('<div class="' + r + 'viewport"></div>').css({
                    overflow: "hidden",
                    position: "relative"
                }).appendTo(i).append(i.container), i.cloneCount = 0, i.cloneOffset = 0, v && (n = e.makeArray(i.slides).reverse(), i.slides = e(n), i.container.empty().append(i.slides))), i.vars.animationLoop && !p && (i.cloneCount = 2, i.cloneOffset = 1, "init" !== t && i.container.find(".clone").remove(), i.container.append(g.uniqueID(i.slides.first().clone().addClass("clone")).attr("aria-hidden", "true")).prepend(g.uniqueID(i.slides.last().clone().addClass("clone")).attr("aria-hidden", "true"))), i.newSlides = e(i.vars.selector, i), a = v ? i.count - 1 - i.currentSlide + i.cloneOffset : i.currentSlide + i.cloneOffset, u && !p ? (i.container.height(200 * (i.count + i.cloneCount) + "%").css("position", "absolute").width("100%"), setTimeout(function () {
                    i.newSlides.css({
                        display: "block"
                    }), i.doMath(), i.viewport.height(i.h), i.setProps(a * i.h, "init")
                }, "init" === t ? 100 : 0)) : (i.container.width(200 * (i.count + i.cloneCount) + "%"), i.setProps(a * i.computedW, "init"), setTimeout(function () {
                    i.doMath(), i.newSlides.css({
                        width: i.computedW,
                        marginRight: i.computedM,
                        float: "left",
                        display: "block"
                    }), i.vars.smoothHeight && g.smoothHeight()
                }, "init" === t ? 100 : 0))
            }
            p || i.slides.removeClass(r + "active-slide").eq(i.currentSlide).addClass(r + "active-slide"), i.vars.init(i)
        }, i.doMath = function () {
            var e = i.slides.first(),
                t = i.vars.itemMargin,
                a = i.vars.minItems,
                n = i.vars.maxItems;
            i.w = void 0 === i.viewport ? i.width() : i.viewport.width(), i.h = e.height(), i.boxPadding = e.outerWidth() - e.width(), p ? (i.itemT = i.vars.itemWidth + t, i.itemM = t, i.minW = a ? a * i.itemT : i.w, i.maxW = n ? n * i.itemT - t : i.w, i.itemW = i.minW > i.w ? (i.w - t * (a - 1)) / a : i.maxW < i.w ? (i.w - t * (n - 1)) / n : i.vars.itemWidth > i.w ? i.w : i.vars.itemWidth, i.visible = Math.floor(i.w / i.itemW), i.move = i.vars.move > 0 && i.vars.move < i.visible ? i.vars.move : i.visible, i.pagingCount = Math.ceil((i.count - i.visible) / i.move + 1), i.last = i.pagingCount - 1, i.limit = 1 === i.pagingCount ? 0 : i.vars.itemWidth > i.w ? i.itemW * (i.count - 1) + t * (i.count - 1) : (i.itemW + t) * i.count - i.w - t) : (i.itemW = i.w, i.itemM = t, i.pagingCount = i.count, i.last = i.count - 1), i.computedW = i.itemW - i.boxPadding, i.computedM = i.itemM
        }, i.update = function (e, t) {
            i.doMath(), p || (e < i.currentSlide ? i.currentSlide += 1 : e <= i.currentSlide && 0 !== e && (i.currentSlide -= 1), i.animatingTo = i.currentSlide), i.vars.controlNav && !i.manualControls && ("add" === t && !p || i.pagingCount > i.controlNav.length ? g.controlNav.update("add") : ("remove" === t && !p || i.pagingCount < i.controlNav.length) && (p && i.currentSlide > i.last && (i.currentSlide -= 1, i.animatingTo -= 1), g.controlNav.update("remove", i.last))), i.vars.directionNav && g.directionNav.update()
        }, i.addSlide = function (t, a) {
            var n = e(t);
            i.count += 1, i.last = i.count - 1, u && v ? void 0 !== a ? i.slides.eq(i.count - a).after(n) : i.container.prepend(n) : void 0 !== a ? i.slides.eq(a).before(n) : i.container.append(n), i.update(a, "add"), i.slides = e(i.vars.selector + ":not(.clone)", i), i.setup(), i.vars.added(i)
        }, i.removeSlide = function (t) {
            var a = isNaN(t) ? i.slides.index(e(t)) : t;
            i.count -= 1, i.last = i.count - 1, isNaN(t) ? e(t, i.slides).remove() : u && v ? i.slides.eq(i.last).remove() : i.slides.eq(t).remove(), i.doMath(), i.update(a, "remove"), i.slides = e(i.vars.selector + ":not(.clone)", i), i.setup(), i.vars.removed(i)
        }, g.init()
    }, e(window).blur(function (e) {
        t = !1
    }).focus(function (e) {
        t = !0
    }), e.flexslider.defaults = {
        namespace: "flex-",
        selector: ".slides > li",
        animation: "fade",
        easing: "swing",
        direction: "horizontal",
        reverse: !1,
        animationLoop: !0,
        smoothHeight: !1,
        startAt: 0,
        slideshow: !0,
        slideshowSpeed: 7e3,
        animationSpeed: 600,
        initDelay: 0,
        randomize: !1,
        fadeFirstSlide: !0,
        thumbCaptions: !1,
        pauseOnAction: !0,
        pauseOnHover: !1,
        pauseInvisible: !0,
        useCSS: !0,
        touch: !0,
        video: !1,
        controlNav: !0,
        directionNav: !0,
        prevText: "Previous",
        nextText: "Next",
        keyboard: !0,
        multipleKeyboard: !1,
        mousewheel: !1,
        pausePlay: !1,
        pauseText: "Pause",
        playText: "Play",
        controlsContainer: "",
        manualControls: "",
        customDirectionNav: "",
        sync: "",
        asNavFor: "",
        itemWidth: 0,
        itemMargin: 0,
        minItems: 1,
        maxItems: 0,
        move: 0,
        allowOneSlide: !0,
        start: function () {},
        before: function () {},
        after: function () {},
        end: function () {},
        added: function () {},
        removed: function () {},
        init: function () {}
    }, e.fn.flexslider = function (t) {
        if (void 0 === t && (t = {}), "object" == typeof t) return this.each(function () {
            var a = e(this),
                n = t.selector ? t.selector : ".slides > li",
                i = a.find(n);
            1 === i.length && t.allowOneSlide === !1 || 0 === i.length ? (i.fadeIn(400), t.start && t.start(a)) : void 0 === a.data("flexslider") && new e.flexslider(this, t)
        });
        var a = e(this).data("flexslider");
        switch (t) {
            case "play":
                a.play();
                break;
            case "pause":
                a.pause();
                break;
            case "stop":
                a.stop();
                break;
            case "next":
                a.flexAnimate(a.getTarget("next"), !0);
                break;
            case "prev":
            case "previous":
                a.flexAnimate(a.getTarget("prev"), !0);
                break;
            default:
                "number" == typeof t && a.flexAnimate(t, !0)
        }
    }
}(jQuery);
! function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
}(function (e) {
    var t, n, i, o, r, a, s = "Close",
        l = "BeforeClose",
        c = "AfterClose",
        d = "BeforeAppend",
        u = "MarkupParse",
        p = "Open",
        f = "Change",
        m = "mfp",
        g = "." + m,
        v = "mfp-ready",
        h = "mfp-removing",
        y = "mfp-prevent-close",
        C = function () {},
        w = !!window.jQuery,
        b = e(window),
        I = function (e, n) {
            t.ev.on(m + e + g, n)
        },
        x = function (t, n, i, o) {
            var r = document.createElement("div");
            return r.className = "mfp-" + t, i && (r.innerHTML = i), o ? n && n.appendChild(r) : (r = e(r), n && r.appendTo(n)), r
        },
        k = function (n, i) {
            t.ev.triggerHandler(m + n, i), t.st.callbacks && (n = n.charAt(0).toLowerCase() + n.slice(1), t.st.callbacks[n] && t.st.callbacks[n].apply(t, e.isArray(i) ? i : [i]))
        },
        T = function (n) {
            return n === a && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)), a = n), t.currTemplate.closeBtn
        },
        _ = function () {
            e.magnificPopup.instance || (t = new C, t.init(), e.magnificPopup.instance = t)
        },
        P = function () {
            var e = document.createElement("p").style,
                t = ["ms", "O", "Moz", "Webkit"];
            if (void 0 !== e.transition) return !0;
            for (; t.length;)
                if (t.pop() + "Transition" in e) return !0;
            return !1
        };
    C.prototype = {
        constructor: C,
        init: function () {
            var n = navigator.appVersion;
            t.isLowIE = t.isIE8 = document.all && !document.addEventListener, t.isAndroid = /android/gi.test(n), t.isIOS = /iphone|ipad|ipod/gi.test(n), t.supportsTransition = P(), t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), i = e(document), t.popupsCache = {}
        },
        open: function (n) {
            var o;
            if (n.isObj === !1) {
                t.items = n.items.toArray(), t.index = 0;
                var a, s = n.items;
                for (o = 0; o < s.length; o++)
                    if (a = s[o], a.parsed && (a = a.el[0]), a === n.el[0]) {
                        t.index = o;
                        break
                    }
            } else t.items = e.isArray(n.items) ? n.items : [n.items], t.index = n.index || 0;
            if (t.isOpen) return void t.updateItemHTML();
            t.types = [], r = "", n.mainEl && n.mainEl.length ? t.ev = n.mainEl.eq(0) : t.ev = i, n.key ? (t.popupsCache[n.key] || (t.popupsCache[n.key] = {}), t.currTemplate = t.popupsCache[n.key]) : t.currTemplate = {}, t.st = e.extend(!0, {}, e.magnificPopup.defaults, n), t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = x("bg").on("click" + g, function () {
                t.close()
            }), t.wrap = x("wrap").attr("tabindex", -1).on("click" + g, function (e) {
                t._checkIfClose(e.target) && t.close()
            }), t.container = x("container", t.wrap)), t.contentContainer = x("content"), t.st.preloader && (t.preloader = x("preloader", t.container, t.st.tLoading));
            var l = e.magnificPopup.modules;
            for (o = 0; o < l.length; o++) {
                var c = l[o];
                c = c.charAt(0).toUpperCase() + c.slice(1), t["init" + c].call(t)
            }
            k("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (I(u, function (e, t, n, i) {
                n.close_replaceWith = T(i.type)
            }), r += " mfp-close-btn-in") : t.wrap.append(T())), t.st.alignTop && (r += " mfp-align-top"), t.fixedContentPos ? t.wrap.css({
                overflow: t.st.overflowY,
                overflowX: "hidden",
                overflowY: t.st.overflowY
            }) : t.wrap.css({
                top: b.scrollTop(),
                position: "absolute"
            }), (t.st.fixedBgPos === !1 || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                height: i.height(),
                position: "absolute"
            }), t.st.enableEscapeKey && i.on("keyup" + g, function (e) {
                27 === e.keyCode && t.close()
            }), b.on("resize" + g, function () {
                t.updateSize()
            }), t.st.closeOnContentClick || (r += " mfp-auto-cursor"), r && t.wrap.addClass(r);
            var d = t.wH = b.height(),
                f = {};
            if (t.fixedContentPos && t._hasScrollBar(d)) {
                var m = t._getScrollbarSize();
                m && (f.marginRight = m)
            }
            t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : f.overflow = "hidden");
            var h = t.st.mainClass;
            return t.isIE7 && (h += " mfp-ie7"), h && t._addClassToMFP(h), t.updateItemHTML(), k("BuildControls"), e("html").css(f), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || e(document.body)), t._lastFocusedEl = document.activeElement, setTimeout(function () {
                t.content ? (t._addClassToMFP(v), t._setFocus()) : t.bgOverlay.addClass(v), i.on("focusin" + g, t._onFocusIn)
            }, 16), t.isOpen = !0, t.updateSize(d), k(p), n
        },
        close: function () {
            t.isOpen && (k(l), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(h), setTimeout(function () {
                t._close()
            }, t.st.removalDelay)) : t._close())
        },
        _close: function () {
            k(s);
            var n = h + " " + v + " ";
            if (t.bgOverlay.detach(), t.wrap.detach(), t.container.empty(), t.st.mainClass && (n += t.st.mainClass + " "), t._removeClassFromMFP(n), t.fixedContentPos) {
                var o = {
                    marginRight: ""
                };
                t.isIE7 ? e("body, html").css("overflow", "") : o.overflow = "", e("html").css(o)
            }
            i.off("keyup" + g + " focusin" + g), t.ev.off(g), t.wrap.attr("class", "mfp-wrap").removeAttr("style"), t.bgOverlay.attr("class", "mfp-bg"), t.container.attr("class", "mfp-container"), !t.st.showCloseBtn || t.st.closeBtnInside && t.currTemplate[t.currItem.type] !== !0 || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(), t.st.autoFocusLast && t._lastFocusedEl && e(t._lastFocusedEl).focus(), t.currItem = null, t.content = null, t.currTemplate = null, t.prevHeight = 0, k(c)
        },
        updateSize: function (e) {
            if (t.isIOS) {
                var n = document.documentElement.clientWidth / window.innerWidth,
                    i = window.innerHeight * n;
                t.wrap.css("height", i), t.wH = i
            } else t.wH = e || b.height();
            t.fixedContentPos || t.wrap.css("height", t.wH), k("Resize")
        },
        updateItemHTML: function () {
            var n = t.items[t.index];
            t.contentContainer.detach(), t.content && t.content.detach(), n.parsed || (n = t.parseEl(t.index));
            var i = n.type;
            if (k("BeforeChange", [t.currItem ? t.currItem.type : "", i]), t.currItem = n, !t.currTemplate[i]) {
                var r = !!t.st[i] && t.st[i].markup;
                k("FirstMarkupParse", r), r ? t.currTemplate[i] = e(r) : t.currTemplate[i] = !0
            }
            o && o !== n.type && t.container.removeClass("mfp-" + o + "-holder");
            var a = t["get" + i.charAt(0).toUpperCase() + i.slice(1)](n, t.currTemplate[i]);
            t.appendContent(a, i), n.preloaded = !0, k(f, n), o = n.type, t.container.prepend(t.contentContainer), k("AfterChange")
        },
        appendContent: function (e, n) {
            t.content = e, e ? t.st.showCloseBtn && t.st.closeBtnInside && t.currTemplate[n] === !0 ? t.content.find(".mfp-close").length || t.content.append(T()) : t.content = e : t.content = "", k(d), t.container.addClass("mfp-" + n + "-holder"), t.contentContainer.append(t.content)
        },
        parseEl: function (n) {
            var i, o = t.items[n];
            if (o.tagName ? o = {
                    el: e(o)
                } : (i = o.type, o = {
                    data: o,
                    src: o.src
                }), o.el) {
                for (var r = t.types, a = 0; a < r.length; a++)
                    if (o.el.hasClass("mfp-" + r[a])) {
                        i = r[a];
                        break
                    }
                o.src = o.el.attr("data-mfp-src"), o.src || (o.src = o.el.attr("href"))
            }
            return o.type = i || t.st.type || "inline", o.index = n, o.parsed = !0, t.items[n] = o, k("ElementParse", o), t.items[n]
        },
        addGroup: function (e, n) {
            var i = function (i) {
                i.mfpEl = this, t._openClick(i, e, n)
            };
            n || (n = {});
            var o = "click.magnificPopup";
            n.mainEl = e, n.items ? (n.isObj = !0, e.off(o).on(o, i)) : (n.isObj = !1, n.delegate ? e.off(o).on(o, n.delegate, i) : (n.items = e, e.off(o).on(o, i)))
        },
        _openClick: function (n, i, o) {
            var r = void 0 !== o.midClick ? o.midClick : e.magnificPopup.defaults.midClick;
            if (r || !(2 === n.which || n.ctrlKey || n.metaKey || n.altKey || n.shiftKey)) {
                var a = void 0 !== o.disableOn ? o.disableOn : e.magnificPopup.defaults.disableOn;
                if (a)
                    if (e.isFunction(a)) {
                        if (!a.call(t)) return !0
                    } else if (b.width() < a) return !0;
                n.type && (n.preventDefault(), t.isOpen && n.stopPropagation()), o.el = e(n.mfpEl), o.delegate && (o.items = i.find(o.delegate)), t.open(o)
            }
        },
        updateStatus: function (e, i) {
            if (t.preloader) {
                n !== e && t.container.removeClass("mfp-s-" + n), i || "loading" !== e || (i = t.st.tLoading);
                var o = {
                    status: e,
                    text: i
                };
                k("UpdateStatus", o), e = o.status, i = o.text, t.preloader.html(i), t.preloader.find("a").on("click", function (e) {
                    e.stopImmediatePropagation()
                }), t.container.addClass("mfp-s-" + e), n = e
            }
        },
        _checkIfClose: function (n) {
            if (!e(n).hasClass(y)) {
                var i = t.st.closeOnContentClick,
                    o = t.st.closeOnBgClick;
                if (i && o) return !0;
                if (!t.content || e(n).hasClass("mfp-close") || t.preloader && n === t.preloader[0]) return !0;
                if (n === t.content[0] || e.contains(t.content[0], n)) {
                    if (i) return !0
                } else if (o && e.contains(document, n)) return !0;
                return !1
            }
        },
        _addClassToMFP: function (e) {
            t.bgOverlay.addClass(e), t.wrap.addClass(e)
        },
        _removeClassFromMFP: function (e) {
            this.bgOverlay.removeClass(e), t.wrap.removeClass(e)
        },
        _hasScrollBar: function (e) {
            return (t.isIE7 ? i.height() : document.body.scrollHeight) > (e || b.height())
        },
        _setFocus: function () {
            (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
        },
        _onFocusIn: function (n) {
            return n.target === t.wrap[0] || e.contains(t.wrap[0], n.target) ? void 0 : (t._setFocus(), !1)
        },
        _parseMarkup: function (t, n, i) {
            var o;
            i.data && (n = e.extend(i.data, n)), k(u, [t, n, i]), e.each(n, function (n, i) {
                if (void 0 === i || i === !1) return !0;
                if (o = n.split("_"), o.length > 1) {
                    var r = t.find(g + "-" + o[0]);
                    if (r.length > 0) {
                        var a = o[1];
                        "replaceWith" === a ? r[0] !== i[0] && r.replaceWith(i) : "img" === a ? r.is("img") ? r.attr("src", i) : r.replaceWith(e("<img>").attr("src", i).attr("class", r.attr("class"))) : r.attr(o[1], i)
                    }
                } else t.find(g + "-" + n).html(i)
            })
        },
        _getScrollbarSize: function () {
            if (void 0 === t.scrollbarSize) {
                var e = document.createElement("div");
                e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(e), t.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e)
            }
            return t.scrollbarSize
        }
    }, e.magnificPopup = {
        instance: null,
        proto: C.prototype,
        modules: [],
        open: function (t, n) {
            return _(), t = t ? e.extend(!0, {}, t) : {}, t.isObj = !0, t.index = n || 0, this.instance.open(t)
        },
        close: function () {
            return e.magnificPopup.instance && e.magnificPopup.instance.close()
        },
        registerModule: function (t, n) {
            n.options && (e.magnificPopup.defaults[t] = n.options), e.extend(this.proto, n.proto), this.modules.push(t)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading...",
            autoFocusLast: !0
        }
    }, e.fn.magnificPopup = function (n) {
        _();
        var i = e(this);
        if ("string" == typeof n)
            if ("open" === n) {
                var o, r = w ? i.data("magnificPopup") : i[0].magnificPopup,
                    a = parseInt(arguments[1], 10) || 0;
                r.items ? o = r.items[a] : (o = i, r.delegate && (o = o.find(r.delegate)), o = o.eq(a)), t._openClick({
                    mfpEl: o
                }, i, r)
            } else t.isOpen && t[n].apply(t, Array.prototype.slice.call(arguments, 1));
        else n = e.extend(!0, {}, n), w ? i.data("magnificPopup", n) : i[0].magnificPopup = n, t.addGroup(i, n);
        return i
    };
    var S, E, z, O = "inline",
        M = function () {
            z && (E.after(z.addClass(S)).detach(), z = null)
        };
    e.magnificPopup.registerModule(O, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function () {
                t.types.push(O), I(s + "." + O, function () {
                    M()
                })
            },
            getInline: function (n, i) {
                if (M(), n.src) {
                    var o = t.st.inline,
                        r = e(n.src);
                    if (r.length) {
                        var a = r[0].parentNode;
                        a && a.tagName && (E || (S = o.hiddenClass, E = x(S), S = "mfp-" + S), z = r.after(E).detach().removeClass(S)), t.updateStatus("ready")
                    } else t.updateStatus("error", o.tNotFound), r = e("<div>");
                    return n.inlineElement = r, r
                }
                return t.updateStatus("ready"), t._parseMarkup(i, {}, n), i
            }
        }
    });
    var B, L = "ajax",
        H = function () {
            B && e(document.body).removeClass(B)
        },
        A = function () {
            H(), t.req && t.req.abort()
        };
    e.magnificPopup.registerModule(L, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        },
        proto: {
            initAjax: function () {
                t.types.push(L), B = t.st.ajax.cursor, I(s + "." + L, A), I("BeforeChange." + L, A)
            },
            getAjax: function (n) {
                B && e(document.body).addClass(B), t.updateStatus("loading");
                var i = e.extend({
                    url: n.src,
                    success: function (i, o, r) {
                        var a = {
                            data: i,
                            xhr: r
                        };
                        k("ParseAjax", a), t.appendContent(e(a.data), L), n.finished = !0, H(), t._setFocus(), setTimeout(function () {
                            t.wrap.addClass(v)
                        }, 16), t.updateStatus("ready"), k("AjaxContentAdded")
                    },
                    error: function () {
                        H(), n.finished = n.loadError = !0, t.updateStatus("error", t.st.ajax.tError.replace("%url%", n.src))
                    }
                }, t.st.ajax.settings);
                return t.req = e.ajax(i), ""
            }
        }
    });
    var F, j = function (n) {
        if (n.data && void 0 !== n.data.title) return n.data.title;
        var i = t.st.image.titleSrc;
        if (i) {
            if (e.isFunction(i)) return i.call(t, n);
            if (n.el) return n.el.attr(i) || ""
        }
        return ""
    };
    e.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        },
        proto: {
            initImage: function () {
                var n = t.st.image,
                    i = ".image";
                t.types.push("image"), I(p + i, function () {
                    "image" === t.currItem.type && n.cursor && e(document.body).addClass(n.cursor)
                }), I(s + i, function () {
                    n.cursor && e(document.body).removeClass(n.cursor), b.off("resize" + g)
                }), I("Resize" + i, t.resizeImage), t.isLowIE && I("AfterChange", t.resizeImage)
            },
            resizeImage: function () {
                var e = t.currItem;
                if (e && e.img && t.st.image.verticalFit) {
                    var n = 0;
                    t.isLowIE && (n = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", t.wH - n)
                }
            },
            _onImageHasSize: function (e) {
                e.img && (e.hasSize = !0, F && clearInterval(F), e.isCheckingImgSize = !1, k("ImageHasSize", e), e.imgHidden && (t.content && t.content.removeClass("mfp-loading"), e.imgHidden = !1))
            },
            findImageSize: function (e) {
                var n = 0,
                    i = e.img[0],
                    o = function (r) {
                        F && clearInterval(F), F = setInterval(function () {
                            return i.naturalWidth > 0 ? void t._onImageHasSize(e) : (n > 200 && clearInterval(F), n++, void(3 === n ? o(10) : 40 === n ? o(50) : 100 === n && o(500)))
                        }, r)
                    };
                o(1)
            },
            getImage: function (n, i) {
                var o = 0,
                    r = function () {
                        n && (n.img[0].complete ? (n.img.off(".mfploader"), n === t.currItem && (t._onImageHasSize(n), t.updateStatus("ready")), n.hasSize = !0, n.loaded = !0, k("ImageLoadComplete")) : (o++, 200 > o ? setTimeout(r, 100) : a()))
                    },
                    a = function () {
                        n && (n.img.off(".mfploader"), n === t.currItem && (t._onImageHasSize(n), t.updateStatus("error", s.tError.replace("%url%", n.src))), n.hasSize = !0, n.loaded = !0, n.loadError = !0)
                    },
                    s = t.st.image,
                    l = i.find(".mfp-img");
                if (l.length) {
                    var c = document.createElement("img");
                    c.className = "mfp-img", n.el && n.el.find("img").length && (c.alt = n.el.find("img").attr("alt")), n.img = e(c).on("load.mfploader", r).on("error.mfploader", a), c.src = n.src, l.is("img") && (n.img = n.img.clone()), c = n.img[0], c.naturalWidth > 0 ? n.hasSize = !0 : c.width || (n.hasSize = !1)
                }
                return t._parseMarkup(i, {
                    title: j(n),
                    img_replaceWith: n.img
                }, n), t.resizeImage(), n.hasSize ? (F && clearInterval(F), n.loadError ? (i.addClass("mfp-loading"), t.updateStatus("error", s.tError.replace("%url%", n.src))) : (i.removeClass("mfp-loading"), t.updateStatus("ready")), i) : (t.updateStatus("loading"), n.loading = !0, n.hasSize || (n.imgHidden = !0, i.addClass("mfp-loading"), t.findImageSize(n)), i)
            }
        }
    });
    var N, W = function () {
        return void 0 === N && (N = void 0 !== document.createElement("p").style.MozTransform), N
    };
    e.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function (e) {
                return e.is("img") ? e : e.find("img")
            }
        },
        proto: {
            initZoom: function () {
                var e, n = t.st.zoom,
                    i = ".zoom";
                if (n.enabled && t.supportsTransition) {
                    var o, r, a = n.duration,
                        c = function (e) {
                            var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                i = "all " + n.duration / 1e3 + "s " + n.easing,
                                o = {
                                    position: "fixed",
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    "-webkit-backface-visibility": "hidden"
                                },
                                r = "transition";
                            return o["-webkit-" + r] = o["-moz-" + r] = o["-o-" + r] = o[r] = i, t.css(o), t
                        },
                        d = function () {
                            t.content.css("visibility", "visible")
                        };
                    I("BuildControls" + i, function () {
                        if (t._allowZoom()) {
                            if (clearTimeout(o), t.content.css("visibility", "hidden"), e = t._getItemToZoom(), !e) return void d();
                            r = c(e), r.css(t._getOffset()), t.wrap.append(r), o = setTimeout(function () {
                                r.css(t._getOffset(!0)), o = setTimeout(function () {
                                    d(), setTimeout(function () {
                                        r.remove(), e = r = null, k("ZoomAnimationEnded")
                                    }, 16)
                                }, a)
                            }, 16)
                        }
                    }), I(l + i, function () {
                        if (t._allowZoom()) {
                            if (clearTimeout(o), t.st.removalDelay = a, !e) {
                                if (e = t._getItemToZoom(), !e) return;
                                r = c(e)
                            }
                            r.css(t._getOffset(!0)), t.wrap.append(r), t.content.css("visibility", "hidden"), setTimeout(function () {
                                r.css(t._getOffset())
                            }, 16)
                        }
                    }), I(s + i, function () {
                        t._allowZoom() && (d(), r && r.remove(), e = null)
                    })
                }
            },
            _allowZoom: function () {
                return "image" === t.currItem.type
            },
            _getItemToZoom: function () {
                return !!t.currItem.hasSize && t.currItem.img
            },
            _getOffset: function (n) {
                var i;
                i = n ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem);
                var o = i.offset(),
                    r = parseInt(i.css("padding-top"), 10),
                    a = parseInt(i.css("padding-bottom"), 10);
                o.top -= e(window).scrollTop() - r;
                var s = {
                    width: i.width(),
                    height: (w ? i.innerHeight() : i[0].offsetHeight) - a - r
                };
                return W() ? s["-moz-transform"] = s.transform = "translate(" + o.left + "px," + o.top + "px)" : (s.left = o.left, s.top = o.top), s
            }
        }
    });
    var Z = "iframe",
        q = "//about:blank",
        R = function (e) {
            if (t.currTemplate[Z]) {
                var n = t.currTemplate[Z].find("iframe");
                n.length && (e || (n[0].src = q), t.isIE8 && n.css("display", e ? "block" : "none"))
            }
        };
    e.magnificPopup.registerModule(Z, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {
                    index: "youtube.com",
                    id: "v=",
                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                },
                vimeo: {
                    index: "vimeo.com/",
                    id: "/",
                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                },
                gmaps: {
                    index: "//maps.google.",
                    src: "%id%&output=embed"
                }
            }
        },
        proto: {
            initIframe: function () {
                t.types.push(Z), I("BeforeChange", function (e, t, n) {
                    t !== n && (t === Z ? R() : n === Z && R(!0))
                }), I(s + "." + Z, function () {
                    R()
                })
            },
            getIframe: function (n, i) {
                var o = n.src,
                    r = t.st.iframe;
                e.each(r.patterns, function () {
                    return o.indexOf(this.index) > -1 ? (this.id && (o = "string" == typeof this.id ? o.substr(o.lastIndexOf(this.id) + this.id.length, o.length) : this.id.call(this, o)), o = this.src.replace("%id%", o), !1) : void 0
                });
                var a = {};
                return r.srcAction && (a[r.srcAction] = o), t._parseMarkup(i, a, n), t.updateStatus("ready"), i
            }
        }
    });
    var K = function (e) {
            var n = t.items.length;
            return e > n - 1 ? e - n : 0 > e ? n + e : e
        },
        D = function (e, t, n) {
            return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, n)
        };
    e.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [0, 2],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        },
        proto: {
            initGallery: function () {
                var n = t.st.gallery,
                    o = ".mfp-gallery";
                return t.direction = !0, !(!n || !n.enabled) && (r += " mfp-gallery", I(p + o, function () {
                    n.navigateByImgClick && t.wrap.on("click" + o, ".mfp-img", function () {
                        return t.items.length > 1 ? (t.next(), !1) : void 0
                    }), i.on("keydown" + o, function (e) {
                        37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                    })
                }), I("UpdateStatus" + o, function (e, n) {
                    n.text && (n.text = D(n.text, t.currItem.index, t.items.length))
                }), I(u + o, function (e, i, o, r) {
                    var a = t.items.length;
                    o.counter = a > 1 ? D(n.tCounter, r.index, a) : ""
                }), I("BuildControls" + o, function () {
                    if (t.items.length > 1 && n.arrows && !t.arrowLeft) {
                        var i = n.arrowMarkup,
                            o = t.arrowLeft = e(i.replace(/%title%/gi, n.tPrev).replace(/%dir%/gi, "left")).addClass(y),
                            r = t.arrowRight = e(i.replace(/%title%/gi, n.tNext).replace(/%dir%/gi, "right")).addClass(y);
                        o.click(function () {
                            t.prev()
                        }), r.click(function () {
                            t.next()
                        }), t.container.append(o.add(r))
                    }
                }), I(f + o, function () {
                    t._preloadTimeout && clearTimeout(t._preloadTimeout), t._preloadTimeout = setTimeout(function () {
                        t.preloadNearbyImages(), t._preloadTimeout = null
                    }, 16)
                }), void I(s + o, function () {
                    i.off(o), t.wrap.off("click" + o), t.arrowRight = t.arrowLeft = null
                }))
            },
            next: function () {
                t.direction = !0, t.index = K(t.index + 1), t.updateItemHTML()
            },
            prev: function () {
                t.direction = !1, t.index = K(t.index - 1), t.updateItemHTML()
            },
            goTo: function (e) {
                t.direction = e >= t.index, t.index = e, t.updateItemHTML()
            },
            preloadNearbyImages: function () {
                var e, n = t.st.gallery.preload,
                    i = Math.min(n[0], t.items.length),
                    o = Math.min(n[1], t.items.length);
                for (e = 1; e <= (t.direction ? o : i); e++) t._preloadItem(t.index + e);
                for (e = 1; e <= (t.direction ? i : o); e++) t._preloadItem(t.index - e)
            },
            _preloadItem: function (n) {
                if (n = K(n), !t.items[n].preloaded) {
                    var i = t.items[n];
                    i.parsed || (i = t.parseEl(n)), k("LazyLoad", i), "image" === i.type && (i.img = e('<img class="mfp-img" />').on("load.mfploader", function () {
                        i.hasSize = !0
                    }).on("error.mfploader", function () {
                        i.hasSize = !0, i.loadError = !0, k("LazyLoadError", i)
                    }).attr("src", i.src)), i.preloaded = !0
                }
            }
        }
    });
    var U = "retina";
    e.magnificPopup.registerModule(U, {
        options: {
            replaceSrc: function (e) {
                return e.src.replace(/\.\w+$/, function (e) {
                    return "@2x" + e
                })
            },
            ratio: 1
        },
        proto: {
            initRetina: function () {
                if (window.devicePixelRatio > 1) {
                    var e = t.st.retina,
                        n = e.ratio;
                    n = isNaN(n) ? n() : n, n > 1 && (I("ImageHasSize." + U, function (e, t) {
                        t.img.css({
                            "max-width": t.img[0].naturalWidth / n,
                            width: "100%"
                        })
                    }), I("ElementParse." + U, function (t, i) {
                        i.src = e.replaceSrc(i, n)
                    }))
                }
            }
        }
    }), _()
});
! function (e, t) {
    "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t() : e.PhotoSwipe = t()
}(this, function () {
    "use strict";
    var e = function (e, t, n, i) {
        var o = {
            features: null,
            bind: function (e, t, n, i) {
                var o = (i ? "remove" : "add") + "EventListener";
                t = t.split(" ");
                for (var a = 0; a < t.length; a++) t[a] && e[o](t[a], n, !1)
            },
            isArray: function (e) {
                return e instanceof Array
            },
            createEl: function (e, t) {
                var n = document.createElement(t || "div");
                return e && (n.className = e), n
            },
            getScrollY: function () {
                var e = window.pageYOffset;
                return void 0 !== e ? e : document.documentElement.scrollTop
            },
            unbind: function (e, t, n) {
                o.bind(e, t, n, !0)
            },
            removeClass: function (e, t) {
                var n = new RegExp("(\\s|^)" + t + "(\\s|$)");
                e.className = e.className.replace(n, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
            },
            addClass: function (e, t) {
                o.hasClass(e, t) || (e.className += (e.className ? " " : "") + t)
            },
            hasClass: function (e, t) {
                return e.className && new RegExp("(^|\\s)" + t + "(\\s|$)").test(e.className)
            },
            getChildByClass: function (e, t) {
                for (var n = e.firstChild; n;) {
                    if (o.hasClass(n, t)) return n;
                    n = n.nextSibling
                }
            },
            arraySearch: function (e, t, n) {
                for (var i = e.length; i--;)
                    if (e[i][n] === t) return i;
                return -1
            },
            extend: function (e, t, n) {
                for (var i in t)
                    if (t.hasOwnProperty(i)) {
                        if (n && e.hasOwnProperty(i)) continue;
                        e[i] = t[i]
                    }
            },
            easing: {
                sine: {
                    out: function (e) {
                        return Math.sin(e * (Math.PI / 2))
                    },
                    inOut: function (e) {
                        return -(Math.cos(Math.PI * e) - 1) / 2
                    }
                },
                cubic: {
                    out: function (e) {
                        return --e * e * e + 1
                    }
                }
            },
            detectFeatures: function () {
                if (o.features) return o.features;
                var e = o.createEl(),
                    t = e.style,
                    n = "",
                    i = {};
                if (i.oldIE = document.all && !document.addEventListener, i.touch = "ontouchstart" in window, window.requestAnimationFrame && (i.raf = window.requestAnimationFrame, i.caf = window.cancelAnimationFrame), i.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !i.pointerEvent) {
                    var a = navigator.userAgent;
                    if (/iP(hone|od)/.test(navigator.platform)) {
                        var r = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                        r && r.length > 0 && (r = parseInt(r[1], 10), r >= 1 && 8 > r && (i.isOldIOSPhone = !0))
                    }
                    var l = a.match(/Android\s([0-9\.]*)/),
                        s = l ? l[1] : 0;
                    s = parseFloat(s), s >= 1 && (4.4 > s && (i.isOldAndroid = !0), i.androidVersion = s), i.isMobileOpera = /opera mini|opera mobi/i.test(a)
                }
                for (var u, c, d = ["transform", "perspective", "animationName"], p = ["", "webkit", "Moz", "ms", "O"], m = 0; 4 > m; m++) {
                    n = p[m];
                    for (var f = 0; 3 > f; f++) u = d[f], c = n + (n ? u.charAt(0).toUpperCase() + u.slice(1) : u), !i[u] && c in t && (i[u] = c);
                    n && !i.raf && (n = n.toLowerCase(), i.raf = window[n + "RequestAnimationFrame"], i.raf && (i.caf = window[n + "CancelAnimationFrame"] || window[n + "CancelRequestAnimationFrame"]))
                }
                if (!i.raf) {
                    var h = 0;
                    i.raf = function (e) {
                        var t = (new Date).getTime(),
                            n = Math.max(0, 16 - (t - h)),
                            i = window.setTimeout(function () {
                                e(t + n)
                            }, n);
                        return h = t + n, i
                    }, i.caf = function (e) {
                        clearTimeout(e)
                    }
                }
                return i.svg = !!document.createElementNS && !!document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, o.features = i, i
            }
        };
        o.detectFeatures(), o.features.oldIE && (o.bind = function (e, t, n, i) {
            t = t.split(" ");
            for (var o, a = (i ? "detach" : "attach") + "Event", r = function () {
                    n.handleEvent.call(n)
                }, l = 0; l < t.length; l++)
                if (o = t[l])
                    if ("object" == typeof n && n.handleEvent) {
                        if (i) {
                            if (!n["oldIE" + o]) return !1
                        } else n["oldIE" + o] = r;
                        e[a]("on" + o, n["oldIE" + o])
                    } else e[a]("on" + o, n)
        });
        var a = this,
            r = 25,
            l = 3,
            s = {
                allowPanToNext: !0,
                spacing: .12,
                bgOpacity: 1,
                mouseUsed: !1,
                loop: !0,
                pinchToClose: !0,
                closeOnScroll: !0,
                closeOnVerticalDrag: !0,
                verticalDragRange: .75,
                hideAnimationDuration: 333,
                showAnimationDuration: 333,
                showHideOpacity: !1,
                focus: !0,
                escKey: !0,
                arrowKeys: !0,
                mainScrollEndFriction: .35,
                panEndFriction: .35,
                isClickableElement: function (e) {
                    return "A" === e.tagName
                },
                getDoubleTapZoom: function (e, t) {
                    return e ? 1 : t.initialZoomLevel < .7 ? 1 : 1.33
                },
                maxSpreadZoom: 1.33,
                modal: !0,
                scaleMode: "fit"
            };
        o.extend(s, i);
        var u, c, d, p, m, f, h, v, y, x, g, w, b, I, C, D, M, T, S, A, E, O, k, R, Z, P, F, L, _, z, N, U, H, Y, B, W, G, X, V, K, q, $, j, J, Q, ee, te, ne, ie, oe, ae, re, le, se, ue, ce, de = function () {
                return {
                    x: 0,
                    y: 0
                }
            },
            pe = de(),
            me = de(),
            fe = de(),
            he = {},
            ve = 0,
            ye = {},
            xe = de(),
            ge = 0,
            we = !0,
            be = [],
            Ie = {},
            Ce = !1,
            De = function (e, t) {
                o.extend(a, t.publicMethods), be.push(e)
            },
            Me = function (e) {
                var t = Qt();
                return e > t - 1 ? e - t : 0 > e ? t + e : e
            },
            Te = {},
            Se = function (e, t) {
                return Te[e] || (Te[e] = []), Te[e].push(t)
            },
            Ae = function (e) {
                var t = Te[e];
                if (t) {
                    var n = Array.prototype.slice.call(arguments);
                    n.shift();
                    for (var i = 0; i < t.length; i++) t[i].apply(a, n)
                }
            },
            Ee = function () {
                return (new Date).getTime()
            },
            Oe = function (e) {
                se = e, a.bg.style.opacity = e * s.bgOpacity
            },
            ke = function (e, t, n, i, o) {
                (!Ce || o && o !== a.currItem) && (i /= o ? o.fitRatio : a.currItem.fitRatio), e[O] = w + t + "px, " + n + "px" + b + " scale(" + i + ")"
            },
            Re = function (e) {
                ie && (e && (x > a.currItem.fitRatio ? Ce || (pn(a.currItem, !1, !0), Ce = !0) : Ce && (pn(a.currItem), Ce = !1)), ke(ie, fe.x, fe.y, x))
            },
            Ze = function (e) {
                e.container && ke(e.container.style, e.initialPosition.x, e.initialPosition.y, e.initialZoomLevel, e)
            },
            Pe = function (e, t) {
                t[O] = w + e + "px, 0px" + b
            },
            Fe = function (e, t) {
                if (!s.loop && t) {
                    var n = p + (xe.x * ve - e) / xe.x,
                        i = Math.round(e - xt.x);
                    (0 > n && i > 0 || n >= Qt() - 1 && 0 > i) && (e = xt.x + i * s.mainScrollEndFriction)
                }
                xt.x = e, Pe(e, m)
            },
            Le = function (e, t) {
                var n = gt[e] - ye[e];
                return me[e] + pe[e] + n - n * (t / g)
            },
            _e = function (e, t) {
                e.x = t.x, e.y = t.y, t.id && (e.id = t.id)
            },
            ze = function (e) {
                e.x = Math.round(e.x), e.y = Math.round(e.y)
            },
            Ne = null,
            Ue = function () {
                Ne && (o.unbind(document, "mousemove", Ue), o.addClass(e, "pswp--has_mouse"), s.mouseUsed = !0, Ae("mouseUsed")), Ne = setTimeout(function () {
                    Ne = null
                }, 100)
            },
            He = function () {
                o.bind(document, "keydown", a), N.transform && o.bind(a.scrollWrap, "click", a), s.mouseUsed || o.bind(document, "mousemove", Ue), o.bind(window, "resize scroll", a), Ae("bindEvents")
            },
            Ye = function () {
                o.unbind(window, "resize", a), o.unbind(window, "scroll", y.scroll), o.unbind(document, "keydown", a), o.unbind(document, "mousemove", Ue), N.transform && o.unbind(a.scrollWrap, "click", a), X && o.unbind(window, h, a), Ae("unbindEvents")
            },
            Be = function (e, t) {
                var n = sn(a.currItem, he, e);
                return t && (ne = n), n
            },
            We = function (e) {
                return e || (e = a.currItem), e.initialZoomLevel
            },
            Ge = function (e) {
                return e || (e = a.currItem), e.w > 0 ? s.maxSpreadZoom : 1
            },
            Xe = function (e, t, n, i) {
                return i === a.currItem.initialZoomLevel ? (n[e] = a.currItem.initialPosition[e], !0) : (n[e] = Le(e, i), n[e] > t.min[e] ? (n[e] = t.min[e], !0) : n[e] < t.max[e] && (n[e] = t.max[e], !0))
            },
            Ve = function () {
                if (O) {
                    var t = N.perspective && !R;
                    return w = "translate" + (t ? "3d(" : "("), void(b = N.perspective ? ", 0px)" : ")")
                }
                O = "left", o.addClass(e, "pswp--ie"), Pe = function (e, t) {
                    t.left = e + "px"
                }, Ze = function (e) {
                    var t = e.fitRatio > 1 ? 1 : e.fitRatio,
                        n = e.container.style,
                        i = t * e.w,
                        o = t * e.h;
                    n.width = i + "px", n.height = o + "px", n.left = e.initialPosition.x + "px", n.top = e.initialPosition.y + "px"
                }, Re = function () {
                    if (ie) {
                        var e = ie,
                            t = a.currItem,
                            n = t.fitRatio > 1 ? 1 : t.fitRatio,
                            i = n * t.w,
                            o = n * t.h;
                        e.width = i + "px", e.height = o + "px", e.left = fe.x + "px", e.top = fe.y + "px"
                    }
                }
            },
            Ke = function (e) {
                var t = "";
                s.escKey && 27 === e.keyCode ? t = "close" : s.arrowKeys && (37 === e.keyCode ? t = "prev" : 39 === e.keyCode && (t = "next")), t && (e.ctrlKey || e.altKey || e.shiftKey || e.metaKey || (e.preventDefault ? e.preventDefault() : e.returnValue = !1, a[t]()))
            },
            qe = function (e) {
                e && (q || K || oe || W) && (e.preventDefault(), e.stopPropagation())
            },
            $e = function () {
                a.setScrollOffset(0, o.getScrollY())
            },
            je = {},
            Je = 0,
            Qe = function (e) {
                je[e] && (je[e].raf && P(je[e].raf), Je--, delete je[e])
            },
            et = function (e) {
                je[e] && Qe(e), je[e] || (Je++, je[e] = {})
            },
            tt = function () {
                for (var e in je) je.hasOwnProperty(e) && Qe(e)
            },
            nt = function (e, t, n, i, o, a, r) {
                var l, s = Ee();
                et(e);
                var u = function () {
                    if (je[e]) {
                        if (l = Ee() - s, l >= i) return Qe(e), a(n), void(r && r());
                        a((n - t) * o(l / i) + t), je[e].raf = Z(u)
                    }
                };
                u()
            },
            it = {
                shout: Ae,
                listen: Se,
                viewportSize: he,
                options: s,
                isMainScrollAnimating: function () {
                    return oe
                },
                getZoomLevel: function () {
                    return x
                },
                getCurrentIndex: function () {
                    return p
                },
                isDragging: function () {
                    return X
                },
                isZooming: function () {
                    return Q
                },
                setScrollOffset: function (e, t) {
                    ye.x = e, z = ye.y = t, Ae("updateScrollOffset", ye)
                },
                applyZoomPan: function (e, t, n, i) {
                    fe.x = t, fe.y = n, x = e, Re(i)
                },
                init: function () {
                    if (!u && !c) {
                        var n;
                        a.framework = o, a.template = e, a.bg = o.getChildByClass(e, "pswp__bg"), F = e.className, u = !0, N = o.detectFeatures(), Z = N.raf, P = N.caf, O = N.transform, _ = N.oldIE, a.scrollWrap = o.getChildByClass(e, "pswp__scroll-wrap"), a.container = o.getChildByClass(a.scrollWrap, "pswp__container"), m = a.container.style, a.itemHolders = D = [{
                            el: a.container.children[0],
                            wrap: 0,
                            index: -1
                        }, {
                            el: a.container.children[1],
                            wrap: 0,
                            index: -1
                        }, {
                            el: a.container.children[2],
                            wrap: 0,
                            index: -1
                        }], D[0].el.style.display = D[2].el.style.display = "none", Ve(), y = {
                            resize: a.updateSize,
                            scroll: $e,
                            keydown: Ke,
                            click: qe
                        };
                        var i = N.isOldIOSPhone || N.isOldAndroid || N.isMobileOpera;
                        for (N.animationName && N.transform && !i || (s.showAnimationDuration = s.hideAnimationDuration = 0), n = 0; n < be.length; n++) a["init" + be[n]]();
                        if (t) {
                            var r = a.ui = new t(a, o);
                            r.init()
                        }
                        Ae("firstUpdate"), p = p || s.index || 0, (isNaN(p) || 0 > p || p >= Qt()) && (p = 0), a.currItem = Jt(p), (N.isOldIOSPhone || N.isOldAndroid) && (we = !1), e.setAttribute("aria-hidden", "false"), s.modal && (we ? e.style.position = "fixed" : (e.style.position = "absolute", e.style.top = o.getScrollY() + "px")), void 0 === z && (Ae("initialLayout"), z = L = o.getScrollY());
                        var d = "pswp--open ";
                        for (s.mainClass && (d += s.mainClass + " "), s.showHideOpacity && (d += "pswp--animate_opacity "), d += R ? "pswp--touch" : "pswp--notouch", d += N.animationName ? " pswp--css_animation" : "", d += N.svg ? " pswp--svg" : "", o.addClass(e, d), a.updateSize(), f = -1, ge = null, n = 0; l > n; n++) Pe((n + f) * xe.x, D[n].el.style);
                        _ || o.bind(a.scrollWrap, v, a), Se("initialZoomInEnd", function () {
                            a.setContent(D[0], p - 1), a.setContent(D[2], p + 1), D[0].el.style.display = D[2].el.style.display = "block", s.focus && e.focus(), He()
                        }), a.setContent(D[1], p), a.updateCurrItem(), Ae("afterInit"), we || (I = setInterval(function () {
                            Je || X || Q || x !== a.currItem.initialZoomLevel || a.updateSize()
                        }, 1e3)), o.addClass(e, "pswp--visible")
                    }
                },
                close: function () {
                    u && (u = !1, c = !0, Ae("close"), Ye(), tn(a.currItem, null, !0, a.destroy))
                },
                destroy: function () {
                    Ae("destroy"), Kt && clearTimeout(Kt), e.setAttribute("aria-hidden", "true"), e.className = F, I && clearInterval(I), o.unbind(a.scrollWrap, v, a), o.unbind(window, "scroll", a), Dt(), tt(), Te = null
                },
                panTo: function (e, t, n) {
                    n || (e > ne.min.x ? e = ne.min.x : e < ne.max.x && (e = ne.max.x), t > ne.min.y ? t = ne.min.y : t < ne.max.y && (t = ne.max.y)), fe.x = e, fe.y = t, Re()
                },
                handleEvent: function (e) {
                    e = e || window.event, y[e.type] && y[e.type](e)
                },
                goTo: function (e) {
                    e = Me(e);
                    var t = e - p;
                    ge = t, p = e, a.currItem = Jt(p), ve -= t, Fe(xe.x * ve), tt(), oe = !1, a.updateCurrItem()
                },
                next: function () {
                    a.goTo(p + 1)
                },
                prev: function () {
                    a.goTo(p - 1)
                },
                updateCurrZoomItem: function (e) {
                    if (e && Ae("beforeChange", 0), D[1].el.children.length) {
                        var t = D[1].el.children[0];
                        ie = o.hasClass(t, "pswp__zoom-wrap") ? t.style : null
                    } else ie = null;
                    ne = a.currItem.bounds, g = x = a.currItem.initialZoomLevel, fe.x = ne.center.x, fe.y = ne.center.y, e && Ae("afterChange")
                },
                invalidateCurrItems: function () {
                    C = !0;
                    for (var e = 0; l > e; e++) D[e].item && (D[e].item.needsUpdate = !0)
                },
                updateCurrItem: function (e) {
                    if (0 !== ge) {
                        var t, n = Math.abs(ge);
                        if (!(e && 2 > n)) {
                            a.currItem = Jt(p), Ce = !1, Ae("beforeChange", ge), n >= l && (f += ge + (ge > 0 ? -l : l), n = l);
                            for (var i = 0; n > i; i++) ge > 0 ? (t = D.shift(), D[l - 1] = t, f++, Pe((f + 2) * xe.x, t.el.style), a.setContent(t, p - n + i + 1 + 1)) : (t = D.pop(), D.unshift(t), f--, Pe(f * xe.x, t.el.style), a.setContent(t, p + n - i - 1 - 1));
                            if (ie && 1 === Math.abs(ge)) {
                                var o = Jt(M);
                                o.initialZoomLevel !== x && (sn(o, he), pn(o), Ze(o))
                            }
                            ge = 0, a.updateCurrZoomItem(), M = p, Ae("afterChange")
                        }
                    }
                },
                updateSize: function (t) {
                    if (!we && s.modal) {
                        var n = o.getScrollY();
                        if (z !== n && (e.style.top = n + "px", z = n), !t && Ie.x === window.innerWidth && Ie.y === window.innerHeight) return;
                        Ie.x = window.innerWidth, Ie.y = window.innerHeight, e.style.height = Ie.y + "px"
                    }
                    if (he.x = a.scrollWrap.clientWidth, he.y = a.scrollWrap.clientHeight, $e(), xe.x = he.x + Math.round(he.x * s.spacing), xe.y = he.y, Fe(xe.x * ve), Ae("beforeResize"), void 0 !== f) {
                        for (var i, r, u, c = 0; l > c; c++) i = D[c], Pe((c + f) * xe.x, i.el.style), u = p + c - 1, s.loop && Qt() > 2 && (u = Me(u)), r = Jt(u), r && (C || r.needsUpdate || !r.bounds) ? (a.cleanSlide(r), a.setContent(i, u), 1 === c && (a.currItem = r, a.updateCurrZoomItem(!0)), r.needsUpdate = !1) : -1 === i.index && u >= 0 && a.setContent(i, u), r && r.container && (sn(r, he), pn(r), Ze(r));
                        C = !1
                    }
                    g = x = a.currItem.initialZoomLevel, ne = a.currItem.bounds, ne && (fe.x = ne.center.x, fe.y = ne.center.y, Re(!0)), Ae("resize")
                },
                zoomTo: function (e, t, n, i, a) {
                    t && (g = x, gt.x = Math.abs(t.x) - fe.x, gt.y = Math.abs(t.y) - fe.y, _e(me, fe));
                    var r = Be(e, !1),
                        l = {};
                    Xe("x", r, l, e), Xe("y", r, l, e);
                    var s = x,
                        u = {
                            x: fe.x,
                            y: fe.y
                        };
                    ze(l);
                    var c = function (t) {
                        1 === t ? (x = e, fe.x = l.x, fe.y = l.y) : (x = (e - s) * t + s, fe.x = (l.x - u.x) * t + u.x, fe.y = (l.y - u.y) * t + u.y), a && a(t), Re(1 === t)
                    };
                    n ? nt("customZoomTo", 0, 1, n, i || o.easing.sine.inOut, c) : c(1)
                }
            },
            ot = 30,
            at = 10,
            rt = {},
            lt = {},
            st = {},
            ut = {},
            ct = {},
            dt = [],
            pt = {},
            mt = [],
            ft = {},
            ht = 0,
            vt = de(),
            yt = 0,
            xt = de(),
            gt = de(),
            wt = de(),
            bt = function (e, t) {
                return e.x === t.x && e.y === t.y
            },
            It = function (e, t) {
                return Math.abs(e.x - t.x) < r && Math.abs(e.y - t.y) < r
            },
            Ct = function (e, t) {
                return ft.x = Math.abs(e.x - t.x), ft.y = Math.abs(e.y - t.y), Math.sqrt(ft.x * ft.x + ft.y * ft.y)
            },
            Dt = function () {
                $ && (P($), $ = null)
            },
            Mt = function () {
                X && ($ = Z(Mt), Ht())
            },
            Tt = function () {
                return !("fit" === s.scaleMode && x === a.currItem.initialZoomLevel)
            },
            St = function (e, t) {
                return !(!e || e === document) && (!(e.getAttribute("class") && e.getAttribute("class").indexOf("pswp__scroll-wrap") > -1) && (t(e) ? e : St(e.parentNode, t)))
            },
            At = {},
            Et = function (e, t) {
                return At.prevent = !St(e.target, s.isClickableElement), Ae("preventDragEvent", e, t, At), At.prevent
            },
            Ot = function (e, t) {
                return t.x = e.pageX, t.y = e.pageY, t.id = e.identifier, t
            },
            kt = function (e, t, n) {
                n.x = .5 * (e.x + t.x), n.y = .5 * (e.y + t.y)
            },
            Rt = function (e, t, n) {
                if (e - H > 50) {
                    var i = mt.length > 2 ? mt.shift() : {};
                    i.x = t, i.y = n, mt.push(i), H = e
                }
            },
            Zt = function () {
                var e = fe.y - a.currItem.initialPosition.y;
                return 1 - Math.abs(e / (he.y / 2))
            },
            Pt = {},
            Ft = {},
            Lt = [],
            _t = function (e) {
                for (; Lt.length > 0;) Lt.pop();
                return k ? (ce = 0, dt.forEach(function (e) {
                    0 === ce ? Lt[0] = e : 1 === ce && (Lt[1] = e), ce++
                })) : e.type.indexOf("touch") > -1 ? e.touches && e.touches.length > 0 && (Lt[0] = Ot(e.touches[0], Pt), e.touches.length > 1 && (Lt[1] = Ot(e.touches[1], Ft))) : (Pt.x = e.pageX, Pt.y = e.pageY, Pt.id = "", Lt[0] = Pt), Lt
            },
            zt = function (e, t) {
                var n, i, o, r, l = 0,
                    u = fe[e] + t[e],
                    c = t[e] > 0,
                    d = xt.x + t.x,
                    p = xt.x - pt.x;
                return n = u > ne.min[e] || u < ne.max[e] ? s.panEndFriction : 1, u = fe[e] + t[e] * n, !s.allowPanToNext && x !== a.currItem.initialZoomLevel || (ie ? "h" !== ae || "x" !== e || K || (c ? (u > ne.min[e] && (n = s.panEndFriction, l = ne.min[e] - u, i = ne.min[e] - me[e]), (0 >= i || 0 > p) && Qt() > 1 ? (r = d, 0 > p && d > pt.x && (r = pt.x)) : ne.min.x !== ne.max.x && (o = u)) : (u < ne.max[e] && (n = s.panEndFriction, l = u - ne.max[e], i = me[e] - ne.max[e]), (0 >= i || p > 0) && Qt() > 1 ? (r = d, p > 0 && d < pt.x && (r = pt.x)) : ne.min.x !== ne.max.x && (o = u))) : r = d, "x" !== e) ? void(oe || j || x > a.currItem.fitRatio && (fe[e] += t[e] * n)) : (void 0 !== r && (Fe(r, !0), j = r !== pt.x), ne.min.x !== ne.max.x && (void 0 !== o ? fe.x = o : j || (fe.x += t.x * n)), void 0 !== r)
            },
            Nt = function (e) {
                if (!("mousedown" === e.type && e.button > 0)) {
                    if (jt) return void e.preventDefault();
                    if (!G || "mousedown" !== e.type) {
                        if (Et(e, !0) && e.preventDefault(), Ae("pointerDown"), k) {
                            var t = o.arraySearch(dt, e.pointerId, "id");
                            0 > t && (t = dt.length), dt[t] = {
                                x: e.pageX,
                                y: e.pageY,
                                id: e.pointerId
                            }
                        }
                        var n = _t(e),
                            i = n.length;
                        J = null, tt(), X && 1 !== i || (X = re = !0, o.bind(window, h, a), B = ue = le = W = j = q = V = K = !1, ae = null, Ae("firstTouchStart", n), _e(me, fe), pe.x = pe.y = 0, _e(ut, n[0]), _e(ct, ut), pt.x = xe.x * ve, mt = [{
                            x: ut.x,
                            y: ut.y
                        }], H = U = Ee(), Be(x, !0), Dt(), Mt()), !Q && i > 1 && !oe && !j && (g = x, K = !1, Q = V = !0, pe.y = pe.x = 0, _e(me, fe), _e(rt, n[0]), _e(lt, n[1]), kt(rt, lt, wt), gt.x = Math.abs(wt.x) - fe.x, gt.y = Math.abs(wt.y) - fe.y, ee = te = Ct(rt, lt))
                    }
                }
            },
            Ut = function (e) {
                if (e.preventDefault(), k) {
                    var t = o.arraySearch(dt, e.pointerId, "id");
                    if (t > -1) {
                        var n = dt[t];
                        n.x = e.pageX, n.y = e.pageY
                    }
                }
                if (X) {
                    var i = _t(e);
                    if (ae || q || Q) J = i;
                    else if (xt.x !== xe.x * ve) ae = "h";
                    else {
                        var a = Math.abs(i[0].x - ut.x) - Math.abs(i[0].y - ut.y);
                        Math.abs(a) >= at && (ae = a > 0 ? "h" : "v", J = i)
                    }
                }
            },
            Ht = function () {
                if (J) {
                    var e = J.length;
                    if (0 !== e)
                        if (_e(rt, J[0]), st.x = rt.x - ut.x, st.y = rt.y - ut.y, Q && e > 1) {
                            if (ut.x = rt.x, ut.y = rt.y, !st.x && !st.y && bt(J[1], lt)) return;
                            _e(lt, J[1]), K || (K = !0, Ae("zoomGestureStarted"));
                            var t = Ct(rt, lt),
                                n = Xt(t);
                            n > a.currItem.initialZoomLevel + a.currItem.initialZoomLevel / 15 && (ue = !0);
                            var i = 1,
                                o = We(),
                                r = Ge();
                            if (o > n)
                                if (s.pinchToClose && !ue && g <= a.currItem.initialZoomLevel) {
                                    var l = o - n,
                                        u = 1 - l / (o / 1.2);
                                    Oe(u), Ae("onPinchClose", u), le = !0
                                } else i = (o - n) / o, i > 1 && (i = 1), n = o - i * (o / 3);
                            else n > r && (i = (n - r) / (6 * o), i > 1 && (i = 1), n = r + i * o);
                            0 > i && (i = 0), ee = t, kt(rt, lt, vt), pe.x += vt.x - wt.x, pe.y += vt.y - wt.y, _e(wt, vt), fe.x = Le("x", n), fe.y = Le("y", n), B = n > x, x = n, Re()
                        } else {
                            if (!ae) return;
                            if (re && (re = !1, Math.abs(st.x) >= at && (st.x -= J[0].x - ct.x), Math.abs(st.y) >= at && (st.y -= J[0].y - ct.y)), ut.x = rt.x, ut.y = rt.y, 0 === st.x && 0 === st.y) return;
                            if ("v" === ae && s.closeOnVerticalDrag && !Tt()) {
                                pe.y += st.y, fe.y += st.y;
                                var c = Zt();
                                return W = !0, Ae("onVerticalDrag", c), Oe(c), void Re()
                            }
                            Rt(Ee(), rt.x, rt.y), q = !0, ne = a.currItem.bounds;
                            var d = zt("x", st);
                            d || (zt("y", st), ze(fe), Re())
                        }
                }
            },
            Yt = function (e) {
                if (N.isOldAndroid) {
                    if (G && "mouseup" === e.type) return;
                    e.type.indexOf("touch") > -1 && (clearTimeout(G), G = setTimeout(function () {
                        G = 0
                    }, 600))
                }
                Ae("pointerUp"), Et(e, !1) && e.preventDefault();
                var t;
                if (k) {
                    var n = o.arraySearch(dt, e.pointerId, "id");
                    if (n > -1)
                        if (t = dt.splice(n, 1)[0], navigator.pointerEnabled) t.type = e.pointerType || "mouse";
                        else {
                            var i = {
                                4: "mouse",
                                2: "touch",
                                3: "pen"
                            };
                            t.type = i[e.pointerType], t.type || (t.type = e.pointerType || "mouse")
                        }
                }
                var r, l = _t(e),
                    u = l.length;
                if ("mouseup" === e.type && (u = 0), 2 === u) return J = null, !0;
                1 === u && _e(ct, l[0]), 0 !== u || ae || oe || (t || ("mouseup" === e.type ? t = {
                    x: e.pageX,
                    y: e.pageY,
                    type: "mouse"
                } : e.changedTouches && e.changedTouches[0] && (t = {
                    x: e.changedTouches[0].pageX,
                    y: e.changedTouches[0].pageY,
                    type: "touch"
                })), Ae("touchRelease", e, t));
                var c = -1;
                if (0 === u && (X = !1, o.unbind(window, h, a), Dt(), Q ? c = 0 : -1 !== yt && (c = Ee() - yt)), yt = 1 === u ? Ee() : -1, r = -1 !== c && 150 > c ? "zoom" : "swipe", Q && 2 > u && (Q = !1, 1 === u && (r = "zoomPointerUp"), Ae("zoomGestureEnded")), J = null, q || K || oe || W)
                    if (tt(), Y || (Y = Bt()), Y.calculateSwipeSpeed("x"), W) {
                        var d = Zt();
                        if (d < s.verticalDragRange) a.close();
                        else {
                            var p = fe.y,
                                m = se;
                            nt("verticalDrag", 0, 1, 300, o.easing.cubic.out, function (e) {
                                fe.y = (a.currItem.initialPosition.y - p) * e + p, Oe((1 - m) * e + m), Re()
                            }), Ae("onVerticalDrag", 1)
                        }
                    } else {
                        if ((j || oe) && 0 === u) {
                            var f = Gt(r, Y);
                            if (f) return;
                            r = "zoomPointerUp"
                        }
                        if (!oe) return "swipe" !== r ? void Vt() : void(!j && x > a.currItem.fitRatio && Wt(Y))
                    }
            },
            Bt = function () {
                var e, t, n = {
                    lastFlickOffset: {},
                    lastFlickDist: {},
                    lastFlickSpeed: {},
                    slowDownRatio: {},
                    slowDownRatioReverse: {},
                    speedDecelerationRatio: {},
                    speedDecelerationRatioAbs: {},
                    distanceOffset: {},
                    backAnimDestination: {},
                    backAnimStarted: {},
                    calculateSwipeSpeed: function (i) {
                        mt.length > 1 ? (e = Ee() - H + 50, t = mt[mt.length - 2][i]) : (e = Ee() - U, t = ct[i]), n.lastFlickOffset[i] = ut[i] - t, n.lastFlickDist[i] = Math.abs(n.lastFlickOffset[i]), n.lastFlickDist[i] > 20 ? n.lastFlickSpeed[i] = n.lastFlickOffset[i] / e : n.lastFlickSpeed[i] = 0, Math.abs(n.lastFlickSpeed[i]) < .1 && (n.lastFlickSpeed[i] = 0), n.slowDownRatio[i] = .95, n.slowDownRatioReverse[i] = 1 - n.slowDownRatio[i], n.speedDecelerationRatio[i] = 1
                    },
                    calculateOverBoundsAnimOffset: function (e, t) {
                        n.backAnimStarted[e] || (fe[e] > ne.min[e] ? n.backAnimDestination[e] = ne.min[e] : fe[e] < ne.max[e] && (n.backAnimDestination[e] = ne.max[e]), void 0 !== n.backAnimDestination[e] && (n.slowDownRatio[e] = .7, n.slowDownRatioReverse[e] = 1 - n.slowDownRatio[e], n.speedDecelerationRatioAbs[e] < .05 && (n.lastFlickSpeed[e] = 0, n.backAnimStarted[e] = !0, nt("bounceZoomPan" + e, fe[e], n.backAnimDestination[e], t || 300, o.easing.sine.out, function (t) {
                            fe[e] = t, Re()
                        }))))
                    },
                    calculateAnimOffset: function (e) {
                        n.backAnimStarted[e] || (n.speedDecelerationRatio[e] = n.speedDecelerationRatio[e] * (n.slowDownRatio[e] + n.slowDownRatioReverse[e] - n.slowDownRatioReverse[e] * n.timeDiff / 10), n.speedDecelerationRatioAbs[e] = Math.abs(n.lastFlickSpeed[e] * n.speedDecelerationRatio[e]), n.distanceOffset[e] = n.lastFlickSpeed[e] * n.speedDecelerationRatio[e] * n.timeDiff, fe[e] += n.distanceOffset[e])
                    },
                    panAnimLoop: function () {
                        return je.zoomPan && (je.zoomPan.raf = Z(n.panAnimLoop), n.now = Ee(), n.timeDiff = n.now - n.lastNow, n.lastNow = n.now, n.calculateAnimOffset("x"), n.calculateAnimOffset("y"), Re(), n.calculateOverBoundsAnimOffset("x"), n.calculateOverBoundsAnimOffset("y"), n.speedDecelerationRatioAbs.x < .05 && n.speedDecelerationRatioAbs.y < .05) ? (fe.x = Math.round(fe.x), fe.y = Math.round(fe.y), Re(), void Qe("zoomPan")) : void 0
                    }
                };
                return n
            },
            Wt = function (e) {
                return e.calculateSwipeSpeed("y"), ne = a.currItem.bounds, e.backAnimDestination = {}, e.backAnimStarted = {}, Math.abs(e.lastFlickSpeed.x) <= .05 && Math.abs(e.lastFlickSpeed.y) <= .05 ? (e.speedDecelerationRatioAbs.x = e.speedDecelerationRatioAbs.y = 0, e.calculateOverBoundsAnimOffset("x"), e.calculateOverBoundsAnimOffset("y"), !0) : (et("zoomPan"), e.lastNow = Ee(), void e.panAnimLoop())
            },
            Gt = function (e, t) {
                var n;
                oe || (ht = p);
                var i;
                if ("swipe" === e) {
                    var r = ut.x - ct.x,
                        l = t.lastFlickDist.x < 10;
                    r > ot && (l || t.lastFlickOffset.x > 20) ? i = -1 : -ot > r && (l || t.lastFlickOffset.x < -20) && (i = 1)
                }
                var u;
                i && (p += i, 0 > p ? (p = s.loop ? Qt() - 1 : 0, u = !0) : p >= Qt() && (p = s.loop ? 0 : Qt() - 1, u = !0), (!u || s.loop) && (ge += i, ve -= i, n = !0));
                var c, d = xe.x * ve,
                    m = Math.abs(d - xt.x);
                return n || d > xt.x == t.lastFlickSpeed.x > 0 ? (c = Math.abs(t.lastFlickSpeed.x) > 0 ? m / Math.abs(t.lastFlickSpeed.x) : 333, c = Math.min(c, 400), c = Math.max(c, 250)) : c = 333, ht === p && (n = !1), oe = !0, Ae("mainScrollAnimStart"), nt("mainScroll", xt.x, d, c, o.easing.cubic.out, Fe, function () {
                    tt(), oe = !1, ht = -1, (n || ht !== p) && a.updateCurrItem(), Ae("mainScrollAnimComplete")
                }), n && a.updateCurrItem(!0), n
            },
            Xt = function (e) {
                return 1 / te * e * g
            },
            Vt = function () {
                var e = x,
                    t = We(),
                    n = Ge();
                t > x ? e = t : x > n && (e = n);
                var i, r = 1,
                    l = se;
                return le && !B && !ue && t > x ? (a.close(), !0) : (le && (i = function (e) {
                    Oe((r - l) * e + l)
                }), a.zoomTo(e, 0, 200, o.easing.cubic.out, i), !0)
            };
        De("Gestures", {
            publicMethods: {
                initGestures: function () {
                    var e = function (e, t, n, i, o) {
                        T = e + t, S = e + n, A = e + i, E = o ? e + o : ""
                    };
                    k = N.pointerEvent, k && N.touch && (N.touch = !1), k ? navigator.pointerEnabled ? e("pointer", "down", "move", "up", "cancel") : e("MSPointer", "Down", "Move", "Up", "Cancel") : N.touch ? (e("touch", "start", "move", "end", "cancel"), R = !0) : e("mouse", "down", "move", "up"), h = S + " " + A + " " + E, v = T, k && !R && (R = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1), a.likelyTouchDevice = R, y[T] = Nt, y[S] = Ut, y[A] = Yt, E && (y[E] = y[A]), N.touch && (v += " mousedown", h += " mousemove mouseup", y.mousedown = y[T], y.mousemove = y[S], y.mouseup = y[A]), R || (s.allowPanToNext = !1)
                }
            }
        });
        var Kt, qt, $t, jt, Jt, Qt, en, tn = function (t, n, i, r) {
                Kt && clearTimeout(Kt), jt = !0, $t = !0;
                var l;
                t.initialLayout ? (l = t.initialLayout, t.initialLayout = null) : l = s.getThumbBoundsFn && s.getThumbBoundsFn(p);
                var u = i ? s.hideAnimationDuration : s.showAnimationDuration,
                    c = function () {
                        Qe("initialZoom"), i ? (a.template.removeAttribute("style"), a.bg.removeAttribute("style")) : (Oe(1), n && (n.style.display = "block"), o.addClass(e, "pswp--animated-in"), Ae("initialZoom" + (i ? "OutEnd" : "InEnd"))), r && r(), jt = !1
                    };
                if (!u || !l || void 0 === l.x) return Ae("initialZoom" + (i ? "Out" : "In")), x = t.initialZoomLevel, _e(fe, t.initialPosition), Re(), e.style.opacity = i ? 0 : 1, Oe(1), void(u ? setTimeout(function () {
                    c()
                }, u) : c());
                var m = function () {
                    var n = d,
                        r = !a.currItem.src || a.currItem.loadError || s.showHideOpacity;
                    t.miniImg && (t.miniImg.style.webkitBackfaceVisibility = "hidden"), i || (x = l.w / t.w, fe.x = l.x, fe.y = l.y - L, a[r ? "template" : "bg"].style.opacity = .001, Re()), et("initialZoom"), i && !n && o.removeClass(e, "pswp--animated-in"), r && (i ? o[(n ? "remove" : "add") + "Class"](e, "pswp--animate_opacity") : setTimeout(function () {
                        o.addClass(e, "pswp--animate_opacity")
                    }, 30)), Kt = setTimeout(function () {
                        if (Ae("initialZoom" + (i ? "Out" : "In")), i) {
                            var a = l.w / t.w,
                                s = {
                                    x: fe.x,
                                    y: fe.y
                                },
                                d = x,
                                p = se,
                                m = function (t) {
                                    1 === t ? (x = a, fe.x = l.x, fe.y = l.y - z) : (x = (a - d) * t + d, fe.x = (l.x - s.x) * t + s.x, fe.y = (l.y - z - s.y) * t + s.y), Re(), r ? e.style.opacity = 1 - t : Oe(p - t * p)
                                };
                            n ? nt("initialZoom", 0, 1, u, o.easing.cubic.out, m, c) : (m(1), Kt = setTimeout(c, u + 20))
                        } else x = t.initialZoomLevel, _e(fe, t.initialPosition), Re(), Oe(1), r ? e.style.opacity = 1 : Oe(1), Kt = setTimeout(c, u + 20)
                    }, i ? 25 : 90)
                };
                m()
            },
            nn = {},
            on = [],
            an = {
                index: 0,
                errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
                forceProgressiveLoading: !1,
                preload: [1, 1],
                getNumItemsFn: function () {
                    return qt.length
                }
            },
            rn = function () {
                return {
                    center: {
                        x: 0,
                        y: 0
                    },
                    max: {
                        x: 0,
                        y: 0
                    },
                    min: {
                        x: 0,
                        y: 0
                    }
                }
            },
            ln = function (e, t, n) {
                var i = e.bounds;
                i.center.x = Math.round((nn.x - t) / 2), i.center.y = Math.round((nn.y - n) / 2) + e.vGap.top, i.max.x = t > nn.x ? Math.round(nn.x - t) : i.center.x, i.max.y = n > nn.y ? Math.round(nn.y - n) + e.vGap.top : i.center.y, i.min.x = t > nn.x ? 0 : i.center.x, i.min.y = n > nn.y ? e.vGap.top : i.center.y
            },
            sn = function (e, t, n) {
                if (e.src && !e.loadError) {
                    var i = !n;
                    if (i && (e.vGap || (e.vGap = {
                            top: 0,
                            bottom: 0
                        }), Ae("parseVerticalMargin", e)), nn.x = t.x, nn.y = t.y - e.vGap.top - e.vGap.bottom, i) {
                        var o = nn.x / e.w,
                            a = nn.y / e.h;
                        e.fitRatio = a > o ? o : a;
                        var r = s.scaleMode;
                        "orig" === r ? n = 1 : "fit" === r && (n = e.fitRatio), n > 1 && (n = 1), e.initialZoomLevel = n, e.bounds || (e.bounds = rn())
                    }
                    if (!n) return;
                    return ln(e, e.w * n, e.h * n), i && n === e.initialZoomLevel && (e.initialPosition = e.bounds.center), e.bounds
                }
                return e.w = e.h = 0, e.initialZoomLevel = e.fitRatio = 1, e.bounds = rn(), e.initialPosition = e.bounds.center, e.bounds
            },
            un = function (e, t, n, i, o, r) {
                t.loadError || i && (t.imageAppended = !0, pn(t, i, t === a.currItem && Ce), n.appendChild(i), r && setTimeout(function () {
                    t && t.loaded && t.placeholder && (t.placeholder.style.display = "none", t.placeholder = null)
                }, 500))
            },
            cn = function (e) {
                e.loading = !0, e.loaded = !1;
                var t = e.img = o.createEl("pswp__img", "img"),
                    n = function () {
                        e.loading = !1, e.loaded = !0, e.loadComplete ? e.loadComplete(e) : e.img = null, t.onload = t.onerror = null, t = null
                    };
                return t.onload = n, t.onerror = function () {
                    e.loadError = !0, n()
                }, t.src = e.src, t
            },
            dn = function (e, t) {
                return e.src && e.loadError && e.container ? (t && (e.container.innerHTML = ""), e.container.innerHTML = s.errorMsg.replace("%url%", e.src), !0) : void 0
            },
            pn = function (e, t, n) {
                if (e.src) {
                    t || (t = e.container.lastChild);
                    var i = n ? e.w : Math.round(e.w * e.fitRatio),
                        o = n ? e.h : Math.round(e.h * e.fitRatio);
                    e.placeholder && !e.loaded && (e.placeholder.style.width = i + "px", e.placeholder.style.height = o + "px"), t.style.width = i + "px", t.style.height = o + "px"
                }
            },
            mn = function () {
                if (on.length) {
                    for (var e, t = 0; t < on.length; t++) e = on[t], e.holder.index === e.index && un(e.index, e.item, e.baseDiv, e.img, !1, e.clearPlaceholder);
                    on = []
                }
            };
        De("Controller", {
            publicMethods: {
                lazyLoadItem: function (e) {
                    e = Me(e);
                    var t = Jt(e);
                    t && (!t.loaded && !t.loading || C) && (Ae("gettingData", e, t), t.src && cn(t))
                },
                initController: function () {
                    o.extend(s, an, !0), a.items = qt = n, Jt = a.getItemAt, Qt = s.getNumItemsFn, en = s.loop, Qt() < 3 && (s.loop = !1), Se("beforeChange", function (e) {
                        var t, n = s.preload,
                            i = null === e || e >= 0,
                            o = Math.min(n[0], Qt()),
                            r = Math.min(n[1], Qt());
                        for (t = 1;
                            (i ? r : o) >= t; t++) a.lazyLoadItem(p + t);
                        for (t = 1;
                            (i ? o : r) >= t; t++) a.lazyLoadItem(p - t)
                    }), Se("initialLayout", function () {
                        a.currItem.initialLayout = s.getThumbBoundsFn && s.getThumbBoundsFn(p)
                    }), Se("mainScrollAnimComplete", mn), Se("initialZoomInEnd", mn), Se("destroy", function () {
                        for (var e, t = 0; t < qt.length; t++) e = qt[t], e.container && (e.container = null), e.placeholder && (e.placeholder = null), e.img && (e.img = null), e.preloader && (e.preloader = null), e.loadError && (e.loaded = e.loadError = !1);
                        on = null
                    })
                },
                getItemAt: function (e) {
                    return e >= 0 && void 0 !== qt[e] && qt[e]
                },
                allowProgressiveImg: function () {
                    return s.forceProgressiveLoading || !R || s.mouseUsed || screen.width > 1200
                },
                setContent: function (e, t) {
                    s.loop && (t = Me(t));
                    var n = a.getItemAt(e.index);
                    n && (n.container = null);
                    var i, r = a.getItemAt(t);
                    if (!r) return void(e.el.innerHTML = "");
                    Ae("gettingData", t, r), e.index = t, e.item = r;
                    var l = r.container = o.createEl("pswp__zoom-wrap");
                    if (!r.src && r.html && (r.html.tagName ? l.appendChild(r.html) : l.innerHTML = r.html), dn(r), sn(r, he), !r.src || r.loadError || r.loaded) r.src && !r.loadError && (i = o.createEl("pswp__img", "img"), i.style.opacity = 1, i.src = r.src, pn(r, i), un(t, r, l, i, !0));
                    else {
                        if (r.loadComplete = function (n) {
                                if (u) {
                                    if (e && e.index === t) {
                                        if (dn(n, !0)) return n.loadComplete = n.img = null, sn(n, he), Ze(n), void(e.index === p && a.updateCurrZoomItem());
                                        n.imageAppended ? !jt && n.placeholder && (n.placeholder.style.display = "none", n.placeholder = null) : N.transform && (oe || jt) ? on.push({
                                            item: n,
                                            baseDiv: l,
                                            img: n.img,
                                            index: t,
                                            holder: e,
                                            clearPlaceholder: !0
                                        }) : un(t, n, l, n.img, oe || jt, !0)
                                    }
                                    n.loadComplete = null, n.img = null, Ae("imageLoadComplete", t, n)
                                }
                            }, o.features.transform) {
                            var c = "pswp__img pswp__img--placeholder";
                            c += r.msrc ? "" : " pswp__img--placeholder--blank";
                            var d = o.createEl(c, r.msrc ? "img" : "");
                            r.msrc && (d.src = r.msrc), pn(r, d), l.appendChild(d), r.placeholder = d
                        }
                        r.loading || cn(r), a.allowProgressiveImg() && (!$t && N.transform ? on.push({
                            item: r,
                            baseDiv: l,
                            img: r.img,
                            index: t,
                            holder: e
                        }) : un(t, r, l, r.img, !0, !0))
                    }
                    $t || t !== p ? Ze(r) : (ie = l.style, tn(r, i || r.img)), e.el.innerHTML = "", e.el.appendChild(l)
                },
                cleanSlide: function (e) {
                    e.img && (e.img.onload = e.img.onerror = null), e.loaded = e.loading = e.img = e.imageAppended = !1
                }
            }
        });
        var fn, hn = {},
            vn = function (e, t, n) {
                var i = document.createEvent("CustomEvent"),
                    o = {
                        origEvent: e,
                        target: e.target,
                        releasePoint: t,
                        pointerType: n || "touch"
                    };
                i.initCustomEvent("pswpTap", !0, !0, o), e.target.dispatchEvent(i)
            };
        De("Tap", {
            publicMethods: {
                initTap: function () {
                    Se("firstTouchStart", a.onTapStart), Se("touchRelease", a.onTapRelease), Se("destroy", function () {
                        hn = {}, fn = null
                    })
                },
                onTapStart: function (e) {
                    e.length > 1 && (clearTimeout(fn), fn = null)
                },
                onTapRelease: function (e, t) {
                    if (t && !q && !V && !Je) {
                        var n = t;
                        if (fn && (clearTimeout(fn), fn = null, It(n, hn))) return void Ae("doubleTap", n);
                        if ("mouse" === t.type) return void vn(e, t, "mouse");
                        var i = e.target.tagName.toUpperCase();
                        if ("BUTTON" === i || o.hasClass(e.target, "pswp__single-tap")) return void vn(e, t);
                        _e(hn, n), fn = setTimeout(function () {
                            vn(e, t), fn = null
                        }, 300)
                    }
                }
            }
        });
        var yn;
        De("DesktopZoom", {
            publicMethods: {
                initDesktopZoom: function () {
                    _ || (R ? Se("mouseUsed", function () {
                        a.setupDesktopZoom()
                    }) : a.setupDesktopZoom(!0))
                },
                setupDesktopZoom: function (t) {
                    yn = {};
                    var n = "wheel mousewheel DOMMouseScroll";
                    Se("bindEvents", function () {
                        o.bind(e, n, a.handleMouseWheel)
                    }), Se("unbindEvents", function () {
                        yn && o.unbind(e, n, a.handleMouseWheel)
                    }), a.mouseZoomedIn = !1;
                    var i, r = function () {
                            a.mouseZoomedIn && (o.removeClass(e, "pswp--zoomed-in"), a.mouseZoomedIn = !1), 1 > x ? o.addClass(e, "pswp--zoom-allowed") : o.removeClass(e, "pswp--zoom-allowed"), l()
                        },
                        l = function () {
                            i && (o.removeClass(e, "pswp--dragging"), i = !1)
                        };
                    Se("resize", r), Se("afterChange", r), Se("pointerDown", function () {
                        a.mouseZoomedIn && (i = !0, o.addClass(e, "pswp--dragging"))
                    }), Se("pointerUp", l), t || r()
                },
                handleMouseWheel: function (e) {
                    if (x <= a.currItem.fitRatio) return s.modal && (!s.closeOnScroll || Je || X ? e.preventDefault() : O && Math.abs(e.deltaY) > 2 && (d = !0, a.close())), !0;
                    if (e.stopPropagation(), yn.x = 0, "deltaX" in e) 1 === e.deltaMode ? (yn.x = 18 * e.deltaX, yn.y = 18 * e.deltaY) : (yn.x = e.deltaX, yn.y = e.deltaY);
                    else if ("wheelDelta" in e) e.wheelDeltaX && (yn.x = -.16 * e.wheelDeltaX), e.wheelDeltaY ? yn.y = -.16 * e.wheelDeltaY : yn.y = -.16 * e.wheelDelta;
                    else {
                        if (!("detail" in e)) return;
                        yn.y = e.detail
                    }
                    Be(x, !0);
                    var t = fe.x - yn.x,
                        n = fe.y - yn.y;
                    (s.modal || t <= ne.min.x && t >= ne.max.x && n <= ne.min.y && n >= ne.max.y) && e.preventDefault(), a.panTo(t, n)
                },
                toggleDesktopZoom: function (t) {
                    t = t || {
                        x: he.x / 2 + ye.x,
                        y: he.y / 2 + ye.y
                    };
                    var n = s.getDoubleTapZoom(!0, a.currItem),
                        i = x === n;
                    a.mouseZoomedIn = !i, a.zoomTo(i ? a.currItem.initialZoomLevel : n, t, 333), o[(i ? "remove" : "add") + "Class"](e, "pswp--zoomed-in")
                }
            }
        });
        var xn, gn, wn, bn, In, Cn, Dn, Mn, Tn, Sn, An, En, On = {
                history: !0,
                galleryUID: 1
            },
            kn = function () {
                return An.hash.substring(1)
            },
            Rn = function () {
                xn && clearTimeout(xn), wn && clearTimeout(wn)
            },
            Zn = function () {
                var e = kn(),
                    t = {};
                if (e.length < 5) return t;
                var n, i = e.split("&");
                for (n = 0; n < i.length; n++)
                    if (i[n]) {
                        var o = i[n].split("=");
                        o.length < 2 || (t[o[0]] = o[1])
                    }
                if (s.galleryPIDs) {
                    var a = t.pid;
                    for (t.pid = 0, n = 0; n < qt.length; n++)
                        if (qt[n].pid === a) {
                            t.pid = n;
                            break
                        }
                } else t.pid = parseInt(t.pid, 10) - 1;
                return t.pid < 0 && (t.pid = 0), t
            },
            Pn = function () {
                if (wn && clearTimeout(wn), Je || X) return void(wn = setTimeout(Pn, 500));
                bn ? clearTimeout(gn) : bn = !0;
                var e = p + 1,
                    t = Jt(p);
                t.hasOwnProperty("pid") && (e = t.pid);
                var n = Dn + "&gid=" + s.galleryUID + "&pid=" + e;
                Mn || -1 === An.hash.indexOf(n) && (Sn = !0);
                var i = An.href.split("#")[0] + "#" + n;
                En ? "#" + n !== window.location.hash && history[Mn ? "replaceState" : "pushState"]("", document.title, i) : Mn ? An.replace(i) : An.hash = n, Mn = !0, gn = setTimeout(function () {
                    bn = !1
                }, 60)
            };
        De("History", {
            publicMethods: {
                initHistory: function () {
                    if (o.extend(s, On, !0), s.history) {
                        An = window.location, Sn = !1, Tn = !1, Mn = !1, Dn = kn(), En = "pushState" in history, Dn.indexOf("gid=") > -1 && (Dn = Dn.split("&gid=")[0], Dn = Dn.split("?gid=")[0]), Se("afterChange", a.updateURL), Se("unbindEvents", function () {
                            o.unbind(window, "hashchange", a.onHashChange)
                        });
                        var e = function () {
                            Cn = !0, Tn || (Sn ? history.back() : Dn ? An.hash = Dn : En ? history.pushState("", document.title, An.pathname + An.search) : An.hash = ""), Rn()
                        };
                        Se("unbindEvents", function () {
                            d && e()
                        }), Se("destroy", function () {
                            Cn || e()
                        }), Se("firstUpdate", function () {
                            p = Zn().pid
                        });
                        var t = Dn.indexOf("pid=");
                        t > -1 && (Dn = Dn.substring(0, t), "&" === Dn.slice(-1) && (Dn = Dn.slice(0, -1))), setTimeout(function () {
                            u && o.bind(window, "hashchange", a.onHashChange)
                        }, 40)
                    }
                },
                onHashChange: function () {
                    return kn() === Dn ? (Tn = !0, void a.close()) : void(bn || (In = !0, a.goTo(Zn().pid), In = !1))
                },
                updateURL: function () {
                    Rn(), In || (Mn ? xn = setTimeout(Pn, 800) : Pn())
                }
            }
        }), o.extend(a, it)
    };
    return e
});
! function (e, t) {
    "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t() : e.PhotoSwipeUI_Default = t()
}(this, function () {
    "use strict";
    var e = function (e, t) {
        var n, o, l, r, i, s, a, u, c, p, d, m, f, h, w, v, g, b, _, C = this,
            T = !1,
            I = !0,
            E = !0,
            F = {
                barsSize: {
                    top: 44,
                    bottom: "auto"
                },
                closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
                timeToIdle: 4e3,
                timeToIdleOutside: 1e3,
                loadingIndicatorDelay: 1e3,
                addCaptionHTMLFn: function (e, t) {
                    return e.title ? (t.children[0].innerHTML = e.title, !0) : (t.children[0].innerHTML = "", !1)
                },
                closeEl: !0,
                captionEl: !0,
                fullscreenEl: !0,
                zoomEl: !0,
                shareEl: !0,
                counterEl: !0,
                arrowEl: !0,
                preloaderEl: !0,
                tapToClose: !1,
                tapToToggleControls: !0,
                clickToCloseNonZoomable: !0,
                shareButtons: [{
                    id: "facebook",
                    label: "Share on Facebook",
                    url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
                }, {
                    id: "twitter",
                    label: "Tweet",
                    url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
                }, {
                    id: "pinterest",
                    label: "Pin it",
                    url: "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
                }, {
                    id: "download",
                    label: "Download image",
                    url: "{{raw_image_url}}",
                    download: !0
                }],
                getImageURLForShare: function () {
                    return e.currItem.src || ""
                },
                getPageURLForShare: function () {
                    return window.location.href
                },
                getTextForShare: function () {
                    return e.currItem.title || ""
                },
                indexIndicatorSep: " / ",
                fitControlsWidth: 1200
            },
            x = function (e) {
                if (v) return !0;
                e = e || window.event, w.timeToIdle && w.mouseUsed && !c && D();
                for (var n, o, l = e.target || e.srcElement, r = l.getAttribute("class") || "", i = 0; i < N.length; i++) n = N[i], n.onTap && r.indexOf("pswp__" + n.name) > -1 && (n.onTap(), o = !0);
                if (o) {
                    e.stopPropagation && e.stopPropagation(), v = !0;
                    var s = t.features.isOldAndroid ? 600 : 30;
                    g = setTimeout(function () {
                        v = !1
                    }, s)
                }
            },
            S = function () {
                return !e.likelyTouchDevice || w.mouseUsed || screen.width > w.fitControlsWidth
            },
            k = function (e, n, o) {
                t[(o ? "add" : "remove") + "Class"](e, "pswp__" + n)
            },
            K = function () {
                var e = 1 === w.getNumItemsFn();
                e !== h && (k(o, "ui--one-slide", e), h = e)
            },
            L = function () {
                k(a, "share-modal--hidden", E)
            },
            O = function () {
                return E = !E, E ? (t.removeClass(a, "pswp__share-modal--fade-in"), setTimeout(function () {
                    E && L()
                }, 300)) : (L(), setTimeout(function () {
                    E || t.addClass(a, "pswp__share-modal--fade-in")
                }, 30)), E || y(), !1
            },
            R = function (t) {
                t = t || window.event;
                var n = t.target || t.srcElement;
                return e.shout("shareLinkClick", t, n), !!n.href && (!!n.hasAttribute("download") || (window.open(n.href, "pswp_share", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), E || O(), !1))
            },
            y = function () {
                for (var e, t, n, o, l, r = "", i = 0; i < w.shareButtons.length; i++) e = w.shareButtons[i], n = w.getImageURLForShare(e), o = w.getPageURLForShare(e), l = w.getTextForShare(e), t = e.url.replace("{{url}}", encodeURIComponent(o)).replace("{{image_url}}", encodeURIComponent(n)).replace("{{raw_image_url}}", n).replace("{{text}}", encodeURIComponent(l)), r += '<a href="' + t + '" target="_blank" class="pswp__share--' + e.id + '"' + (e.download ? "download" : "") + ">" + e.label + "</a>", w.parseShareButtonOut && (r = w.parseShareButtonOut(e, r));
                a.children[0].innerHTML = r, a.children[0].onclick = R
            },
            z = function (e) {
                for (var n = 0; n < w.closeElClasses.length; n++)
                    if (t.hasClass(e, "pswp__" + w.closeElClasses[n])) return !0
            },
            M = 0,
            D = function () {
                clearTimeout(_), M = 0, c && C.setIdle(!1)
            },
            A = function (e) {
                e = e ? e : window.event;
                var t = e.relatedTarget || e.toElement;
                t && "HTML" !== t.nodeName || (clearTimeout(_), _ = setTimeout(function () {
                    C.setIdle(!0)
                }, w.timeToIdleOutside))
            },
            P = function () {
                w.fullscreenEl && !t.features.isOldAndroid && (n || (n = C.getFullscreenAPI()), n ? (t.bind(document, n.eventK, C.updateFullscreen), C.updateFullscreen(), t.addClass(e.template, "pswp--supports-fs")) : t.removeClass(e.template, "pswp--supports-fs"))
            },
            U = function () {
                w.preloaderEl && (Z(!0), p("beforeChange", function () {
                    clearTimeout(f), f = setTimeout(function () {
                        e.currItem && e.currItem.loading ? (!e.allowProgressiveImg() || e.currItem.img && !e.currItem.img.naturalWidth) && Z(!1) : Z(!0)
                    }, w.loadingIndicatorDelay)
                }), p("imageLoadComplete", function (t, n) {
                    e.currItem === n && Z(!0)
                }))
            },
            Z = function (e) {
                m !== e && (k(d, "preloader--active", !e), m = e)
            },
            q = function (e) {
                var n = e.vGap;
                if (S()) {
                    var i = w.barsSize;
                    if (w.captionEl && "auto" === i.bottom)
                        if (r || (r = t.createEl("pswp__caption pswp__caption--fake"), r.appendChild(t.createEl("pswp__caption__center")), o.insertBefore(r, l), t.addClass(o, "pswp__ui--fit")), w.addCaptionHTMLFn(e, r, !0)) {
                            var s = r.clientHeight;
                            n.bottom = parseInt(s, 10) || 44
                        } else n.bottom = i.top;
                    else n.bottom = "auto" === i.bottom ? 0 : i.bottom;
                    n.top = i.top
                } else n.top = n.bottom = 0
            },
            B = function () {
                w.timeToIdle && p("mouseUsed", function () {
                    t.bind(document, "mousemove", D), t.bind(document, "mouseout", A), b = setInterval(function () {
                        M++, 2 === M && C.setIdle(!0)
                    }, w.timeToIdle / 2)
                })
            },
            H = function () {
                p("onVerticalDrag", function (e) {
                    I && .95 > e ? C.hideControls() : !I && e >= .95 && C.showControls()
                });
                var e;
                p("onPinchClose", function (t) {
                    I && .9 > t ? (C.hideControls(), e = !0) : e && !I && t > .9 && C.showControls()
                }), p("zoomGestureEnded", function () {
                    e = !1, e && !I && C.showControls()
                })
            },
            N = [{
                name: "caption",
                option: "captionEl",
                onInit: function (e) {
                    l = e
                }
            }, {
                name: "share-modal",
                option: "shareEl",
                onInit: function (e) {
                    a = e
                },
                onTap: function () {
                    O()
                }
            }, {
                name: "button--share",
                option: "shareEl",
                onInit: function (e) {
                    s = e
                },
                onTap: function () {
                    O()
                }
            }, {
                name: "button--zoom",
                option: "zoomEl",
                onTap: e.toggleDesktopZoom
            }, {
                name: "counter",
                option: "counterEl",
                onInit: function (e) {
                    i = e
                }
            }, {
                name: "button--close",
                option: "closeEl",
                onTap: e.close
            }, {
                name: "button--arrow--left",
                option: "arrowEl",
                onTap: e.prev
            }, {
                name: "button--arrow--right",
                option: "arrowEl",
                onTap: e.next
            }, {
                name: "button--fs",
                option: "fullscreenEl",
                onTap: function () {
                    n.isFullscreen() ? n.exit() : n.enter()
                }
            }, {
                name: "preloader",
                option: "preloaderEl",
                onInit: function (e) {
                    d = e
                }
            }],
            W = function () {
                var e, n, l, r = function (o) {
                    if (o)
                        for (var r = o.length, i = 0; r > i; i++) {
                            e = o[i], n = e.className;
                            for (var s = 0; s < N.length; s++) l = N[s], n.indexOf("pswp__" + l.name) > -1 && (w[l.option] ? (t.removeClass(e, "pswp__element--disabled"), l.onInit && l.onInit(e)) : t.addClass(e, "pswp__element--disabled"))
                        }
                };
                r(o.children);
                var i = t.getChildByClass(o, "pswp__top-bar");
                i && r(i.children)
            };
        C.init = function () {
            t.extend(e.options, F, !0), w = e.options, o = t.getChildByClass(e.scrollWrap, "pswp__ui"), p = e.listen, H(), p("beforeChange", C.update), p("doubleTap", function (t) {
                var n = e.currItem.initialZoomLevel;
                e.getZoomLevel() !== n ? e.zoomTo(n, t, 333) : e.zoomTo(w.getDoubleTapZoom(!1, e.currItem), t, 333)
            }), p("preventDragEvent", function (e, t, n) {
                var o = e.target || e.srcElement;
                o && o.getAttribute("class") && e.type.indexOf("mouse") > -1 && (o.getAttribute("class").indexOf("__caption") > 0 || /(SMALL|STRONG|EM)/i.test(o.tagName)) && (n.prevent = !1)
            }), p("bindEvents", function () {
                t.bind(o, "pswpTap click", x), t.bind(e.scrollWrap, "pswpTap", C.onGlobalTap), e.likelyTouchDevice || t.bind(e.scrollWrap, "mouseover", C.onMouseOver)
            }), p("unbindEvents", function () {
                E || O(), b && clearInterval(b), t.unbind(document, "mouseout", A), t.unbind(document, "mousemove", D), t.unbind(o, "pswpTap click", x), t.unbind(e.scrollWrap, "pswpTap", C.onGlobalTap), t.unbind(e.scrollWrap, "mouseover", C.onMouseOver), n && (t.unbind(document, n.eventK, C.updateFullscreen), n.isFullscreen() && (w.hideAnimationDuration = 0, n.exit()), n = null)
            }), p("destroy", function () {
                w.captionEl && (r && o.removeChild(r), t.removeClass(l, "pswp__caption--empty")), a && (a.children[0].onclick = null), t.removeClass(o, "pswp__ui--over-close"), t.addClass(o, "pswp__ui--hidden"), C.setIdle(!1)
            }), w.showAnimationDuration || t.removeClass(o, "pswp__ui--hidden"), p("initialZoomIn", function () {
                w.showAnimationDuration && t.removeClass(o, "pswp__ui--hidden")
            }), p("initialZoomOut", function () {
                t.addClass(o, "pswp__ui--hidden")
            }), p("parseVerticalMargin", q), W(), w.shareEl && s && a && (E = !0), K(), B(), P(), U()
        }, C.setIdle = function (e) {
            c = e, k(o, "ui--idle", e)
        }, C.update = function () {
            I && e.currItem ? (C.updateIndexIndicator(), w.captionEl && (w.addCaptionHTMLFn(e.currItem, l), k(l, "caption--empty", !e.currItem.title)), T = !0) : T = !1, E || O(), K()
        }, C.updateFullscreen = function (o) {
            o && setTimeout(function () {
                e.setScrollOffset(0, t.getScrollY())
            }, 50), t[(n.isFullscreen() ? "add" : "remove") + "Class"](e.template, "pswp--fs")
        }, C.updateIndexIndicator = function () {
            w.counterEl && (i.innerHTML = e.getCurrentIndex() + 1 + w.indexIndicatorSep + w.getNumItemsFn())
        }, C.onGlobalTap = function (n) {
            n = n || window.event;
            var o = n.target || n.srcElement;
            if (!v)
                if (n.detail && "mouse" === n.detail.pointerType) {
                    if (z(o)) return void e.close();
                    t.hasClass(o, "pswp__img") && (1 === e.getZoomLevel() && e.getZoomLevel() <= e.currItem.fitRatio ? w.clickToCloseNonZoomable && e.close() : e.toggleDesktopZoom(n.detail.releasePoint))
                } else if (w.tapToToggleControls && (I ? C.hideControls() : C.showControls()), w.tapToClose && (t.hasClass(o, "pswp__img") || z(o))) return void e.close()
        }, C.onMouseOver = function (e) {
            e = e || window.event;
            var t = e.target || e.srcElement;
            k(o, "ui--over-close", z(t))
        }, C.hideControls = function () {
            t.addClass(o, "pswp__ui--hidden"), I = !1
        }, C.showControls = function () {
            I = !0, T || C.update(), t.removeClass(o, "pswp__ui--hidden")
        }, C.supportsFullscreen = function () {
            var e = document;
            return !!(e.exitFullscreen || e.mozCancelFullScreen || e.webkitExitFullscreen || e.msExitFullscreen)
        }, C.getFullscreenAPI = function () {
            var t, n = document.documentElement,
                o = "fullscreenchange";
            return n.requestFullscreen ? t = {
                enterK: "requestFullscreen",
                exitK: "exitFullscreen",
                elementK: "fullscreenElement",
                eventK: o
            } : n.mozRequestFullScreen ? t = {
                enterK: "mozRequestFullScreen",
                exitK: "mozCancelFullScreen",
                elementK: "mozFullScreenElement",
                eventK: "moz" + o
            } : n.webkitRequestFullscreen ? t = {
                enterK: "webkitRequestFullscreen",
                exitK: "webkitExitFullscreen",
                elementK: "webkitFullscreenElement",
                eventK: "webkit" + o
            } : n.msRequestFullscreen && (t = {
                enterK: "msRequestFullscreen",
                exitK: "msExitFullscreen",
                elementK: "msFullscreenElement",
                eventK: "MSFullscreenChange"
            }), t && (t.enter = function () {
                return u = w.closeOnScroll, w.closeOnScroll = !1, "webkitRequestFullscreen" !== this.enterK ? e.template[this.enterK]() : void e.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
            }, t.exit = function () {
                return w.closeOnScroll = u, document[this.exitK]()
            }, t.isFullscreen = function () {
                return document[this.elementK]
            }), t
        }
    };
    return e
});
! function (t, e, i, s) {
    function n(e, i) {
        this.settings = null, this.options = t.extend({}, n.Defaults, i), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        }, this._states = {
            current: {},
            tags: {
                initializing: ["busy"],
                animating: ["busy"],
                dragging: ["interacting"]
            }
        }, t.each(["onResize", "onThrottledResize"], t.proxy(function (e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this)), t.each(n.Plugins, t.proxy(function (t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this)), t.each(n.Workers, t.proxy(function (e, i) {
            this._pipe.push({
                filter: i.filter,
                run: t.proxy(i.run, this)
            })
        }, this)), this.setup(), this.initialize()
    }
    n.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }, n.Width = {
        Default: "default",
        Inner: "inner",
        Outer: "outer"
    }, n.Type = {
        Event: "event",
        State: "state"
    }, n.Plugins = {}, n.Workers = [{
        filter: ["width", "settings"],
        run: function () {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"],
        run: function () {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (t) {
            var e = this.settings.margin || "",
                i = !this.settings.autoWidth,
                s = this.settings.rtl,
                n = {
                    width: "auto",
                    "margin-left": s ? e : "",
                    "margin-right": s ? "" : e
                };
            !i && this.$stage.children().css(n), t.css = n
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (t) {
            var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                i = null,
                s = this._items.length,
                n = !this.settings.autoWidth,
                o = [];
            for (t.items = {
                    merge: !1,
                    width: e
                }; s--;) i = this._mergers[s], i = this.settings.mergeFit && Math.min(i, this.settings.items) || i, t.items.merge = i > 1 || t.items.merge, o[s] = n ? e * i : this._items[s].width();
            this._widths = o
        }
    }, {
        filter: ["items", "settings"],
        run: function () {
            var e = [],
                i = this._items,
                s = this.settings,
                n = Math.max(2 * s.items, 4),
                o = 2 * Math.ceil(i.length / 2),
                r = s.loop && i.length ? s.rewind ? n : Math.max(n, o) : 0,
                a = "",
                h = "";
            for (r /= 2; r--;) e.push(this.normalize(e.length / 2, !0)), a += i[e[e.length - 1]][0].outerHTML, e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)), h = i[e[e.length - 1]][0].outerHTML + h;
            this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(h).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function () {
            for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, i = -1, s = 0, n = 0, o = []; ++i < e;) s = o[i - 1] || 0, n = this._widths[this.relative(i)] + this.settings.margin, o.push(s + n * t);
            this._coordinates = o
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function () {
            var t = this.settings.stagePadding,
                e = this._coordinates,
                i = {
                    width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                    "padding-left": t || "",
                    "padding-right": t || ""
                };
            this.$stage.css(i)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (t) {
            var e = this._coordinates.length,
                i = !this.settings.autoWidth,
                s = this.$stage.children();
            if (i && t.items.merge)
                for (; e--;) t.css.width = this._widths[this.relative(e)], s.eq(e).css(t.css);
            else i && (t.css.width = t.items.width, s.css(t.css))
        }
    }, {
        filter: ["items"],
        run: function () {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
        }
    }, {
        filter: ["position"],
        run: function () {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"],
        run: function () {
            var t, e, i, s, n = this.settings.rtl ? 1 : -1,
                o = 2 * this.settings.stagePadding,
                r = this.coordinates(this.current()) + o,
                a = r + this.width() * n,
                h = [];
            for (i = 0, s = this._coordinates.length; s > i; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + o * n, (this.op(t, "<=", r) && this.op(t, ">", a) || this.op(e, "<", r) && this.op(e, ">", a)) && h.push(i);
            this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + h.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
        }
    }], n.prototype.initialize = function () {
        if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
            var e, i, n;
            e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : s, n = this.$element.children(i).width(), e.length && 0 >= n && this.preloadAutoWidthImages(e)
        }
        this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
    }, n.prototype.setup = function () {
        var e = this.viewport(),
            i = this.options.responsive,
            s = -1,
            n = null;
        i ? (t.each(i, function (t) {
            e >= t && t > s && (s = Number(t))
        }), n = t.extend({}, this.options, i[s]), "function" == typeof n.stagePadding && (n.stagePadding = n.stagePadding()), delete n.responsive, n.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + s))) : n = t.extend({}, this.options), this.trigger("change", {
            property: {
                name: "settings",
                value: n
            }
        }), this._breakpoint = s, this.settings = n, this.invalidate("settings"), this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    }, n.prototype.optionsLogic = function () {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, n.prototype.prepare = function (e) {
        var i = this.trigger("prepare", {
            content: e
        });
        return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {
            content: i.data
        }), i.data
    }, n.prototype.update = function () {
        for (var e = 0, i = this._pipe.length, s = t.proxy(function (t) {
                return this[t]
            }, this._invalidated), n = {}; i > e;)(this._invalidated.all || t.grep(this._pipe[e].filter, s).length > 0) && this._pipe[e].run(n), e++;
        this._invalidated = {}, !this.is("valid") && this.enter("valid")
    }, n.prototype.width = function (t) {
        switch (t = t || n.Width.Default) {
            case n.Width.Inner:
            case n.Width.Outer:
                return this._width;
            default:
                return this._width - 2 * this.settings.stagePadding + this.settings.margin
        }
    }, n.prototype.refresh = function () {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
    }, n.prototype.onThrottledResize = function () {
        e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    }, n.prototype.onResize = function () {
        return !!this._items.length && (this._width !== this.$element.width() && (!!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))))
    }, n.prototype.registerEventHandlers = function () {
        t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), this.settings.responsive !== !1 && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
            return !1
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
    }, n.prototype.onDragStart = function (e) {
        var s = null;
        3 !== e.which && (t.support.transform ? (s = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), s = {
            x: s[16 === s.length ? 12 : 4],
            y: s[16 === s.length ? 13 : 5]
        }) : (s = this.$stage.position(), s = {
            x: this.settings.rtl ? s.left + this.$stage.width() - this.width() + this.settings.margin : s.left,
            y: s.top
        }), this.is("animating") && (t.support.transform ? this.animate(s.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = s, this._drag.stage.current = s, this._drag.pointer = this.pointer(e), t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function (e) {
            var s = this.difference(this._drag.pointer, this.pointer(e));
            t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(s.x) < Math.abs(s.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
        }, this)))
    }, n.prototype.onDragMove = function (t) {
        var e = null,
            i = null,
            s = null,
            n = this.difference(this._drag.pointer, this.pointer(t)),
            o = this.difference(this._drag.stage.start, n);
        this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), i = this.coordinates(this.maximum() + 1) - e, o.x = ((o.x - e) % i + i) % i + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), s = this.settings.pullDrag ? -1 * n.x / 5 : 0, o.x = Math.max(Math.min(o.x, e + s), i + s)), this._drag.stage.current = o, this.animate(o.x))
    }, n.prototype.onDragEnd = function (e) {
        var s = this.difference(this._drag.pointer, this.pointer(e)),
            n = this._drag.stage.current,
            o = s.x > 0 ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== s.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(n.x, 0 !== s.x ? o : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = o, (Math.abs(s.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
            return !1
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
    }, n.prototype.closest = function (e, i) {
        var s = -1,
            n = 30,
            o = this.width(),
            r = this.coordinates();
        return this.settings.freeDrag || t.each(r, t.proxy(function (t, a) {
            return "left" === i && e > a - n && a + n > e ? s = t : "right" === i && e > a - o - n && a - o + n > e ? s = t + 1 : this.op(e, "<", a) && this.op(e, ">", r[t + 1] || a - o) && (s = "left" === i ? t + 1 : t), -1 === s
        }, this)), this.settings.loop || (this.op(e, ">", r[this.minimum()]) ? s = e = this.minimum() : this.op(e, "<", r[this.maximum()]) && (s = e = this.maximum())), s
    }, n.prototype.animate = function (e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(), i && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
            transform: "translate3d(" + e + "px,0px,0px)",
            transition: this.speed() / 1e3 + "s"
        }) : i ? this.$stage.animate({
            left: e + "px"
        }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({
            left: e + "px"
        })
    }, n.prototype.is = function (t) {
        return this._states.current[t] && this._states.current[t] > 0
    }, n.prototype.current = function (t) {
        if (t === s) return this._current;
        if (0 === this._items.length) return s;
        if (t = this.normalize(t), this._current !== t) {
            var e = this.trigger("change", {
                property: {
                    name: "position",
                    value: t
                }
            });
            e.data !== s && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
                property: {
                    name: "position",
                    value: this._current
                }
            })
        }
        return this._current
    }, n.prototype.invalidate = function (e) {
        return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function (t, e) {
            return e
        })
    }, n.prototype.reset = function (t) {
        t = this.normalize(t), t !== s && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
    }, n.prototype.normalize = function (t, e) {
        var i = this._items.length,
            n = e ? 0 : this._clones.length;
        return !this.isNumeric(t) || 1 > i ? t = s : (0 > t || t >= i + n) && (t = ((t - n / 2) % i + i) % i + n / 2), t
    }, n.prototype.relative = function (t) {
        return t -= this._clones.length / 2, this.normalize(t, !0)
    }, n.prototype.maximum = function (t) {
        var e, i, s, n = this.settings,
            o = this._coordinates.length;
        if (n.loop) o = this._clones.length / 2 + this._items.length - 1;
        else if (n.autoWidth || n.merge) {
            for (e = this._items.length, i = this._items[--e].width(), s = this.$element.width(); e-- && (i += this._items[e].width() + this.settings.margin, !(i > s)););
            o = e + 1
        } else o = n.center ? this._items.length - 1 : this._items.length - n.items;
        return t && (o -= this._clones.length / 2), Math.max(o, 0)
    }, n.prototype.minimum = function (t) {
        return t ? 0 : this._clones.length / 2
    }, n.prototype.items = function (t) {
        return t === s ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
    }, n.prototype.mergers = function (t) {
        return t === s ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
    }, n.prototype.clones = function (e) {
        var i = this._clones.length / 2,
            n = i + this._items.length,
            o = function (t) {
                return t % 2 === 0 ? n + t / 2 : i - (t + 1) / 2
            };
        return e === s ? t.map(this._clones, function (t, e) {
            return o(e)
        }) : t.map(this._clones, function (t, i) {
            return t === e ? o(i) : null
        })
    }, n.prototype.speed = function (t) {
        return t !== s && (this._speed = t), this._speed
    }, n.prototype.coordinates = function (e) {
        var i, n = 1,
            o = e - 1;
        return e === s ? t.map(this._coordinates, t.proxy(function (t, e) {
            return this.coordinates(e)
        }, this)) : (this.settings.center ? (this.settings.rtl && (n = -1, o = e + 1), i = this._coordinates[e], i += (this.width() - i + (this._coordinates[o] || 0)) / 2 * n) : i = this._coordinates[o] || 0, i = Math.ceil(i))
    }, n.prototype.duration = function (t, e, i) {
        return 0 === i ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    }, n.prototype.to = function (t, e) {
        var i = this.current(),
            s = null,
            n = t - this.relative(i),
            o = (n > 0) - (0 > n),
            r = this._items.length,
            a = this.minimum(),
            h = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(n) > r / 2 && (n += -1 * o * r), t = i + n, s = ((t - a) % r + r) % r + a, s !== t && h >= s - n && s - n > 0 && (i = s - n, t = s, this.reset(i))) : this.settings.rewind ? (h += 1, t = (t % h + h) % h) : t = Math.max(a, Math.min(h, t)), this.speed(this.duration(i, t, e)), this.current(t), this.$element.is(":visible") && this.update()
    }, n.prototype.next = function (t) {
        t = t || !1, this.to(this.relative(this.current()) + 1, t)
    }, n.prototype.prev = function (t) {
        t = t || !1, this.to(this.relative(this.current()) - 1, t)
    }, n.prototype.onTransitionEnd = function (t) {
        return (t === s || (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) === this.$stage.get(0))) && (this.leave("animating"), void this.trigger("translated"))
    }, n.prototype.viewport = function () {
        var s;
        if (this.options.responsiveBaseElement !== e) s = t(this.options.responsiveBaseElement).width();
        else if (e.innerWidth) s = e.innerWidth;
        else {
            if (!i.documentElement || !i.documentElement.clientWidth) throw "Can not detect viewport width.";
            s = i.documentElement.clientWidth
        }
        return s
    }, n.prototype.replace = function (e) {
        this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function () {
            return 1 === this.nodeType
        }).each(t.proxy(function (t, e) {
            e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, n.prototype.add = function (e, i) {
        var n = this.relative(this._current);
        i = i === s ? this._items.length : this.normalize(i, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
            content: e,
            position: i
        }), e = this.prepare(e), 0 === this._items.length || i === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[i - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[i].before(e), this._items.splice(i, 0, e), this._mergers.splice(i, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[n] && this.reset(this._items[n].index()), this.invalidate("items"), this.trigger("added", {
            content: e,
            position: i
        })
    }, n.prototype.remove = function (t) {
        t = this.normalize(t, !0), t !== s && (this.trigger("remove", {
            content: this._items[t],
            position: t
        }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
            content: null,
            position: t
        }))
    }, n.prototype.preloadAutoWidthImages = function (e) {
        e.each(t.proxy(function (e, i) {
            this.enter("pre-loading"), i = t(i), t(new Image).one("load", t.proxy(function (t) {
                i.attr("src", t.target.src), i.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
        }, this))
    }, n.prototype.destroy = function () {
        this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), this.settings.responsive !== !1 && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize));
        for (var s in this._plugins) this._plugins[s].destroy();
        this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    }, n.prototype.op = function (t, e, i) {
        var s = this.settings.rtl;
        switch (e) {
            case "<":
                return s ? t > i : i > t;
            case ">":
                return s ? i > t : t > i;
            case ">=":
                return s ? i >= t : t >= i;
            case "<=":
                return s ? t >= i : i >= t
        }
    }, n.prototype.on = function (t, e, i, s) {
        t.addEventListener ? t.addEventListener(e, i, s) : t.attachEvent && t.attachEvent("on" + e, i)
    }, n.prototype.off = function (t, e, i, s) {
        t.removeEventListener ? t.removeEventListener(e, i, s) : t.detachEvent && t.detachEvent("on" + e, i)
    }, n.prototype.trigger = function (e, i, s, o, r) {
        var a = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            h = t.camelCase(t.grep(["on", e, s], function (t) {
                return t
            }).join("-").toLowerCase()),
            l = t.Event([e, "owl", s || "carousel"].join(".").toLowerCase(), t.extend({
                relatedTarget: this
            }, a, i));
        return this._supress[e] || (t.each(this._plugins, function (t, e) {
            e.onTrigger && e.onTrigger(l)
        }), this.register({
            type: n.Type.Event,
            name: e
        }), this.$element.trigger(l), this.settings && "function" == typeof this.settings[h] && this.settings[h].call(this, l)), l
    }, n.prototype.enter = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            this._states.current[e] === s && (this._states.current[e] = 0), this._states.current[e]++
        }, this))
    }, n.prototype.leave = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            this._states.current[e]--
        }, this))
    }, n.prototype.register = function (e) {
        if (e.type === n.Type.Event) {
            if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
                var i = t.event.special[e.name]._default;
                t.event.special[e.name]._default = function (t) {
                    return !i || !i.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : i.apply(this, arguments)
                }, t.event.special[e.name].owl = !0
            }
        } else e.type === n.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function (i, s) {
            return t.inArray(i, this._states.tags[e.name]) === s
        }, this)))
    }, n.prototype.suppress = function (e) {
        t.each(e, t.proxy(function (t, e) {
            this._supress[e] = !0
        }, this))
    }, n.prototype.release = function (e) {
        t.each(e, t.proxy(function (t, e) {
            delete this._supress[e]
        }, this))
    }, n.prototype.pointer = function (t) {
        var i = {
            x: null,
            y: null
        };
        return t = t.originalEvent || t || e.event, t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t, t.pageX ? (i.x = t.pageX, i.y = t.pageY) : (i.x = t.clientX, i.y = t.clientY), i
    }, n.prototype.isNumeric = function (t) {
        return !isNaN(parseFloat(t))
    }, n.prototype.difference = function (t, e) {
        return {
            x: t.x - e.x,
            y: t.y - e.y
        }
    }, t.fn.owlCarousel = function (e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var s = t(this),
                o = s.data("owl.carousel");
            o || (o = new n(this, "object" == typeof e && e), s.data("owl.carousel", o), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (e, i) {
                o.register({
                    type: n.Type.Event,
                    name: i
                }), o.$element.on(i + ".owl.carousel.core", t.proxy(function (t) {
                    t.namespace && t.relatedTarget !== this && (this.suppress([i]), o[i].apply(this, [].slice.call(arguments, 1)), this.release([i]))
                }, o))
            })), "string" == typeof e && "_" !== e.charAt(0) && o[e].apply(o, i)
        })
    }, t.fn.owlCarousel.Constructor = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._interval = null, this._visible = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoRefresh && this.watch()
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoRefresh: !0,
        autoRefreshInterval: 500
    }, n.prototype.watch = function () {
        this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
    }, n.prototype.refresh = function () {
        this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
    }, n.prototype.destroy = function () {
        var t, i;
        e.clearInterval(this._interval);
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._loaded = [], this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function (e) {
                if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type))
                    for (var i = this._core.settings, n = i.center && Math.ceil(i.items / 2) || i.items, o = i.center && -1 * n || 0, r = (e.property && e.property.value !== s ? e.property.value : this._core.current()) + o, a = this._core.clones().length, h = t.proxy(function (t, e) {
                            this.load(e)
                        }, this); o++ < n;) this.load(a / 2 + this._core.relative(r)), a && t.each(this._core.clones(this._core.relative(r)), h), r++
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        lazyLoad: !1
    }, n.prototype.load = function (i) {
        var s = this._core.$stage.children().eq(i),
            n = s && s.find(".owl-lazy");
        !n || t.inArray(s.get(0), this._loaded) > -1 || (n.each(t.proxy(function (i, s) {
            var n, o = t(s),
                r = e.devicePixelRatio > 1 && o.attr("data-src-retina") || o.attr("data-src");
            this._core.trigger("load", {
                element: o,
                url: r
            }, "lazy"), o.is("img") ? o.one("load.owl.lazy", t.proxy(function () {
                o.css("opacity", 1), this._core.trigger("loaded", {
                    element: o,
                    url: r
                }, "lazy")
            }, this)).attr("src", r) : (n = new Image, n.onload = t.proxy(function () {
                o.css({
                    "background-image": "url(" + r + ")",
                    opacity: "1"
                }), this._core.trigger("loaded", {
                    element: o,
                    url: r
                }, "lazy")
            }, this), n.src = r)
        }, this)), this._loaded.push(s.get(0)))
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Lazy = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && this.update()
            }, this),
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update()
            }, this),
            "loaded.owl.lazy": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoHeight: !1,
        autoHeightClass: "owl-height"
    }, n.prototype.update = function () {
        var e = this._core._current,
            i = e + this._core.settings.items,
            s = this._core.$stage.children().toArray().slice(e, i),
            n = [],
            o = 0;
        t.each(s, function (e, i) {
            n.push(t(i).height())
        }), o = Math.max.apply(null, n), this._core.$stage.parent().height(o).addClass(this._core.settings.autoHeightClass)
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._videos = {}, this._playing = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.register({
                    type: "state",
                    name: "playing",
                    tags: ["interacting"]
                })
            }, this),
            "resize.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault()
            }, this),
            "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
            }, this),
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" === t.property.name && this._playing && this.stop()
            }, this),
            "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find(".owl-video");
                    i.length && (i.css("display", "none"), this.fetch(i, t(e.content)))
                }
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function (t) {
            this.play(t)
        }, this))
    };
    n.Defaults = {
        video: !1,
        videoHeight: !1,
        videoWidth: !1
    }, n.prototype.fetch = function (t, e) {
        var i = function () {
                return t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube"
            }(),
            s = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
            n = t.attr("data-width") || this._core.settings.videoWidth,
            o = t.attr("data-height") || this._core.settings.videoHeight,
            r = t.attr("href");
        if (!r) throw new Error("Missing video URL.");
        if (s = r.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), s[3].indexOf("youtu") > -1) i = "youtube";
        else if (s[3].indexOf("vimeo") > -1) i = "vimeo";
        else {
            if (!(s[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
            i = "vzaar"
        }
        s = s[6], this._videos[r] = {
            type: i,
            id: s,
            width: n,
            height: o
        }, e.attr("data-video", r), this.thumbnail(t, this._videos[r])
    }, n.prototype.thumbnail = function (e, i) {
        var s, n, o, r = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
            a = e.find("img"),
            h = "src",
            l = "",
            c = this._core.settings,
            p = function (t) {
                n = '<div class="owl-video-play-icon"></div>', s = c.lazyLoad ? '<div class="owl-video-tn ' + l + '" ' + h + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>', e.after(s), e.after(n)
            };
        return e.wrap('<div class="owl-video-wrapper"' + r + "></div>"), this._core.settings.lazyLoad && (h = "data-src", l = "owl-lazy"), a.length ? (p(a.attr(h)), a.remove(), !1) : void("youtube" === i.type ? (o = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg", p(o)) : "vimeo" === i.type ? t.ajax({
            type: "GET",
            url: "//vimeo.com/api/v2/video/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                o = t[0].thumbnail_large, p(o)
            }
        }) : "vzaar" === i.type && t.ajax({
            type: "GET",
            url: "//vzaar.com/api/videos/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                o = t.framegrab_url, p(o)
            }
        }))
    }, n.prototype.stop = function () {
        this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
    }, n.prototype.play = function (e) {
        var i, s = t(e.target),
            n = s.closest("." + this._core.settings.itemClass),
            o = this._videos[n.attr("data-video")],
            r = o.width || "100%",
            a = o.height || this._core.$stage.height();
        this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), "youtube" === o.type ? i = '<iframe width="' + r + '" height="' + a + '" src="//www.youtube.com/embed/' + o.id + "?autoplay=1&v=" + o.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === o.type ? i = '<iframe src="//player.vimeo.com/video/' + o.id + '?autoplay=1" width="' + r + '" height="' + a + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === o.type && (i = '<iframe frameborder="0"height="' + a + '"width="' + r + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + o.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + i + "</div>").insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
    }, n.prototype.isInFullScreen = function () {
        var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
        return e && t(e).parent().hasClass("owl-video-frame")
    }, n.prototype.destroy = function () {
        var t, e;
        this._core.$element.off("click.owl.video");
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Video = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this.core = e, this.core.options = t.extend({}, n.Defaults, this.core.options), this.swapping = !0, this.previous = s, this.next = s, this.handlers = {
            "change.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
            }, this),
            "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
                t.namespace && (this.swapping = "translated" == t.type)
            }, this),
            "translate.owl.carousel": t.proxy(function (t) {
                t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
            }, this)
        }, this.core.$element.on(this.handlers)
    };
    n.Defaults = {
        animateOut: !1,
        animateIn: !1
    }, n.prototype.swap = function () {
        if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
            this.core.speed(0);
            var e, i = t.proxy(this.clear, this),
                s = this.core.$stage.children().eq(this.previous),
                n = this.core.$stage.children().eq(this.next),
                o = this.core.settings.animateIn,
                r = this.core.settings.animateOut;
            this.core.current() !== this.previous && (r && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), s.one(t.support.animation.end, i).css({
                left: e + "px"
            }).addClass("animated owl-animated-out").addClass(r)), o && n.one(t.support.animation.end, i).addClass("animated owl-animated-in").addClass(o))
        }
    }, n.prototype.clear = function (e) {
        t(e.target).css({
            left: ""
        }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
    }, t.fn.owlCarousel.Constructor.Plugins.Animate = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._timeout = null, this._paused = !1, this._handlers = {
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._core.settings.autoplay && this._setAutoPlayInterval()
            }, this),
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoplay && this.play()
            }, this),
            "play.owl.autoplay": t.proxy(function (t, e, i) {
                t.namespace && this.play(e, i)
            }, this),
            "stop.owl.autoplay": t.proxy(function (t) {
                t.namespace && this.stop()
            }, this),
            "mouseover.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this),
            "mouseleave.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
            }, this),
            "touchstart.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this),
            "touchend.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this.play()
            }, this)
        }, this._core.$element.on(this._handlers), this._core.options = t.extend({}, n.Defaults, this._core.options)
    };
    n.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    }, n.prototype.play = function (t, e) {
        this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval())
    }, n.prototype._getNextTimeout = function (s, n) {
        return this._timeout && e.clearTimeout(this._timeout), e.setTimeout(t.proxy(function () {
            this._paused || this._core.is("busy") || this._core.is("interacting") || i.hidden || this._core.next(n || this._core.settings.autoplaySpeed)
        }, this), s || this._core.settings.autoplayTimeout)
    }, n.prototype._setAutoPlayInterval = function () {
        this._timeout = this._getNextTimeout()
    }, n.prototype.stop = function () {
        this._core.is("rotating") && (e.clearTimeout(this._timeout), this._core.leave("rotating"))
    }, n.prototype.pause = function () {
        this._core.is("rotating") && (this._paused = !0)
    }, n.prototype.destroy = function () {
        var t, e;
        this.stop();
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.autoplay = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    "use strict";
    var n = function (e) {
        this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        }, this._handlers = {
            "prepared.owl.carousel": t.proxy(function (e) {
                e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
            }, this),
            "added.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop())
            }, this),
            "remove.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1)
            }, this),
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && this.draw()
            }, this),
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
            }, this),
            "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this.$element.on(this._handlers)
    };
    n.Defaults = {
        nav: !1,
        navText: ["prev", "next"],
        navSpeed: !1,
        navElement: "div",
        navContainer: !1,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    }, n.prototype.initialize = function () {
        var e, i = this._core.settings;
        this._controls.$relative = (i.navContainer ? t(i.navContainer) : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = t("<" + i.navElement + ">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.prev(i.navSpeed)
        }, this)), this._controls.$next = t("<" + i.navElement + ">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.next(i.navSpeed)
        }, this)), i.dotsData || (this._templates = [t("<div>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]), this._controls.$absolute = (i.dotsContainer ? t(i.dotsContainer) : t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", t.proxy(function (e) {
            var s = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
            e.preventDefault(), this.to(s, i.dotsSpeed)
        }, this));
        for (e in this._overrides) this._core[e] = t.proxy(this[e], this)
    }, n.prototype.destroy = function () {
        var t, e, i, s;
        for (t in this._handlers) this.$element.off(t, this._handlers[t]);
        for (e in this._controls) this._controls[e].remove();
        for (s in this.overides) this._core[s] = this._overrides[s];
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, n.prototype.update = function () {
        var t, e, i, s = this._core.clones().length / 2,
            n = s + this._core.items().length,
            o = this._core.maximum(!0),
            r = this._core.settings,
            a = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;
        if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy)
            for (this._pages = [], t = s, e = 0, i = 0; n > t; t++) {
                if (e >= a || 0 === e) {
                    if (this._pages.push({
                            start: Math.min(o, t - s),
                            end: t - s + a - 1
                        }), Math.min(o, t - s) === o) break;
                    e = 0, ++i
                }
                e += this._core.mergers(this._core.relative(t))
            }
    }, n.prototype.draw = function () {
        var e, i = this._core.settings,
            s = this._core.items().length <= i.items,
            n = this._core.relative(this._core.current()),
            o = i.loop || i.rewind;
        this._controls.$relative.toggleClass("disabled", !i.nav || s), i.nav && (this._controls.$previous.toggleClass("disabled", !o && n <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !o && n >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !i.dots || s), i.dots && (e = this._pages.length - this._controls.$absolute.children().length, i.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : 0 > e && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"))
    }, n.prototype.onTrigger = function (e) {
        var i = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
        }
    }, n.prototype.current = function () {
        var e = this._core.relative(this._core.current());
        return t.grep(this._pages, t.proxy(function (t, i) {
            return t.start <= e && t.end >= e
        }, this)).pop()
    }, n.prototype.getPosition = function (e) {
        var i, s, n = this._core.settings;
        return "page" == n.slideBy ? (i = t.inArray(this.current(), this._pages), s = this._pages.length, e ? ++i : --i, i = this._pages[(i % s + s) % s].start) : (i = this._core.relative(this._core.current()), s = this._core.items().length, e ? i += n.slideBy : i -= n.slideBy), i
    }, n.prototype.next = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
    }, n.prototype.prev = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
    }, n.prototype.to = function (e, i, s) {
        var n;
        !s && this._pages.length ? (n = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % n + n) % n].start, i)) : t.proxy(this._overrides.to, this._core)(e, i)
    }, t.fn.owlCarousel.Constructor.Plugins.Navigation = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    "use strict";
    var n = function (i) {
        this._core = i, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (i) {
                i.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
            }, this),
            "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                    if (!i) return;
                    this._hashes[i] = e.content
                }
            }, this),
            "changed.owl.carousel": t.proxy(function (i) {
                if (i.namespace && "position" === i.property.name) {
                    var s = this._core.items(this._core.relative(this._core.current())),
                        n = t.map(this._hashes, function (t, e) {
                            return t === s ? e : null
                        }).join();
                    if (!n || e.location.hash.slice(1) === n) return;
                    e.location.hash = n
                }
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function (t) {
            var i = e.location.hash.substring(1),
                n = this._core.$stage.children(),
                o = this._hashes[i] && n.index(this._hashes[i]);
            o !== s && o !== this._core.current() && this._core.to(this._core.relative(o), !1, !0)
        }, this))
    };
    n.Defaults = {
        URLhashListener: !1
    }, n.prototype.destroy = function () {
        var i, s;
        t(e).off("hashchange.owl.navigation");
        for (i in this._handlers) this._core.$element.off(i, this._handlers[i]);
        for (s in Object.getOwnPropertyNames(this)) "function" != typeof this[s] && (this[s] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Hash = n
}(window.Zepto || window.jQuery, window, document),
function (t, e, i, s) {
    function n(e, i) {
        var n = !1,
            o = e.charAt(0).toUpperCase() + e.slice(1);
        return t.each((e + " " + a.join(o + " ") + o).split(" "), function (t, e) {
            return r[e] !== s ? (n = !i || e, !1) : void 0
        }), n
    }

    function o(t) {
        return n(t, !0)
    }
    var r = t("<support>").get(0).style,
        a = "Webkit Moz O ms".split(" "),
        h = {
            transition: {
                end: {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd",
                    transition: "transitionend"
                }
            },
            animation: {
                end: {
                    WebkitAnimation: "webkitAnimationEnd",
                    MozAnimation: "animationend",
                    OAnimation: "oAnimationEnd",
                    animation: "animationend"
                }
            }
        },
        l = {
            csstransforms: function () {
                return !!n("transform")
            },
            csstransforms3d: function () {
                return !!n("perspective")
            },
            csstransitions: function () {
                return !!n("transition")
            },
            cssanimations: function () {
                return !!n("animation")
            }
        };
    l.csstransitions() && (t.support.transition = new String(o("transition")), t.support.transition.end = h.transition.end[t.support.transition]), l.cssanimations() && (t.support.animation = new String(o("animation")), t.support.animation.end = h.animation.end[t.support.animation]), l.csstransforms() && (t.support.transform = new String(o("transform")), t.support.transform3d = l.csstransforms3d())
}(window.Zepto || window.jQuery, window, document);
! function (t, e) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}(this, function () {
    function t() {}
    var e = t.prototype;
    return e.on = function (t, e) {
        if (t && e) {
            var i = this._events = this._events || {},
                n = i[t] = i[t] || [];
            return -1 == n.indexOf(e) && n.push(e), this
        }
    }, e.once = function (t, e) {
        if (t && e) {
            this.on(t, e);
            var i = this._onceEvents = this._onceEvents || {},
                n = i[t] = i[t] || [];
            return n[e] = !0, this
        }
    }, e.off = function (t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = i.indexOf(e);
            return -1 != n && i.splice(n, 1), this
        }
    }, e.emitEvent = function (t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = 0,
                o = i[n];
            e = e || [];
            for (var r = this._onceEvents && this._onceEvents[t]; o;) {
                var s = r && r[o];
                s && (this.off(t, o), delete r[o]), o.apply(this, e), n += s ? 0 : 1, o = i[n]
            }
            return this
        }
    }, t
}),
function (t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function (i) {
        return e(t, i)
    }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.imagesLoaded = e(t, t.EvEmitter)
}(window, function (t, e) {
    function i(t, e) {
        for (var i in e) t[i] = e[i];
        return t
    }

    function n(t) {
        var e = [];
        if (Array.isArray(t)) e = t;
        else if ("number" == typeof t.length)
            for (var i = 0; i < t.length; i++) e.push(t[i]);
        else e.push(t);
        return e
    }

    function o(t, e, r) {
        return this instanceof o ? ("string" == typeof t && (t = document.querySelectorAll(t)), this.elements = n(t), this.options = i({}, this.options), "function" == typeof e ? r = e : i(this.options, e), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred), void setTimeout(function () {
            this.check()
        }.bind(this))) : new o(t, e, r)
    }

    function r(t) {
        this.img = t
    }

    function s(t, e) {
        this.url = t, this.element = e, this.img = new Image
    }
    var h = t.jQuery,
        a = t.console;
    o.prototype = Object.create(e.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
        this.images = [], this.elements.forEach(this.addElementImages, this)
    }, o.prototype.addElementImages = function (t) {
        "IMG" == t.nodeName && this.addImage(t), this.options.background === !0 && this.addElementBackgroundImages(t);
        var e = t.nodeType;
        if (e && d[e]) {
            for (var i = t.querySelectorAll("img"), n = 0; n < i.length; n++) {
                var o = i[n];
                this.addImage(o)
            }
            if ("string" == typeof this.options.background) {
                var r = t.querySelectorAll(this.options.background);
                for (n = 0; n < r.length; n++) {
                    var s = r[n];
                    this.addElementBackgroundImages(s)
                }
            }
        }
    };
    var d = {
        1: !0,
        9: !0,
        11: !0
    };
    return o.prototype.addElementBackgroundImages = function (t) {
        var e = getComputedStyle(t);
        if (e)
            for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(e.backgroundImage); null !== n;) {
                var o = n && n[2];
                o && this.addBackground(o, t), n = i.exec(e.backgroundImage)
            }
    }, o.prototype.addImage = function (t) {
        var e = new r(t);
        this.images.push(e)
    }, o.prototype.addBackground = function (t, e) {
        var i = new s(t, e);
        this.images.push(i)
    }, o.prototype.check = function () {
        function t(t, i, n) {
            setTimeout(function () {
                e.progress(t, i, n)
            })
        }
        var e = this;
        return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (e) {
            e.once("progress", t), e.check()
        }) : void this.complete()
    }, o.prototype.progress = function (t, e, i) {
        this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, t, e)
    }, o.prototype.complete = function () {
        var t = this.hasAnyBroken ? "fail" : "done";
        if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
            var e = this.hasAnyBroken ? "reject" : "resolve";
            this.jqDeferred[e](this)
        }
    }, r.prototype = Object.create(e.prototype), r.prototype.check = function () {
        var t = this.getIsImageComplete();
        return t ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
    }, r.prototype.getIsImageComplete = function () {
        return this.img.complete && void 0 !== this.img.naturalWidth
    }, r.prototype.confirm = function (t, e) {
        this.isLoaded = t, this.emitEvent("progress", [this, this.img, e])
    }, r.prototype.handleEvent = function (t) {
        var e = "on" + t.type;
        this[e] && this[e](t)
    }, r.prototype.onload = function () {
        this.confirm(!0, "onload"), this.unbindEvents()
    }, r.prototype.onerror = function () {
        this.confirm(!1, "onerror"), this.unbindEvents()
    }, r.prototype.unbindEvents = function () {
        this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
        this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
        var t = this.getIsImageComplete();
        t && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
    }, s.prototype.unbindEvents = function () {
        this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype.confirm = function (t, e) {
        this.isLoaded = t, this.emitEvent("progress", [this, this.element, e])
    }, o.makeJQueryPlugin = function (e) {
        e = e || t.jQuery, e && (h = e, h.fn.imagesLoaded = function (t, e) {
            var i = new o(this, t, e);
            return i.jqDeferred.promise(h(this))
        })
    }, o.makeJQueryPlugin(), o
});
! function (t, e, i) {
    ! function (t) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], t) : jQuery && !jQuery.fn.qtip && t(jQuery)
    }(function (s) {
        "use strict";

        function o(t, e, i, o) {
            this.id = i, this.target = t, this.tooltip = k, this.elements = {
                target: t
            }, this._id = R + "-" + i, this.timers = {
                img: {}
            }, this.options = e, this.plugins = {}, this.cache = {
                event: {},
                target: s(),
                disabled: I,
                attr: o,
                onTooltip: I,
                lastClass: ""
            }, this.rendered = this.destroyed = this.disabled = this.waiting = this.hiddenDuringWait = this.positioning = this.triggering = I
        }

        function n(t) {
            return t === k || "object" !== s.type(t)
        }

        function r(t) {
            return !(s.isFunction(t) || t && t.attr || t.length || "object" === s.type(t) && (t.jquery || t.then))
        }

        function a(t) {
            var e, i, o, a;
            return n(t) ? I : (n(t.metadata) && (t.metadata = {
                type: t.metadata
            }), "content" in t && (e = t.content, n(e) || e.jquery || e.done ? e = t.content = {
                text: i = r(e) ? I : e
            } : i = e.text, "ajax" in e && (o = e.ajax, a = o && o.once !== I, delete e.ajax, e.text = function (t, e) {
                var n = i || s(this).attr(e.options.content.attr) || "Loading...",
                    r = s.ajax(s.extend({}, o, {
                        context: e
                    })).then(o.success, k, o.error).then(function (t) {
                        return t && a && e.set("content.text", t), t
                    }, function (t, i, s) {
                        e.destroyed || 0 === t.status || e.set("content.text", i + ": " + s)
                    });
                return a ? n : (e.set("content.text", n), r)
            }), "title" in e && (s.isPlainObject(e.title) && (e.button = e.title.button, e.title = e.title.text), r(e.title || I) && (e.title = I))), "position" in t && n(t.position) && (t.position = {
                my: t.position,
                at: t.position
            }), "show" in t && n(t.show) && (t.show = t.show.jquery ? {
                target: t.show
            } : t.show === W ? {
                ready: W
            } : {
                event: t.show
            }), "hide" in t && n(t.hide) && (t.hide = t.hide.jquery ? {
                target: t.hide
            } : {
                event: t.hide
            }), "style" in t && n(t.style) && (t.style = {
                classes: t.style
            }), s.each(V, function () {
                this.sanitize && this.sanitize(t)
            }), t)
        }

        function h(t, e) {
            for (var i, s = 0, o = t, n = e.split("."); o = o[n[s++]];) s < n.length && (i = o);
            return [i || t, n.pop()]
        }

        function l(t, e) {
            var i, s, o;
            for (i in this.checks)
                for (s in this.checks[i])(o = new RegExp(s, "i").exec(t)) && (e.push(o), ("builtin" === i || this.plugins[i]) && this.checks[i][s].apply(this.plugins[i] || this, e))
        }

        function c(t) {
            return Y.concat("").join(t ? "-" + t + " " : " ")
        }

        function d(t, e) {
            return e > 0 ? setTimeout(s.proxy(t, this), e) : void t.call(this)
        }

        function p(t) {
            this.tooltip.hasClass(tt) || (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this.timers.show = d.call(this, function () {
                this.toggle(W, t)
            }, this.options.show.delay))
        }

        function u(t) {
            if (!this.tooltip.hasClass(tt) && !this.destroyed) {
                var e = s(t.relatedTarget),
                    i = e.closest(G)[0] === this.tooltip[0],
                    o = e[0] === this.options.show.target[0];
                if (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this !== e[0] && "mouse" === this.options.position.target && i || this.options.hide.fixed && /mouse(out|leave|move)/.test(t.type) && (i || o)) try {
                    t.preventDefault(), t.stopImmediatePropagation()
                } catch (t) {} else this.timers.hide = d.call(this, function () {
                    this.toggle(I, t)
                }, this.options.hide.delay, this)
            }
        }

        function f(t) {
            !this.tooltip.hasClass(tt) && this.options.hide.inactive && (clearTimeout(this.timers.inactive), this.timers.inactive = d.call(this, function () {
                this.hide(t)
            }, this.options.hide.inactive))
        }

        function g(t) {
            this.rendered && this.tooltip[0].offsetWidth > 0 && this.reposition(t)
        }

        function m(t, i, o) {
            s(e.body).delegate(t, (i.split ? i : i.join("." + R + " ")) + "." + R, function () {
                var t = q.api[s.attr(this, X)];
                t && !t.disabled && o.apply(t, arguments)
            })
        }

        function v(t, i, n) {
            var r, h, l, c, d, p = s(e.body),
                u = t[0] === e ? p : t,
                f = t.metadata ? t.metadata(n.metadata) : k,
                g = "html5" === n.metadata.type && f ? f[n.metadata.name] : k,
                m = t.data(n.metadata.name || "qtipopts");
            try {
                m = "string" == typeof m ? s.parseJSON(m) : m
            } catch (t) {}
            if (c = s.extend(W, {}, q.defaults, n, "object" == typeof m ? a(m) : k, a(g || f)), h = c.position, c.id = i, "boolean" == typeof c.content.text) {
                if (l = t.attr(c.content.attr), c.content.attr === I || !l) return I;
                c.content.text = l
            }
            if (h.container.length || (h.container = p), h.target === I && (h.target = u), c.show.target === I && (c.show.target = u), c.show.solo === W && (c.show.solo = h.container.closest("body")), c.hide.target === I && (c.hide.target = u), c.position.viewport === W && (c.position.viewport = h.container), h.container = h.container.eq(0), h.at = new j(h.at, W), h.my = new j(h.my), t.data(R))
                if (c.overwrite) t.qtip("destroy", !0);
                else if (c.overwrite === I) return I;
            return t.attr(H, i), c.suppress && (d = t.attr("title")) && t.removeAttr("title").attr(it, d).attr("title", ""), r = new o(t, c, i, (!!l)), t.data(R, r), r
        }

        function y(t) {
            return t.charAt(0).toUpperCase() + t.slice(1)
        }

        function b(t, e) {
            var s, o, n = e.charAt(0).toUpperCase() + e.slice(1),
                r = (e + " " + vt.join(n + " ") + n).split(" "),
                a = 0;
            if (mt[e]) return t.css(mt[e]);
            for (; s = r[a++];)
                if ((o = t.css(s)) !== i) return mt[e] = s, o
        }

        function w(t, e) {
            return Math.ceil(parseFloat(b(t, e)))
        }

        function _(t, e) {
            this._ns = "tip", this.options = e, this.offset = e.offset, this.size = [e.width, e.height], this.init(this.qtip = t)
        }

        function x(t, e) {
            this.options = e, this._ns = "-modal", this.init(this.qtip = t)
        }

        function C(t) {
            this._ns = "ie6", this.init(this.qtip = t)
        }
        var q, T, j, z, M, W = !0,
            I = !1,
            k = null,
            E = "x",
            S = "y",
            A = "width",
            L = "height",
            P = "top",
            D = "left",
            B = "bottom",
            F = "right",
            N = "center",
            O = "flipinvert",
            $ = "shift",
            V = {},
            R = "qtip",
            H = "data-hasqtip",
            X = "data-qtip-id",
            Y = ["ui-widget", "ui-tooltip"],
            G = "." + R,
            U = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),
            Q = R + "-fixed",
            J = R + "-default",
            K = R + "-focus",
            Z = R + "-hover",
            tt = R + "-disabled",
            et = "_replacedByqTip",
            it = "oldtitle",
            st = {
                ie: function () {
                    for (var t = 4, i = e.createElement("div");
                        (i.innerHTML = "<!--[if gt IE " + t + "]><i></i><![endif]-->") && i.getElementsByTagName("i")[0]; t += 1);
                    return t > 4 ? t : NaN
                }(),
                iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || I
            };
        T = o.prototype, T._when = function (t) {
            return s.when.apply(s, t)
        }, T.render = function (t) {
            if (this.rendered || this.destroyed) return this;
            var e, i = this,
                o = this.options,
                n = this.cache,
                r = this.elements,
                a = o.content.text,
                h = o.content.title,
                l = o.content.button,
                c = o.position,
                d = ("." + this._id + " ", []);
            return s.attr(this.target[0], "aria-describedby", this._id), n.posClass = this._createPosClass((this.position = {
                my: c.my,
                at: c.at
            }).my), this.tooltip = r.tooltip = e = s("<div/>", {
                id: this._id,
                class: [R, J, o.style.classes, n.posClass].join(" "),
                width: o.style.width || "",
                height: o.style.height || "",
                tracking: "mouse" === c.target && c.adjust.mouse,
                role: "alert",
                "aria-live": "polite",
                "aria-atomic": I,
                "aria-describedby": this._id + "-content",
                "aria-hidden": W
            }).toggleClass(tt, this.disabled).attr(X, this.id).data(R, this).appendTo(c.container).append(r.content = s("<div />", {
                class: R + "-content",
                id: this._id + "-content",
                "aria-atomic": W
            })), this.rendered = -1, this.positioning = W, h && (this._createTitle(), s.isFunction(h) || d.push(this._updateTitle(h, I))), l && this._createButton(), s.isFunction(a) || d.push(this._updateContent(a, I)), this.rendered = W, this._setWidget(), s.each(V, function (t) {
                var e;
                "render" === this.initialize && (e = this(i)) && (i.plugins[t] = e)
            }), this._unassignEvents(), this._assignEvents(), this._when(d).then(function () {
                i._trigger("render"), i.positioning = I, i.hiddenDuringWait || !o.show.ready && !t || i.toggle(W, n.event, I), i.hiddenDuringWait = I
            }), q.api[this.id] = this, this
        }, T.destroy = function (t) {
            function e() {
                if (!this.destroyed) {
                    this.destroyed = W;
                    var t, e = this.target,
                        i = e.attr(it);
                    this.rendered && this.tooltip.stop(1, 0).find("*").remove().end().remove(), s.each(this.plugins, function () {
                        this.destroy && this.destroy()
                    });
                    for (t in this.timers) clearTimeout(this.timers[t]);
                    e.removeData(R).removeAttr(X).removeAttr(H).removeAttr("aria-describedby"), this.options.suppress && i && e.attr("title", i).removeAttr(it), this._unassignEvents(), this.options = this.elements = this.cache = this.timers = this.plugins = this.mouse = k, delete q.api[this.id]
                }
            }
            return this.destroyed ? this.target : (t === W && "hide" !== this.triggering || !this.rendered ? e.call(this) : (this.tooltip.one("tooltiphidden", s.proxy(e, this)), !this.triggering && this.hide()), this.target)
        }, z = T.checks = {
            builtin: {
                "^id$": function (t, e, i, o) {
                    var n = i === W ? q.nextid : i,
                        r = R + "-" + n;
                    n !== I && n.length > 0 && !s("#" + r).length ? (this._id = r, this.rendered && (this.tooltip[0].id = this._id, this.elements.content[0].id = this._id + "-content", this.elements.title[0].id = this._id + "-title")) : t[e] = o
                },
                "^prerender": function (t, e, i) {
                    i && !this.rendered && this.render(this.options.show.ready)
                },
                "^content.text$": function (t, e, i) {
                    this._updateContent(i)
                },
                "^content.attr$": function (t, e, i, s) {
                    this.options.content.text === this.target.attr(s) && this._updateContent(this.target.attr(i))
                },
                "^content.title$": function (t, e, i) {
                    return i ? (i && !this.elements.title && this._createTitle(), void this._updateTitle(i)) : this._removeTitle()
                },
                "^content.button$": function (t, e, i) {
                    this._updateButton(i)
                },
                "^content.title.(text|button)$": function (t, e, i) {
                    this.set("content." + e, i)
                },
                "^position.(my|at)$": function (t, e, i) {
                    "string" == typeof i && (this.position[e] = t[e] = new j(i, "at" === e))
                },
                "^position.container$": function (t, e, i) {
                    this.rendered && this.tooltip.appendTo(i)
                },
                "^show.ready$": function (t, e, i) {
                    i && (!this.rendered && this.render(W) || this.toggle(W))
                },
                "^style.classes$": function (t, e, i, s) {
                    this.rendered && this.tooltip.removeClass(s).addClass(i)
                },
                "^style.(width|height)": function (t, e, i) {
                    this.rendered && this.tooltip.css(e, i)
                },
                "^style.widget|content.title": function () {
                    this.rendered && this._setWidget()
                },
                "^style.def": function (t, e, i) {
                    this.rendered && this.tooltip.toggleClass(J, !!i)
                },
                "^events.(render|show|move|hide|focus|blur)$": function (t, e, i) {
                    this.rendered && this.tooltip[(s.isFunction(i) ? "" : "un") + "bind"]("tooltip" + e, i)
                },
                "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function () {
                    if (this.rendered) {
                        var t = this.options.position;
                        this.tooltip.attr("tracking", "mouse" === t.target && t.adjust.mouse), this._unassignEvents(), this._assignEvents()
                    }
                }
            }
        }, T.get = function (t) {
            if (this.destroyed) return this;
            var e = h(this.options, t.toLowerCase()),
                i = e[0][e[1]];
            return i.precedance ? i.string() : i
        };
        var ot = /^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,
            nt = /^prerender|show\.ready/i;
        T.set = function (t, e) {
            if (this.destroyed) return this;
            var i, o = this.rendered,
                n = I,
                r = this.options;
            return this.checks, "string" == typeof t ? (i = t, t = {}, t[i] = e) : t = s.extend({}, t), s.each(t, function (e, i) {
                if (o && nt.test(e)) return void delete t[e];
                var a, l = h(r, e.toLowerCase());
                a = l[0][l[1]], l[0][l[1]] = i && i.nodeType ? s(i) : i, n = ot.test(e) || n, t[e] = [l[0], l[1], i, a]
            }), a(r), this.positioning = W, s.each(t, s.proxy(l, this)), this.positioning = I, this.rendered && this.tooltip[0].offsetWidth > 0 && n && this.reposition("mouse" === r.position.target ? k : this.cache.event), this
        }, T._update = function (t, e) {
            var i = this,
                o = this.cache;
            return this.rendered && t ? (s.isFunction(t) && (t = t.call(this.elements.target, o.event, this) || ""), s.isFunction(t.then) ? (o.waiting = W, t.then(function (t) {
                return o.waiting = I, i._update(t, e)
            }, k, function (t) {
                return i._update(t, e)
            })) : t === I || !t && "" !== t ? I : (t.jquery && t.length > 0 ? e.empty().append(t.css({
                display: "block",
                visibility: "visible"
            })) : e.html(t), this._waitForContent(e).then(function (t) {
                i.rendered && i.tooltip[0].offsetWidth > 0 && i.reposition(o.event, !t.length)
            }))) : I
        }, T._waitForContent = function (t) {
            var e = this.cache;
            return e.waiting = W, (s.fn.imagesLoaded ? t.imagesLoaded() : s.Deferred().resolve([])).done(function () {
                e.waiting = I
            }).promise()
        }, T._updateContent = function (t, e) {
            this._update(t, this.elements.content, e)
        }, T._updateTitle = function (t, e) {
            this._update(t, this.elements.title, e) === I && this._removeTitle(I)
        }, T._createTitle = function () {
            var t = this.elements,
                e = this._id + "-title";
            t.titlebar && this._removeTitle(), t.titlebar = s("<div />", {
                class: R + "-titlebar " + (this.options.style.widget ? c("header") : "")
            }).append(t.title = s("<div />", {
                id: e,
                class: R + "-title",
                "aria-atomic": W
            })).insertBefore(t.content).delegate(".qtip-close", "mousedown keydown mouseup keyup mouseout", function (t) {
                s(this).toggleClass("ui-state-active ui-state-focus", "down" === t.type.substr(-4))
            }).delegate(".qtip-close", "mouseover mouseout", function (t) {
                s(this).toggleClass("ui-state-hover", "mouseover" === t.type)
            }), this.options.content.button && this._createButton()
        }, T._removeTitle = function (t) {
            var e = this.elements;
            e.title && (e.titlebar.remove(), e.titlebar = e.title = e.button = k, t !== I && this.reposition())
        }, T._createPosClass = function (t) {
            return R + "-pos-" + (t || this.options.position.my).abbrev()
        }, T.reposition = function (i, o) {
            if (!this.rendered || this.positioning || this.destroyed) return this;
            this.positioning = W;
            var n, r, a, h, l = this.cache,
                c = this.tooltip,
                d = this.options.position,
                p = d.target,
                u = d.my,
                f = d.at,
                g = d.viewport,
                m = d.container,
                v = d.adjust,
                y = v.method.split(" "),
                b = c.outerWidth(I),
                w = c.outerHeight(I),
                _ = 0,
                x = 0,
                C = c.css("position"),
                q = {
                    left: 0,
                    top: 0
                },
                T = c[0].offsetWidth > 0,
                j = i && "scroll" === i.type,
                z = s(t),
                M = m[0].ownerDocument,
                k = this.mouse;
            if (s.isArray(p) && 2 === p.length) f = {
                x: D,
                y: P
            }, q = {
                left: p[0],
                top: p[1]
            };
            else if ("mouse" === p) f = {
                x: D,
                y: P
            }, (!v.mouse || this.options.hide.distance) && l.origin && l.origin.pageX ? i = l.origin : !i || i && ("resize" === i.type || "scroll" === i.type) ? i = l.event : k && k.pageX && (i = k), "static" !== C && (q = m.offset()), M.body.offsetWidth !== (t.innerWidth || M.documentElement.clientWidth) && (r = s(e.body).offset()), q = {
                left: i.pageX - q.left + (r && r.left || 0),
                top: i.pageY - q.top + (r && r.top || 0)
            }, v.mouse && j && k && (q.left -= (k.scrollX || 0) - z.scrollLeft(), q.top -= (k.scrollY || 0) - z.scrollTop());
            else {
                if ("event" === p ? i && i.target && "scroll" !== i.type && "resize" !== i.type ? l.target = s(i.target) : i.target || (l.target = this.elements.target) : "event" !== p && (l.target = s(p.jquery ? p : this.elements.target)), p = l.target, p = s(p).eq(0), 0 === p.length) return this;
                p[0] === e || p[0] === t ? (_ = st.iOS ? t.innerWidth : p.width(), x = st.iOS ? t.innerHeight : p.height(), p[0] === t && (q = {
                    top: (g || p).scrollTop(),
                    left: (g || p).scrollLeft()
                })) : V.imagemap && p.is("area") ? n = V.imagemap(this, p, f, V.viewport ? y : I) : V.svg && p && p[0].ownerSVGElement ? n = V.svg(this, p, f, V.viewport ? y : I) : (_ = p.outerWidth(I), x = p.outerHeight(I), q = p.offset()), n && (_ = n.width, x = n.height, r = n.offset, q = n.position), q = this.reposition.offset(p, q, m), (st.iOS > 3.1 && st.iOS < 4.1 || st.iOS >= 4.3 && st.iOS < 4.33 || !st.iOS && "fixed" === C) && (q.left -= z.scrollLeft(), q.top -= z.scrollTop()), (!n || n && n.adjustable !== I) && (q.left += f.x === F ? _ : f.x === N ? _ / 2 : 0, q.top += f.y === B ? x : f.y === N ? x / 2 : 0)
            }
            return q.left += v.x + (u.x === F ? -b : u.x === N ? -b / 2 : 0), q.top += v.y + (u.y === B ? -w : u.y === N ? -w / 2 : 0), V.viewport ? (a = q.adjusted = V.viewport(this, q, d, _, x, b, w), r && a.left && (q.left += r.left), r && a.top && (q.top += r.top), a.my && (this.position.my = a.my)) : q.adjusted = {
                left: 0,
                top: 0
            }, l.posClass !== (h = this._createPosClass(this.position.my)) && c.removeClass(l.posClass).addClass(l.posClass = h), this._trigger("move", [q, g.elem || g], i) ? (delete q.adjusted, o === I || !T || isNaN(q.left) || isNaN(q.top) || "mouse" === p || !s.isFunction(d.effect) ? c.css(q) : s.isFunction(d.effect) && (d.effect.call(c, this, s.extend({}, q)), c.queue(function (t) {
                s(this).css({
                    opacity: "",
                    height: ""
                }), st.ie && this.style.removeAttribute("filter"), t()
            })), this.positioning = I, this) : this
        }, T.reposition.offset = function (t, i, o) {
            function n(t, e) {
                i.left += e * t.scrollLeft(), i.top += e * t.scrollTop()
            }
            if (!o[0]) return i;
            var r, a, h, l, c = s(t[0].ownerDocument),
                d = !!st.ie && "CSS1Compat" !== e.compatMode,
                p = o[0];
            do "static" !== (a = s.css(p, "position")) && ("fixed" === a ? (h = p.getBoundingClientRect(), n(c, -1)) : (h = s(p).position(), h.left += parseFloat(s.css(p, "borderLeftWidth")) || 0, h.top += parseFloat(s.css(p, "borderTopWidth")) || 0), i.left -= h.left + (parseFloat(s.css(p, "marginLeft")) || 0), i.top -= h.top + (parseFloat(s.css(p, "marginTop")) || 0), r || "hidden" === (l = s.css(p, "overflow")) || "visible" === l || (r = s(p))); while (p = p.offsetParent);
            return r && (r[0] !== c[0] || d) && n(r, 1), i
        };
        var rt = (j = T.reposition.Corner = function (t, e) {
            t = ("" + t).replace(/([A-Z])/, " $1").replace(/middle/gi, N).toLowerCase(), this.x = (t.match(/left|right/i) || t.match(/center/) || ["inherit"])[0].toLowerCase(), this.y = (t.match(/top|bottom|center/i) || ["inherit"])[0].toLowerCase(), this.forceY = !!e;
            var i = t.charAt(0);
            this.precedance = "t" === i || "b" === i ? S : E
        }).prototype;
        rt.invert = function (t, e) {
            this[t] = this[t] === D ? F : this[t] === F ? D : e || this[t]
        }, rt.string = function (t) {
            var e = this.x,
                i = this.y,
                s = e !== i ? "center" === e || "center" !== i && (this.precedance === S || this.forceY) ? [i, e] : [e, i] : [e];
            return t !== !1 ? s.join(" ") : s
        }, rt.abbrev = function () {
            var t = this.string(!1);
            return t[0].charAt(0) + (t[1] && t[1].charAt(0) || "")
        }, rt.clone = function () {
            return new j(this.string(), this.forceY)
        }, T.toggle = function (t, i) {
            var o = this.cache,
                n = this.options,
                r = this.tooltip;
            if (i) {
                if (/over|enter/.test(i.type) && o.event && /out|leave/.test(o.event.type) && n.show.target.add(i.target).length === n.show.target.length && r.has(i.relatedTarget).length) return this;
                o.event = s.event.fix(i)
            }
            if (this.waiting && !t && (this.hiddenDuringWait = W), !this.rendered) return t ? this.render(1) : this;
            if (this.destroyed || this.disabled) return this;
            var a, h, l, c = t ? "show" : "hide",
                d = this.options[c],
                p = (this.options[t ? "hide" : "show"], this.options.position),
                u = this.options.content,
                f = this.tooltip.css("width"),
                g = this.tooltip.is(":visible"),
                m = t || 1 === d.target.length,
                v = !i || d.target.length < 2 || o.target[0] === i.target;
            return (typeof t).search("boolean|number") && (t = !g), a = !r.is(":animated") && g === t && v, h = a ? k : !!this._trigger(c, [90]), this.destroyed ? this : (h !== I && t && this.focus(i), !h || a ? this : (s.attr(r[0], "aria-hidden", !t), t ? (this.mouse && (o.origin = s.event.fix(this.mouse)), s.isFunction(u.text) && this._updateContent(u.text, I), s.isFunction(u.title) && this._updateTitle(u.title, I), !M && "mouse" === p.target && p.adjust.mouse && (s(e).bind("mousemove." + R, this._storeMouse), M = W), f || r.css("width", r.outerWidth(I)), this.reposition(i, arguments[2]), f || r.css("width", ""), d.solo && ("string" == typeof d.solo ? s(d.solo) : s(G, d.solo)).not(r).not(d.target).qtip("hide", s.Event("tooltipsolo"))) : (clearTimeout(this.timers.show), delete o.origin, M && !s(G + '[tracking="true"]:visible', d.solo).not(r).length && (s(e).unbind("mousemove." + R), M = I), this.blur(i)), l = s.proxy(function () {
                t ? (st.ie && r[0].style.removeAttribute("filter"), r.css("overflow", ""), "string" == typeof d.autofocus && s(this.options.show.autofocus, r).focus(), this.options.show.target.trigger("qtip-" + this.id + "-inactive")) : r.css({
                    display: "",
                    visibility: "",
                    opacity: "",
                    left: "",
                    top: ""
                }), this._trigger(t ? "visible" : "hidden")
            }, this), d.effect === I || m === I ? (r[c](), l()) : s.isFunction(d.effect) ? (r.stop(1, 1), d.effect.call(r, this), r.queue("fx", function (t) {
                l(), t()
            })) : r.fadeTo(90, t ? 1 : 0, l), t && d.target.trigger("qtip-" + this.id + "-inactive"), this))
        }, T.show = function (t) {
            return this.toggle(W, t)
        }, T.hide = function (t) {
            return this.toggle(I, t)
        }, T.focus = function (t) {
            if (!this.rendered || this.destroyed) return this;
            var e = s(G),
                i = this.tooltip,
                o = parseInt(i[0].style.zIndex, 10),
                n = q.zindex + e.length;
            return i.hasClass(K) || this._trigger("focus", [n], t) && (o !== n && (e.each(function () {
                this.style.zIndex > o && (this.style.zIndex = this.style.zIndex - 1)
            }), e.filter("." + K).qtip("blur", t)), i.addClass(K)[0].style.zIndex = n), this
        }, T.blur = function (t) {
            return !this.rendered || this.destroyed ? this : (this.tooltip.removeClass(K), this._trigger("blur", [this.tooltip.css("zIndex")], t), this)
        }, T.disable = function (t) {
            return this.destroyed ? this : ("toggle" === t ? t = !(this.rendered ? this.tooltip.hasClass(tt) : this.disabled) : "boolean" != typeof t && (t = W), this.rendered && this.tooltip.toggleClass(tt, t).attr("aria-disabled", t), this.disabled = !!t, this)
        }, T.enable = function () {
            return this.disable(I)
        }, T._createButton = function () {
            var t = this,
                e = this.elements,
                i = e.tooltip,
                o = this.options.content.button,
                n = "string" == typeof o,
                r = n ? o : "Close tooltip";
            e.button && e.button.remove(), e.button = o.jquery ? o : s("<a />", {
                class: "qtip-close " + (this.options.style.widget ? "" : R + "-icon"),
                title: r,
                "aria-label": r
            }).prepend(s("<span />", {
                class: "ui-icon ui-icon-close",
                html: "&times;"
            })), e.button.appendTo(e.titlebar || i).attr("role", "button").click(function (e) {
                return i.hasClass(tt) || t.hide(e), I
            })
        }, T._updateButton = function (t) {
            if (!this.rendered) return I;
            var e = this.elements.button;
            t ? this._createButton() : e.remove()
        }, T._setWidget = function () {
            var t = this.options.style.widget,
                e = this.elements,
                i = e.tooltip,
                s = i.hasClass(tt);
            i.removeClass(tt), tt = t ? "ui-state-disabled" : "qtip-disabled", i.toggleClass(tt, s), i.toggleClass("ui-helper-reset " + c(), t).toggleClass(J, this.options.style.def && !t), e.content && e.content.toggleClass(c("content"), t), e.titlebar && e.titlebar.toggleClass(c("header"), t), e.button && e.button.toggleClass(R + "-icon", !t)
        }, T._storeMouse = function (t) {
            return (this.mouse = s.event.fix(t)).type = "mousemove", this
        }, T._bind = function (t, e, i, o, n) {
            if (t && i && e.length) {
                var r = "." + this._id + (o ? "-" + o : "");
                return s(t).bind((e.split ? e : e.join(r + " ")) + r, s.proxy(i, n || this)), this
            }
        }, T._unbind = function (t, e) {
            return t && s(t).unbind("." + this._id + (e ? "-" + e : "")), this
        }, T._trigger = function (t, e, i) {
            var o = s.Event("tooltip" + t);
            return o.originalEvent = i && s.extend({}, i) || this.cache.event || k, this.triggering = t, this.tooltip.trigger(o, [this].concat(e || [])), this.triggering = I, !o.isDefaultPrevented()
        }, T._bindEvents = function (t, e, i, o, n, r) {
            var a = i.filter(o).add(o.filter(i)),
                h = [];
            a.length && (s.each(e, function (e, i) {
                var o = s.inArray(i, t);
                o > -1 && h.push(t.splice(o, 1)[0])
            }), h.length && (this._bind(a, h, function (t) {
                var e = !!this.rendered && this.tooltip[0].offsetWidth > 0;
                (e ? r : n).call(this, t)
            }), i = i.not(a), o = o.not(a))), this._bind(i, t, n), this._bind(o, e, r)
        }, T._assignInitialEvents = function (t) {
            function e(t) {
                return this.disabled || this.destroyed ? I : (this.cache.event = t && s.event.fix(t), this.cache.target = t && s(t.target), clearTimeout(this.timers.show), void(this.timers.show = d.call(this, function () {
                    this.render("object" == typeof t || i.show.ready)
                }, i.prerender ? 0 : i.show.delay)))
            }
            var i = this.options,
                o = i.show.target,
                n = i.hide.target,
                r = i.show.event ? s.trim("" + i.show.event).split(" ") : [],
                a = i.hide.event ? s.trim("" + i.hide.event).split(" ") : [];
            this._bind(this.elements.target, ["remove", "removeqtip"], function () {
                this.destroy(!0)
            }, "destroy"), /mouse(over|enter)/i.test(i.show.event) && !/mouse(out|leave)/i.test(i.hide.event) && a.push("mouseleave"), this._bind(o, "mousemove", function (t) {
                this._storeMouse(t), this.cache.onTarget = W
            }), this._bindEvents(r, a, o, n, e, function () {
                return this.timers ? void clearTimeout(this.timers.show) : I
            }), (i.show.ready || i.prerender) && e.call(this, t)
        }, T._assignEvents = function () {
            var i = this,
                o = this.options,
                n = o.position,
                r = this.tooltip,
                a = o.show.target,
                h = o.hide.target,
                l = n.container,
                c = n.viewport,
                d = s(e),
                m = (s(e.body), s(t)),
                v = o.show.event ? s.trim("" + o.show.event).split(" ") : [],
                y = o.hide.event ? s.trim("" + o.hide.event).split(" ") : [];
            s.each(o.events, function (t, e) {
                i._bind(r, "toggle" === t ? ["tooltipshow", "tooltiphide"] : ["tooltip" + t], e, null, r)
            }), /mouse(out|leave)/i.test(o.hide.event) && "window" === o.hide.leave && this._bind(d, ["mouseout", "blur"], function (t) {
                /select|option/.test(t.target.nodeName) || t.relatedTarget || this.hide(t)
            }), o.hide.fixed ? h = h.add(r.addClass(Q)) : /mouse(over|enter)/i.test(o.show.event) && this._bind(h, "mouseleave", function () {
                clearTimeout(this.timers.show)
            }), ("" + o.hide.event).indexOf("unfocus") > -1 && this._bind(l.closest("html"), ["mousedown", "touchstart"], function (t) {
                var e = s(t.target),
                    i = this.rendered && !this.tooltip.hasClass(tt) && this.tooltip[0].offsetWidth > 0,
                    o = e.parents(G).filter(this.tooltip[0]).length > 0;
                e[0] === this.target[0] || e[0] === this.tooltip[0] || o || this.target.has(e[0]).length || !i || this.hide(t)
            }), "number" == typeof o.hide.inactive && (this._bind(a, "qtip-" + this.id + "-inactive", f, "inactive"), this._bind(h.add(r), q.inactiveEvents, f)), this._bindEvents(v, y, a, h, p, u), this._bind(a.add(r), "mousemove", function (t) {
                if ("number" == typeof o.hide.distance) {
                    var e = this.cache.origin || {},
                        i = this.options.hide.distance,
                        s = Math.abs;
                    (s(t.pageX - e.pageX) >= i || s(t.pageY - e.pageY) >= i) && this.hide(t)
                }
                this._storeMouse(t)
            }), "mouse" === n.target && n.adjust.mouse && (o.hide.event && this._bind(a, ["mouseenter", "mouseleave"], function (t) {
                return this.cache ? void(this.cache.onTarget = "mouseenter" === t.type) : I
            }), this._bind(d, "mousemove", function (t) {
                this.rendered && this.cache.onTarget && !this.tooltip.hasClass(tt) && this.tooltip[0].offsetWidth > 0 && this.reposition(t)
            })), (n.adjust.resize || c.length) && this._bind(s.event.special.resize ? c : m, "resize", g), n.adjust.scroll && this._bind(m.add(n.container), "scroll", g)
        }, T._unassignEvents = function () {
            var i = this.options,
                o = i.show.target,
                n = i.hide.target,
                r = s.grep([this.elements.target[0], this.rendered && this.tooltip[0], i.position.container[0], i.position.viewport[0], i.position.container.closest("html")[0], t, e], function (t) {
                    return "object" == typeof t
                });
            o && o.toArray && (r = r.concat(o.toArray())), n && n.toArray && (r = r.concat(n.toArray())), this._unbind(r)._unbind(r, "destroy")._unbind(r, "inactive")
        }, s(function () {
            m(G, ["mouseenter", "mouseleave"], function (t) {
                var e = "mouseenter" === t.type,
                    i = s(t.currentTarget),
                    o = s(t.relatedTarget || t.target),
                    n = this.options;
                e ? (this.focus(t), i.hasClass(Q) && !i.hasClass(tt) && clearTimeout(this.timers.hide)) : "mouse" === n.position.target && n.position.adjust.mouse && n.hide.event && n.show.target && !o.closest(n.show.target[0]).length && this.hide(t), i.toggleClass(Z, e)
            }), m("[" + X + "]", U, f)
        }), q = s.fn.qtip = function (t, e, o) {
            var n = ("" + t).toLowerCase(),
                r = k,
                h = s.makeArray(arguments).slice(1),
                l = h[h.length - 1],
                c = this[0] ? s.data(this[0], R) : k;
            return !arguments.length && c || "api" === n ? c : "string" == typeof t ? (this.each(function () {
                var t = s.data(this, R);
                if (!t) return W;
                if (l && l.timeStamp && (t.cache.event = l), !e || "option" !== n && "options" !== n) t[n] && t[n].apply(t, h);
                else {
                    if (o === i && !s.isPlainObject(e)) return r = t.get(e), I;
                    t.set(e, o)
                }
            }), r !== k ? r : this) : "object" != typeof t && arguments.length ? void 0 : (c = a(s.extend(W, {}, t)), this.each(function (t) {
                var e, i;
                return i = s.isArray(c.id) ? c.id[t] : c.id, i = !i || i === I || i.length < 1 || q.api[i] ? q.nextid++ : i, e = v(s(this), i, c), e === I ? W : (q.api[i] = e, s.each(V, function () {
                    "initialize" === this.initialize && this(e)
                }), void e._assignInitialEvents(l))
            }))
        }, s.qtip = o, q.api = {}, s.each({
            attr: function (t, e) {
                if (this.length) {
                    var i = this[0],
                        o = "title",
                        n = s.data(i, "qtip");
                    if (t === o && n && "object" == typeof n && n.options.suppress) return arguments.length < 2 ? s.attr(i, it) : (n && n.options.content.attr === o && n.cache.attr && n.set("content.text", e), this.attr(it, e))
                }
                return s.fn["attr" + et].apply(this, arguments)
            },
            clone: function (t) {
                var e = (s([]), s.fn["clone" + et].apply(this, arguments));
                return t || e.filter("[" + it + "]").attr("title", function () {
                    return s.attr(this, it)
                }).removeAttr(it), e
            }
        }, function (t, e) {
            if (!e || s.fn[t + et]) return W;
            var i = s.fn[t + et] = s.fn[t];
            s.fn[t] = function () {
                return e.apply(this, arguments) || i.apply(this, arguments)
            }
        }), s.ui || (s["cleanData" + et] = s.cleanData, s.cleanData = function (t) {
            for (var e, i = 0;
                (e = s(t[i])).length; i++)
                if (e.attr(H)) try {
                    e.triggerHandler("removeqtip")
                } catch (t) {}
                s["cleanData" + et].apply(this, arguments)
        }), q.version = "2.2.1", q.nextid = 0, q.inactiveEvents = U, q.zindex = 15e3, q.defaults = {
            prerender: I,
            id: I,
            overwrite: W,
            suppress: W,
            content: {
                text: W,
                attr: "title",
                title: I,
                button: I
            },
            position: {
                my: "top left",
                at: "bottom right",
                target: I,
                container: I,
                viewport: I,
                adjust: {
                    x: 0,
                    y: 0,
                    mouse: W,
                    scroll: W,
                    resize: W,
                    method: "flipinvert flipinvert"
                },
                effect: function (t, e) {
                    s(this).animate(e, {
                        duration: 200,
                        queue: I
                    })
                }
            },
            show: {
                target: I,
                event: "mouseenter",
                effect: W,
                delay: 90,
                solo: I,
                ready: I,
                autofocus: I
            },
            hide: {
                target: I,
                event: "mouseleave",
                effect: W,
                delay: 0,
                fixed: I,
                inactive: I,
                leave: "window",
                distance: I
            },
            style: {
                classes: "",
                widget: I,
                width: I,
                height: I,
                def: W
            },
            events: {
                render: k,
                move: k,
                show: k,
                hide: k,
                toggle: k,
                visible: k,
                hidden: k,
                focus: k,
                blur: k
            }
        };
        var at, ht = "margin",
            lt = "border",
            ct = "color",
            dt = "background-color",
            pt = "transparent",
            ut = " !important",
            ft = !!e.createElement("canvas").getContext,
            gt = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,
            mt = {},
            vt = ["Webkit", "O", "Moz", "ms"];
        if (ft) var yt = t.devicePixelRatio || 1,
            bt = function () {
                var t = e.createElement("canvas").getContext("2d");
                return t.backingStorePixelRatio || t.webkitBackingStorePixelRatio || t.mozBackingStorePixelRatio || t.msBackingStorePixelRatio || t.oBackingStorePixelRatio || 1
            }(),
            wt = yt / bt;
        else var _t = function (t, e, i) {
            return "<qtipvml:" + t + ' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" ' + (e || "") + ' style="behavior: url(#default#VML); ' + (i || "") + '" />'
        };
        s.extend(_.prototype, {
            init: function (t) {
                var e, i;
                i = this.element = t.elements.tip = s("<div />", {
                    class: R + "-tip"
                }).prependTo(t.tooltip), ft ? (e = s("<canvas />").appendTo(this.element)[0].getContext("2d"), e.lineJoin = "miter", e.miterLimit = 1e5, e.save()) : (e = _t("shape", 'coordorigin="0,0"', "position:absolute;"), this.element.html(e + e), t._bind(s("*", i).add(i), ["click", "mousedown"], function (t) {
                    t.stopPropagation()
                }, this._ns)), t._bind(t.tooltip, "tooltipmove", this.reposition, this._ns, this), this.create()
            },
            _swapDimensions: function () {
                this.size[0] = this.options.height, this.size[1] = this.options.width
            },
            _resetDimensions: function () {
                this.size[0] = this.options.width, this.size[1] = this.options.height
            },
            _useTitle: function (t) {
                var e = this.qtip.elements.titlebar;
                return e && (t.y === P || t.y === N && this.element.position().top + this.size[1] / 2 + this.options.offset < e.outerHeight(W))
            },
            _parseCorner: function (t) {
                var e = this.qtip.options.position.my;
                return t === I || e === I ? t = I : t === W ? t = new j(e.string()) : t.string || (t = new j(t), t.fixed = W), t
            },
            _parseWidth: function (t, e, i) {
                var s = this.qtip.elements,
                    o = lt + y(e) + "Width";
                return (i ? w(i, o) : w(s.content, o) || w(this._useTitle(t) && s.titlebar || s.content, o) || w(s.tooltip, o)) || 0
            },
            _parseRadius: function (t) {
                var e = this.qtip.elements,
                    i = lt + y(t.y) + y(t.x) + "Radius";
                return st.ie < 9 ? 0 : w(this._useTitle(t) && e.titlebar || e.content, i) || w(e.tooltip, i) || 0
            },
            _invalidColour: function (t, e, i) {
                var s = t.css(e);
                return !s || i && s === t.css(i) || gt.test(s) ? I : s
            },
            _parseColours: function (t) {
                var e = this.qtip.elements,
                    i = this.element.css("cssText", ""),
                    o = lt + y(t[t.precedance]) + y(ct),
                    n = this._useTitle(t) && e.titlebar || e.content,
                    r = this._invalidColour,
                    a = [];
                return a[0] = r(i, dt) || r(n, dt) || r(e.content, dt) || r(e.tooltip, dt) || i.css(dt), a[1] = r(i, o, ct) || r(n, o, ct) || r(e.content, o, ct) || r(e.tooltip, o, ct) || e.tooltip.css(o), s("*", i).add(i).css("cssText", dt + ":" + pt + ut + ";" + lt + ":0" + ut + ";"), a
            },
            _calculateSize: function (t) {
                var e, i, s, o = t.precedance === S,
                    n = this.options.width,
                    r = this.options.height,
                    a = "c" === t.abbrev(),
                    h = (o ? n : r) * (a ? .5 : 1),
                    l = Math.pow,
                    c = Math.round,
                    d = Math.sqrt(l(h, 2) + l(r, 2)),
                    p = [this.border / h * d, this.border / r * d];
                return p[2] = Math.sqrt(l(p[0], 2) - l(this.border, 2)), p[3] = Math.sqrt(l(p[1], 2) - l(this.border, 2)), e = d + p[2] + p[3] + (a ? 0 : p[0]), i = e / d, s = [c(i * n), c(i * r)], o ? s : s.reverse()
            },
            _calculateTip: function (t, e, i) {
                i = i || 1, e = e || this.size;
                var s = e[0] * i,
                    o = e[1] * i,
                    n = Math.ceil(s / 2),
                    r = Math.ceil(o / 2),
                    a = {
                        br: [0, 0, s, o, s, 0],
                        bl: [0, 0, s, 0, 0, o],
                        tr: [0, o, s, 0, s, o],
                        tl: [0, 0, 0, o, s, o],
                        tc: [0, o, n, 0, s, o],
                        bc: [0, 0, s, 0, n, o],
                        rc: [0, 0, s, r, 0, o],
                        lc: [s, 0, s, o, 0, r]
                    };
                return a.lt = a.br, a.rt = a.bl, a.lb = a.tr, a.rb = a.tl, a[t.abbrev()]
            },
            _drawCoords: function (t, e) {
                t.beginPath(), t.moveTo(e[0], e[1]), t.lineTo(e[2], e[3]), t.lineTo(e[4], e[5]), t.closePath()
            },
            create: function () {
                var t = this.corner = (ft || st.ie) && this._parseCorner(this.options.corner);
                return (this.enabled = !!this.corner && "c" !== this.corner.abbrev()) && (this.qtip.cache.corner = t.clone(), this.update()), this.element.toggle(this.enabled), this.corner
            },
            update: function (e, i) {
                if (!this.enabled) return this;
                var o, n, r, a, h, l, c, d, p = this.qtip.elements,
                    u = this.element,
                    f = u.children(),
                    g = this.options,
                    m = this.size,
                    v = g.mimic,
                    y = Math.round;
                e || (e = this.qtip.cache.corner || this.corner), v === I ? v = e : (v = new j(v), v.precedance = e.precedance, "inherit" === v.x ? v.x = e.x : "inherit" === v.y ? v.y = e.y : v.x === v.y && (v[e.precedance] = e[e.precedance])), n = v.precedance, e.precedance === E ? this._swapDimensions() : this._resetDimensions(), o = this.color = this._parseColours(e), o[1] !== pt ? (d = this.border = this._parseWidth(e, e[e.precedance]), g.border && 1 > d && !gt.test(o[1]) && (o[0] = o[1]), this.border = d = g.border !== W ? g.border : d) : this.border = d = 0, c = this.size = this._calculateSize(e), u.css({
                    width: c[0],
                    height: c[1],
                    lineHeight: c[1] + "px"
                }), l = e.precedance === S ? [y(v.x === D ? d : v.x === F ? c[0] - m[0] - d : (c[0] - m[0]) / 2), y(v.y === P ? c[1] - m[1] : 0)] : [y(v.x === D ? c[0] - m[0] : 0), y(v.y === P ? d : v.y === B ? c[1] - m[1] - d : (c[1] - m[1]) / 2)], ft ? (r = f[0].getContext("2d"), r.restore(), r.save(), r.clearRect(0, 0, 6e3, 6e3), a = this._calculateTip(v, m, wt), h = this._calculateTip(v, this.size, wt), f.attr(A, c[0] * wt).attr(L, c[1] * wt), f.css(A, c[0]).css(L, c[1]), this._drawCoords(r, h), r.fillStyle = o[1], r.fill(), r.translate(l[0] * wt, l[1] * wt), this._drawCoords(r, a), r.fillStyle = o[0], r.fill()) : (a = this._calculateTip(v), a = "m" + a[0] + "," + a[1] + " l" + a[2] + "," + a[3] + " " + a[4] + "," + a[5] + " xe", l[2] = d && /^(r|b)/i.test(e.string()) ? 8 === st.ie ? 2 : 1 : 0, f.css({
                    coordsize: c[0] + d + " " + (c[1] + d),
                    antialias: "" + (v.string().indexOf(N) > -1),
                    left: l[0] - l[2] * Number(n === E),
                    top: l[1] - l[2] * Number(n === S),
                    width: c[0] + d,
                    height: c[1] + d
                }).each(function (t) {
                    var e = s(this);
                    e[e.prop ? "prop" : "attr"]({
                        coordsize: c[0] + d + " " + (c[1] + d),
                        path: a,
                        fillcolor: o[0],
                        filled: !!t,
                        stroked: !t
                    }).toggle(!(!d && !t)), !t && e.html(_t("stroke", 'weight="' + 2 * d + 'px" color="' + o[1] + '" miterlimit="1000" joinstyle="miter"'))
                })), t.opera && setTimeout(function () {
                    p.tip.css({
                        display: "inline-block",
                        visibility: "visible"
                    })
                }, 1), i !== I && this.calculate(e, c)
            },
            calculate: function (t, e) {
                if (!this.enabled) return I;
                var i, o, n = this,
                    r = this.qtip.elements,
                    a = this.element,
                    h = this.options.offset,
                    l = (r.tooltip.hasClass("ui-widget"), {});
                return t = t || this.corner,
                    i = t.precedance, e = e || this._calculateSize(t), o = [t.x, t.y], i === E && o.reverse(), s.each(o, function (s, o) {
                        var a, c, d;
                        o === N ? (a = i === S ? D : P, l[a] = "50%", l[ht + "-" + a] = -Math.round(e[i === S ? 0 : 1] / 2) + h) : (a = n._parseWidth(t, o, r.tooltip), c = n._parseWidth(t, o, r.content), d = n._parseRadius(t), l[o] = Math.max(-n.border, s ? c : h + (d > a ? d : -a)))
                    }), l[t[i]] -= e[i === E ? 0 : 1], a.css({
                        margin: "",
                        top: "",
                        bottom: "",
                        left: "",
                        right: ""
                    }).css(l), l
            },
            reposition: function (t, e, s) {
                function o(t, e, i, s, o) {
                    t === $ && l.precedance === e && c[s] && l[i] !== N ? l.precedance = l.precedance === E ? S : E : t !== $ && c[s] && (l[e] = l[e] === N ? c[s] > 0 ? s : o : l[e] === s ? o : s)
                }

                function n(t, e, o) {
                    l[t] === N ? g[ht + "-" + e] = f[t] = r[ht + "-" + e] - c[e] : (a = r[o] !== i ? [c[e], -r[e]] : [-c[e], r[e]], (f[t] = Math.max(a[0], a[1])) > a[0] && (s[e] -= c[e], f[e] = I), g[r[o] !== i ? o : e] = f[t])
                }
                if (this.enabled) {
                    var r, a, h = e.cache,
                        l = this.corner.clone(),
                        c = s.adjusted,
                        d = e.options.position.adjust.method.split(" "),
                        p = d[0],
                        u = d[1] || d[0],
                        f = {
                            left: I,
                            top: I,
                            x: 0,
                            y: 0
                        },
                        g = {};
                    this.corner.fixed !== W && (o(p, E, S, D, F), o(u, S, E, P, B), (l.string() !== h.corner.string() || h.cornerTop !== c.top || h.cornerLeft !== c.left) && this.update(l, I)), r = this.calculate(l), r.right !== i && (r.left = -r.right), r.bottom !== i && (r.top = -r.bottom), r.user = this.offset, (f.left = p === $ && !!c.left) && n(E, D, F), (f.top = u === $ && !!c.top) && n(S, P, B), this.element.css(g).toggle(!(f.x && f.y || l.x === N && f.y || l.y === N && f.x)), s.left -= r.left.charAt ? r.user : p !== $ || f.top || !f.left && !f.top ? r.left + this.border : 0, s.top -= r.top.charAt ? r.user : u !== $ || f.left || !f.left && !f.top ? r.top + this.border : 0, h.cornerLeft = c.left, h.cornerTop = c.top, h.corner = l.clone()
                }
            },
            destroy: function () {
                this.qtip._unbind(this.qtip.tooltip, this._ns), this.qtip.elements.tip && this.qtip.elements.tip.find("*").remove().end().remove()
            }
        }), at = V.tip = function (t) {
            return new _(t, t.options.style.tip)
        }, at.initialize = "render", at.sanitize = function (t) {
            if (t.style && "tip" in t.style) {
                var e = t.style.tip;
                "object" != typeof e && (e = t.style.tip = {
                    corner: e
                }), /string|boolean/i.test(typeof e.corner) || (e.corner = W)
            }
        }, z.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function () {
                this.create(), this.qtip.reposition()
            },
            "^style.tip.(height|width)$": function (t) {
                this.size = [t.width, t.height], this.update(), this.qtip.reposition()
            },
            "^content.title|style.(classes|widget)$": function () {
                this.update()
            }
        }, s.extend(W, q.defaults, {
            style: {
                tip: {
                    corner: W,
                    mimic: I,
                    width: 6,
                    height: 6,
                    border: W,
                    offset: 0
                }
            }
        });
        var xt, Ct, qt = "qtip-modal",
            Tt = "." + qt;
        Ct = function () {
            function t(t) {
                if (s.expr[":"].focusable) return s.expr[":"].focusable;
                var e, i, o, n = !isNaN(s.attr(t, "tabindex")),
                    r = t.nodeName && t.nodeName.toLowerCase();
                return "area" === r ? (e = t.parentNode, i = e.name, !(!t.href || !i || "map" !== e.nodeName.toLowerCase()) && (o = s("img[usemap=#" + i + "]")[0], !!o && o.is(":visible"))) : /input|select|textarea|button|object/.test(r) ? !t.disabled : "a" === r ? t.href || n : n
            }

            function i(t) {
                c.length < 1 && t.length ? t.not("body").blur() : c.first().focus()
            }

            function o(t) {
                if (h.is(":visible")) {
                    var e, o = s(t.target),
                        a = n.tooltip,
                        l = o.closest(G);
                    e = l.length < 1 ? I : parseInt(l[0].style.zIndex, 10) > parseInt(a[0].style.zIndex, 10), e || o.closest(G)[0] === a[0] || i(o), r = t.target === c[c.length - 1]
                }
            }
            var n, r, a, h, l = this,
                c = {};
            s.extend(l, {
                init: function () {
                    return h = l.elem = s("<div />", {
                        id: "qtip-overlay",
                        html: "<div></div>",
                        mousedown: function () {
                            return I
                        }
                    }).hide(), s(e.body).bind("focusin" + Tt, o), s(e).bind("keydown" + Tt, function (t) {
                        n && n.options.show.modal.escape && 27 === t.keyCode && n.hide(t)
                    }), h.bind("click" + Tt, function (t) {
                        n && n.options.show.modal.blur && n.hide(t)
                    }), l
                },
                update: function (e) {
                    n = e, c = e.options.show.modal.stealfocus !== I ? e.tooltip.find("*").filter(function () {
                        return t(this)
                    }) : []
                },
                toggle: function (t, o, r) {
                    var c = (s(e.body), t.tooltip),
                        d = t.options.show.modal,
                        p = d.effect,
                        u = o ? "show" : "hide",
                        f = h.is(":visible"),
                        g = s(Tt).filter(":visible:not(:animated)").not(c);
                    return l.update(t), o && d.stealfocus !== I && i(s(":focus")), h.toggleClass("blurs", d.blur), o && h.appendTo(e.body), h.is(":animated") && f === o && a !== I || !o && g.length ? l : (h.stop(W, I), s.isFunction(p) ? p.call(h, o) : p === I ? h[u]() : h.fadeTo(parseInt(r, 10) || 90, o ? 1 : 0, function () {
                        o || h.hide()
                    }), o || h.queue(function (t) {
                        h.css({
                            left: "",
                            top: ""
                        }), s(Tt).length || h.detach(), t()
                    }), a = o, n.destroyed && (n = k), l)
                }
            }), l.init()
        }, Ct = new Ct, s.extend(x.prototype, {
            init: function (t) {
                var e = t.tooltip;
                return this.options.on ? (t.elements.overlay = Ct.elem, e.addClass(qt).css("z-index", q.modal_zindex + s(Tt).length), t._bind(e, ["tooltipshow", "tooltiphide"], function (t, i, o) {
                    var n = t.originalEvent;
                    if (t.target === e[0])
                        if (n && "tooltiphide" === t.type && /mouse(leave|enter)/.test(n.type) && s(n.relatedTarget).closest(Ct.elem[0]).length) try {
                            t.preventDefault()
                        } catch (t) {} else(!n || n && "tooltipsolo" !== n.type) && this.toggle(t, "tooltipshow" === t.type, o)
                }, this._ns, this), t._bind(e, "tooltipfocus", function (t, i) {
                    if (!t.isDefaultPrevented() && t.target === e[0]) {
                        var o = s(Tt),
                            n = q.modal_zindex + o.length,
                            r = parseInt(e[0].style.zIndex, 10);
                        Ct.elem[0].style.zIndex = n - 1, o.each(function () {
                            this.style.zIndex > r && (this.style.zIndex -= 1)
                        }), o.filter("." + K).qtip("blur", t.originalEvent), e.addClass(K)[0].style.zIndex = n, Ct.update(i);
                        try {
                            t.preventDefault()
                        } catch (t) {}
                    }
                }, this._ns, this), void t._bind(e, "tooltiphide", function (t) {
                    t.target === e[0] && s(Tt).filter(":visible").not(e).last().qtip("focus", t)
                }, this._ns, this)) : this
            },
            toggle: function (t, e, i) {
                return t && t.isDefaultPrevented() ? this : void Ct.toggle(this.qtip, !!e, i)
            },
            destroy: function () {
                this.qtip.tooltip.removeClass(qt), this.qtip._unbind(this.qtip.tooltip, this._ns), Ct.toggle(this.qtip, I), delete this.qtip.elements.overlay
            }
        }), xt = V.modal = function (t) {
            return new x(t, t.options.show.modal)
        }, xt.sanitize = function (t) {
            t.show && ("object" != typeof t.show.modal ? t.show.modal = {
                on: !!t.show.modal
            } : "undefined" == typeof t.show.modal.on && (t.show.modal.on = W))
        }, q.modal_zindex = q.zindex - 200, xt.initialize = "render", z.modal = {
            "^show.modal.(on|blur)$": function () {
                this.destroy(), this.init(), this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth > 0)
            }
        }, s.extend(W, q.defaults, {
            show: {
                modal: {
                    on: I,
                    effect: W,
                    blur: W,
                    stealfocus: W,
                    escape: W
                }
            }
        }), V.viewport = function (i, s, o, n, r, a, h) {
            function l(t, e, i, o, n, r, a, h, l) {
                var c = s[n],
                    y = w[t],
                    b = _[t],
                    x = i === $,
                    C = y === n ? l : y === r ? -l : -l / 2,
                    q = b === n ? h : b === r ? -h : -h / 2,
                    T = m[n] + v[n] - (u ? 0 : p[n]),
                    j = T - c,
                    z = c + l - (a === A ? f : g) - T,
                    M = C - (w.precedance === t || y === w[e] ? q : 0) - (b === N ? h / 2 : 0);
                return x ? (M = (y === n ? 1 : -1) * C, s[n] += j > 0 ? j : z > 0 ? -z : 0, s[n] = Math.max(-p[n] + v[n], c - M, Math.min(Math.max(-p[n] + v[n] + (a === A ? f : g), c + M), s[n], "center" === y ? c - C : 1e9))) : (o *= i === O ? 2 : 0, j > 0 && (y !== n || z > 0) ? (s[n] -= M + o, d.invert(t, n)) : z > 0 && (y !== r || j > 0) && (s[n] -= (y === N ? -M : M) + o, d.invert(t, r)), s[n] < m && -s[n] > z && (s[n] = c, d = w.clone())), s[n] - c
            }
            var c, d, p, u, f, g, m, v, y = o.target,
                b = i.elements.tooltip,
                w = o.my,
                _ = o.at,
                x = o.adjust,
                C = x.method.split(" "),
                q = C[0],
                T = C[1] || C[0],
                j = o.viewport,
                z = o.container,
                M = (i.cache, {
                    left: 0,
                    top: 0
                });
            return j.jquery && y[0] !== t && y[0] !== e.body && "none" !== x.method ? (p = z.offset() || M, u = "static" === z.css("position"), c = "fixed" === b.css("position"), f = j[0] === t ? j.width() : j.outerWidth(I), g = j[0] === t ? j.height() : j.outerHeight(I), m = {
                left: c ? 0 : j.scrollLeft(),
                top: c ? 0 : j.scrollTop()
            }, v = j.offset() || M, ("shift" !== q || "shift" !== T) && (d = w.clone()), M = {
                left: "none" !== q ? l(E, S, q, x.x, D, F, A, n, a) : 0,
                top: "none" !== T ? l(S, E, T, x.y, P, B, L, r, h) : 0,
                my: d
            }) : M
        }, V.polys = {
            polygon: function (t, e) {
                var i, s, o, n = {
                        width: 0,
                        height: 0,
                        position: {
                            top: 1e10,
                            right: 0,
                            bottom: 0,
                            left: 1e10
                        },
                        adjustable: I
                    },
                    r = 0,
                    a = [],
                    h = 1,
                    l = 1,
                    c = 0,
                    d = 0;
                for (r = t.length; r--;) i = [parseInt(t[--r], 10), parseInt(t[r + 1], 10)], i[0] > n.position.right && (n.position.right = i[0]), i[0] < n.position.left && (n.position.left = i[0]), i[1] > n.position.bottom && (n.position.bottom = i[1]), i[1] < n.position.top && (n.position.top = i[1]), a.push(i);
                if (s = n.width = Math.abs(n.position.right - n.position.left), o = n.height = Math.abs(n.position.bottom - n.position.top), "c" === e.abbrev()) n.position = {
                    left: n.position.left + n.width / 2,
                    top: n.position.top + n.height / 2
                };
                else {
                    for (; s > 0 && o > 0 && h > 0 && l > 0;)
                        for (s = Math.floor(s / 2), o = Math.floor(o / 2), e.x === D ? h = s : e.x === F ? h = n.width - s : h += Math.floor(s / 2), e.y === P ? l = o : e.y === B ? l = n.height - o : l += Math.floor(o / 2), r = a.length; r-- && !(a.length < 2);) c = a[r][0] - n.position.left, d = a[r][1] - n.position.top, (e.x === D && c >= h || e.x === F && h >= c || e.x === N && (h > c || c > n.width - h) || e.y === P && d >= l || e.y === B && l >= d || e.y === N && (l > d || d > n.height - l)) && a.splice(r, 1);
                    n.position = {
                        left: a[0][0],
                        top: a[0][1]
                    }
                }
                return n
            },
            rect: function (t, e, i, s) {
                return {
                    width: Math.abs(i - t),
                    height: Math.abs(s - e),
                    position: {
                        left: Math.min(t, i),
                        top: Math.min(e, s)
                    }
                }
            },
            _angles: {
                tc: 1.5,
                tr: 7 / 4,
                tl: 5 / 4,
                bc: .5,
                br: .25,
                bl: .75,
                rc: 2,
                lc: 1,
                c: 0
            },
            ellipse: function (t, e, i, s, o) {
                var n = V.polys._angles[o.abbrev()],
                    r = 0 === n ? 0 : i * Math.cos(n * Math.PI),
                    a = s * Math.sin(n * Math.PI);
                return {
                    width: 2 * i - Math.abs(r),
                    height: 2 * s - Math.abs(a),
                    position: {
                        left: t + r,
                        top: e + a
                    },
                    adjustable: I
                }
            },
            circle: function (t, e, i, s) {
                return V.polys.ellipse(t, e, i, i, s)
            }
        }, V.svg = function (t, i, o) {
            for (var n, r, a, h, l, c, d, p, u, f = (s(e), i[0]), g = s(f.ownerSVGElement), m = f.ownerDocument, v = (parseInt(i.css("stroke-width"), 10) || 0) / 2; !f.getBBox;) f = f.parentNode;
            if (!f.getBBox || !f.parentNode) return I;
            switch (f.nodeName) {
                case "ellipse":
                case "circle":
                    p = V.polys.ellipse(f.cx.baseVal.value, f.cy.baseVal.value, (f.rx || f.r).baseVal.value + v, (f.ry || f.r).baseVal.value + v, o);
                    break;
                case "line":
                case "polygon":
                case "polyline":
                    for (d = f.points || [{
                            x: f.x1.baseVal.value,
                            y: f.y1.baseVal.value
                        }, {
                            x: f.x2.baseVal.value,
                            y: f.y2.baseVal.value
                        }], p = [], c = -1, h = d.numberOfItems || d.length; ++c < h;) l = d.getItem ? d.getItem(c) : d[c], p.push.apply(p, [l.x, l.y]);
                    p = V.polys.polygon(p, o);
                    break;
                default:
                    p = f.getBBox(), p = {
                        width: p.width,
                        height: p.height,
                        position: {
                            left: p.x,
                            top: p.y
                        }
                    }
            }
            return u = p.position, g = g[0], g.createSVGPoint && (r = f.getScreenCTM(), d = g.createSVGPoint(), d.x = u.left, d.y = u.top, a = d.matrixTransform(r), u.left = a.x, u.top = a.y), m !== e && "mouse" !== t.position.target && (n = s((m.defaultView || m.parentWindow).frameElement).offset(), n && (u.left += n.left, u.top += n.top)), m = s(m), u.left += m.scrollLeft(), u.top += m.scrollTop(), p
        }, V.imagemap = function (t, e, i) {
            e.jquery || (e = s(e));
            var o, n, r, a, h, l = (e.attr("shape") || "rect").toLowerCase().replace("poly", "polygon"),
                c = s('img[usemap="#' + e.parent("map").attr("name") + '"]'),
                d = s.trim(e.attr("coords")),
                p = d.replace(/,$/, "").split(",");
            if (!c.length) return I;
            if ("polygon" === l) a = V.polys.polygon(p, i);
            else {
                if (!V.polys[l]) return I;
                for (r = -1, h = p.length, n = []; ++r < h;) n.push(parseInt(p[r], 10));
                a = V.polys[l].apply(this, n.concat(i))
            }
            return o = c.offset(), o.left += Math.ceil((c.outerWidth(I) - c.width()) / 2), o.top += Math.ceil((c.outerHeight(I) - c.height()) / 2), a.position.left += o.left, a.position.top += o.top, a
        };
        var jt, zt = '<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';
        s.extend(C.prototype, {
            _scroll: function () {
                var e = this.qtip.elements.overlay;
                e && (e[0].style.top = s(t).scrollTop() + "px")
            },
            init: function (i) {
                var o = i.tooltip;
                s("select, object").length < 1 && (this.bgiframe = i.elements.bgiframe = s(zt).appendTo(o), i._bind(o, "tooltipmove", this.adjustBGIFrame, this._ns, this)), this.redrawContainer = s("<div/>", {
                    id: R + "-rcontainer"
                }).appendTo(e.body), i.elements.overlay && i.elements.overlay.addClass("qtipmodal-ie6fix") && (i._bind(t, ["scroll", "resize"], this._scroll, this._ns, this), i._bind(o, ["tooltipshow"], this._scroll, this._ns, this)), this.redraw()
            },
            adjustBGIFrame: function () {
                var t, e, i = this.qtip.tooltip,
                    s = {
                        height: i.outerHeight(I),
                        width: i.outerWidth(I)
                    },
                    o = this.qtip.plugins.tip,
                    n = this.qtip.elements.tip;
                e = parseInt(i.css("borderLeftWidth"), 10) || 0, e = {
                    left: -e,
                    top: -e
                }, o && n && (t = "x" === o.corner.precedance ? [A, D] : [L, P], e[t[1]] -= n[t[0]]()), this.bgiframe.css(e).css(s)
            },
            redraw: function () {
                if (this.qtip.rendered < 1 || this.drawing) return this;
                var t, e, i, s, o = this.qtip.tooltip,
                    n = this.qtip.options.style,
                    r = this.qtip.options.position.container;
                return this.qtip.drawing = 1, n.height && o.css(L, n.height), n.width ? o.css(A, n.width) : (o.css(A, "").appendTo(this.redrawContainer), e = o.width(), 1 > e % 2 && (e += 1), i = o.css("maxWidth") || "", s = o.css("minWidth") || "", t = (i + s).indexOf("%") > -1 ? r.width() / 100 : 0, i = (i.indexOf("%") > -1 ? t : 1) * parseInt(i, 10) || e, s = (s.indexOf("%") > -1 ? t : 1) * parseInt(s, 10) || 0, e = i + s ? Math.min(Math.max(e, s), i) : e, o.css(A, Math.round(e)).appendTo(r)), this.drawing = 0, this
            },
            destroy: function () {
                this.bgiframe && this.bgiframe.remove(), this.qtip._unbind([t, this.qtip.tooltip], this._ns)
            }
        }), jt = V.ie6 = function (t) {
            return 6 === st.ie ? new C(t) : I
        }, jt.initialize = "render", z.ie6 = {
            "^content|style$": function () {
                this.redraw()
            }
        }
    })
}(window, document);
! function (t) {
    "use strict";
    t.fn.fitVids = function (e) {
        var i = {
            customSelector: null
        };
        if (!document.getElementById("fit-vids-style")) {
            var r = document.head || document.getElementsByTagName("head")[0],
                d = ".fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}",
                a = document.createElement("div");
            a.innerHTML = '<p>x</p><style id="fit-vids-style">' + d + "</style>", r.appendChild(a.childNodes[1])
        }
        return e && t.extend(i, e), this.each(function () {
            var e = ["iframe[src*='player.vimeo.com']", "iframe[src*='youtube.com']", "iframe[src*='youtube-nocookie.com']", "iframe[src*='kickstarter.com'][src*='video.html']", "object", "embed"];
            i.customSelector && e.push(i.customSelector);
            var r = t(this).find(e.join(","));
            r = r.not("object object"), r.each(function () {
                var e = t(this);
                if (!("embed" === this.tagName.toLowerCase() && e.parent("object").length || e.parent(".fluid-width-video-wrapper").length)) {
                    var i = "object" === this.tagName.toLowerCase() || e.attr("height") && !isNaN(parseInt(e.attr("height"), 10)) ? parseInt(e.attr("height"), 10) : e.height(),
                        r = isNaN(parseInt(e.attr("width"), 10)) ? e.width() : parseInt(e.attr("width"), 10),
                        d = i / r;
                    if (!e.attr("id")) {
                        var a = "fitvid" + Math.floor(999999 * Math.random());
                        e.attr("id", a)
                    }
                    e.wrap('<div class="fluid-width-video-wrapper"></div>').parent(".fluid-width-video-wrapper").css("padding-top", 100 * d + "%"), e.removeAttr("height").removeAttr("width")
                }
            })
        })
    }
}(window.jQuery || window.Zepto);
! function () {
    window.$clamp = function (e, t) {
        function n(e, t) {
            return p.getComputedStyle || (p.getComputedStyle = function (e, t) {
                return this.el = e, this.getPropertyValue = function (t) {
                    var n = /(\-([a-z]){1})/g;
                    return "float" == t && (t = "styleFloat"), n.test(t) && (t = t.replace(n, function (e, t, n) {
                        return n.toUpperCase()
                    })), e.currentStyle && e.currentStyle[t] ? e.currentStyle[t] : null
                }, this
            }), p.getComputedStyle(e, null).getPropertyValue(t)
        }

        function i(t) {
            t = t || e.clientHeight;
            var n = a(e);
            return Math.max(Math.floor(t / n), 0)
        }

        function l(t) {
            return a(e) * t
        }

        function a(e) {
            var t = n(e, "line-height");
            return "normal" == t && (t = 1.2 * parseInt(n(e, "font-size"))), parseInt(t)
        }

        function r(t) {
            return t.lastChild.children && 0 < t.lastChild.children.length ? r(Array.prototype.slice.call(t.children).pop()) : t.lastChild && t.lastChild.nodeValue && "" != t.lastChild.nodeValue && t.lastChild.nodeValue != s.truncationChar ? t.lastChild : (t.lastChild.parentNode.removeChild(t.lastChild), r(e))
        }

        function o(t, n) {
            if (n) {
                var i = t.nodeValue.replace(s.truncationChar, "");
                if (g || (H = 0 < v.length ? v.shift() : "", g = i.split(H)), 1 < g.length ? (y = g.pop(), u(t, g.join(H))) : g = null, c && (t.nodeValue = t.nodeValue.replace(s.truncationChar, ""), e.innerHTML = t.nodeValue + " " + c.innerHTML + s.truncationChar), g) {
                    if (e.clientHeight <= n) {
                        if (!(0 <= v.length && "" != H)) return e.innerHTML;
                        u(t, g.join(H) + H + y), g = null
                    }
                } else "" == H && (u(t, ""), t = r(e), v = s.splitOnChars.slice(0), H = v[0], y = g = null);
                if (!s.animate) return o(t, n);
                setTimeout(function () {
                    o(t, n)
                }, !0 === s.animate ? 10 : s.animate)
            }
        }

        function u(e, t) {
            e.nodeValue = t + s.truncationChar
        }
        t = t || {};
        var c, p = window,
            s = {
                clamp: t.clamp || 2,
                useNativeClamp: "undefined" == typeof t.useNativeClamp || t.useNativeClamp,
                splitOnChars: t.splitOnChars || [".", "-", "–", "—", " "],
                animate: t.animate || !1,
                truncationChar: t.truncationChar || "…",
                truncationHTML: t.truncationHTML
            },
            h = e.style,
            d = e.innerHTML,
            f = "undefined" != typeof e.style.webkitLineClamp,
            C = s.clamp,
            m = C.indexOf && (-1 < C.indexOf("px") || -1 < C.indexOf("em"));
        s.truncationHTML && (c = document.createElement("span"), c.innerHTML = s.truncationHTML);
        var g, y, v = s.splitOnChars.slice(0),
            H = v[0];
        "auto" == C ? C = i() : m && (C = i(parseInt(C)));
        var L;
        return f && s.useNativeClamp ? (h.overflow = "hidden", h.textOverflow = "ellipsis", h.webkitBoxOrient = "vertical", h.display = "-webkit-box", h.webkitLineClamp = C, m && (h.height = s.clamp + "px")) : (h = l(C), h <= e.clientHeight && (L = o(r(e), h))), {
            original: d,
            clamped: L
        }
    }
}();
var app = app || {};
app.menu = new function () {
    this.okay = !0, this.toggle = function () {
        if (app.menu.okay) {
            app.menu.okay = !1;
            var e = jQuery(".wrapper");
            e.hasClass("show_menu") ? (e.removeClass("show_menu").addClass("hide_menu"), jQuery("#responsive-menu-icon").removeClass("close"), jQuery("#responsive-menu").fadeOut(300, function () {
                e.removeClass("hide_menu"), jQuery("html, body").css("overflow", "").removeClass("menu"), setTimeout(function () {
                    app.menu.okay = !0
                }, 300)
            })) : (jQuery("html, body").css("overflow", "hidden").addClass("menu"), e.addClass("show_menu"), jQuery("#responsive-menu-icon").addClass("close"), setTimeout(function () {
                jQuery("#responsive-menu").fadeIn(300, function () {
                    app.menu.okay = !0
                })
            }, 300))
        }
        return !1
    }
}, app.pswp = null, app.pswp_counter = 0, "function" != typeof String.prototype.endsWith && (String.prototype.endsWith = function (e) {
    return -1 !== this.indexOf(e, this.length - e.length)
}), jQuery(function (e) {
    if (e(".accordion > .accordion-container > div > a").click(function () {
            return e("+ div", e(this).toggleClass("open")).slideToggle(), !1
        }), e(".flexslider").flexslider({
            controlNav: !1
        }), e("a.open-gallery").click(function () {
            return app.pswp = new PhotoSwipe(e(".pswp")[0], PhotoSwipeUI_Default, (that = e(this)).closest(".gallery").data("gallery"), {
                history: !1,
                index: that.data("index") ? that.data("index") : 0
            }), app.pswp.listen("close", function () {
                window.location.href.endsWith("gallery/") && history.back()
            }), app.pswp.init(), "undefined" != typeof history && (that.attr("data-pswp_counter", app.pswp_counter), history.pushState(app.pswp_counter++, null, window.location.href + "gallery/")), !1
        }), window.addEventListener("popstate", function (t) {
            window.location.href.endsWith("gallery/") && "undefined" !== t.state ? (app.pswp = new PhotoSwipe(e(".pswp")[0], PhotoSwipeUI_Default, (that = e('[data-pswp_counter="' + t.state + '"]')).closest(".gallery").data("gallery"), {
                history: !1,
                index: that.data("index") ? that.data("index") : 0
            }), app.pswp.listen("close", function () {
                window.location.href.endsWith("gallery/") && history.back()
            }), app.pswp.init()) : app.pswp && app.pswp.close()
        }), e('.hero-carousel').each(function() {
		(that = $(this)).owlCarousel({
			dots: false,
			center: true,
			loop: true,
			margin: 10,
			nav: true,
			responsive: {
				0: {
					center: true,
					items: 1
				},
				500: {
					center: false,
					items: 2
				},
				768: {
					items: that.data('items')
				}
			},
			autoplay: true,
			autoplayTimeout: that.data('interval'),
			responsiveClass: true,
			autoplaySpeed: 1000
		})
	}), e("body:not(.single-apps-books) .generic-page :not(.carousel) > .owl-carousel:not(.ak-post-widgets, .hero-carousel)").each(function () {
            (that = e(this)).owlCarousel({
                dots: !1,
                loop: !0,
                margin: 10,
                nav: !0,
                responsive: {
                    0: {
                        items: 1
                    },
                    425: {
                        items: 2
                    },
                    768: {
                        items: that.data("items")
                    }
                },
                responsiveClass: !0
            }), (that = e(this)).parent().hasClass("flexible-content") || e(".owl-nav", that).appendTo(e(".owl-stage-outer", that))
        }), e(".recipe-categories-search ~ .owl-carousel, .ak-post-widgets.owl-carousel").each(function () {
            (that = e(this)).owlCarousel({
                dots: !1,
                center: !0,
                loop: !0,
                margin: 10,
                nav: !0,
                responsive: {
                    0: {
                        center: !0,
                        items: 1
                    },
                    500: {
                        center: !1,
                        items: 2
                    },
                    768: {
                        items: that.data("items")
                    }
                },
                responsiveClass: !0
            })
        }), e("body.single-apps-books .generic-page .owl-carousel:not(.ak-post-widgets)").each(function () {
            3 <= e(this).children().length && ((that = e(this)).owlCarousel({
                loop: !0,
                margin: 10,
                nav: !0,
                responsiveClass: !0
            }), e(".owl-nav", that = e(this)).appendTo(e(".owl-stage-outer", that)))
        }), e("body .generic-page .carousel > .owl-carousel").each(function () {
            (that = e(this)).owlCarousel({
                items: 1,
                loop: !0,
                margin: 10,
                nav: !0,
                responsiveClass: !0
            }), e(".owl-nav", that = e(this)).appendTo(e(".owl-stage-outer", that))
        }), e(".popup-youtube").magnificPopup({
            type: "iframe"
        }), e(".recipe-filter .accordion button").click(function () {
            return (that = e(this)).hasClass("all") && !that.hasClass("selected") ? (e("> button.selected", parent = that.parent()).removeClass("selected"), e('> input[type="checkbox"]', parent).attr("checked", !1), that.addClass("selected")) : !that.hasClass("all") && that.hasClass("selected") ? (that.removeClass("selected"), e('+ input[type="checkbox"]', that).attr("checked", !1), e('> input[type="checkbox"]:checked', parent = that.parent()).length || e("> button.all", parent).addClass("selected")) : that.hasClass("all") || that.hasClass("selected") || (that.addClass("selected"), e('+ input[type="checkbox"]', that).attr("checked", !0), e("> button.all", that.parent()).removeClass("selected")), !1
        }), e('.qtip2[title!=""]').qtip({
            position: {
                my: "bottom center",
                at: "top center"
            }
        }), e(".book-types select").change(function () {
            e("> .button", parent = (that = e(this)).closest(".book-types")).remove(), (link = (option = e("option:selected", that)).data("link")) && e('<a class="button" href="' + link + '">' + option.data("text") + "</a>").appendTo(parent)
        }), e(".search-title select").change(function () {
            window.location.href = e(this).val()
        }), e("select.gfield_select, .address_country > select").wrap('<div class="bootstrap-namespaced"></div>').selectpicker(), e(".save > a").click(function () {
            return e("#save-content-to-id").val(e(this).parent().data("save-id")), 1 < e("#save-content-to select option").length ? e.magnificPopup.open({
                items: {
                    src: e("#save-content-to").css("display", "inline-block")
                }
            }) : e("body").hasClass("logged-in") ? e.magnificPopup.open({
                items: {
                    src: e("#create-new-collection").css("display", "inline-block")
                }
            }) : e.magnificPopup.open({
                items: {
                    src: e("#login").css("display", "flex")
                }
            }), !1
        }), e("#create-new-collection form").submit(function () {
            return e.ajax((that = e(this)).attr("action"), {
                data: {
                    collection_name: e('input[type="text"]', that).val()
                },
                dataType: "json",
                method: "POST",
                success: function (t, a, n) {
                    if (e.magnificPopup.close(), (keys = Object.keys(t)).length) {
                        for (e("select > option:not([disabled])", that = e("#save-content-to")).remove(), i = 0; i < keys.length; i++) e("select", that).append('<option value="' + keys[i] + '">' + t[keys[i]].title + "</option>");
                        e("select", that).selectpicker("refresh"), e.magnificPopup.close(), e.magnificPopup.open({
                            items: {
                                src: that.css("display", "inline-block")
                            }
                        })
                    }
                }
            }), !1
        }), e('a[href="#create-new-collection"]').click(function () {
            return e.magnificPopup.close(), e.magnificPopup.open({
                items: {
                    src: e("#create-new-collection").css("display", "inline-block")
                }
            }), !1
        }), e("#delete-existing-collection").click(function () {
            return e.ajax("/delete-collection/", {
                data: {
                    collection_id: e(this).data("collection-id")
                },
                dataType: "json",
                method: "POST",
                success: function (e, t, a) {
                    window.location.href = "/your-saved/"
                }
            }), !1
        }), e("#edit-collection form").submit(function () {
            return e.ajax((that = e(this)).attr("action"), {
                data: {
                    collection_id: e('input[type="hidden"]', that).val(),
                    collection_name: e('input[type="text"]', that).val()
                },
                dataType: "json",
                method: "POST",
                success: function (e, t, a) {
                    window.location.reload()
                }
            }), !1
        }), e("#edit-existing-collection").click(function () {
            var t = e("#edit-collection");
            return e('input[name="collection_id"]', t).val(e(this).data("collection-id")), e.magnificPopup.close(), e.magnificPopup.open({
                items: {
                    src: t.css("display", "inline-block")
                }
            }), !1
        }), (article_jumps = e(".article-jump")).length) {
        var t = 1,
            a = [];
        if (e(".flexible-content > .wysiwyg-content > h2").each(function () {
                (that = e(this)).attr("id", "aj-h2-" + t), a.push(that.html()), t += 1
            }), a.length)
            for (i = 0; i < a.length; i++) article_jumps.append('<a class="button" href="#aj-h2-' + (i + 1) + '">' + a[i] + "</a>");
        else article_jumps.remove()
    }
    if (e(".video-wrapper").fitVids(), e('a[href$="#login"]').click(function () {
            return e(".wrapper").hasClass("show_menu") && app.menu.toggle(), e.magnificPopup.open({
                items: {
                    src: e("#login").css("display", "flex")
                }
            }), !1
        }), (resize_cards = function () {
            e(".post-widget:not(.large-post-widget) .rec_tit").each(function () {
                for (var t = e(this), a = parseInt(t.css("font-size")); 60 < t.outerHeight() && (t.css("font-size", --a + "px"), !(12 >= a)););
                //t.outerHeight(60)
            }), e(".post-widget:not(.large-post-widget) .bottom-cat").each(function () {
                for (var t = e(this), a = parseInt(t.css("font-size")); 36 < t.outerHeight() && (t.css("font-size", --a + "px"), !(10 >= a)););
                t.outerHeight(36)
            })
        })(), e.fn.almComplete = function () {
            resize_cards()
        }, e(".feel-inspired > .buttons > button").click(function () {
            (that = e(this)).hasClass("selected") || (e("> .selected", parent = that.parent()).removeClass("selected"), that.addClass("selected"), e("> :visible", recipes = parent.next()).hide(), e("> .recipes-" + that.data("panel"), recipes).show(), e("> :visible", products = recipes.next()).hide(), e("> .products-" + that.data("panel"), products).show())
        }), e(".flexible-content > .owl-carousel:not(.ak-post-widgets) .item h4").each(function () {
            $clamp(e(this)[0], {
                clamp: 2
            })
        }), e("body.page-template-saved-content .post-widget > .mfp-close").click(function () {
            return window.location.href = "/unsave/?save_id=" + e(this).data("post-id"), !1
        }), e("#responsive-menu .ubermenu-item-has-children > a").click(function () {
            return e(this).parent().addClass("active-panel").closest(".ubermenu-nav, .ubermenu-submenu").addClass("slide-away"), !1
        }), e("#responsive-menu .ubermenu-submenu").each(function () {
            (that = e(this)).is("ul") ? that.prepend('<li class="ubermenu-item"><a class="go-back ubermenu-target"><span class="ubermenu-target-text">&laquo; Back</span></a></li>') : that.prepend('<a class="go-back ubermenu-target"><span class="ubermenu-target-text">&laquo; Back</span></a>')
        }), e("#responsive-menu .go-back").click(function () {
            e(this).closest(".active-panel").removeClass("active-panel").closest(".ubermenu-nav, .ubermenu-submenu").removeClass("slide-away")
        }), e("#responsive-menu-icon").click(function () {
            return app.menu.toggle()
        }), e("#choice_2_10_0").click(function () {
            e("body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield").show()
        }), e("#choice_2_10_1").click(function () {
            e("body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-register .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield").hide()
        }), e("#choice_3_10_0").click(function () {
            e("body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield").show()
        }), e("#choice_3_10_1").click(function () {
            e("body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field, body.page-slug-edit-profile .gform_wrapper .top_label li.gfield.gf_right_half.gf_repeater_child_field+.gfield").hide()
        }), e(".ginput_container_radio").each(function () {
            (ul = e("> ul.gfield_radio", that = e(this))).hide(), e("> li", ul).each(function () {
                (input = e("> input", e(this))).is(":checked") ? (that.append('<button class="selected" type="button">' + e("> label", e(this)).html() + "</button>"), input[0].click()) : that.append('<button type="button">' + e("> label", e(this)).html() + "</button>")
            })
        }), e(".ginput_container_radio > button").click(function () {
            (that = e(this)).hasClass("selected") || ((buttons = e("> button", par = that.parent())).removeClass("selected"), that.addClass("selected"), e("li:eq(" + buttons.index(that) + ") > input", par)[0].click())
        }), (that = e(".my-children")).length) {
        var n = that.data("children");
        if (n.length > 1)
            for (i = 0; i < n.length - 1; i++) gfRepeater_repeatRepeater(3, 1);
        for (i = 1; i <= n.length; i++) e('input[name="input_12-1-' + i + '"]').val(n[i - 1])
    }
});
$(document).ready(function () {
    $(".widget_ak_subscription_widget").append('<div class="side-skids"></div>')
});


$(document).ready(function () {
    equalheight = function (container) {

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
        $(document).find(container).each(function () {

            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }

     $(window).load(function () {
        equalheight('.post-widget .post-widget-header');
    });

    $(window).resize(function () {
        equalheight('.post-widget .post-widget-header');
    });
	
	$.fn.almComplete = function(alm){
		setTimeout(function(){
			resize_cards();
			equalheight('.post-widget .post-widget-header');
		},500);
	};
	
  
});




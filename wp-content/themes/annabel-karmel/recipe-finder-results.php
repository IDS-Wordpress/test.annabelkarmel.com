<?php
/**
 * Template Name: Recipe Finder Results
 */

if ('POST' != $_SERVER['REQUEST_METHOD']) {
	wp_redirect(site_url('/recipes/'));
	exit;
}

get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php echo get_post_type_object('recipes')->labels->name; ?></h1>
				<?php get_template_part('parts/recipe-finder'); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
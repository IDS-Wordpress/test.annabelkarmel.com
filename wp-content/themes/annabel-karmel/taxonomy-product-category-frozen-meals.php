<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
            <?php
		 $terms = get_the_terms( $post->ID, 'product-category' );
if ( !empty( $terms ) ){
    // get the first term
    $term = array_shift( $terms );
    echo  '<h1>'.$term->name.'</h1>';
}
?>
				<?php if ($image = get_field('featured_image', 'product-category_' . get_queried_object()->term_id)): ?>
				<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['product-category-header']; ?>" title="<?php echo esc_attr($image['title']); ?>">
				<?php endif; ?>
				<?php if ($description = category_description()): ?>
				<div class="flexible-content">
					<div class="wysiwyg-content">
						<?php echo $description; ?>
					</div>
				</div>
				<?php endif; ?>
				<?php $the_query = new WP_Query([
					'post_type' => 'products',
					'posts_per_page' => -1,
					'tax_query' => [
						[
							'taxonomy' => get_queried_object()->taxonomy,
							'terms' => get_queried_object()->term_id
						]
					]
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="ak-post-widgets">
					<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
					<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php $the_query = new WP_Query([
					'post_type' => 'post',
					'posts_per_page' => -1,
					'tax_query' => [
						[
							'field' => 'slug',
							'taxonomy' => 'category',
							'terms' => 'frozen-toddler'
						]
					]
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<h2>Articles</h2>
				<div class="ak-post-widgets">
					<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
					<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<div class="frozen-sarah">
					<a href="<?php echo site_url('/nutritionist-sarah-almond-bushell/'); ?>">
						<img alt="" src="<?php echo get_template_directory_uri(); ?>/images/frozen-sarah.png">
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
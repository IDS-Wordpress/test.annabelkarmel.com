<?php if ($shop_items = get_sub_field('shop_items')): ?>
<div class="ak-post-widgets shop-list" data-num-per-row="3">
<?php
	foreach($shop_items as $shopItm):
	//echo "<pre>"; print_r($shopItm);  echo "</pre>"; 
	?>
	<div class="shop-item">
		<div class="post-widget" data-post-type="products">
			<a class="has-post-thumbnail" href="<?php echo ($shopItm['link_source']=='inbuilt')?get_permalink($shopItm['inbuild_link']):$shopItm['custom_url']; ?>">
				<div class="post-widget-header">
					<h4><?php echo $shopItm['shop_name']; ?></h4>
				</div>	
				<div class="post-widget-thumbnail">
					<img src="<?php echo $shopItm['shop_image']; ?>" class="attachment-verb-ak-post-widget-padded size-verb-ak-post-widget-padded wp-post-image" alt="">
				</div>
			</a>
		</div>
	</div>
	<?php
	endforeach;
?>
</div>
<?php endif; ?>
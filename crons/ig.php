<?php
require '../wp-load.php';

/*http://instagram.pixelunion.net/ */

$result = json_decode(file_get_contents('https://api.instagram.com/v1/users/212133815/media/recent/?access_token=212133815.1677ed0.2a15cb9d096f4bb8802ee6e49659c161'));
if(!empty($result->data)){
	foreach ($result->data as $post) {
		$the_query = new WP_Query([
			'meta_key' => 'id_str',
			'meta_value' => $post->id,
			'post_status' => 'publish',
			'post_type' => 'ig-posts',
			'posts_per_page' => -1
		]);			
		if (!$the_query->have_posts()) {
			$save_args = [
				'post_title' => $post->id,
				'post_content' => $post->caption->text,
				'post_date' => date('Y-m-d H:i:s', $post->created_time),
				'post_status' => 'publish',
				'post_type' => 'ig-posts'
			];
			$id = wp_insert_post($save_args);
			add_post_meta($id, 'id_str', $post->id);
			add_post_meta($id, 'ig_image', $post->images->low_resolution->url);
			add_post_meta($id, 'ig_link', $post->link);
			add_post_meta($id, 'ig_type', $post->type);			
		}
	}
	wp_reset_postdata();
}
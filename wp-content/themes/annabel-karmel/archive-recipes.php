<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<h1><?php echo get_post_type_object('recipes')->labels->name; ?></h1>
				<?php if (!isset($_POST['rf'])): ?>
				<div class="recipe-categories-search table">
					<?php if ($categories = get_terms([
						'parent' => 0,
						'taxonomy' => 'recipe-category'
					])): ?>
					<div class="cell">
						<span>Search recipes by category...</span>
						<div class="recipe-categories">
							<?php foreach ($categories as $category): ?>
							<a href="<?php echo get_term_link($category); ?>">
								<?php if ($image = get_field('image', $category->taxonomy . '_' . $category->term_id)): ?>
								<div class="box-image">
									<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['recipe-category-small']; ?>" title="<?php echo esc_attr($image['title']); ?>">
								</div>
								<?php endif; ?>
								<h4 class="skid">
									<span><?php echo $category->name; ?></span>
								</h4>
							</a>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="cell">
						<span>Search recipes by keyword...</span>
						<form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
							<input class="search-field" name="s" placeholder="Enter search..." type="search"><input type="submit" value="SEARCH">
							<input name="s_pt" type="hidden" value="recipes">
						</form>
						<a href="#recipe-filter">
							Or filter to find your ideal recipe<i class="fa fa-angle-down"></i>
						</a>
					</div>
				</div>
				<?php if (have_posts()): ?>
				<h2>Latest <?php echo get_post_type_object('recipes')->labels->name; ?></h2>
				<div class="ak-post-widgets owl-carousel owl-theme" data-items="3">
					<?php while (have_posts()): the_post(); ?>
					<div class="item">
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
					</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<?php get_template_part('parts/recipe-finder'); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
'use strict';
 
var gulp = require('gulp');

var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var jsValidate = require('gulp-jsvalidate');
var Promise = require('es6-promise').Promise;
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var stripbom = require('gulp-stripbom');
var uglify = require('gulp-uglify');

gulp.task('js', function() {
	return gulp.src([
		'./js/modernizr.js',
		'./bower_components/bootstrap/js/dropdown.js',
		'./bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
		'./bower_components/flexslider/jquery.flexslider-min.js',
		'./bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
		'./bower_components/photoswipe/dist/photoswipe.min.js',
		'./bower_components/photoswipe/dist/photoswipe-ui-default.min.js',
		'./bower_components/owl.carousel/dist/owl.carousel.min.js',
		'./bower_components/imagesloaded/imagesloaded.pkgd.min.js',
		'./bower_components/qtip2/jquery.qtip.min.js',
		'./bower_components/jquery.fitvids/jquery.fitvids.js',
		'./js/clamp.min.js',
		'./js/page.js',
		'./js/marley.js'
	])
	.pipe(jsValidate())
	.pipe(uglify())
	.pipe(concat('main.js'))
	.pipe(gulp.dest('./js'))
});

gulp.task('pswp', function() {
	return gulp.src([
		'./bower_components/photoswipe/dist/default-skin/default-skin.png',
		'./bower_components/photoswipe/dist/default-skin/default-skin.svg',
		'./bower_components/photoswipe/dist/default-skin/preloader.gif'
	])
	.pipe(gulp.dest('./css'));
});

gulp.task('sass', function() {
	return gulp.src([
		'./css/bootstrap-namespaced.css',
		'./bower_components/bootstrap-select/dist/css/bootstrap-select.min.css',
		'./bower_components/flexslider/flexslider.css',
		'./bower_components/magnific-popup/dist/magnific-popup.css',
		'./bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
		'./bower_components/owl.carousel/dist/assets/owl.theme.default.min.css',
		'./bower_components/photoswipe/dist/photoswipe.css',
		'./bower_components/photoswipe/dist/default-skin/default-skin.css',
		'./bower_components/qtip2/jquery.qtip.css',
		'./sass/**/*.scss'
	])
	.pipe(sourcemaps.init())
	.pipe(autoprefixer())
	.pipe(sass({
		outputStyle: 'compressed'
	}).on('error', sass.logError))
	.pipe(stripbom())
	.pipe(concat('main.css'))
	.pipe(gulp.dest('./css'));
});

gulp.task('watch', function () {
	gulp.watch([
		'./js/**/*.js',
		'!./js/main.js'
	], [
		'js'
	]);
	gulp.watch('./sass/**/*.scss', [
		'sass'
	]);
});
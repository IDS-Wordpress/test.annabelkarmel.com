<div class="competition-widget<?php if (($close = get_field('competition_close')) && time() >= DateTime::createFromFormat('d/m/Y g:i a', $close)->getTimestamp()) echo ' expired'; ?>">
	<div>
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('competitions'); ?>
		</a>
	</div>
	<div>
		<a href="<?php the_permalink(); ?>">
			<span><?php echo get_post_type_object('offers')->labels->name; ?></span>
        	<?php if (($close = get_field('competition_close')) && time() >= DateTime::createFromFormat('d/m/Y g:i a', $close)->getTimestamp()): ?>
        	<span>Expired</span>
        	<?php endif; ?>
			<h2><?php the_title(); ?></h2>
			<?php if (get_the_excerpt()): ?>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
			<?php endif; ?>
		</a>
		<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
			<a>SAVE</a>
		</div>
	</div>
</div>
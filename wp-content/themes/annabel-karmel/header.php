<!doctype html>
<?php error_reporting(0);?>
<html <?php language_attributes();  ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
<title>
<?php wp_title('|', true, 'right'); ?>
</title>
<link href="<?php echo site_url('/favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon">
<link href="http://gmpg.org/xfn/11" rel="profile">
<link href="<?php bloginfo('pingback_url'); ?>" rel="pingback">
<link rel="canonical" href="<?php echo get_permalink(); ?>" />
<?php wp_head(); ?>
<!-- Facebook Pixel Code -->
<script>
 !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)} 
;if(!f._fbq)f._fbq=n;
 n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1337309409664540');
 fbq('track', 'PageView');
 </script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1337309409664540&ev=PageView&noscript=1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!--script>(function(e){var t=document,n=t.createElement("script");n.async=!0,n.defer=!0,n.src=e,t.getElementsByTagName("head")[0].appendChild(n)})("//c.vepxl1.net/4-19718.js?id=19718&m=4")</script-->
<script type="text/javascript">
setTimeout(function()
{var a=document.createElement("script"); var b=document.getElementsByTagName("script")[0]; a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0069/2490.js?"+Math.floor(new Date().getTime()/3600000); a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}

, 1);
</script>
</head>
<?php if (is_singular('products') && has_term('frozen-meals', 'product-category')): ?>
<body <?php body_class('frozen-meals'); ?>>
<?php else: ?>
<body <?php body_class(); ?>>
<?php endif; ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1104764539603478";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
<?php
		/* echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('page_skin_adzone') ?: 12603), 'post', false, ICL_LANGUAGE_CODE) . ']'); */
		if(get_field('page_skin_adzone'))
		{
			echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(get_field('page_skin_adzone'), 'post', false, ICL_LANGUAGE_CODE) . ']');
		}
		?>
<?php if(ICL_LANGUAGE_CODE=='nl'){ ?>
<aside id="responsive-menu">
  <?php if (false === ($header_menu = get_transient('ak_header_menu_' . ICL_LANGUAGE_CODE))) set_transient('ak_header_menu_' . ICL_LANGUAGE_CODE, ($header_menu = wp_nav_menu([
				'container' => '',
				'echo' => false,
				'theme_location' => 'header-menu'
			])), DAY_IN_SECONDS); ?>
  <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?> </aside>
<?php } else { ?>
<aside id="responsive-menu">
  <form action="<?php echo home_url('/'); ?>" class="search-form" method="get" role="search">
    <input class="search-field" name="s" placeholder="Search our website..." type="search">
  </form>
  <div class="top-bar"> <a href="<?php echo get_post_type_archive_link('competitions'); ?>">Competitions</a> <a class="icon-heart" href="<?php echo site_url('/your-saved/'); ?>">Your Saved</a>
    <?php if (is_user_logged_in()): ?>
    <a href="<?php echo site_url('/logout/'); ?>">Logout</a>
    <?php else: ?>
    <a href="#login">Login / Register</a>
    <?php endif; ?>
  </div>
  <?php if (false === ($header_menu = get_transient('ak_header_menu_' . ICL_LANGUAGE_CODE))) set_transient('ak_header_menu_' . ICL_LANGUAGE_CODE, ($header_menu = wp_nav_menu([
				'container' => '',
				'echo' => false,
				'theme_location' => 'header-menu'
			])), DAY_IN_SECONDS); ?>
  <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?> </aside>
<?php } ?>
<div class="wrapper">
<div class="header-container">
  <style>.contact-us-nl {position:absolute; right:20px; top:4px;}</style>
  <div class="app_btn" id="gplay"  style="display:none;"> <a  href="javascript:void();"class="app_close"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" alt="Google Play"></a>
    <div class="app_btn-inner"> <span class="app-logo"><?php if( get_field("logo", 'option') ): ?><img src="<?php the_field("logo", 'option'); ?>" alt=""><?php else:?><img src="<?php echo get_template_directory_uri(); ?>/images/app-logo.png" alt=""><?php endif; ?></span>
      <div class="app_btn-inner-text">
        <p class="app_msg">
          <?php if( get_field("title", 'option') ): ?><?php the_field("title", 'option'); ?><?php endif; ?>
        </p>
        
        <p class="star-main">		
        	<?php
            if( get_field("google_rating", 'option') ): 
                $starRating = get_field("google_rating", 'option');				
                $lStar = floor($starRating);
                $point = (float)$starRating - (float)$lStar;
				$balanceStar = 5 - (float)$starRating;
				$blankStar = $balanceStar - $point;
                
                if( $starRating<5 )
                {
					for($i=1; $i<=$lStar; $i++)
					{
						echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
					}
                    if($point > 0 && $point < 1)
                    {
                        echo '<i class="fa fa-star-half-o" aria-hidden="true"></i>&nbsp;';
                    }
					for($j=1; $j<=$blankStar; $j++)
					{
						echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
					}
                }		 
            endif;
            ?>
        
         <a href="<?php if( get_field("download_rul", 'option') ): the_field("download_rul", 'option');   endif; ?>">
           <?php if( get_field("download_caption_text", 'option') ): the_field("download_caption_text", 'option'); endif; ?>
          </a> </p>

      </div>
    </div>
  </div>
  
  <div class="app_btn" id="iphone" style="display:none;"> <a  href="javascript:void();"class="app_close"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" alt="Google Play"></a>
    <div class="app_btn-inner"> <span class="app-logo"><?php if( get_field("logo_iphone_logo", 'option') ): ?><img src="<?php the_field("logo_iphone_logo", 'option'); ?>" alt=""><?php else:?><img src="<?php echo get_template_directory_uri(); ?>/images/app-logo.png" alt=""><?php endif; ?></span>
      <div class="app_btn-inner-text">
        <p class="app_msg">
           <?php if( get_field("title_iphone", 'option') ): ?><?php the_field("title_iphone", 'option'); ?><?php endif; ?>
        </p>
        
        <p class="star-main">
			<?php
            if( get_field("google_rating_iphone", 'option') ): 
                $starRating = get_field("google_rating_iphone", 'option');				
                $lStar = floor($starRating);
                $point = (float)$starRating - (float)$lStar;
				$balanceStar = 5 - (float)$starRating;
				$blankStar = $balanceStar - $point;
                
                if( $starRating<5 )
                {
					for($i=1; $i<=$lStar; $i++)
					{
						echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
					}
                    if($point > 0 && $point < 1)
                    {
                        echo '<i class="fa fa-star-half-o" aria-hidden="true"></i>&nbsp;';
                    }
					for($j=1; $j<=$blankStar; $j++)
					{
						echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
					}
                }		 
            endif;
            ?>
         
        <a href="<?php if( get_field("download_rul_iphone", 'option') ): the_field("download_rul_iphone", 'option'); endif; ?>">
			<?php if( get_field("download_caption_iphone", 'option') ): the_field("download_caption_iphone", 'option'); endif; ?>
        </a>
		</p>
      </div>
    </div>
  </div>
  <header id="header">
    <div class="top-bar skid">
      <div class="container"> <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu(['container' => '','echo' => false,'theme_location' => 'top-bar-menu'])); ?>
        <?php if(ICL_LANGUAGE_CODE=='nl'){ ?>
        <?php } else { ?>
        <form action="<?php echo home_url(); ?>" class="search-form" id="search-form" method="get">
          <input class="search-field" name="s" id="s" placeholder="Search our website..." type="search">
        </form>
        <?php } ?>
      </div>
    </div>
    <?php if(ICL_LANGUAGE_CODE=='nl'){ ?>
    <div class="container"> <a href="https://www.annabelkarmel.com/nl/hartelijk-welkom-bij-annabel-karmel/"><img width="390" height="261" src="https://www.annabelkarmel.com/wp-content/uploads/2016/08/logo.png" class="custom-logo" alt="" itemprop="logo"></a>
      <?php if (1 == get_current_user_id()): ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
							'container' => '',
							'echo' => false,
							'theme_location' => 'header-menu'
						])); ?>
      <?php else: ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
      <?php endif; ?>
      <div id="responsive-menu-icon">
        <button class="lines-button" type="button"> <span class="lines"></span> </button>
      </div>
    </div>
    <?php } else { ?>
    <div class="container">
      <?php the_custom_logo(); ?>
      <?php if (1 == get_current_user_id()): ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), wp_nav_menu([
							'container' => '',
							'echo' => false,
							'theme_location' => 'header-menu'
						])); ?>
      <?php else: ?>
      <?php echo str_replace('{{user_first_name}}', (wp_get_current_user()->user_firstname ?: 'Your Profile'), $header_menu); ?>
      <?php endif; ?>
      <div id="responsive-menu-icon">
        <button class="lines-button" type="button"> <span class="lines"></span> </button>
      </div>
    </div>
    <?php } ?>
  </header>
</div>
<div id="outer-content">
<?php if (!is_user_logged_in() && ((is_string($enable_in = get_field('subscribe_banner_enable_in', 'option')) && 'header' == $enable_in) || (is_array($enable_in) && in_array('header', $enable_in)))): ?>
<div class="subscribe skid">
  <div class="container"> <a href="<?php echo site_url('/register/'); ?>"> <img alt="Join the AK Club" src="<?php echo get_template_directory_uri(); ?>/images/join-the-ak-club.png"> </a> </div>
</div>
<?php endif; ?>
<?php
namespace Verb;
// add_action('wp', 'custom_maybe_activate_user', 9);
// function custom_maybe_activate_user() {

//     $template_path = STYLESHEETPATH . '/gfur-activate-template/activate.php';
//     $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';

//     if( ! file_exists( $template_path ) || ! $is_activate_page  )
//         return;

//     require_once( $template_path );

//     exit();
// }

class Annabel_Karmel {

	protected $algorithms;
	
	public function __construct() {
		$this->algorithms = [
			32 => 'md5',
			40 => 'sha1',
			64 => 'sha256',
			128 => 'sha512'
		];
		
		add_action('admin_init', [
			$this,
			'admin_init'
		]);
		/*add_action('gform_activate_user', [
			$this,
			'gform_activate_user'
		], 10, 3);*/
		add_action( 'gform_after_save_form', [
			$this,
			'gform_after_save_form'
		], 10, 2);
		add_action('gform_pre_submission_5', [
			$this,
			'gform_pre_submission_5'
		]);
		add_filter('gform_pre_submission_6', [
			$this,
			'gform_pre_submission_6'
		]);
		add_action('gform_post_submission', [
			$this,
			'gform_post_submission'
		], 10, 2);
		add_action('gform_user_updated', [
			$this,
			'gform_user_updated'
		], 10, 3);
		add_action('init', [
			$this,
			'cpt'
		]);
		add_action('init', [
			$this,
			'register_menus'
		]);
		add_action('init', [
			$this,
			'wp_doin_verify_user_key'
		], 999);
		add_action('loop_start', [
			$this,
			'loop_start'
		]);
		add_action('restrict_manage_posts', [
			$this,
			'restrict_manage_posts'
		]);
		add_action('widgets_init', [
			$this,
			'widgets_init'
		]); 
		add_action('wp_enqueue_scripts', [
			$this,
			'wp_enqueue_scripts'
		]);

		add_action('wp_login_failed', [
			$this,
			'wp_login_failed'
		]);
		add_action('wp_update_nav_menu', [
			$this,
			'wp_update_nav_menu'
		]);
		
		/*add_filter('authenticate', [
			$this,
			'authenticate'
		], 30, 3);*/
		add_filter('body_class', [
			$this,
			'body_class'
		]);
		add_filter('check_password', [
			$this,
			'check_password'
		], 10, 4);
		add_filter('excerpt_length', [
			$this,
			'excerpt_length'
		]);
		add_filter('get_the_excerpt', [
			$this,
			'get_the_excerpt'
		]);
		add_filter('gform_author_dropdown_args', [
			$this,
			'gform_author_dropdown_args'
		]);
		add_filter('gform_countries', [
			$this,
			'gform_countries'
		]);
		add_filter('gform_disable_post_creation_11', [
			$this, 
			'disable_gf_post_creation'
		] );
		add_filter('gform_field_validation_5', [
			$this,
			'gform_field_validation_5'
		], 10, 4);
		add_filter('gform_field_value', [
			$this,
			'gform_field_value'
		], 10, 3);
		add_filter('gform_media_upload_path_11', [
			$this,
			'change_gf_media_upload_location'
		], 10, 2);
		add_filter('gform_pre_render_3', [
			$this,
			'gform_pre_render_3'
		]);
		add_filter('img_caption_shortcode', [
			$this,
			'img_caption_shortcode'
		], 10, 3);
		/*add_filter('login_redirect', [
			$this,
			'login_redirect'
		]);*/
		add_filter('lostpassword_url', [
			$this,
			'lostpassword_url'
		]);
		add_filter('pre_get_posts', [
			$this,
			'pre_get_posts'
		]);
		add_filter('redirection_role', [
			$this,
			'redirection_role'
		]);
		add_filter('rewrite_rules_array', [
			$this,
			'rewrite_rules_array'
		], 999);
		add_filter('send_password_change_email', '__return_false');
		add_filter('show_admin_bar', [
			$this,
			'show_admin_bar'
		]);
		add_filter('tiny_mce_before_init', [
			$this,
			'tiny_mce_before_init'
		]);
		add_filter('views_edit-recipes', [
			$this,
			'views_edit_recipes'
		]);
		add_filter('wp_pro_ads_banner_limit_categories', [
			$this,
			'wp_pro_ads_banner_limit_categories'
		]);
		add_filter('wpseo_metabox_prio', [
			$this,
			'wpseo_metabox_prio'
		]);
		add_action('restrict_manage_posts', [$this,'tsm_filter_post_type_by_taxonomy']);
		add_filter('parse_query', [$this,'tsm_convert_id_to_term_in_query']);
		
		add_action("wp_ajax_login_validation", [$this,'login_validation']);
		add_action("wp_ajax_nopriv_login_validation", [$this,'login_validation']);
		
		//add_action('admin_footer', [$this,'custom_admin_js_edit_user']);


		add_action("wp_ajax_register_template_post_data", [$this,'register_template_post_data']);
		add_action("wp_ajax_nopriv_register_template_post_data", [$this,'register_template_post_data']);


		add_action("wp_ajax_update_template_post_data", [$this,'update_template_post_data']);
		add_action("wp_ajax_nopriv_update_template_post_data", [$this,'update_template_post_data']);

		add_action( 'wp_ajax_check_email_exists',[$this,'check_email_exists']);
		add_action( 'wp_ajax_nopriv_check_email_exists', [$this,'check_email_exists']);

		add_action("wp_trash_post", [$this,'manage_deleted_post_info']);
		add_action("delete_user", [$this,'delete_gf_user_entry'], 10);
		add_action("admin_init", [$this,'allow_admin_area_to_admins_only']);
		//add_filter('oembed_result',[$this,'Oembed_youtube_no_title'],10,3);
		add_filter('oembed_fetch_url', [$this,'wpse5362_autoplay_youtube_oembed'], 10, 3);
		add_filter('allowed_http_origins', [$this,'add_allowed_origins']);
		$this->add_image_sizes();
		$this->add_shortcodes();
		$this->add_theme_options();
		$this->add_theme_support();
	}
	public function add_allowed_origins($origins) {
		$origins[] = home_url();
		return $origins;
	}
	 
	public function wpse5362_autoplay_youtube_oembed( $provider, $url, $args ) {
		if (strpos($provider, 'youtube')!==FALSE) {
			$provider = add_query_arg('autoplay', 1, $provider);
		}

		return $provider;
	}

	public function Oembed_youtube_no_title($html,$url,$args){
		$url_string = parse_url($url, PHP_URL_QUERY);
		parse_str($url_string, $id);
		if (isset($id['v'])) {
			return '<iframe width="'.$args['width'].'" height="'.$args['height'].'" src="http://www.youtube.com/embed/'.$id['v'].'?autoplay=1&vq=hd1080&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
		}
		return $html;
	}
	public function allow_admin_area_to_admins_only() {
		  if( defined('DOING_AJAX') && DOING_AJAX ) {
				//Allow ajax calls
				return;
		  }
		  $user = wp_get_current_user();
		  if( empty( $user ) || ( !in_array( "administrator", (array) $user->roles ) && !in_array( "verb", (array) $user->roles )) )  {
			   //Redirect to main page if no user or if the user has no "administrator" role assigned
			   wp_redirect( get_site_url( ) );
			   exit();
		  }
	}
	public function delete_gf_user_entry( $user_id ) {
		global $wpdb;
        $user_obj = get_userdata( $user_id );
        $username = $user_obj->user_email;
		$meta = $wpdb->get_var($wpdb->prepare('select meta from '.$wpdb->prefix.'signups where user_login = %s or user_email = %s', $username, $username));
		if ($meta) {
			$meta = unserialize($meta);			
			$lead_id = $meta['lead_id'];
			
			//Delete from detail long
			$sql = $wpdb->prepare("DELETE FROM ".$wpdb->prefix ."rg_lead_detail_long
									WHERE lead_detail_id IN(
										SELECT id FROM ".$wpdb->prefix ."rg_lead_detail WHERE lead_id=%d
									)", $lead_id);
			$wpdb->query($sql);
		 
			//Delete from lead details
			$sql = $wpdb->prepare("DELETE FROM ".$wpdb->prefix ."rg_lead_detail WHERE lead_id=%d", $lead_id);
			$wpdb->query($sql);
		 
			//Delete from lead notes
			$sql = $wpdb->prepare("DELETE FROM ".$wpdb->prefix ."rg_lead_notes WHERE lead_id=%d", $lead_id);
			$wpdb->query($sql);
		 
			//Delete from lead
			$sql = $wpdb->prepare("DELETE FROM ".$wpdb->prefix ."rg_lead WHERE id=%d", $lead_id);
			$wpdb->query($sql);			
		}
		//Delete Signups table entry
		$signups_sql = $wpdb->prepare("DELETE FROM ".$wpdb->prefix ."signups WHERE user_login = %s OR user_email = %s",$username, $username);
		$wpdb->query($signups_sql);
	}
	public function manage_deleted_post_info( $pid )
	{
		global $wpdb;
		$wpdb->insert(
		$wpdb->prefix.'posts_deleted', 
			array( 
				'post_id' => $pid, 
				'post_type' => get_post_type($pid) ,
				'deleted_date'=>date("Y-m-d H:i:s")
			) 
		);
	}
	

	// Update user data by ajax
	public function update_template_post_data()
	{
		//print_r($_POST['formData']);
		global $wpdb;
		parse_str($_POST['formData'], $searcharray);
		//echo '<pre>'; print_r($searcharray); echo '</pre>';
		$user_id     = $searcharray['user_id'];
		$full_name   = $searcharray['full_name'];
		$last_name   = $searcharray['last_name']; 
		$about_email = $searcharray['about_email'];
		$password    = $searcharray['pwd'];
		$about_iam   = $searcharray['about_iam'];
		$house_no    = $searcharray['house_no'];
		$street      = $searcharray['street'];
		$city        = $searcharray['city'];
		$post_code   = $searcharray['post_code'];
		$subscribe   = (!empty($searcharray['subscribe']) && $searcharray['subscribe']==1)?1:0;
		$agree       = $searcharray['agree'];
		$country     = $searcharray['country'];

		$little_name = $searcharray['little_name'];
		$little_dob  = $searcharray['little_dob'];
		//echo '<pre>'; print_r($little_name); echo '</pre>';
		//echo '<pre>'; print_r($little_dob); echo '</pre>';
		//die;
		$name= implode (",", $little_name);
		$dob= implode (",", $little_dob);


		if(!empty($user_id))
		{ 
			update_user_meta( $user_id, 'first_name', $full_name);
			update_user_meta( $user_id, 'last_name', $last_name);
			if(!empty($password)){wp_set_password( $password, $user_id );}
			update_user_meta( $user_id, 'address_line_1', $house_no);
			update_user_meta( $user_id, 'postcode', $post_code);
			update_user_meta( $user_id, 'country',$country);
			update_user_meta( $user_id, 'is_pushed_to_mailchimp', $subscribe);
			
			/********/
			update_user_meta( $user_id, 'children', count($little_name));
			update_user_meta( $user_id, '_children', 'field_57a8cf0cc1070');

			if(!empty(count($little_name))){
				update_user_meta( $user_id, 'do_you_have_any_children', 1);
			}
			$num = 0;
			foreach($little_name as $little_name1){
				update_user_meta( $user_id, 'children_'.$num.'_childs_name', $little_name1);
				update_user_meta( $user_id, '_children_'.$num.'_childs_name', 'field_5b4d895c338d9');
				$num++;
			}
			$num = 0;
			foreach($little_dob as $little_dob1){
				if(!empty($little_dob1)){
					$little_date_check = date('Ymd', strtotime($little_dob1));
				}else{
					$little_date_check = ""; 
				}
				update_user_meta( $user_id, 'children_'.$num.'_childs_birthday', $little_date_check);
				update_user_meta( $user_id, '_children_'.$num.'_childs_birthday', 'field_57a8cf17c1071');
				$num++;
			}
			
			$user = get_userdata($user_id);
			$user_email = $user->user_email;
			// for simplicity, lets assume that user has typed their first and last name when they sign up
			$user_full_name =  $full_name;
			
			$user_data = get_userdata($user_id);
			 
			$to = $user_data->user_email;
			$country_name    = get_user_meta($user_id,'country',true);
			$user_first_name = get_user_meta($user_id,'first_name',true);
			$user_last_name  = get_user_meta($user_id,'last_name',true);
			$user_subscribe  = get_user_meta($user_id,'is_pushed_to_mailchimp',true); 
			$user_post_code       = get_user_meta($user_id,'postcode',true); 
			$subject = "Update Confirmation (Annabel Karmel)!";
			$body = '
                <h2>Dear ' . $user_first_name . ' '.$user_last_name.',</h2></br>
                <p>Thank you for update</p>
                <p>Please go ahead and navigate around your account.</p>
                <p>Let me know if you have further questions, I am here to help.</p>
                <p>Enjoy the rest of your day!</p>
                <p>Kind Regards,</p>
                <p>AK Team</p>
			';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			/*if (wp_mail($to, $subject, $body, $headers)) {
				echo "email has been successfully sent to user whose email is";
			}else{
				echo "email failed to sent to user whose email is";
			}*/

			$apikey = get_field('api_key', 'option'); 
			$united_kingdom_list_id = get_field('united_kingdom', 'option');
			$united_states_of_america_list_id = get_field('united_states_of_america', 'option');
			$australia_list_id = get_field('australia', 'option');
			$international_list_id = get_field('international', 'option');
			//$apikey = 'c6a36793022d1d7b6dd2525022a5fa36-us15'; 
			//$apikey = 'd7cee6bca3923b77b745274963192d22-us19';
			//$listID = '1ed11cdd55';
			if($country_name=='United Kingdom') { $listID = $united_kingdom_list_id; }
			elseif($country_name=='United States of America'){ $listID = $united_states_of_america_list_id; }
			elseif($country_name=='Australia'){ $listID = $australia_list_id; }
			else{  $listID = $international_list_id; }

			$memberID = md5(strtolower($user_data->user_email));

			$dataCenter = substr($apikey,strpos($apikey,'-')+1);
			$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
			if($subscribe)
			{	
				$data = array(
				        'apikey'        => $apikey,
				        'email_address' => $user_data->user_email
				        );
		        $json_data = json_encode($data);
		    	$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
				$result = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

			    $json = json_decode($result);
			    $mailchimp_status =  $json->{'status'};

				if($mailchimp_status!='subscribed')
				{		
					$json = json_encode([
								'email_address' => $user_data->user_email,
								'status'        => 'subscribed',
								'merge_fields'  => [
									'FNAME'     => $user_full_name,
									'LNAME'     => $user_last_name,
									'ADDRESS' =>  array(
										'addr1' => '',
										'addr2' => '',
										'city' => '',
										'state' => '',
										'zip' => $post_code,
										'country' => $country 
									)
								]	
							]);

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
					curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					$result = curl_exec($ch);
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);
					$json = json_decode($result);
					$subscribe_status =  $json->{'status'}; 
				
				} else{$subscribe_status='subscribed';}
			}
			else
			{
				$data = array(
				        'apikey'        => $apikey,
				        'email_address' => $user_data->user_email
				        );
		        $json = json_encode($data);
		    	$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				$result = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

			    $json = json_decode($result);
			    $mailchimp_status =  $json->{'status'};

				if($mailchimp_status!='unsubscribed' && $mailchimp_status!=400 && $mailchimp_status!=404)
				{
					/********** insert subscribe status to maiklchimp**********/ 
					$json = json_encode([
						'email_address' => $user_data->user_email,
						'status'        => 'unsubscribed',
						'merge_fields'  => [
								'FNAME'     => $user_full_name,
								'LNAME'     => $user_last_name,
								'ADDRESS' =>  array(
									'addr1' => '',
									'addr2' => '',
									'city' => '',
									'state' => '',
									'zip' => $post_code,
									'country' => $country 
								)
							]
						]);
	          
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
					curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					$result = curl_exec($ch);
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);
					$json = json_decode($result);
			    	$subscribe_status =  $json->{'status'}; 
				}
			}
		}
		//echo (!empty($subscribe_status))?$subscribe_status:0;
		//echo json_encode($subscribe_status);
		/*if(!empty($subscribe_status)){
			$response = array('status'=>1);
		}
		else{
			$response = array('status'=>0);
		}*/
		$response = array('status'=>1);
		echo json_encode($response);
		die();
	}

	public function register_template_post_data(){
		//print_r($_POST['formData']);
		global $wpdb;
		parse_str($_POST['formData'], $searcharray);
		//echo '<pre>'; print_r($searcharray); echo '</pre>';
		$full_name   = $searcharray['full_name'];
		$last_name   = $searcharray['last_name'];
		$about_email = $searcharray['about_email'];
		$password    = $searcharray['pwd'];
		$about_iam   = $searcharray['about_iam'];
		$house_no    = $searcharray['house_no'];
		$street      = $searcharray['street'];
		$city        = $searcharray['city'];
		$post_code   = $searcharray['post_code'];
		$subscribe   = $searcharray['subscribe'];
		$agree       = $searcharray['agree'];
		$country     = $searcharray['country'];

		$little_name = $searcharray['little_name'];
		$little_dob  = $searcharray['little_dob'];
		//echo '<pre>'; print_r($little_name); echo '</pre>';
		//echo '<pre>'; print_r($little_dob); echo '</pre>';
		//die;
		$name= implode (",", $little_name);
		$dob= implode (",", $little_dob);

		$userData = array(
					    'user_login'  => $about_email,
					    'user_email'  => $about_email,
					    'user_pass'   => $password,
					    'display_name'=> $full_name,
					    'user_registered' =>date('Y-m-d H:i:s'),
					);

		$user_id = wp_insert_user($userData);
		/*************************/
		if(! is_wp_error( $user_id )){ 
			update_user_meta( $user_id, 'first_name', $full_name);
			update_user_meta( $user_id, 'last_name', $last_name);
			update_user_meta( $user_id, 'nickname', $full_name);
			update_user_meta( $user_id, 'email', $about_email);
			update_user_meta( $user_id, 'about_iam', $about_iam);
			update_user_meta( $user_id, 'address_line_1', $house_no);
			//update_user_meta( $user_id, 'house_no', $house_no);
			update_user_meta( $user_id, 'address_line_2', $street);
			//update_user_meta( $user_id, 'street', $street);
			//update_user_meta( $user_id, 'city', $city);
			update_user_meta( $user_id, 'postcode', $post_code);
			update_user_meta( $user_id, 'country',$country);
			update_user_meta( $user_id, 'agree', $agree);
			update_user_meta( $user_id, 'is_pushed_to_mailchimp', $subscribe);
			
			/********/
			update_user_meta( $user_id, 'children', count($little_name));
			update_user_meta( $user_id, '_children', 'field_57a8cf0cc1070');
			if(!empty(count($little_name))){
				update_user_meta( $user_id, 'do_you_have_any_children', 1);
			}
			$num = 0;
			foreach($little_name as $little_name1){
				update_user_meta( $user_id, 'children_'.$num.'_childs_name', $little_name1);
				update_user_meta( $user_id, '_children_'.$num.'_childs_name', 'field_5b4d895c338d9');
				$num++;
			}
			$num = 0;
			foreach($little_dob as $little_dob1){
				if(!empty($little_dob1)){
					$little_date_check = date('Ymd', strtotime($little_dob1));
				}else{
					$little_date_check = ""; 
				}
				update_user_meta( $user_id, 'children_'.$num.'_childs_birthday', $little_date_check);
				update_user_meta( $user_id, '_children_'.$num.'_childs_birthday', 'field_57a8cf17c1071');
				$num++;
			}
			//update_user_meta( $user_id,'little_name',$name);
			//update_user_meta( $user_id,'little_dob',$dob);
			/*****/
			$user = get_userdata($user_id);
			$user_email = $user->user_email;
			// for simplicity, lets assume that user has typed their first and last name when they sign up
			$user_full_name =  $full_name;
			
			$user_data = get_userdata($user_id);
			 
			$to = $user_data->user_email;
			$country_name    = get_user_meta($user_id,'country',true);
			$user_first_name = get_user_meta($user_id,'first_name',true);
			$user_last_name  = get_user_meta($user_id,'last_name',true);
			$user_subscribe  = get_user_meta($user_id,'is_pushed_to_mailchimp',true);
			$subject = "Registration Confirmation (Annabel Karmel)!";
			$body = '
                <h2>Dear ' . $user_first_name . ' '.$user_last_name.',</h2></br>
                <p>Thank you for joining our site. Your account is now active.</p>
                <p>Please go ahead and navigate around your account.</p>
                <p>Let me know if you have further questions, I am here to help.</p>
                <p>Enjoy the rest of your day!</p>
                <p>Kind Regards,</p>
                <p>AK Team</p>
			';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			if (wp_mail($to, $subject, $body, $headers)) {
				//echo "email has been successfully sent to user whose email is";
			}else{
				//echo "email failed to sent to user whose email is";
			}
			if($user_subscribe==1){
				/*$apiKey = 'c6a36793022d1d7b6dd2525022a5fa36-us15'; 
				if($country_name=='United Kingdom') { $listID = '1ed11cdd55'; }
				elseif($country_name=='United States of America'){ $listID = 'f4996b1f7b'; }
				elseif($country_name=='Australia'){ $listID = 'd7b9de9f23'; }
				else{  $listID = '415ff3e853'; }*/
				
				$apiKey = get_field('api_key', 'option'); 
				$united_kingdom_list_id = get_field('united_kingdom', 'option');
				$united_states_of_america_list_id = get_field('united_states_of_america', 'option');
				$australia_list_id = get_field('australia', 'option');
				$international_list_id = get_field('international', 'option');
				//$apikey = 'c6a36793022d1d7b6dd2525022a5fa36-us15'; 
				//$apikey = 'd7cee6bca3923b77b745274963192d22-us19';
				//$listID = '1ed11cdd55';
				if($country_name=='United Kingdom') { $listID = $united_kingdom_list_id; }
				elseif($country_name=='United States of America'){ $listID = $united_states_of_america_list_id; }
				elseif($country_name=='Australia'){ $listID = $australia_list_id; }
				else{  $listID = $international_list_id; }
				

				$memberID = md5(strtolower($user_data->user_email));
				$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
				$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
          
         
				$json = json_encode([
					'email_address' => $user_data->user_email,
					'status'        => 'subscribed',
					'merge_fields'  => [
								'FNAME'     => $user_full_name,
								'LNAME'     => $user_last_name,
								'ADDRESS' =>  array(
									'addr1' => 'n/a',
									'city' => 'n/a',
									'state' => 'n/a',
									'zip' => $post_code,
									'country' => $country_name
								)
							]
					]);
          
          
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				$result = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
			}
			
			$user_data = get_userdata($user_id);
			$u_email = $user_data->user_email;
			$u_pwd   = $password;
			
			$creds = array();
			$creds['user_login'] = $u_email;
			$creds['user_password'] = $u_pwd;
			$creds['remember'] = true;
			
			$user_login_data = wp_signon( $creds, false );
			if (is_wp_error($user_login_data) ){ 
				$user_login_data->get_error_message();
			} else{
				$userID = $user_login_data->ID;
				wp_set_current_user( $userID, $u_email);
				wp_set_auth_cookie( $userID, true, false );
				do_action( 'wp_signon', $u_email);
				$response = array('status'=>1);
			}
		}
		
		/*************************/	 
		echo json_encode($response);
		die();
	}

	
	
	
	public	function check_email_exists(){
		
		$email_exists = email_exists($_POST['about_email']);
		if($email_exists > 0){
			//echo 'exists';
		//echo json_encode(array('valid' => true), JSON_FORCE_OBJECT);
			echo json_encode(array(
				'valid' => false,
			));
		}
		else{
			//echo 'not-exists';
			echo json_encode(array(
				'valid' => true,
			));
		}
		die();
	}


	public function login_validation()
	{
		$tok = base64_decode($_REQUEST['tok']);
		list($username,$password) = explode(':',$tok);
		$user = get_user_by( 'login', $username );
	
		if (!username_exists( $username ) )
		{
			echo "We do not recognise this username/email. Please check your entry or join the AK club if you have not registered.";
		}
        else if (username_exists( $username ) && !wp_check_password( $password, $user->data->user_pass, $user->ID) )
		{
			echo "Password is not correct for user '".$username."'. Please enter a valid password.";
		}
		else
		{
			$creds = array();
			$creds['user_login'] = $username;
			$creds['user_password'] = $password;
			$creds['remember'] = true;
			$user = wp_signon( $creds, false );
			if ( is_wp_error($user) )
			{
				echo $user->get_error_message();
			}
			else
			{
				echo 'success';
			}			
		}           
		die;
	}
	public function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'recipes'; // change to your post type
	$taxonomy  = 'recipe-filter'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => false,
			'hide_empty'      => true,
			'hierarchical'    => true,
		));
	};
	}

	public function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'recipes'; // change to your post type
	$taxonomy  = 'recipe-filter'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
	}
	protected function add_image_sizes() {
		global $_wp_additional_image_sizes;
		
		add_image_size('boxes', 275, 200, true);
		add_image_size('boxes-with-image', 565, 425, true);
		add_image_size('competitions', 240, 160, true);
		add_image_size('offers', 240, 160, true);
		add_image_size('gallery-carousel', 220, 185, true);
		add_image_size('gallery-grid', 375, 270, true);
		add_image_size('gallery-large', 1800, 1800);
		add_image_size('gallery-thumbnail', 240, 240, true);
		add_image_size('gallery-slide', 780, 200, true);
		add_image_size('header-for-overlaid-wysiwyg', 1160);
		add_image_size('icon', 150);
		add_image_size('illustration', 200, 400);
		add_image_size('list-icon', 39, 39);
		add_image_size('product-category-header', 1160);
		add_image_size('product-icon', 60, 60);
		add_image_size('product-lifestyle', 420, 400, true);
		add_image_size('recipe-category-medium', 250, 150, true);
		add_image_size('recipe-category-mediums', 250, 90, true);
		add_image_size('recipe-category-small', 200, 150, true);
		add_image_size('single-header', 780, 280, true);
		add_image_size('single-recipes', 380, 315, true);
		add_image_size('single-recipes-padded', 384, 318);
		$_wp_additional_image_sizes['single-recipes-padded']['padded'] = true;
		add_image_size('single-recipes-small', 115, 90, true);
		add_image_size('subscribe-banner', 220, 115);
		add_image_size('verb-ak-large-post-widget', 860, 375, true);
		add_image_size('videos', 422, 222, true);
	}
	
	protected function add_shortcodes() {
		add_shortcode('verify_user_pass', [
			$this,
			'verify_user_pass'
		]);
	}
	
	protected function add_theme_options() {
		if (function_exists('acf_add_options_page')) {
			acf_add_options_page([
				'page_title' => 'AK Theme Options'
			]);
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Apps & Books',
				'parent' => 'acf-options-ak-theme-options'
			]);
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Competitions',
				'parent' => 'acf-options-ak-theme-options'
			]);
			
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Offers',
				'parent' => 'acf-options-ak-theme-options'
			]);
			
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Social Accounts',
				'parent' => 'acf-options-ak-theme-options'
			]);
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Subscribe Banners',
				'parent' => 'acf-options-ak-theme-options'
			]);
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Videos',
				'parent' => 'acf-options-ak-theme-options'
			]);
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Others',
				'parent' => 'acf-options-ak-theme-options'
			]);
			
			acf_add_options_sub_page([
				'capability' => 'edit_posts',
				'page_title' => 'Smart Banner',
				'parent' => 'acf-options-ak-theme-options'
			]);
		}
	}
	
	protected function add_theme_support() {
		add_theme_support('custom-logo', [
			'header-text' => [
				'site-description',
				'site-title'
			]
		]);
		add_theme_support('html5', [
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form'
		]);
		add_theme_support('page-attributes');
		add_theme_support('post-thumbnails');
	}
	
	public function admin_init() {
		add_editor_style('css/editor.css');
	}
	
	public function ak_category_item($post_id = null, $excerpt = true, $force_recipe_excerpt = false) {
		global $post;
		
		
		if ($post_id && !($post_id = icl_object_id($post_id, 'post', false, ICL_LANGUAGE_CODE))) {
			return;
		} elseif (!$post_id && !($post_id = icl_object_id($post->ID, 'post', false, ICL_LANGUAGE_CODE))) {
			return;
		}
		
		if ($post_id) {
			$post = get_post($post_id);
			setup_postdata($post);
		}
		include 'parts/category-single-item.php';
		
		wp_reset_postdata();
	}
	
	public function ak_large_item($post_id = null) {
		global $post;
		
		if ($post_id && !($post_id = icl_object_id($post_id, 'post', false, ICL_LANGUAGE_CODE))) {
			return;
		}
		
		if ($post_id) {
			$post = get_post($post_id);
			setup_postdata($post);
		}
		
		include 'parts/category-large-item.php';
		
		wp_reset_postdata();
	}
	
	public function ak_video_item($post_id = null) {
		global $post;
		
		if ($post_id && !($post_id = icl_object_id($post_id, 'post', false, ICL_LANGUAGE_CODE))) {
			return;
		}
		
		if ($post_id) {
			$post = get_post($post_id);
			setup_postdata($post);
		}
		
		include 'parts/category-video-item.php';
		
		wp_reset_postdata();
	}
	
	public function alm_recipe_finder($default_recipe_categories = []) {
		$obj = get_queried_object();
		$taxonomies = [];
		$terms = [];
		
		if ($default_recipe_categories) {
			$taxonomies[] = 'recipe-category';
			$terms[] = $default_recipe_categories;
		}
		
		foreach ($_POST as $k => $v) {
			if ('rf_category' == $k) {
				if (!is_tax('recipe-category')) {
					$taxonomies[] = 'recipe-category';
					$term_array = $v;
					
					foreach ($v as $obj) {
						$obj = get_term($obj, 'recipe-category');
						
						while ($obj->parent) {
							$obj = get_term($obj->parent, $obj->taxonomy);
							
							$term_array[] = $obj->slug;
						}
					}
					
					$terms[] = $term_array;
				}
			} elseif ('rf_allergen' == $k) {
				$taxonomies[] = 'allergen';
				$terms[] = $v;
			} elseif (0 === strpos($k, 'rf_')) {
				$taxonomies[] = 'recipe-filter';
				$terms[] = $v;
			}
		}
		
		$operators = [];
		
		foreach ($taxonomies as $taxonomy) {
			$operators[] = ('recipe-category' == $taxonomy
				? 'IN'
				: 'AND'
			);
		}
		
		return 'taxonomy="' . implode(':', $taxonomies) . '" taxonomy_terms="' . implode(':', array_map(function($item) {
			return implode(', ', $item);
		}, $terms)) . '" taxonomy_operator="' . implode(':', $operators) . '" taxonomy_relation="AND"';
	}
	
	/*public function authenticate($user, $username, $password) {
		global $wpdb;
		if ($wpdb->get_var($wpdb->prepare('select signup_id from ' . $wpdb->prefix . 'signups where user_login = %s or user_email = %s', $username, $username))) {
			if (($referer = wp_get_referer()) && !strstr($referer, 'wp-admin') && !strstr($referer, 'wp-admin')) {
				wp_redirect(add_query_arg('ak-login', 'pending', $referer));
				
				exit;
			}
		}
		if (!$username || !$password) {
			if (($referer = wp_get_referer()) && !strstr($referer, 'wp-admin') && !strstr($referer, 'wp-admin')) {
				wp_redirect(add_query_arg('ak-login', 'failed', $referer));
				
				exit;
			}
		}
		
		return $user;
	}
	*/
	public function body_class($classes) {
		if (is_singular()) {
			$classes[] = $GLOBALS['post']->post_type . '-slug-' . $GLOBALS['post']->post_name;
		}
		
		return $classes;
	}
	
	public function bottom_cat($post) {
		$term = null;
		
		if (!is_object($top_cat = $this->top_cat($post, true, true))) {
			return $term;
		}
		
		if ('recipes' == get_post_type($post) && ($terms = get_the_terms($post->ID, 'recipe-category'))) {
			foreach ($terms as $maybe_age) {
				if ($maybe_age->parent) {
					return $maybe_age->name;
				}
			}
		}
		
		$terms = [];
		
		foreach (get_the_terms($post->ID, $top_cat->taxonomy) as $a) {
			foreach (array_map(function($item) use ($top_cat) {
				return get_term($item, $top_cat->taxonomy);
			}, get_term_children($top_cat->term_id, $top_cat->taxonomy)) as $b) {
				if ($a->term_id === $b->term_id) {
					$terms[] = $a;
				}
			}
		}
		
		if ($terms) {
			$term = $terms[0]->name;
		}
		
		return $term;
	}
	
	public function change_gf_media_upload_location( $upload_dir, $form_id ) {
		$upload_dir['path'] = '/home/akc/public_html/wp-content/uploads/competition-entry/form-' . $form_id;
		$upload_dir['url'] = 'http://www.annabelkarmel.com/wp-content/uploads/competition-entry/form-' . $form_id;
		return $upload_dir;
	}
	
	public function check_password($check, $password, $hash, $user_id) {
		global $wpdb;
		
		// Perhaps it's Expression Engine
		if (!$check && 2 == count($split = explode(':', $hash)) && md5($split[1] . $password) == $split[0]) {
			$user = $wpdb->get_row($wpdb->prepare('select * from ' . $wpdb->prefix . 'users where ID = %d', $user_id));
			
			if ($user->old_id && hash($this->algorithms[strlen($hash)], $user->user_salt . $password) == $hash) {
				$check = true;
				wp_set_password($password, $user_id);
			}
		}
		
		return $check;
	}
	
	public function cpt() {
		// Recipes
		register_post_type('recipes', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New Recipe',
				'all_items' => 'All Recipes',
				'edit_item' => 'Edit Recipe',
				'name' => 'Recipes',
				'new_item' => 'New Recipe',
				'not_found' => 'No recipes found',
				'not_found_in_trash' => 'No recipes found in Trash',
				'search_item' => 'Search Recipes',
				'singular_name' => 'Recipe',
				'view_item' => 'View Recipe'
			],
			'menu_icon' => 'dashicons-carrot',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		register_taxonomy('recipe-category', 'recipes', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Category',
				'all_itmes' => 'All Categories',
				'edit_item' => 'Edit Category',
				'name' => 'Recipe Categories',
				'new_item_name' => 'New Category Name',
				'parent_item' => 'Parent Category',
				'parent_item_colon' => 'Parent Category:',
				'search_items' => 'Search Categories',
				'singular_name' => 'Category',
				'view_item' => 'View Category'
			]
		]);
		
		register_taxonomy('mobile-recipe-category', 'recipes', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Category',
				'all_itmes' => 'All Categories',
				'edit_item' => 'Edit Category',
				'name' => 'Mobile Recipe Categories',
				'new_item_name' => 'New Category Name',
				'parent_item' => 'Parent Category',
				'parent_item_colon' => 'Parent Category:',
				'search_items' => 'Search Categories',
				'singular_name' => 'Category',
				'view_item' => 'View Category'
			]
		]);
		register_taxonomy_for_object_type('recipe-category', 'recipes');
		
		register_taxonomy('recipe-tag', 'recipes', [
			'labels' => [
				'add_new_item' => 'Add New Tag',
				'all_itmes' => 'All Tags',
				'edit_item' => 'Edit Tag',
				'name' => 'Recipe Tags',
				'new_item_name' => 'New Tag Name',
				'search_items' => 'Search Tags',
				'singular_name' => 'Tag',
				'view_item' => 'View Tag'
			]
		]);
		register_taxonomy_for_object_type('recipe-tag', 'recipes');
		
		register_taxonomy('recipe-filter', 'recipes', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Filter',
				'all_itmes' => 'All Filters',
				'edit_item' => 'Edit Filter',
				'name' => 'Filters',
				'new_item_name' => 'New Filter Name',
				'parent_item' => 'Parent Filter',
				'parent_item_colon' => 'Parent Filter:',
				'search_items' => 'Search Filters',
				'singular_name' => 'Filter',
				'view_item' => 'View Filter'
			]
		]);
		register_taxonomy_for_object_type('recipe-filter', 'recipes');
		
		register_taxonomy('prod-allergen', 'products', [
			'labels' => [
				'add_new_item' => 'Add New Allergen / Dietary',
				'all_itmes' => 'All Allergens & Dietary',
				'edit_item' => 'Edit Allergen / Dietary',
				'name' => 'Allergens & Dietary',
				'new_item_name' => 'New Allergen / Dietary',
				'search_items' => 'Search Allergens & Dietary',
				'singular_name' => 'Allergen / Dietary',
				'view_item' => 'View Allergen / Dietary'
			],
			'meta_box_cb' => false
		]);
		register_taxonomy_for_object_type('prod-allergen', 'products');
		
		register_taxonomy('allergen', 'recipes', [
			'labels' => [
				'add_new_item' => 'Add New Allergen / Dietary Requirement',
				'all_itmes' => 'All Allergens & Dietary Requirements',
				'edit_item' => 'Edit Allergen / Dietary Requirement',
				'name' => 'Allergens & Dietary Requirements',
				'new_item_name' => 'New Allergen / Dietary Requirement',
				'search_items' => 'Search Allergens & Dietary Requirements',
				'singular_name' => 'Allergen / Dietary Requirement',
				'view_item' => 'View Allergen / Dietary Requirement'
			],
			'meta_box_cb' => false
		]);
		register_taxonomy_for_object_type('allergen', 'recipes');
		register_taxonomy_for_object_type('allergen', 'products');
		
		// Videos
		register_post_type('videos', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New Video',
				'all_items' => 'All Videos',
				'edit_item' => 'Edit Video',
				'name' => 'Videos',
				'new_item' => 'New Video',
				'not_found' => 'No videos found',
				'not_found_in_trash' => 'No videos found in Trash',
				'search_item' => 'Search Videos',
				'singular_name' => 'Video',
				'view_item' => 'View Video'
			],
			'menu_icon' => 'dashicons-video-alt3',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		register_taxonomy('video-category', 'videos', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Category',
				'all_itmes' => 'All Categories',
				'edit_item' => 'Edit Category',
				'name' => 'Video Categories',
				'new_item_name' => 'New Category Name',
				'parent_item' => 'Parent Category',
				'parent_item_colon' => 'Parent Category:',
				'search_items' => 'Search Categories',
				'singular_name' => 'Category',
				'view_item' => 'View Category'
			]
		]);
		register_taxonomy_for_object_type('video-category', 'videos');
		
		// Products
		register_post_type('products', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New Product',
				'all_items' => 'All Products',
				'edit_item' => 'Edit Product',
				'name' => 'Products',
				'new_item' => 'New Product',
				'not_found' => 'No products found',
				'not_found_in_trash' => 'No products found in Trash',
				'search_item' => 'Search Products',
				'singular_name' => 'Product',
				'view_item' => 'View Product'
			],
			'menu_icon' => 'dashicons-products',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		register_taxonomy('product-category', 'products', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Category',
				'all_itmes' => 'All Categories',
				'edit_item' => 'Edit Category',
				'name' => 'Product Categories',
				'new_item_name' => 'New Category Name',
				'parent_item' => 'Parent Category',
				'parent_item_colon' => 'Parent Category:',
				'search_items' => 'Search Categories',
				'singular_name' => 'Category',
				'view_item' => 'View Category'
			]
		]);
		register_taxonomy_for_object_type('product-category', 'products');
		
		register_taxonomy('product-tag', 'products', [
			'labels' => [
				'add_new_item' => 'Add New Tag',
				'all_itmes' => 'All Tags',
				'edit_item' => 'Edit Tag',
				'name' => 'Product Tags',
				'new_item_name' => 'New Tag Name',
				'search_items' => 'Search Tags',
				'singular_name' => 'Tag',
				'view_item' => 'View Tag'
			]
		]);
		register_taxonomy_for_object_type('product-tag', 'products');
		
		
		
		// Apps & Books
		register_post_type('apps-books', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New App / Book',
				'all_items' => 'All Apps & Books',
				'edit_item' => 'Edit App / Book',
				'name' => 'Apps & Books',
				'new_item' => 'New App / Book',
				'not_found' => 'No apps / books found',
				'not_found_in_trash' => 'No apps / bounds found in Trash',
				'search_item' => 'Search Apps & Books',
				'singular_name' => 'App / Book',
				'view_item' => 'View App / Book'
			],
			'menu_icon' => 'dashicons-book-alt',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		register_taxonomy('app-book-category', 'apps-books', [
			'hierarchical' => true,
			'labels' => [
				'add_new_item' => 'Add New Category',
				'all_itmes' => 'All Categories',
				'edit_item' => 'Edit Category',
				'name' => 'App / Book Categories',
				'new_item_name' => 'New Category Name',
				'parent_item' => 'Parent Category',
				'parent_item_colon' => 'Parent Category:',
				'search_items' => 'Search Categories',
				'singular_name' => 'Category',
				'view_item' => 'View Category'
			]
		]);
		register_taxonomy_for_object_type('app-book-category', 'apps-books');
		
		register_taxonomy('app-book-tag', 'apps-books', [
			'labels' => [
				'add_new_item' => 'Add New Tag',
				'all_itmes' => 'All Tags',
				'edit_item' => 'Edit Tag',
				'name' => 'App / Book Tags',
				'new_item_name' => 'New Tag Name',
				'search_items' => 'Search Tags',
				'singular_name' => 'Tag',
				'view_item' => 'View Tag'
			]
		]);
		register_taxonomy_for_object_type('app-book-tag', 'apps-books');
		
		// Competitions
		register_post_type('competitions', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New Competition',
				'all_items' => 'All Competitions',
				'edit_item' => 'Edit Competition',
				'name' => 'Competitions',
				'new_item' => 'New Competition',
				'not_found' => 'No competitions found',
				'not_found_in_trash' => 'No competitions found in Trash',
				'search_item' => 'Search Competitions',
				'singular_name' => 'Competition',
				'view_item' => 'View Competition'
			],
			'menu_icon' => 'dashicons-awards',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		
		// Offers
		register_post_type('offers', [
			'has_archive' => true,
			'labels' => [
				'add_new_item' => 'Add New Offer',
				'all_items' => 'All Offers',
				'edit_item' => 'Edit Offer',
				'name' => 'Offers',
				'new_item' => 'New Offer',
				'not_found' => 'No offers found',
				'not_found_in_trash' => 'No offers found in Trash',
				'search_item' => 'Search Offers',
				'singular_name' => 'Offer',
				'view_item' => 'View offers'
			],
			'menu_icon' => 'dashicons-awards',
			'menu_position' => 7,
			'public' => true,
			'supports' => [
				'editor',
				'thumbnail',
				'title'
			]
		]);
		
		// Galleries
		register_post_type('galleries', [
			'labels' => [
				'add_new_item' => 'Add New Gallery',
				'all_items' => 'All Galleries',
				'edit_item' => 'Edit Gallery',
				'name' => 'Galleries',
				'new_item' => 'New Gallery',
				'not_found' => 'No galleries found',
				'not_found_in_trash' => 'No galleries found in Trash',
				'search_item' => 'Search Galleries',
				'singular_name' => 'Gallery',
				'view_item' => 'View Gallery'
			],
			'menu_icon' => 'dashicons-format-gallery',
			'menu_position' => 5,
			'public' => true,
			'supports' => [
				'title'
			]
		]);
		
		register_post_type('ig-posts', [
			'description' => 'Internal post type to keep track of AK\'s latest posts on Instgram.',
			'exclude_from_search' => true,
			'labels' => [
				'name' => 'Instagram Posts',
				'singular_name' => 'Instagram Post',
			],
			'public' => false
		]);
		
		register_post_type('tweet', [
			'description' => 'Internal post type to keep track of Tossed\'s latest tweets on Twitter.',
			'exclude_from_search' => true,
			'labels' => [
				'name' => 'Tweets',
				'singular_name' => 'Tweet',
			],
			'public' => false
		]);
		
		register_post_type('fb-posts', [
		'description' => 'Internal post type to keep track of AK\'s latest posts on Facebook.',
		'exclude_from_search' => true,
		'labels' => [
			'name' => 'Facebook Posts',
			'singular_name' => 'Facebook Post',
		],
		'public' => false
	]);
	}
	
	public function disable_gf_post_creation( $is_disabled ) {
		return true;
	}
	
	public function excerpt_length() {
		return (is_post_type_archive('competitions') && 'competitions' == get_post_type()
			? 30
			: 10
		);
	}
	
	public function generate_gallery_json($images, $echo = true) {
		$output = esc_attr(json_encode(array_map(function($image) {
			return [
				'h' => $image['sizes']['gallery-large-height'],
				'src' => $image['sizes']['gallery-large'],
				'w' => $image['sizes']['gallery-large-width']
			];
		}, $images)));
		
		if ($echo) {
			echo $output;
		} else {
			return $output;
		}
	}
	
	public function get_the_excerpt($excerpt) {
		if (!$excerpt && have_rows('flexible_content')) {
			while (have_rows('flexible_content')) {
				the_row();
				
				if (in_array(get_row_layout(), [
					'wysiwyg_content',
					'wysiwyg_content_overlaying_image'
				])) {
					$excerpt = wp_trim_words(str_replace(']]>', ']]&gt;', apply_filters('the_content', get_sub_field('wysiwyg_content'))), apply_filters('excerpt_length', 10), apply_filters('excerpt_more', ' ' . '[&hellip;]'));
					
					reset_rows();
					
					break;
				}
			}
		} elseif (!$excerpt && ($product_information = get_field('product_information'))) {
			$excerpt = wp_trim_words(str_replace(']]>', ']]&gt;', apply_filters('the_content', $product_information)), apply_filters('excerpt_length', 10), apply_filters('excerpt_more', ' ' . '[&hellip;]'));
		}
		
		return $excerpt;
	}
	
	public function gform_activate_user($user_id, $user_data, $signup_meta) {
		$entry = \GFAPI::get_entry($signup_meta['entry_id']);
		
		// Title
		update_field('field_57a8ce74c106b', $entry[1], 'user_' . $user_id);
		
		// Date of Birth
		update_field('field_57a8ceacc106c', \DateTime::createFromFormat('Y-m-d', $entry[7])->format('Ymd'), 'user_' . $user_id);
		
		// Are you pregnant?
		update_field('field_57a8cec5c106d', ($pregnant = ('Yes' == $entry[8])), 'user_' . $user_id);
		
		// When are you expecting?
		if ($pregnant) {
			update_field('field_57a8cedbc106e', \DateTime::createFromFormat('Y-m-d', $entry[9])->format('Ymd'), 'user_' . $user_id);
		}
		
		// Do you have any children?
		update_field('field_57a8cf03c106f', ($children = ('Yes' == $entry[10])), 'user_' . $user_id);
		
		// Children's Date of Birth
		if ($children) {
			$field = [];
			
			foreach (unserialize($entry[11]) as $dob) {
				if ($dob) {
					$field[] = [
						'childs_birthday' => \DateTime::createFromFormat('d/m/Y', $dob[12][0])->format('Ymd')
					];
				}
			}
			
			update_field('field_57a8cf0cc1070', $field, 'user_' . $user_id);
		}
		
		// Address Line 1
		update_field('field_57a8cf31c1072', $entry['15.1'], 'user_' . $user_id);
		
		// Address Line 2
		update_field('field_57a8cf37c1073', $entry['15.2'], 'user_' . $user_id);
		
		// City
		update_field('field_57a8cf3cc1074', $entry['15.3'], 'user_' . $user_id);
		
		// Country
		update_field('field_57a8cf3fc1075', $entry['15.4'], 'user_' . $user_id);
		
		// Postcode
		update_field('field_57a8cf48c1076', $entry['15.5'], 'user_' . $user_id);
		
		// Country
		update_field('field_57a8cf55c1077', $entry['15.6'], 'user_' . $user_id);
		
		// AND now we need to send them through to ee
		$this->__transfer_user_to_ee($user_id);
	}
	
	public function gform_author_dropdown_args($args) {
		return array_merge($args, [
			'who' => 'authors'
		]);
	}
	
	public function gform_countries($countries) {
		$countries[] = 'Gibraltar';
		
		sort($countries);
		
		return $countries;
	}
	
	public function gform_field_validation_5($result, $value, $form, $field) {
		if (in_array('user-email', explode(' ', preg_replace('/\s{2,}/', ' ', $field['cssClass'])))) {
			if (!($user = get_user_by_email(trim($value))) || !apply_filters('allow_password_reset', true, $user->ID)) {
				$result['is_valid'] = false;
				$result['message'] = 'Sorry &mdash; that email address wasn\'t recognised.';
			} else {
				$result['is_valid'] = true;
			}
		}
		
		return $result;
	}
	
	public function gform_field_value($value, $field, $name) {
		if ($user_id = get_current_user_id()) {
			if ('ep_title' == $name) {
				return get_field('title', 'user_' . $user_id);
			} elseif ('ep_dob' == $name) {
				return get_field('date_of_birth', 'user_' . $user_id);
			} elseif ('ep_pregnant' == $name) {
				return (get_field('are_you_pregnant', 'user_' . $user_id)
					? 'Yes'
					: 'No'
				);
			} elseif ('ep_children' == $name) {
				return (get_field('do_you_have_any_children', 'user_' . $user_id)
					? 'Yes'
					: 'No'
				);
			} elseif ('ep_due_date' == $name && get_field('are_you_pregnant', 'user_' . $user_id)) {
				return get_field('when_are_you_expecting', 'user_' . $user_id);
			} elseif ('ep_address_1' == $name) {
				return get_field('address_line_1', 'user_' . $user_id);
			} elseif ('ep_address_2' == $name) {
				return get_field('address_line_2', 'user_' . $user_id);
			} elseif ('ep_city' == $name) {
				return get_field('city', 'user_' . $user_id);
			} elseif ('ep_postcode' == $name) {
				return get_field('postcode', 'user_' . $user_id);
			} elseif ('ep_country' == $name) {
				return get_field('country', 'user_' . $user_id);
			}
		}
		
		return $value;
	}
	
	public function gform_pre_render_3($form) {
		if ($user_id = get_current_user_id()) {
			echo '<div class="my-children" data-children="' . esc_attr(json_encode(array_map(function($item) {
				return $item['childs_birthday'];
			}, (get_field('children', 'user_' . $user_id) ?: [])))) . '"></div>';
		}
		
		return $form;
	}
	
	public function gform_pre_submission_5($form) {
		global $wp_hasher, $wpdb;
		
		if (!$wp_hasher) {
			require_once ABSPATH . WPINC . '/class-phpass.php';
			
			$wp_hasher = new \PasswordHash(8, true);
		}
		
		// Update user with new activation key
		$user = get_user_by_email(trim($_POST['input_1']));
		$wpdb->update($wpdb->users, [
			'user_activation_key' => time() . ':' . $wp_hasher->HashPassword($key = wp_generate_password(20, false))
		], [
			'user_login' => $user->user_login
		]);
		
		// construct the email message for the user
		$message = __( 'Someone requested that the password be reset for the following account:' ) . "\r\n\r\n";
		$message .= sprintf( __( 'Username: %s' ), $user->user_login ) . "\r\n\r\n";
		$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
		$message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
		$message .= '<' . network_site_url( "/forgot-password/reset-password/?action=rp&method=gf&key=$key&login=" . rawurlencode( $user->user_login ), 'login' ) . ">\r\n";
		
		if ( is_multisite() ) {
			$blogname = $GLOBALS['current_site']->site_name;
		} else {
			/*
			 * The blogname option is escaped with esc_html on the way into the database
			 * in sanitize_option we want to reverse this for the plain text arena of emails.
			 */
			$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		}
		
		$title = sprintf( __( '[%s] Password Reset' ), $blogname );
		
		/**
		 * Filter the subject of the password reset email.
		 *
		 * @since 2.8.0
		 *
		 * @param string $title Default email title.
		 */
		$title = apply_filters( 'retrieve_password_title', $title );
		
		/**
		 * Filter the message body of the password reset mail.
		 *
		 * @since 2.8.0
		 * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
		 *
		 * @param string  $message    Default mail message.
		 * @param string  $key        The activation key.
		 * @param string  $user_login The username for the user.
		 * @param WP_User $user_data  WP_User object.
		 */
		$message = apply_filters( 'retrieve_password_message', $message, $key, $user->user_login, $user );
		
		if ( $message && !wp_mail( $user->user_email, wp_specialchars_decode( $title ), $message ) )
			wp_die( __( 'The e-mail could not be sent.' ) . "<br />\n" . __( 'Possible reason: your host may have disabled the mail() function.' ) );
		
		return;
	}
	
	public function gform_pre_submission_6($form) {
		// we'll need the data created before to update the correct user
		global $gf_reset_user;
		
		list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
		$rp_cookie = 'wp-resetpass-' . COOKIEHASH;
		
		// get the old and new pass values
		$pass = $_POST['input_1'];
		$pass_confirm = $_POST['input_1_2'];
		
		// if we're doing a cron job let's forget about it
		if ( defined( 'DOING_CRON' ) || isset( $_GET['doing_wp_cron'] ) )
			return;
		
		// let's check if a user with given name exists
		// we're already doing that in the form validation, but this gives us another bridge of safety
		$user_id = username_exists( $gf_reset_user->ID );
		
		// let's validate the email and the user
		if ( !$user_id ) {
			// let's add another safety check to make sure that the passwords remain unchanged
			if ( !empty( $pass ) and ! empty( $pass_confirm ) and $pass === $pass_confirm ) {
				reset_password( $gf_reset_user, $pass );
				setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
				
				wp_clear_auth_cookie();
			    wp_set_current_user ( $gf_reset_user->ID );
			    wp_set_auth_cookie  ( $gf_reset_user->ID );
			}
		} else {
			// validation failed
			return;
		}
	}
	function gform_after_save_form( $form, $is_new )
	{
		if($is_new)
		{
			$forms_id_string = (!empty(get_field('competitions_form_id','option')))?get_field('competitions_form_id','option'):'';
			$forms_id_array = @explode(",",$forms_id_string);
			array_push($forms_id_array,$form['id']);
			$forms_id_string_updated = @implode(",",$forms_id_array);
			update_field('competitions_form_id',$forms_id_string_updated,'option');
		}
	}
	function gform_post_submission( $entry, $form )
	{	
		$forms_id_string = (!empty(get_field('competitions_form_id','option')))?get_field('competitions_form_id','option'):'';
		$forms_id_array = explode(",",$forms_id_string);
		if(empty($forms_id_array) || !in_array($form["id"],$forms_id_array))
			return;
		
		// displays the types of every field in the form
		foreach ( $form['fields'] as $field ) {
		   if($field->type=='fileupload')
		   {
			   $this->compress_and_delete_file($entry, $form, $field->id);
		   }
		}
	}
	public function compress_and_delete_file($entry, $form, $field_id)
	{
		$upload_info = wp_upload_dir();

		//original name/path
		$original_file_url = $entry[$field_id];
		$original_file_path = str_replace($upload_info["baseurl"], $upload_info["basedir"], $original_file_url);
		$original_file_info = pathinfo($original_file_url);

		//New File Name (without extension).
		//NOTE: Change this line to create the filename you would like to have
		$new_name = $original_file_info['filename'].'_'.$form["id"].'_'.$entry["id"].'_'.$field_id;

		//adding same extension as original
		$new_name .= "." . $original_file_info["extension"];

		$new_file_url = str_replace($original_file_info["basename"], $new_name, $original_file_url);
		$new_file_path = str_replace($original_file_info["basename"], $new_name, $original_file_path);
		
		$this->compress($original_file_path,$new_file_path,80);
				
		//if file was renamed successfully, updating entry so that it points to the new file
		if($this->compress($original_file_path,$new_file_path,80)){
			global $wpdb;
			//echo RGFormsModel::get_lead_details_table_name(); die;
			$wpdb->update('wp_rg_lead_detail', array("value" => $new_file_url), array("lead_id" => $entry["id"], "value" => $original_file_url));
			
			unlink($original_file_path);
		}		
	}
	
	public function compress($source, $destination, $quality) {

		$info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($source);

		elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($source);

		elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($source);

		imagejpeg($image, $destination, $quality);

		return $destination;
	}
	
	public function gform_user_updated($user_id, $feed, $entry) {
		// Title
		update_field('field_57a8ce74c106b', $entry[1], 'user_' . $user_id);
		
		// Date of Birth
		update_field('field_57a8ceacc106c', \DateTime::createFromFormat('Y-m-d', $entry[7])->format('Ymd'), 'user_' . $user_id);
		
		// Are you pregnant?
		update_field('field_57a8cec5c106d', ($pregnant = ('Yes' == $entry[8])), 'user_' . $user_id);
		
		// When are you expecting?
		if ($pregnant) {
			update_field('field_57a8cedbc106e', \DateTime::createFromFormat('Y-m-d', $entry[9])->format('Ymd'), 'user_' . $user_id);
		}
		
		// Do you have any children?
		update_field('field_57a8cf03c106f', ($children = ('Yes' == $entry[10])), 'user_' . $user_id);
		
		// Children's Date of Birth
		if ($children) {
			$field = [];
			
			foreach (unserialize($entry[11]) as $dob) {
				if ($dob) {
					$field[] = [
						'childs_birthday' => \DateTime::createFromFormat('d/m/Y', $dob[12][0])->format('Ymd')
					];
				}
			}
			
			update_field('field_57a8cf0cc1070', $field, 'user_' . $user_id);
		}
		
		// Address Line 1
		update_field('field_57a8cf31c1072', $entry['15.1'], 'user_' . $user_id);
		
		// Address Line 2
		update_field('field_57a8cf37c1073', $entry['15.2'], 'user_' . $user_id);
		
		// City
		update_field('field_57a8cf3cc1074', $entry['15.3'], 'user_' . $user_id);
		
		// Country
		update_field('field_57a8cf3fc1075', $entry['15.4'], 'user_' . $user_id);
		
		// Postcode
		update_field('field_57a8cf48c1076', $entry['15.5'], 'user_' . $user_id);
		
		// Country
		update_field('field_57a8cf55c1077', $entry['15.6'], 'user_' . $user_id);
		
		// AND now we need to send the update through to ee
		$this->__transfer_user_to_ee($user_id, true);
	}
	
	public function img_caption_shortcode($output, $attr, $content) {
		$atts = shortcode_atts( array(
			'id'	  => '',
			'align'	  => 'alignnone',
			'width'	  => '',
			'caption' => '',
			'class'   => '',
		), $attr, 'caption' );
	
		$atts['width'] = (int) $atts['width'];
		if ( $atts['width'] < 1 || empty( $atts['caption'] ) )
			return $content;
	
		if ( ! empty( $atts['id'] ) )
			$atts['id'] = 'id="' . esc_attr( sanitize_html_class( $atts['id'] ) ) . '" ';
	
		$class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );
	
		$html5 = current_theme_supports( 'html5', 'caption' );
		// HTML5 captions never added the extra 10px to the image width
		$width = $html5 ? $atts['width'] : ( 10 + $atts['width'] );
	
		/**
		 * Filter the width of an image's caption.
		 *
		 * By default, the caption is 10 pixels greater than the width of the image,
		 * to prevent post content from running up against a floated image.
		 *
		 * @since 3.7.0
		 *
		 * @see img_caption_shortcode()
		 *
		 * @param int    $width    Width of the caption in pixels. To remove this inline style,
		 *                         return zero.
		 * @param array  $atts     Attributes of the caption shortcode.
		 * @param string $content  The image element, possibly wrapped in a hyperlink.
		 */
		$caption_width = apply_filters( 'img_caption_shortcode_width', $width, $atts, $content );
	
		$style = '';
		if ( $caption_width )
			$style = 'style="width: ' . (int) $caption_width . 'px" ';
	
		$html = '';
		if ( $html5 ) {
			$html = '<figure ' . $atts['id'] . $style . 'class="' . esc_attr( $class ) . '">'
			. do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $atts['caption'] . '</figcaption></figure>';
		} else {
			$html = '<div ' . $atts['id'] . $style . 'class="' . esc_attr( $class ) . '">'
			. do_shortcode( $content ) . '<p class="wp-caption-text">' . $atts['caption'] . '</p></div>';
		}
	
		return '<div class="caption-wrapper">' . $html . '</div>';
	}
	
	public function login_redirect($redirect_to) {
		return home_url();
	}
	
	public function loop_start() {
		remove_filter('the_content', 'sharing_display', 19);
		remove_filter('the_excerpt', 'sharing_display', 19);
		
		if (class_exists('Jetpack_Likes')) {
			remove_filter('the_content', [
				Jetpack_Likes::init(),
				'post_likes'
			], 30, 1);
		}
	}
	
	public function lostpassword_url($url) {
		return site_url('/forgot-password/');
	}
	
	public function pre_get_posts($query) {
		global $pagenow, $typenow, $wpdb;
		
		if (!is_admin() && $query->is_main_query()) {
			if (is_category()) {
				$query->set('posts_per_page', 4);
			} elseif (is_post_type_archive('recipes') || is_tax('recipe-category') || is_post_type_archive('apps-books')) {
				$query->set('posts_per_page', 6);
			}
		} elseif (is_admin() && $query->is_main_query() && 'edit.php' == $pagenow && 'recipes' == $typenow) {
			if (isset($_GET['availability']) && is_array($_GET['availability']) && 1 == count($_GET['availability'])  && $_GET['availability'][0]!='') {	
				if ('gated' == $_GET['availability'][0]) {
					$query->set('meta_query', [
						[
							'key' => 'gated',
							'value' => 1
						]
					]);
				} else {
					$query->set('meta_query', [
						'relation' => 'OR',
						[
							'key' => 'gated',
							'value' => 0
						],
						[
							'compare' => 'NOT EXISTS',
							'key' => 'gated'
						]
					]);
				}
			}
			
			if (isset($_GET['missing_subcategory']) && $_GET['missing_subcategory']) {
				$query->set('post__in', $wpdb->get_col($wpdb->prepare('select distinct p.ID from wp_posts p inner join wp_icl_translations it on p.ID = it.element_id and it.language_code = %s where p.post_type = \'recipes\' and p.ID not in (select p.ID from wp_posts p inner join wp_term_relationships tr on p.ID = tr.object_id inner join wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id and tt.taxonomy = \'recipe-category\' and tt.parent > 0 where p.post_type = \'recipes\') and p.post_status != \'trash\'', ICL_LANGUAGE_CODE)));
			}
		}
	}
	
	public function redirection_role($role) {
		return 'verb';
	}
	
	public function register_menus() {
		register_nav_menu('footer-menu', 'Footer Menu');
		register_nav_menu('header-menu', 'Header Menu');
		register_nav_menu('top-bar-menu', 'Top Bar Menu');
	}
	
	public function restrict_manage_posts() {
		global $pagenow;
		
		if (is_admin() && 'edit.php' == $pagenow && 'recipes' == $_GET['post_type']) {
			$dropdown_options = [
				'show_option_all' => get_taxonomy('recipe-category')->labels->all_items,
				'hierarchical' => 1,
				'show_count' => 1,
				'orderby' => 'name',
				'selected' => (isset($_GET['recipe-category'])
					? $_GET['recipe-category']
					: ''
				),
				'taxonomy' => 'recipe-category',
				'name' => 'recipe-category',
				'value_field' => 'slug'
			];

			echo '<label class="screen-reader-text" for="cat">' . __( 'Filter by category' ) . '</label>';
			wp_dropdown_categories( $dropdown_options );
			
			if (!is_array($_GET['availability'])) {
				$_GET['availability'] = [];
			}
			
			echo '<select name="availability[]"><option value="">All</option><option ' . selected(in_array('open', $_GET['availability']), true, false) . ' value="open">Open</option><option ' . selected(in_array('gated', $_GET['availability']), true, false) . ' value="gated">Gated</option></select>';
		}
	}
	
	public function rewrite_rules_array($rules) {
		return [
			'(.?.+?)/embed/?$' => 'index.php?pagename=$matches[1]&embed=true',
			'(.?.+?)/trackback/?$' => 'index.php?pagename=$matches[1]&tb=1',
			'(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$' => 'index.php?pagename=$matches[1]&feed=$matches[2]',
			'(.?.+?)/(feed|rdf|rss|rss2|atom)/?$' => 'index.php?pagename=$matches[1]&feed=$matches[2]',
			'(.?.+?)/page/?([0-9]{1,})/?$' => 'index.php?pagename=$matches[1]&paged=$matches[2]',
			'(.?.+?)/comment-page-([0-9]{1,})/?$' => 'index.php?pagename=$matches[1]&cpage=$matches[2]',
			'(.?.+?)(?:/([0-9]+))?/?$' => 'index.php?pagename=$matches[1]&page=$matches[2]'
		] + $rules;
	}
	
	public function show_admin_bar($show) {
		if (!current_user_can('edit_posts')) {
			$show = false;
		}
		
		return $show;
	}
	
	public function top_cat($post, $return_wp_term = false, $force = false) {
		global $tc_return_null, $tc_return_pt;
		
		$term = null;
		
		if ($tc_return_null && !$force) {
			return $term;
		}
		
		if (in_array(($post_type = get_post_type($post)), [
			'competitions',
			'page',
			'videos'
		])) {
			return [
				'competitions' => 'Competition',
				'page' => 'Article',
				'videos' => 'Video'
			][$post_type];
		}
		
		if ($tc_return_pt && !$force) {
			return (array_key_exists($post_type, ($pt_mappings = [
				'apps-books' => 'Apps & Books',
				'post' => 'Article'
			]))
				? $pt_mappings[$post_type]
				: ucwords($post_type)
			);
		}
		
		$parent_terms = $unique_slugs = [];
		
		if ($terms = get_the_terms($post->ID, ('post' == $post_type
			? ''
			: preg_replace('/s(?=-)|s$/', '', $post_type) . '-'
		) . 'category')) {			
			foreach ($terms as $term) {
				while ($term->parent) {
					$term = get_term($term->parent, $term->taxonomy);
				}
				
				if ((!is_singular('apps-books') || !in_array($term->slug, [
					'apps',
					'our-books'
				])) && !in_array($term->slug, $unique_slugs)) {
					$parent_terms[] = $term;
					$unique_slugs[] = $term->slug;
				}
			}
			
			if (!$return_wp_term) {
				$term = $terms[0]->name;
			}
		}
		
		return array_unique($parent_terms, SORT_REGULAR);
	}
	
	public function tiny_mce_before_init($options) {
		return $options + [
			'style_formats' => json_encode([
				[
					'block' => 'p',
					'classes' => 'button',
					'inline' => 'a',
					'title' => 'Button',
					'wrapper' => false
				],
				[
					'classes' => 'dropcap',
					'inline' => 'span',
					'title' => 'Drop Cap (no bg)',
					'wrapper' => false
				],
				[
					'classes' => 'dropcap dropcap-dark',
					'inline' => 'span',
					'title' => 'Drop Cap (dark blue bg)',
					'wrapper' => false
				],
				[
					'classes' => 'dropcap dropcap-light',
					'inline' => 'span',
					'title' => 'Drop Cap (light blue bg)',
					'wrapper' => false
				]
			]),
			'table_class_list' => json_encode([
				[
					'title' => 'None',
					'value' => ''
				],
				[
					'title' => 'Style #1',
					'value' => 'table-style-1'
				],
				[
					'title' => 'Style #2',
					'value' => 'table-style-2'
				],
				[
					'title' => 'Style #3',
					'value' => 'table-style-3'
				]
			])
		];
	}
	
	protected function __transfer_user_to_ee($user_id, $update = false) {
		// Connect to old DB
		$wpdb2 = new \wpdb('akarmelc_akcousr', 's)lv^OllHPhM', 'akarmelc_akcomdb', 'localhost');
		
		$data = [];
		
		// Get the userdata
		$userdata = get_userdata($user_id);
		
		// This gives us first name, last name, email
		$data['first_name'] = $userdata->first_name;
		$data['last_name'] = $userdata->last_name;
		$data['email'] = $userdata->user_email;
		$data['join_date'] = strtotime($userdata->user_registered);
		
		// Title
		$data['title'] = get_field('title', 'user_' . $user_id);
		
		// Date of Birth
		$data['dob'] = \DateTime::createFromFormat('d/m/Y H:i:s', get_field('date_of_birth', 'user_' . $user_id) . ' 00:00:00');
		
		if ($data['dob']) {
			$data['dob'] = $data['dob']->getTimestamp();
		}
		
		// Pregnant
		if ('Yes' == ($data['pregnant'] = (get_field('are_you_pregnant', 'user_' . $user_id)
			? 'Yes'
			: 'No'
		))) {
			$data['due_date'] = strtotime(get_field('when_are_you_expecting', 'user_' . $user_id));
		}
		
		// Children
		if (get_field('do_you_have_any_children', 'user_' . $user_id)) {
			$data['children'] = array_map(function($item) {
				return \DateTime::createFromFormat('d/m/Y H:i:s', $item['childs_birthday'] . ' 00:00:00')->getTimestamp();
			}, array_slice((get_field('children', 'user_' . $user_id) ?: []), 0, 5));
			
			$data['num_children'] = count($data['children']);
		}
		
		// Address
		$data['address_line_1'] = get_field('address_line_1', 'user_' . $user_id);
		$data['address_line_2'] = get_field('address_line_2', 'user_' . $user_id);
		$data['city'] = get_field('city', 'user_' . $user_id);
		$data['county'] = get_field('county', 'user_' . $user_id);
		$data['postcode'] = get_field('postcode', 'user_' . $user_id);
		$data['country'] = get_field('country', 'user_' . $user_id);
		
		if (!$update) {
			// Now let's try entering the user in
			$wpdb2->insert('exp_members', [
				'group_id' => 5,
				'username' => $data['email'],
				'email' => $data['email'],
				'join_date' => $data['join_date']
			], [
				'%d',
				'%s',
				'%s',
				'%s'
			]);
			$member_id = $wpdb2->insert_id;
			
			// Now add an extra into exp_channel_titles
			$wpdb2->insert('exp_channel_titles', [
				'site_id' => 1,
				'channel_id' => 19,
				'author_id' => $member_id,
				'title' => $data['email'],
				'status' => 'Members-id5',
				'entry_date' => $data['join_date']
			], [
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%d'
			]);
			$entry_id = $wpdb2->insert_id;
		}
		
		// And finally add the exp_channel_data entry
		$entries = [
			'entry_id' => $entry_id,
			'site_id' => 1,
			'channel_id' => 19,
			'field_id_76' => $data['first_name'], // First Name
			'field_id_77' => $data['last_name'], // Last Name
			'field_id_78' => ('Mr' == $data['title']
				? 'Male'
				: 'Female'
			), // Gender (based upon Title)
			'field_id_79' => $data['dob'], // Date of Birth
			'field_id_85' => $data['address_line_1'], // Address Line 1
			'field_id_86' => $data['address_line_2'], // Address Line 2
			'field_id_87' => $data['city'], // City
			'field_id_88' => $data['county'], // County
			'field_id_89' => $data['country'], // Country
			'field_id_90' => $data['postcode'], // Postcode
			'field_id_93' => $data['pregnant'], // Pregnant
			'field_id_106' => $data['num_children'] // # of Children
		];
		$format = [
			'%d',
			'%d',
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d'
		];
		
		// Add due date?
		if ('Yes' == $data['pregnant'] && $data['due_date']) {
			$entries['field_id_94'] = $data['due_date'];
			$format[] = '%d';
		}
		
		// Add children's DOBs?
		$index_to_id = [
			96,
			99,
			102,
			107,
			110
		];
		
		if (0 < $data['num_children'] && $data['children']) {
			foreach ($data['children'] as $k => $childs_dob) {
				$entries['field_id_' . $index_to_id[$k]] = $childs_dob;
				$format[] = '%d';
			}
		}
		
		// And insert!
		if (!$update) {
			$wpdb2->insert('exp_channel_data', $entries, $format);
		} elseif ($entry_id = $wpdb2->get_var($wpdb2->prepare('select entry_id from exp_channel_titles where channel_id = 19 and title = %s', $data['email']))) {
			unset($entries['entry_id']);
			$format = array_slice($format, 1);
			
			$wpdb2->update('exp_channel_data', $entries, [
				'entry_id' => $entry_id
			], $format, [
				'%d'
			]);
		}
	}
	
	public function verify_user_pass($args, $content = null) {
		// lets make usage of the custom global variable to fetch
		// the values from the safe / cookie validation functions
		global $gf_reset_user;
		
		// start output buffering for a more visually appealing output
		ob_start();
		
		if ( !$gf_reset_user || is_wp_error( $gf_reset_user ) ) {
			if ( $gf_reset_user && $gf_reset_user->get_error_code() === 'expired_key' )
				echo 'The key has expired.';
			else
				echo 'The key is invalid.';
		} else {
			echo do_shortcode( $content );
		}
		
		return ob_get_clean();
	}
	
	public function views_edit_recipes($views) {
		global $wpdb;
		
		$count = $wpdb->get_var($wpdb->prepare('select count(distinct p.ID) from wp_posts p inner join wp_icl_translations it on p.ID = it.element_id and it.language_code = %s where p.post_type = \'recipes\' and p.ID not in (select p.ID from wp_posts p inner join wp_term_relationships tr on p.ID = tr.object_id inner join wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id and tt.taxonomy = \'recipe-category\' and tt.parent > 0 where p.post_type = \'recipes\') and p.post_status != \'trash\'', ICL_LANGUAGE_CODE));
		
		$views['missing-subcategory'] = '<a href="edit.php?post_type=recipes&missing_subcategory=1"' . (isset($_GET['missing_subcategory']) && $_GET['missing_subcategory']
			? ' class="current"'
			: ''
		) . '>Recipes Missing Subcategories / Ages <span class="count">(' . $count . ')</span></a>';
		
		return $views;
	}
	
	public function widgets_init() {
		register_sidebar([
			'after_title' => '</h2>',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'before_widget' => '<div class="%2$s widget" id="%1$s">',
			'id' => 'sidebar',
			'name' => 'Sidebar'
		]);
	}
	
	public function wp_doin_verify_user_key() {
		global $gf_reset_user;
		
		// analyze wp-login.php for a better understanding of these values
		list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
		$rp_cookie = 'wp-resetpass-' . COOKIEHASH;
		// lets redirect the user on pass change, so that nobody could spoof his key
		if ( isset( $_GET['key'] ) and isset( $_GET['method'] ) ) {
			if ( $_GET['method'] == 'gf' ) {
				$value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
				setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
				wp_safe_redirect( remove_query_arg( array( 'key', 'login', 'method' ) ) );
				exit;
			}
		}
		// lets compare the validation cookie with the hash key stored with the database data
		// if they match user data will be returned
		if ( isset( $_COOKIE[$rp_cookie] ) && 0 < strpos( $_COOKIE[$rp_cookie], ':' ) ) {
			list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[$rp_cookie] ), 2 );
			$user = check_password_reset_key( $rp_key, $rp_login );
			if ( isset( $_POST['pass1'] ) && !hash_equals( $rp_key, $_POST['rp_key'] ) ) {
				$user = false;
			}
		} else {
			$user = false;
		}
		// if any error occured make sure to remove the validation cookie
		if ( !$user || is_wp_error( $user ) ) {
			setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
		}
		// make sure our user is available for later reference
		$gf_reset_user = $user;
	}
	
	public function wp_enqueue_scripts() {
		wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Glegoo:400,700|Open+Sans:400,600,700|Rosario:400,700,400italic,700italic');
		wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css');
		
		wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/d0ef08d14a.js');
		wp_enqueue_script('jquery-verb', 'https://code.jquery.com/jquery-1.12.4.min.js');
		wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', [
			'jquery-verb'
		], false, true);



	}
	
	public function wp_login_failed($username) {
		global $wpdb;
		if ($wpdb->get_var($wpdb->prepare('select signup_id from ' . $wpdb->prefix . 'signups where user_login = %s or user_email = %s', $username, $username))) {
			if (($referer = wp_get_referer()) && !strstr($referer, 'wp-admin') && !strstr($referer, 'wp-admin')) {
				wp_redirect(add_query_arg('ak-login', 'pending', $referer));
				
				exit;
			}
		}
		if (($referer = wp_get_referer()) && !strstr($referer, 'wp-admin') && !strstr($referer, 'wp-admin')) {
			wp_redirect(add_query_arg('ak-login', 'failed', $referer));
			
			exit;
		}
	}
	
	public function wp_login_form($args = []) {
		$echo = (isset($args['echo'])
			? $args['echo']
			: true
		);
		$args['echo'] = false;
		
		$form = str_replace([
			'name="log"',
			'name="pwd"'
		], [
			'name="log" placeholder="Email address..."',
			'name="pwd" placeholder="Password..."'
		], wp_login_form($args));
		
		if ($echo) {
			echo $form;
		} else {
			return $form;
		}
	}
	
	public function wp_pro_ads_banner_limit_categories($taxonomies) {
		return array_merge($taxonomies, [
			'recipe-category',
			'recipe-filter',
			'allergen',
			'product-category',
			'app-book-category',
			'video-category'
		]);
	}
	
	public function wp_update_nav_menu($menu_id) {
		delete_transient('ak_header_menu_' . ICL_LANGUAGE_CODE);
	}
	
	public function wpseo_metabox_prio() {
		return 'low';
	}
}
$GLOBALS['Annabel_Karmel'] = new Annabel_Karmel;

class Content_Transfer_Functions {
	public function __construct() {
		add_action('wp_loaded', [
			$this,
			'wp_loaded'
		]);
	}
	
	public function wp_loaded() {
		global $wpdb, $wpdb2;
			
		if (1 == get_current_user_id() && !is_admin() && isset($_GET['ctf']) && $_GET['ctf']) {
			set_time_limit(0);
			
			$time = microtime(true);
			
			//header('Content-Type: text/plain');
			
			// Files we need
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/media.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/file.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/image.php';
			
			$wpdb2 = new \wpdb('akrecipe_akrecip', 'wd;ppc{[KI0o', 'akrecipe_akrecipes', 'localhost');
			
			$this->users();
			
			echo (microtime(true) - $time) . ' elapsed';
			
			exit;
		}
	}
	
	protected function cats_to_filters() {
		global $wpdb;
		
		foreach ($wpdb->get_col('select ID from wp_posts where post_type = \'recipes\'') as $post_id) {
			$filters = [];
			
			foreach (get_the_terms($post_id, 'recipe-category') as $term) {
				if (!in_array(($term->parent ?: $term->term_id), [
					296,
					319,
					380,
					381
				])) {
					$filters[] = $term->slug;
				}
			}
			
			if ($filters) {
				wp_set_object_terms($post_id, $filters, 'recipe-filter');
			}
		}
	}
	
	protected function filters_to_allergens() {
		global $wpdb;
		
		foreach ($wpdb->get_col('select ID from wp_posts where post_type = \'recipes\'') as $post_id) {
			$allergens = [];
			
			foreach (get_the_terms($post_id, 'recipe-filter') as $term) {
				if (get_term_by('slug', $term->slug, 'allergen')) {
					$allergens[] = $term->slug;
				}
			}
			
			if ($allergens) {
				wp_set_object_terms($post_id, $allergens, 'allergen');
			}
		}
	}
	
	protected function gated() {
		global $wpdb;
		
		$recipes = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/serialised-recipes.txt'));
		$count = count($recipes);
		
		foreach ($recipes as $k => $recipe) {
			if ($recipe['gated']) {
				foreach ($wpdb->get_col($wpdb->prepare('select ID from wp_posts where post_type = \'recipes\' and post_title = %s', $recipe['title'])) as $post_id) {
					// Gated
					update_field('field_579cb45bbbeb0', 1, $post_id);
				}
			}
			
			echo 'Done ' . ($k + 1) . '/' . $count . ': ' . $recipe['title'] . ".\n";
		}
		
		exit;
	}
	
	protected function recipe_apps_books() {
		global $wpdb;
		
		if ($f = fopen($_SERVER['DOCUMENT_ROOT'] . '/Recipe Book Match Final.csv', 'r')) {
			while (false !== ($row = fgetcsv($f))) {
				if ('Title' != $row[1] && $row[2]) {
					if ($id = $wpdb->get_var($wpdb->prepare('select p.ID from wp_posts p inner join wp_icl_translations it on p.ID = it.element_id and it.language_code = %s where p.post_title = %s', ICL_LANGUAGE_CODE, $row[1]))) {
						$apps_books = [];
						
						foreach (explode('|', $row[2]) as $app_book) {
							if ($app_book_id = $wpdb->get_var($wpdb->prepare('select p.ID from wp_posts p inner join wp_icl_translations it on p.ID = it.element_id and it.language_code = %s where p.post_title = %s', ICL_LANGUAGE_CODE, $app_book))) {
								$apps_books[] = $app_book_id;
							}
						}
						
						if ($apps_books) {
							update_field('field_57e3717844644', $apps_books, $id);
						}
					}
				}
			}
			
			echo 'Done\'d.';
			
			fclose($f);
		}
	}
	
	protected function __recipe_categories($term, $parent = 0) {
		global $wpdb2;
		
		// Create the term
		$new_term = wp_insert_term($term->name, 'recipe-category', [
			'slug' => $term->slug,
			'parent' => $parent
		]);
		
		// Pull out children
		foreach ($wpdb2->get_results($wpdb2->prepare('select t.* from wp_terms t inner join wp_term_taxonomy tt on t.term_id = tt.term_id and tt.taxonomy = \'recipe-category\' and tt.parent = %d order by t.name asc', $term->term_id)) as $term) {
			$this->__recipe_categories($term, $new_term['term_id']);
		}
	}
	
	protected function recipe_categories() {
		global $wpdb2;
		
		foreach ($wpdb2->get_results('select t.* from wp_terms t inner join wp_term_taxonomy tt on t.term_id = tt.term_id and tt.taxonomy = \'recipe-category\' and tt.parent = 0 order by t.name asc') as $term) {
			$this->__recipe_categories($term);
		}
	}
	
	protected function recipes() {
		$recipes = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/serialised-recipes (1).txt'));
		$count = count($recipes);
		
		foreach ($recipes as $k => $recipe) {
			if ($post_id = wp_insert_post([
				'post_date' => $recipe['date'],
				'post_content' => $recipe['content'],
				'post_title' => $recipe['title'],
				'post_status' => 'publish',
				'post_type' => 'recipes'
			])) {
				// Ingredients
				if ($recipe['ingredient_sections']) {
					update_field('field_578f1a8af6e46', array_map(function($item) {
						$item['ingredients'] = implode("\n", $item['ingredients']);
						
						return $item;
					}, $recipe['ingredient_sections']), $post_id);
				}
				
				// Method
				if ($recipe['method']) {
					update_field('field_578f1b1ff6e4b', $recipe['method'], $post_id);
				}
				
				// Prep Time
				if ($recipe['prep_time']) {
					update_field('field_578f1b43f6e4d', $recipe['prep_time'], $post_id);
				}
				
				// Cook Time
				if ($recipe['cook_time']) {
					update_field('field_578f1b5af6e4e', $recipe['cook_time'], $post_id);
				}
				
				// Yield
				if ($recipe['serves__makes']) {
					update_field('field_578f1b93f6e4f', $recipe['serves__makes'], $post_id);
					update_field('field_578f1bb0f6e50', $recipe[$recipe['serves__makes']], $post_id);
				}
				
				// Suitable for Freezing?
				if ($recipe['suitable_for_freezing']) {
					update_field('field_578f1bc9f6e51', 1, $post_id);
				}
				
				// Featured Image
				if ($recipe['featured_image']) {
					// Attach image to the post
					media_sideload_image($recipe['featured_image'], $post_id);
					
					// Pull attachment ID...
					$the_query = new \WP_Query([
						'post_parent' => $post_id,
						'post_status' => 'any',
						'post_type' => 'attachment',
						'posts_per_page' => 1
					]);
					
					// ... and set as the post thumbnail
					if ($the_query->have_posts()) {
						$the_query->the_post();
						set_post_thumbnail($post_id, get_the_ID());
						wp_reset_postdata();
					}
				}
				
				// Categories
				if ($recipe['categories']) {
					wp_set_object_terms($post_id, $recipe['categories'], 'recipe-category');
				}
				
				// Gated
				if ($recipe['gated']) {
					update_field('field_579cb45bbbeb0', 1, $post_id);
				}
				
				echo 'Done ' . ($k + 1) . '/' . $count . ': ' . $recipe['title'] . ".\n";
			} else {
				echo 'Problem with ' . $recipe['title'] . ".\n";
			}
		}
	}
	
	protected function users() {
		global $wpdb;
		
		foreach ($wpdb->get_results('select * from exp_members order by member_id asc limit 0, 10') as $user) {
			$ee_user_data = $wpdb->get_results($wpdb->prepare('select * from exp_member_data where member_id = %d', $user->member_id));
			
			$user_data = [
				'user_pass' => $user->password,
				'user_login' => $user->username,
				'user_email' => $user->email,
				'user_registered' => date('Y-m-d H:i:s', $user->join_date)
			];
			
			// First Name
			if ($ee_user_data->m_field_id_1) {
				$user_data['first_name'] = $ee_user_data->m_field_id_1;
			}
			
			// Last Name
			if ($ee_user_data->m_field_id_2) {
				$user_data['last_name'] = $ee_user_data->m_field_id_2;
			}
			
			// Display Name
			if ($user->screen_name) {
				$user_data['display_name'] = $user->screen_name;
			} elseif ($ee_user_data->m_field_id_1 || $ee_user_data->m_field_id_2) {
				$user_data['display_name'] = implode(' ' , array_filter([
					$ee_user_data->m_field_id_1,
					$ee_user_data->m_field_id_2
				]));
			}
			
			if (!is_wp_error($user_id = wp_insert_user($user_data))) {
				// Set the old member_id, password and salt
				$wpdb->update($wpdb->prefix . 'users', [
					'old_id' => $user->member_id,
					'user_pass' => $user->password,
					'user_salt' => $user->salt
				], [
					'ID' => $user_id
				], [
					'%d',
					'%s',
					'%s'
				], [
					'%d'
				]);
				
				// Date of Birth
				if ($user->bday_d && $user->bday_m && $user->bday_y && ($datetime = \DateTime::createFromFormat('Ynj', $user->bday_y . $user->bday_m . $user->bday_d))) {
					update_field('field_57a8ceacc106c', $datetime->format('Ymd'), 'user_' . $user_id);
				}
				
				// Are you pregnant?
				update_field('field_57a8cec5c106d', 0, 'user_' . $user_id);
				
				// Do you have any children?
				update_field('field_57a8cf03c106f', 0, 'user_' . $user_id);
			}
		}
	}
}
new Content_Transfer_Functions;

class Content_Transfer_Functions_2 {
	public function __construct() {
		add_action('wp_loaded', [
			$this,
			'wp_loaded'
		]);
	}
	
	public function wp_loaded() {
		global $wpdb, $wpdb2;
			
		if (1 == get_current_user_id() && !is_admin() && isset($_GET['ctf2']) && $_GET['ctf2']) {
			set_time_limit(0);
			
			header('Content-Type: text/plain');
			
			// Files we need
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/media.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/file.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/image.php';
			
			$wpdb2 = new \wpdb('akarmelc_akcousr', 's)lv^OllHPhM', 'akarmelc_akcomdb', 'localhost');
			
			$this->push_user_data();
			
			exit;
		}
	}
	
	public function push_user_data() {
		global $wpdb, $wpdb2;
		
		// Let's run through 'em all...
		$counter = 0;
		$count = count($user_ids = $wpdb->get_col('select ID from ' . $wpdb->prefix . 'users where user_salt is null order by ID asc'));
		
		foreach ($user_ids as $user_id) {
			$data = [];
			
			// Get the userdata
			$userdata = get_userdata($user_id);
			
			// This gives us first name, last name, email
			$data['first_name'] = $userdata->first_name;
			$data['last_name'] = $userdata->last_name;
			$data['email'] = $userdata->user_email;
			$data['join_date'] = strtotime($userdata->user_registered);
			
			// Title
			$data['title'] = get_field('title', 'user_' . $user_id);
			
			// Date of Birth
			$data['dob'] = \DateTime::createFromFormat('d/m/Y H:i:s', get_field('date_of_birth', 'user_' . $user_id) . ' 00:00:00');
			
			if ($data['dob']) {
				$data['dob'] = $data['dob']->getTimestamp();
			}
			
			// Pregnant
			if ('Yes' == ($data['pregnant'] = (get_field('are_you_pregnant', 'user_' . $user_id)
				? 'Yes'
				: 'No'
			))) {
				$data['due_date'] = strtotime(get_field('when_are_you_expecting', 'user_' . $user_id));
			}
			
			// Children
			if (get_field('do_you_have_any_children', 'user_' . $user_id)) {
				$data['children'] = array_map(function($item) {
					return \DateTime::createFromFormat('d/m/Y H:i:s', $item['childs_birthday'] . ' 00:00:00')->getTimestamp();
				}, array_slice((get_field('children', 'user_' . $user_id) ?: []), 0, 5));
				
				$data['num_children'] = count($data['children']);
			}
			
			// Address
			$data['address_line_1'] = get_field('address_line_1', 'user_' . $user_id);
			$data['address_line_2'] = get_field('address_line_2', 'user_' . $user_id);
			$data['city'] = get_field('city', 'user_' . $user_id);
			$data['county'] = get_field('county', 'user_' . $user_id);
			$data['postcode'] = get_field('postcode', 'user_' . $user_id);
			$data['country'] = get_field('country', 'user_' . $user_id);
			
			// Now let's try entering the user in
			$wpdb2->insert('exp_members', [
				'group_id' => 5,
				'username' => $data['email'],
				'email' => $data['email'],
				'join_date' => $data['join_date']
			], [
				'%d',
				'%s',
				'%s',
				'%s'
			]);
			$member_id = $wpdb2->insert_id;
			
			// Now add an extra into exp_channel_titles
			$wpdb2->insert('exp_channel_titles', [
				'site_id' => 1,
				'channel_id' => 19,
				'author_id' => $member_id,
				'title' => $data['email'],
				'status' => 'Members-id5',
				'entry_date' => $data['join_date']
			], [
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%d'
			]);
			$entry_id = $wpdb2->insert_id;
			
			// And finally add the exp_channel_data entry
			$entries = [
				'entry_id' => $entry_id,
				'site_id' => 1,
				'channel_id' => 19,
				'field_id_76' => $data['first_name'], // First Name
				'field_id_77' => $data['last_name'], // Last Name
				'field_id_78' => ('Mr' == $data['title']
					? 'Male'
					: 'Female'
				), // Gender (based upon Title)
				'field_id_79' => $data['dob'], // Date of Birth
				'field_id_85' => $data['address_line_1'], // Address Line 1
				'field_id_86' => $data['address_line_2'], // Address Line 2
				'field_id_87' => $data['city'], // City
				'field_id_88' => $data['county'], // County
				'field_id_89' => $data['country'], // Country
				'field_id_90' => $data['postcode'], // Postcode
				'field_id_93' => $data['pregnant'], // Pregnant
				'field_id_106' => $data['num_children'] // # of Children
			];
			$format = [
				'%d',
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d'
			];
			
			// Add due date?
			if ('Yes' == $data['pregnant'] && $data['due_date']) {
				$entries['field_id_94'] = $data['due_date'];
				$format[] = '%d';
			}
			
			// Add children's DOBs?
			$index_to_id = [
				96,
				99,
				102,
				107,
				110
			];
			
			if (0 < $data['num_children'] && $data['children']) {
				foreach ($data['children'] as $k => $childs_dob) {
					$entries['field_id_' . $index_to_id[$k]] = $childs_dob;
					$format[] = '%d';
				}
			}
			
			// And insert!
			$wpdb2->insert('exp_channel_data', $entries, $format);
			
			echo ++$counter . '/' . $count . "\n";
			flush();
		}
	}

}
new Content_Transfer_Functions_2;
add_filter( 'wpseo_canonical', '__return_false' );

if( function_exists('acf_add_options_page') ) {
	
	
	/*acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));*/
	
}
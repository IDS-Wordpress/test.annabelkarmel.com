<?php
/**
 * Template Name: Logout
 */
wp_logout();
wp_redirect(icl_get_home_url(), 302);

exit;
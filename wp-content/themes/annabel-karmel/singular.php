<?php get_header(); ?>

<?php if (have_posts()): ?>

<?php while (have_posts()): the_post(); ?>

<?php if (function_exists('bcn_display')): ?>

<section class="breadcrumbs">

	<div class="container">

		<?php bcn_display(); ?>

	</div>

</section>

<?php endif; ?>

<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>



<section class="generic-page">

	<div class="container">

		<div class="table">

			<div class="cell">

				<div class="singular-wrapper">

					<div class="page-header<?php if (get_field('sponsored_content')) echo ' sponsored'; ?>" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting">

						<meta content="<?php echo the_time('Y-m-d'); ?>" itemprop="datePublished">

						<meta content="<?php the_permalink(); ?>" itemprop="url">

						 <div class="title-allergens">

						 <h1 itemprop="name"><?php the_title(); ?><?php if( get_field('blog_post_title') ): ?><br /> <?php the_field('blog_post_title'); endif; ?></h1>            

						<?php if( get_field('apps_&_books_title_2') ): ?><?php the_field('apps_&_books_title_2'); ?> <?php endif; ?>


             <?php if(get_field('sponsor_logo')): ?>

                            <a href="<?php  echo get_field( "sponsor_url", get_the_ID() ); ?>" target="_blank"><img src="<?php the_field('sponsor_logo' ) ?>"></a>

                            <?php else: ?>

                            <?php endif; ?> </div>

						<?php get_template_part('parts/save-share'); ?>

                        <?php 
$image = get_field('image');
if( !empty($image) ): ?>
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>

<?php if ( get_field('mark_this_as_featured_image') == false ) { ?>


						<?php if ($banner = get_field('banner_image')): ?>

						<img alt="<?php echo esc_attr($banner['alt'] ?: $banner['title']); ?>" itemprop="image thumbnailUrl" src="<?php echo $banner['sizes']['single-header']; ?>" title="<?php echo esc_attr($banner['title']); ?>">

						<?php elseif (has_post_thumbnail()): ?>

						<?php the_post_thumbnail('single-header', [

							'itemprop' => 'image thumbnailUrl'

						]); ?>

						<?php endif; ?>


<?php } else { ?>
<?php } ?>
					</div>

                  <?php /*?> <?php if (get_the_content()): ?>

					<div style="padding:0 2em 2em 2em;"class="wysiwyg-content itemprop="description">

						<?php the_content(); ?>

					</div>

					<div class="clear"></div>

					<?php endif; ?><?php */?>

					<?php get_template_part('parts/flexible-content'); ?>

                    

					<div class="page-footer">

						<?php get_template_part('parts/save-share'); ?>

					</div>

				</div>

				<?php if ('post' == get_post_type()): ?>

				

				<?php if ($articles = get_field('related_articles')): ?>

				<div class="related-content">

					<h2>Related Articles  </h2>
					<div class="clear"></div>

	<?php /*?><div class="ak-post-widgets fi-panel">
						 <?php while ( $block1Featured->have_posts() ) : $block1Featured->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div><?php */?>

<div class="ak-post-widgets fi-panel">
<?php 
$posts = get_field('related_articles_new');
if( $posts ): ?>
<?php foreach( $posts as $post ):  ?>
          
                <a href="<?php echo get_permalink( $post->ID ); ?>"></a>
<div class="post-widget">
<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
	
		<div class="post-widget-header">
			<div class="rec_tit">
				<h4><?php echo get_the_title( $post->ID ); ?></h4>
			</div>
			<?php if (is_array($top_cats = $GLOBALS['Annabel_Karmel']->top_cat($GLOBALS['post'], true))): $top_cat = $top_cats[0]->name; ?>
			<span><?php echo implode(' / ', array_map(function($item) {
				return $item->name;
			}, $top_cats)); ?></span>
			<?php elseif ($top_cats): ?>
			<span><?php echo $top_cats; ?></span>
			<?php endif; ?>
		</div>	
		<div class="post-widget-thumbnail">
			<?php the_post_thumbnail(in_array(get_post_type(), [
				'apps-books',
				'products'
							])
				? 'verb-ak-post-widget-padded'
				: 'verb-ak-post-widget'
			); ?>
		</div>
	</a>
	<?php if (($bottom_cat = (get_sub_field('tagline') ?: $GLOBALS['Annabel_Karmel']->bottom_cat($GLOBALS['post']))) && $bottom_cat != $top_cat): ?>
	<div class="bottom-cat">
		<?php echo $bottom_cat; ?>
	</div>
	<?php endif; ?>
	
	<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
		<a>SAVE</a>
	</div>
</div>
<?php endforeach; ?>
<?php wp_reset_postdata();?>
<?php endif; ?>
</div>

</div>
				<?php wp_reset_postdata(); ?>

				<?php else: ?>

				<?php $args = [

					'orderby' => 'rand',

					'post_type' => 'post',

					'post__not_in' => [

						get_the_ID()

					],

					'posts_per_page' => 3

				]; ?>

				<?php if ($terms = get_the_terms(get_the_ID(), 'category')): ?>

				<?php $args['tax_query'] = array_merge([

					'relation' => 'OR'

				], array_map(function($item) {

					return [

						'taxonomy' => $item->taxonomy,

						'terms' => $item->term_id

					];

				}, $terms)); ?>

				<?php endif; ?>

				<?php $the_query = new WP_Query($args); ?>

				<?php if ($the_query->have_posts()): ?>

				<div class="related-content">

					<h2>Related Articles </h2>

					<div class="clear"></div>

				<?php /*?>	<div class="ak-post-widgets fi-panel">

						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>

						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>

						<?php endwhile; ?>

					</div><?php */?>
<?php if (get_field('related_articles_new')) : ?>
<div class="ak-post-widgets fi-panel">
<?php 
$posts = get_field('related_articles_new');
if( $posts ): ?>
<?php foreach( $posts as $post ):  ?>
          
                <a href="<?php echo get_permalink( $post->ID ); ?>"></a> 
<div class="post-widget">
	<a<?php if (has_post_thumbnail()) echo ' class="has-post-thumbnail"';?> href="<?php the_permalink(); ?>">
		<div class="post-widget-header">
			<div class="rec_tit">
				<h4><?php echo get_the_title( $post->ID ); ?></h4>
			</div>
			<?php if (is_array($top_cats = $GLOBALS['Annabel_Karmel']->top_cat($GLOBALS['post'], true))): $top_cat = $top_cats[0]->name; ?>
			<span><?php echo implode(' / ', array_map(function($item) {
				return $item->name;
			}, $top_cats)); ?></span>
			<?php elseif ($top_cats): ?>
			<span><?php echo $top_cats; ?></span>
			<?php endif; ?>
		</div>	
		<div class="post-widget-thumbnail">
			<?php the_post_thumbnail(in_array(get_post_type(), [
				'apps-books',
				'products'
							])
				? 'verb-ak-post-widget-padded'
				: 'verb-ak-post-widget'
			); ?>
		</div>
	</a>
	<?php if (($bottom_cat = (get_sub_field('tagline') ?: $GLOBALS['Annabel_Karmel']->bottom_cat($GLOBALS['post']))) && $bottom_cat != $top_cat): ?>
	<div class="bottom-cat">
		<?php echo $bottom_cat; ?>
	</div>
	<?php endif; ?>
	
	<div class="save" data-save-id="<?php echo get_the_ID(); ?>">
		<a>SAVE</a>
	</div>
</div>
<?php endforeach; ?>
<?php wp_reset_postdata();?>
<?php endif; ?>
</div>
<?php else : ?>

	<div class="ak-post-widgets fi-panel">

						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>

						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>

						<?php endwhile; ?>

					</div>

<?php endif; ?>
				</div>

				<?php wp_reset_postdata(); ?>

				<?php endif; ?>

				<?php endif; ?>

				

				<?php $args = [

					'orderby' => 'rand',

					'post_type' => [

						'apps-books',

						'products'

					],

					'posts_per_page' => 6

				];

				

				if ($terms = get_the_terms(get_the_ID(), 'category', [

					'parent' => 0

				])) {

					$args['tax_query'] = [

						'relation' => 'OR',

						[

							'field' => 'slug',

							'taxonomy' => 'product-category',

							'terms' => ($terms = array_map(function($item) {

								return $item->slug;

							}, $terms))

						],

						[

							'field' => 'slug',

							'taxonomy' => 'app-book-category',

							'terms' => $terms

						]

					];

				} ?>

				<?php $the_query = new WP_Query($args); ?>

				<?php if ($the_query->have_posts()): ?>

				<div class="related-content">

					<h2>Products You'll Love</h2>

					<div class="clear"></div>

					<div class="ak-post-widgets owl-carousel owl-theme">

						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>

						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>

						<?php endwhile; ?>

					</div>

				</div>

				<?php wp_reset_postdata(); ?>

				<?php endif; ?>

				

				<?php $the_query = new WP_Query([

					'meta_key' => 'force_popular',

					'meta_value' => 1,

					'post_type' => 'post',

					'posts_per_page' => 4

				]); ?>

				<?php if ($the_query->have_posts()): ?>

				<div class="related-content">

					<h2>Popular Features</h2>

					<div class="clear"></div>

					<div class="ak-post-widgets fi-panel wide-ak-post-widgets">

						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>

						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>

						<?php endwhile; ?>

					</div>

				</div>

				<?php wp_reset_postdata(); ?>

				<?php endif; ?>

				

				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>

		</div>

	</div>

</section>

<?php endwhile; ?>

<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

	<div class="pswp__bg"></div>

	<div class="pswp__scroll-wrap">

		<div class="pswp__container">

			<div class="pswp__item"></div>

			<div class="pswp__item"></div>

			<div class="pswp__item"></div>

		</div>

		<div class="pswp__ui pswp__ui--hidden">

			<div class="pswp__top-bar">

				<div class="pswp__counter"></div>

				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

				<div class="pswp__preloader">

					<div class="pswp__preloader__icn">

						<div class="pswp__preloader__cut">

							<div class="pswp__preloader__donut"></div>

						</div>

					</div>

				</div>

			</div>

			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">

				<div class="pswp__share-tooltip"></div> 

			</div>

			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>

			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>

			<div class="pswp__caption">

				<div class="pswp__caption__center"></div>

			</div>

		</div>

	</div>

</div>

<?php endif; ?>

<?php get_footer(); ?>
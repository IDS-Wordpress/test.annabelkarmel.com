<?php
define('WP_USE_THEMES', false);
require('../../wp-load.php');
include('classes/order_api.php');

/****** Auth Username/Password for Authorization***************/
$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$obj = new API_ORDER($username,$password);

/****** Parameters for routing on base of action***************/
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

/****** Start Data Recieving from Json Raw Data OR Form Post Data***************/
$params = (array) json_decode(file_get_contents('php://input'), TRUE);
if(!empty($params)) {	$data = $params; }
else {	$data = isset($_POST)?$_POST : array(); }
/****** End Data Recieving from Json Raw Data OR Form Post Data***************/

switch ($action)
{
	case 'capture':				
		$obj->capture_order($data);
		break;
	case 'checker':				
		$obj->order_checker($data);
		break;
	default:
		$obj->response(array('status'=>'fail','error_code'=>400,'response_msg'=>'Bad Request'));
		header("HTTP/1.0 400 Bad Request");	
		break;			
}
?>	 
<?php global $fp_col; $fp_col = false; ?>
<?php get_header(); ?>
<h1 id="page-titles">Annabel Karmel - Recipes & Advice for Babies</h1>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<?php $GLOBALS['tc_return_pt'] = true; ?>
				<?php if (have_rows('front_page')): ?>
				<div class="hero-carousel-section">
					<?php while (have_rows('front_page')): the_row(); ?>
					<?php if ('hero_carousel' == ($row_layout = get_row_layout())): ?>
						<?php get_template_part('parts/flexible-hero_carousel'); ?>
					<?php endif; endwhile; ?>
				</div>	
				<div class="front-page-board">
					<?php while (have_rows('front_page')): the_row(); ?>
					<?php if ('advertisement' == ($row_layout = get_row_layout())): ?>
					<?php if (!$fp_col): $fp_col = true; ?>
					<div class="fp-col">
					<?php endif; ?>
					<div class="front-advert"><?php echo do_shortcode(get_sub_field('ads_shortcode')); ?></div>
					<?php elseif ('small_cards' == ($row_layout = get_row_layout())): ?>
					<?php if (!$fp_col): $fp_col = true; ?>
					<div class="fp-col">
					<?php endif; ?>
						<div class="fp-small-cards">
							<?php while (have_rows('fp_content')): the_row(); ?>
							<?php $post = get_sub_field('content'); setup_postdata($post); ?>
							<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
							<?php endwhile; ?>
						</div>
					<?php wp_reset_postdata(); ?>
					<?php elseif ('large_card__slider' == $row_layout): ?>
					<?php if (!$fp_col): $fp_col = true; ?>
					<div class="fp-col">
					<?php endif; ?>
					<?php if(get_sub_field('heading')){ ?><div class="recipe-filter"><h2><?php echo get_sub_field('heading'); ?></h2></div> <?php } ?>
					<div class="owl-carousel owl-theme" data-items="1">
						<?php while (have_rows('fp_content')): the_row(); ?>
						<?php $post = get_sub_field('content'); setup_postdata($post); ?>
                        
						<div class="item">
							<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false, true); ?>
						</div>
						<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					</div>
					<?php elseif ('break' == $row_layout && $fp_col): ?>
					</div>
					<div class="fp-col">
					<?php elseif ('large_card' == $row_layout): ?>
					<?php if ($fp_col): $fp_col = false; ?>
					</div>
					<?php endif; ?>
					<?php $GLOBALS['Annabel_Karmel']->ak_large_item(get_sub_field('content')->ID); ?>
					<?php endif; ?>
					<?php endwhile; ?>
					<?php if ($fp_col): ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php $GLOBALS['tc_return_pt'] = false; ?>
				<?php get_template_part('parts/flexible-content'); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>
<?php get_header(); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<div class="cell">
				<div class="singular-wrapper">
					<div class="page-header" itemid="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/Product">
						<meta content="Annabel Karmel" itemprop="brand">
						<h1 itemprop="name"><?php the_title(); ?></h1>
						<?php get_template_part('parts/save-share'); ?>
						<div class="images-info table">
							<?php if (has_post_thumbnail()): ?>
							<?php if ('owl' == get_field('gallery_type')): ?>
							<div class="cell carousel">
								<div class="owl-carousel owl-theme" data-items="1">
									<?php the_post_thumbnail('single-recipes-padded', [
										'class' => 'item',
										'itemprop' => 'image thumbnailUrl'
									]); ?>
									<?php if (get_field('video') || get_field('additional_images')): ?>
									<?php if (get_field('video')): ?>
									<a class="item popup-youtube" href="<?php echo get_post_meta(get_the_ID(), 'video', true); ?>">
										<img alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
										<?php the_post_thumbnail('single-recipes'); ?>
									</a>
									<?php endif; ?>
									<?php foreach ((get_field('additional_images') ?: []) as $image): ?>
									<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" class="item" src="<?php echo $image['sizes']['single-recipes']; ?>" title="<?php echo esc_attr($image['title']); ?>">
									<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
							<?php else: ?>
							<div class="cell gallery" data-gallery="<?php $GLOBALS['Annabel_Karmel']->generate_gallery_json(array_merge([
									acf_get_attachment(get_post_thumbnail_id())
								], (get_field('additional_images') ?: []))); ?>">
								<a class="open-gallery" data-index="<?php echo ($index = 0); ?>">
									<?php the_post_thumbnail('single-recipes-padded', [
										'itemprop' => 'image'
									]); ?>
								</a>
								<?php if (get_field('video') || get_field('additional_images')): ?>
								<span>Take a look inside:</span>
								<div class="image-thumbnails owl-carousel owl-theme">
									<?php if (get_field('video')): ?>
									<a class="popup-youtube" href="<?php echo get_post_meta(get_the_ID(), 'video', true); ?>">
										<img alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play.png">
										<?php the_post_thumbnail('single-recipes-small'); ?>
									</a>
									<?php endif; ?>
									<?php foreach (get_field('additional_images') as $image): ?>
									<a class="open-gallery" data-index="<?php echo ++$index; ?>">
										<img alt="<?php echo esc_attr($image['alt'] ?: $image['title']); ?>" src="<?php echo $image['sizes']['single-recipes-small']; ?>" title="<?php echo esc_attr($image['title']); ?>">
									</a>
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<?php endif; ?>
							<div class="cell">
								<?php if (have_rows('app__book_information')): ?>
								<div class="recipe-info">
									<div class="recipe-info-container">
										<?php while (have_rows('app__book_information')): the_row(); ?>
										<div>
											<h5><?php the_sub_field('title'); ?>:</h5>
											<?php the_sub_field('text'); ?>
										</div>
										<?php endwhile; ?>
									</div>
								</div>
								<?php endif; ?>
								<?php if (($book_type = get_field('app__book_type')) && ($book_link = get_field('app__book_link'))): ?>
								<div class="book-types">
									<span>Buy Now:</span>
									<div class="book-type">
										<a href="<?php echo $book_link; ?>" target="_blank">
											<?php if ('amazon' == $book_type): ?>
											<img alt="Available at Amazon" src="<?php echo get_template_directory_uri(); ?>/images/amazon-logo_transparent.png" title="Available at Amazon">
											<?php else: ?>
											<img alt="Get it on iTunes" src="<?php echo get_template_directory_uri(); ?>/images/Get_it_on_iTunes_Badge_US_1114.svg" title="Get it on iTunes">
											<?php endif; ?>
										</a>
									</div>
								</div>
								<?php endif; ?>
								<?php if ($tags = get_the_terms(get_the_ID(), 'app-book-tag')): ?>
								<div class="recipe-tags">
									<img alt="App / Book Tags" src="<?php echo get_template_directory_uri(); ?>/images/tags.png">
									<?php echo implode(' &middot; ', array_map(function($item) {
										return '<a href="' . get_term_link($item) . '">' . $item->name . '</a>';
									}, $tags)); ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php if (get_the_content()): ?>
					<div class="wysiwyg-content" itemprop="description">
						<?php the_content(); ?>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
				</div>
				<?php if ($recipes = get_field('recipes_from_the_app__book')): ?>
				<div class="related-content">
					<h2>Recipes from the Book</h2>
					<div class="clear"></div>
					<div class="ak-post-widgets fi-panel">
						<?php foreach ($recipes as $post): setup_postdata($post); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php $the_query = new WP_Query([
					'meta_key' => 'force_popular',
					'meta_value' => 1,
					'post_type' => 'recipes',
					'posts_per_page' => 3
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>Popular Recipes</h2>
					<div class="clear"></div>
					<div class="ak-post-widgets fi-panel">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
						<?php if (3 > $the_query->post_count): ?>
						<?php $the_query = new WP_Query([
							'post_type' => 'recipes',
							'posts_per_page' => 3 - $the_query->post_count
						]); ?>
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php $the_query = new WP_Query([
					'post_type' => 'recipes',
					'posts_per_page' => 3
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>Recently Added Recipes</h2>
					<a href="<?php echo get_post_type_archive_link('recipes'); ?>">View All Recipes &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets fi-panel">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php endif; ?>
				<?php endif; ?>
				<?php $the_query = new WP_Query([
					'orderby' => 'rand',
					'post_type' => 'apps-books',
					'post__not_in' => [
						get_the_ID()
					],
					'posts_per_page' => 6
				]); ?>
				<?php if ($the_query->have_posts()): ?>
				<div class="related-content">
					<h2>You may also like...</h2>
					<a href="<?php echo get_post_type_archive_link('apps-books'); ?>">View All Apps &amp; Books &raquo;</a>
					<div class="clear"></div>
					<div class="ak-post-widgets">
						<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>
			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>
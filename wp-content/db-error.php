<?php header('HTTP/1.1 500 Internal Server Error'); ?>
<!DOCTYPE html>
<html>
<head>
<title>Maintenance</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<style>
body {
	margin: 0;
	padding: 0;
}
.under_maintenance {
	max-width: 1024px;
	margin: auto;
}
.maintenace_container {
	width: 100%;
	float: left;
	text-align: center;
	max-width: 1024px;
	position: absolute;
	top: 50%;
	left: 0px;
	transform: translate(0%, -50%);
	right: 0px;
	margin: auto;
}

</style>
</head>
<body>

<div class="under_maintenance" style="">
  <div class="maintenace_container">
    <div class="maintenance_logo"><img src="web-maintenance-message.jpg" alt="" style=""></div>
 </div>
</div>
<!--under_maintenance-->
<!--- Database maintenance---->
</body>
</html>

<?php
include('auth_api.php');
class API_BOOKS extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	public function __construct($u,$p)
	{
		$this->auth_username = $u;
		$this->auth_password = $p;	
	}
	
	public function authRequest()
	{
		$this->resArr = $this->authorize($this->auth_username,$this->auth_password);
		if(!empty($this->resArr))
		{
			$this->response($this->resArr);
			header("HTTP/1.0 401 Unauthorized");
			die;
		}
		else
		{
			$userInfo['username'] = $this->auth_username;
			$userInfo['password'] = $this->auth_password;
			return $userInfo;
		}
	}
	public function deleted_items($post_type,$version)
	{
		global $wpdb;
		$deletedSql = "SELECT * FROM ".$wpdb->prefix."posts_deleted WHERE post_type='".$post_type."'";
		if($version!='V0')
		{
			$vData = $this->getversionData($post_type,$version);
			$deletedSql.= " AND deleted_date BETWEEN '".$vData['from_date']."' AND '".$vData['to_date']."'";
		}
		$results = $wpdb->get_results($deletedSql);
		if(!empty($results))
		{
			$arrDel = array();
			foreach($results as $di)
			{					
				$arrDel[] = (int)$di->post_id;					
			}
		}
		return (!empty($arrDel))?$arrDel:array();
	}
	
	public function get_all($data)
	{
		$recArr = array();
		$version = $data['version'];
		$post_type = 'apps-books';		
		$args = array('post_type' => $post_type,
			'post_status' => array( 'mobile', 'publish'),
			'meta_query'=> array(
			'relation' => 'OR',
			array(
				'key'     => 'visibility',
				'value'   => 'Both',
				'compare' => '='
			),
			array(
				'key'     => 'visibility',
				'value'   => 'Mobile',
				'compare' => '='
			)),
			'posts_per_page' => -1);
			if(isset($version) && !empty($version))
			{
				$vData = $this->getversionData($post_type,$version);
				if(isset($vData) && !empty($vData))
				{
					if($version == 'V0')
					{
						$conPart = array(array('column'  => 'post_modified_gmt','before' => $vData['to_date'],'inclusive' => true));
					}
					else
					{
						$conPart = array(array('column'  => 'post_modified_gmt','after' => $vData['from_date'],'before' => $vData['to_date'],'inclusive' => true));
					}
					
					if(isset($conPart))
					{
						$args['date_query'] = $conPart;
					}
				}
			}
		wp_reset_query();
		if(isset($args) && !empty($args))
		{   
			$records = array();
			query_posts( $args );
			if ( have_posts() ):
				while ( have_posts() ):	the_post();
					$recItem = array();
					$recItem['book_id'] = get_the_ID();
					//$catIds = wp_get_post_terms(get_the_ID(), 'app-book-category', array("fields" => "ids"));
					//$recItem['book_category_id'] = $catIds;
					$recItem['book_title'] = html_entity_decode(get_the_title());		
					if ( has_post_thumbnail())
					{
						$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),'original');
						$image_url = $large_image_url[0];
					}
					$recItem['book_image'] = (!empty($image_url))?$image_url:null;
					$recItem['book_desc'] = (!empty(get_the_content()))?html_entity_decode(get_the_content()):null;
					$recItem['book_type'] = (!empty(get_field('app__book_type'))) ? get_field('app__book_type') : null;
					$recItem['book_available_at']= (!empty(get_field('app__book_link'))) ? get_field('app__book_link'):null;
					$recItem['book_website_url']= (!empty(get_field('website_url')))?get_field('website_url'):get_permalink();
					array_push($records,$recItem);
				endwhile;
				endif;
				$recArr['records'] = $records;
				if(isset($version) && $version!='V0'):
					$deletedItems = $this->deleted_items($post_type,$version);
					$recArr['deleted_items'] = $deletedItems;
				endif;
		}
		else
		{
			$recArr = array('status'=>'fail','response_code'=>601,'response_msg'=>'Invalid data format');
			header("HTTP/1.0 400 Bad Request");
		}
		
		$this->response($recArr);		
	}
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}
}
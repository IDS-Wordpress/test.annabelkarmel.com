				<footer id="footer">					
					<div class="container skid">
						<?php echo str_replace('{{user_first_name}}', wp_get_current_user()->user_firstname, wp_nav_menu([
							'container' => '',
							'echo' => false,
							'theme_location' => 'footer-menu'
						])); ?>
					</div>
					<div class="copy-attr">
						<div class="container">
							<div id="copyright">
								&copy; <?php echo date('Y'); ?> Annabel Karmel Group Holdings Limited
							</div>
							<div id="attribution">
								<!--Digital &amp; Site by Verb-->
							</div>
						</div>
					</div>
				</footer>
<?php include('login_form.php');?>
<?php wp_footer(); ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script src="https://1000hz.github.io/bootstrap-validator/dist/validator.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrapValidator.min.js"> </script>  
<script src="https://1000hz.github.io/bootstrap-validator/dist/validator.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script> 
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mobile.datepicker.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mobile.datepicker.theme.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/datepicker.js"></script>
<script type="text/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

window.onload = checkMobile;
function checkMobile (){
var gplay = document.getElementById("gplay");
var iphone = document.getElementById("iphone");
if ( isMobile.iOS() ){
  // iphone.style.display =  "inline-block";
  // gplay.style.display =  "none";
  var popUpdata = localStorage.getItem('popst');
	$('.app_btn .app_close img').click(function () {
		$(".app_btn").fadeOut("fast");
		$("body").removeClass("app-body");
		localStorage.setItem('popst', true);
	});
	if(!popUpdata) {
		$("#iphone").fadeIn("fast");
		//localStorage.setItem('popst', true);
		$("body").addClass("app-body");
	}
}
if ( isMobile.Android() ){
	// iphone.style.display =  "none";
  	// gplay.style.display =  "inline-block";
	var popUpdata = localStorage.getItem('popst');
	$('.app_btn .app_close img').click(function () {
		$(".app_btn").fadeOut("fast");
		$("body").removeClass("app-body");
		localStorage.setItem('popst', true);
	});
	if(!popUpdata) {
		$("#gplay").fadeIn("fast");
		//localStorage.setItem('popst', true);
		$("body").addClass("app-body");
	}
}
}

$(document).ready(function(){	
	$('#pp_loginform').submit(function(){
		var username = $('#pp_user_login').val();
		var password = $('#pp_user_pass').val();
		var redirect_to = $('[name=redirect_to]').val();
		var tok = username + ':' + password;
		var hash = btoa(tok);
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
		
		if(username == '')
		{
			$('#msg').html('<span class="warning">Please enter your username/email in the Email Address field below.</span>');
			return false;
		}
		else if(password == '')
		{
			$('#msg').html('<span class="warning">Please enter your password.</span>');
			return false;
		}
		else
		{	
			$.ajax({
				url : ajaxurl,
				type: "POST",
				data : {action: "login_validation",tok : hash},
				success:function(response, textStatus, jqXHR) 
				{
					if(response == "success")
					 {
						// window.location.href= redirect_to;
						 location.reload(true);
					 }
					 else
					 {
						$('#msg').html('<span class="warning">'+response+'</span>');
						return false;
					 }
					 
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					//alert(jqXHR.responseText);
					return false;  												
				}
			});
			return false;											
		}																		
	});

$('.selectpicker').selectpicker('refresh');
equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

//$(window).load(function() {
 $(window).on("load",function(){
  equalheight('.videos-middle-section.flexible-content .owl-stage .item a+p, .cookies-with-water .video-listing .video-list-item .post-widget, .videos-middle-section .buttons.other-videos-cats ul li');
});


$(window).resize(function(){
  equalheight('.videos-middle-section.flexible-content owl-stage .item a+p, .cookies-with-water .video-listing .video-list-item .post-widget, .videos-middle-section .buttons.other-videos-cats ul li');
});


	$("a[href='#login']").on("click", function(){
		$("#pp_loginform .login_form_group").each(function(){
			if($(this).find('input').val() != ""){
				$(this).find("label").addClass("active");
			}
		});
	});
	$("#pp_loginform .login_form_group input").on("keyup", function(){
		if($(this).val() != "") {
			$(this).siblings("label").addClass("active");
		} else {
			$(this).siblings("label").removeClass("active");
		}
	});
	
	$('#pp_loginform').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			validating: 'glyphicon glyphicon-refresh',
			invalid: 'glyphicon glyphicon-remove'
		},
		fields: {
			log: {
				group: '.login_form_group',
				validators: {
					emailAddress: {message: 'Please enter a valid Email'},
					notEmpty: {message: 'Email is required'},
					callback: {
                        callback: function(value, validator, $field) {
							var regs = /[ ]/g;
							var newVal = value.replace(regs, '');
							$field.val(newVal);
							value = newVal;
							
                            if (value.length >= 7) {
								var atReg = /[*\%|\$|\&|\*|\£|\&|\"|\(]/g;
								
								if(value.search(atReg) > 0) {
									
									return {
										valid: false,
										message: "The Email field only accepts letters (a-z), numbers, and the following special characters + @ . -"
									}	
								} else {
									var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									
									if(value.search(reg) < 0) {
										return {
											valid: false,
											message: 'Please enter a valid Email'
										}
									}									
								}
								
                                
                            }
							
							
							if(value.length == 0){
								return {
									valid: false,
									message: 'The Email field is mandatory – please enter at least seven characters for this field'
								};
							}
                            return true
                        }
                    }
				}
			},
			pwd: {
				group: '.login_form_group',
				validators: {
					notEmpty: {message: 'Password is required'}
				}
			}
		}
	});
	$('#pp_loginform .login-password.login_form_group .form-control-feedback').click(function(){
	  if ($("#pp_loginform .login-password.login_form_group .input").attr("type") == "password") {
		$("#pp_loginform .login-password.login_form_group .input").attr("type", "text");
	  } else {
		$("#pp_loginform .login-password.login_form_group .input").attr("type", "password");
	  }
	});
});
</script>
<script type="text/javascript">ggv2id='8862bca0';</script>
<script type="text/javascript" src="https://js.gumgum.com/services.js"></script>
</body>
</html>
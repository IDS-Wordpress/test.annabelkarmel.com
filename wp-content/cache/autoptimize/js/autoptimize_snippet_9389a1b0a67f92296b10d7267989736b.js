var almSEO={};jQuery(document).ready(function($){if(typeof window.history.pushState=='function'){almSEO.init=true;almSEO.paging=false;almSEO.previousUrl=window.location.href;almSEO.isAnimating=false;almSEO.defaultPage=1;almSEO.fromPopstate=false;$.fn.almSEO=function(alm){if(alm.seo_scroll===undefined){alm.seo_scroll=true;}
almSEO.canonical_url=alm.canonical_url;almSEO.speed=1000;almSEO.permalink=alm.permalink;almSEO.pageview=alm.pageview;almSEO.postsPerPage=alm.posts_per_page;almSEO.preloaded=alm.preloaded;almSEO.scroll=alm.seo_scroll;almSEO.speed=alm.seo_scroll_speed;almSEO.scrolltop=alm.seo_scrolltop;almSEO.newPath='';almSEO.paging=alm.paging;almSEO.content=alm.content;almSEO.first=$('.alm-seo').eq(0);var page=alm.page+1,start=1;if(almSEO.preloaded==='true'){start=0;page=page+1;}
if(almSEO.permalink==='default'){var querystring=window.location.search,url=almSEO.cleanURL(window.location.toString());if(querystring!==''&&page>start){if(!almSEO.getQueryVariable('paged')){almSEO.newPath=url+'&paged='+page;}else{almSEO.newPath=url.replace(/(paged=)[^\&]+/,'$1'+page);}}else{if(page>1){almSEO.newPath=url+'?paged='+page;}else{almSEO.newPath=url;}}}
else
{var pathName=window.location.pathname,url=almSEO.cleanURL(window.location.toString());var urlPieces=url.split('/'),currPageNumber=urlPieces[urlPieces.length-2];if(page===1){almSEO.newPath=almSEO.canonical_url;}else{almSEO.newPath=almSEO.canonical_url+'page/'+page+'/';}}
if((almSEO.init&&page>1)||page>1){if(!almSEO.paging&&almSEO.scroll==='true'){almSEO.scrollToPage(page);}}
if(almSEO.paging&&almSEO.init){almSEO.defaultPage=page;}
if(almSEO.paging){if(!almSEO.fromPopstate){almSEO.setURL(page,almSEO.newPath);}else{almSEO.fromPopstate=false;}}
almSEO.init=false;}
var almSEOTimer;$(window).bind('scroll touchstart',function(){if(!almSEO.isAnimating&&!almSEO.paging&&!almSEO.init){if(almSEOTimer){window.clearTimeout(almSEOTimer);}
almSEOTimer=window.setTimeout(function(){var fromTop=parseInt(Math.round($(this).scrollTop()))+parseInt(almSEO.scrolltop),posts=$('.alm-seo'),url=window.location.href;var cur=posts.map(function(){if($(this).offset().top<fromTop)
return this;});cur=cur[cur.length-1];var page=$(cur).data('page'),permalink=$(cur).data('url');if(page===undefined){page=almSEO.first.data('page');permalink=almSEO.first.data('url');}
if(url!==permalink){almSEO.setURL(page,permalink);}},25);}});almSEO.setURL=function(page,permalink){var state={page:page,permalink:permalink};if(permalink!==almSEO.previousUrl&&!almSEO.fromPopstate){history.pushState(state,window.location.title,permalink);if($.isFunction($.fn.almUrlUpdate)){$.fn.almUrlUpdate(permalink,'seo');}
if(almSEO.pageview==='true'){var location=window.location.href,path='/'+window.location.pathname;if(typeof ga!=='undefined'&&$.isFunction(ga)){ga('send','pageview',path);}
if(typeof __gaTracker!=='undefined'&&$.isFunction(__gaTracker)){__gaTracker('send','pageview',path);}}
almSEO.previousUrl=permalink;}
almSEO.fromPopstate=false;}
window.addEventListener('popstate',function(event){if($('.alm-listing.alm-ajax').attr('data-seo')==='true'){if(!almSEO.paging){almSEO.getPageState(event.state);}else{if($.isFunction($.fn.almSetCurrentPage)&&$.isFunction($.fn.almGetObj)){var current=event.state,almBtnWrap=$.fn.almGetParentContainer(),almObj=$.fn.almGetObj();if(current===null){current=almSEO.defaultPage;}else{current=event.state.page;}
almSEO.fromPopstate=true;$.fn.almSetCurrentPage(current,almBtnWrap,almObj);}}}});almSEO.getPageState=function(data){var page;if(data===null){page=-1;}else{page=data.page;}
if($('#ajax-load-more').length){if(almSEO.scroll==='true'){almSEO.scrollToPage(page);}}}
almSEO.getCurrentPageTop=function(page){if(almSEO.scroll==='true'){almSEO.scrollToPage(page);}}
almSEO.scrollToPage=function(page){if(page===undefined||page==='undefined'||page==='-1'||page===-1){page=$('.alm-seo').eq(0).data('page');}
if(!almSEO.isAnimating){almSEO.isAnimating=true;var top=$('.alm-seo[data-page="'+page+'"]').offset().top-almSEO.scrolltop+5+'px';$('html, body').delay(250).animate({scrollTop:top},almSEO.speed,'almSEO_easeInOutQuad',function(){almSEO.isAnimating=false;});}}
almSEO.isNumeric=function(n){return!isNaN(parseFloat(n))&&isFinite(n);}
almSEO.cleanURL=function(path){var loc=path,index=loc.indexOf('#');if(index>0){path=loc.substring(0,index);}
return path;}
almSEO.getQueryVariable=function(variable){var query=window.location.search.substring(1);var vars=query.split('&');for(var i=0;i<vars.length;i++){var pair=vars[i].split('=');if(decodeURIComponent(pair[0])==variable){return decodeURIComponent(pair[1]);}}
return false;}
$.easing.almSEO_easeInOutQuad=function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b;};}});
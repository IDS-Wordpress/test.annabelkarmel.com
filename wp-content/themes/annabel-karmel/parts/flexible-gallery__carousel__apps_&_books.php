<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
?>
<?php if(get_sub_field('heading')): ?>
<div class="carousel_heading recipe-filter"><h2><?php echo get_sub_field('heading'); ?></h2></div>
<?php else: ?>
<?php endif; ?>  

 <?php if($isLoginRequired) { ?>
<div class="loginRequired"> 
<div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
     <?php
    }
    ?>
<?php if(get_sub_field('heading')) { ?>

<!--<div class="carousel_heading"> <h2><?php //echo get_sub_field('heading'); ?></h2></div>-->
<?php } ?>
<div class="owl-carousel owl-theme" data-items="<?php echo esc_attr($num_items = get_sub_field('number_of_items')); ?>">    
	<?php if ('manual' == ($type = get_sub_field('type'))): ?>
	<?php foreach (get_sub_field('apps_&_books') as $product): ?>
	<div class="item">
    
		<a href="<?php the_permalink($product->ID); ?>">
			<?php if (has_post_thumbnail($product->ID)): ?>
			<!-- <img alt="<?php //echo esc_attr(get_the_title($product->ID)); ?>" src="<?php //echo wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'gallery-carousel')[0]; ?>"> -->
			<?php echo get_the_post_thumbnail($product->ID, 'gallery-carousel',array( 'alt' => esc_attr(get_the_title($product->ID)) ) ); ?>
		</a>
			<?php endif; ?>
			<h4><a href="<?php the_permalink($product->ID); ?>"><?php echo (get_field('short_title', $product->ID) ?: get_the_title($product->ID)); ?></a></h4>
		
	</div>
	<?php endforeach; ?>
	<?php else: ?>
	<?php $args = [
		'post_type' => 'apps-books',
		'posts_per_page' => get_sub_field('total_number_of_items')
	]; ?>
	<?php if ('category' == $type): ?>
	<?php $args['tax_query'] = [
		[
			'taxonomy' => get_sub_field('category')->taxonomy,
			'terms' => get_sub_field('category')->term_id
		]
	]; ?>
	<?php endif; ?>
	<?php $the_query = new WP_Query($args); ?>
	<?php if ($the_query->have_posts()): ?>
	<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
	<div class="item">
		<a href="<?php the_permalink(); ?>">
			<?php if (has_post_thumbnail()): ?>
			<?php the_post_thumbnail('gallery-carousel'); ?>
			<?php endif; ?>
			<h4><?php echo (get_field('short_title') ?: get_the_title()); ?></h4>
		</a>
	</div>
    
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	<?php endif; ?>
</div>

 <?php if($isLoginRequired) { ?>
</div>
     <?php
    }
    ?>
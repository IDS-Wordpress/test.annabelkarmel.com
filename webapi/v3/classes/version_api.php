<?php
include('auth_api.php');
class API_VERSION extends API_AUTH
{
	public $resArr = array();
	public $auth_username;
	public $auth_password;
	public function __construct($u,$p)
	{
		$this->auth_username = $u;
		$this->auth_password = $p;	
	}
	
	public function authRequest()
	{
		$this->resArr = $this->authorize($this->auth_username,$this->auth_password);
		if(!empty($this->resArr))
		{
			$this->response($this->resArr);
			header("HTTP/1.0 401 Unauthorized");
			die;
		}
		else
		{
			$userInfo['username'] = $this->auth_username;
			$userInfo['password'] = $this->auth_password;
			return $userInfo;
		}
	}
	public function get_version()
	{
		//$userInfo = $this->authRequest();
		global $wpdb;
		$arrayIndex = array('post'=>'articles','recipes'=>'recipes','products'=>'products','apps-books'=>'books');
		$recArr = array();
		$currentDate = date('Y-m-d H:i:s');
		$sql = "select max(version) as version,type,max(from_date) as from_date from wp_api_version GROUP BY type ORDER BY version DESC";
		$records = $wpdb->get_results($sql);
		//print_r($records);
		foreach($records as $record)
		{
			if(array_key_exists($record->type,$arrayIndex))
			{
				$recArr[$arrayIndex[$record->type]] = 'V'.$record->version;
			}
			else
			{
				$recArr[$record->type] = 'V'.$record->version;
			}	
		}
		//$recArr['downloadRecipeCategories'] = (get_field('is_category_downloadable', 'option')==true)?true:false;
		$this->response($recArr);		
	}
	public function response($arrResponse)
	{
		header('Content-Type: application/json');
		echo json_encode($arrResponse);
	}
}
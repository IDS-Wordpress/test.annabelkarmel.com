
<?php 
$isLoginRequired = 0;
if(get_sub_field('is_gated') && !is_user_logged_in()) { $isLoginRequired = 1; }
?>
 <?php if($isLoginRequired) { ?>
<div class="loginRequired"> 
<div class="child_bg" >
        <div class="child_overlay">
        <a href="#login">Please login to access content</a>
        </div>
    </div>
     <?php
    }
    ?>
<?php if ($title = get_sub_field('title')): ?>
<div class="wysiwyg-content">
	<h2><?php echo $title; ?></h2>
</div>
<?php endif; ?>
<?php if ($galleries = get_sub_field('galleries')): ?>
<div class="galleries">
	<?php foreach ($galleries as $gallery): ?>
	<div class="gallery" data-gallery="<?php $GLOBALS['Annabel_Karmel']->generate_gallery_json($images = get_field('gallery', $gallery->ID)); ?>">
		<a class="open-gallery">
 
			<?php if ($images): ?>
			<img alt="<?php echo esc_attr(get_the_title($gallery)); ?>" src="<?php echo current($images)['sizes']['gallery-thumbnail']; ?>">
			<?php endif; ?>
		</a>
		<div>
			<a class="open-gallery"><?php echo mb_strtoupper(get_the_title($gallery)); ?></a>
			<a class="save-this-post" data-id="<?php echo $gallery->ID; ?>">SAVE</a>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>

 <?php if($isLoginRequired) { ?>
</div>
     <?php
    }
    ?>
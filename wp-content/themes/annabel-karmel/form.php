<?php
/**
 * Template Name: Form
 */
get_header(); ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id((get_field('header_adzone') ?: 12497), 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<section class="form-page">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<?php if (get_the_content()): ?>
		<div class="wysiwyg-content">
			<?php the_content(); ?>
		</div>
		<?php endif; ?>
	</div>
</section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
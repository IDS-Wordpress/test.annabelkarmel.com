<?php
if(isset($_GET['post_type']))
{
    if($_GET['post_type'] == 'videos')
	{
        load_template(TEMPLATEPATH . '/search-videos.php');
		exit;
    }
}
?>
<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<section class="category-page">
	<div class="container">
		<div class="table">
			<div class="cell">				
				<div class="search-title">
					<?php
					$args = array('s' =>get_search_query(),'post_status'=>'publish','showposts'=>-1,'ignore_sticky_posts'=> true,);
					if(!empty($_GET['pt']) && $_GET['pt']=='recipes'){
						$args['post_type'] = 'recipes';
					}
					else if(!empty($_GET['pt']) && $_GET['pt']=='products'){
						$args['post_type'] = 'products';
					}
					else if(!empty($_GET['pt']) && $_GET['pt']=='apps-books'){
						$args['post_type'] = 'apps-books';
					}
					else if(!empty($_GET['pt']) && $_GET['pt']=='post,page'){
						$args['post_type'] = array('post','page');
					}
					else
					{ 
						$args['post_type'] = array('post','page','recipes','products','apps-books','offers','videos','competitions');
					} 	

						$allsearch = new WP_Query($args);
						//echo '<pre>';print_r($allsearch); echo "</pre>";
						//$key = wp_specialchars($s, 1);
						$count = $allsearch->post_count;
						wp_reset_query();
						if(($count==1)||($count==0)){$result='result';}else{$result='results';} 
						?>
						<!-- <span><?php //echo ($count = count($posts)); ?></span> result<?php echo (1 == $count
							? ''
							: 's'
						); ?> for <span>'<?php //echo get_search_query(); ?>'</span> -->
					
					<h1><span class="search-terms1"><?php echo (!empty($count)?$count:0); ?></span> <?php echo $result;?> for <span> <span>'<?php echo get_search_query(); ?>'</span>
					</h1>
					<div class="bootstrap-namespaced">
						<select class="selectpicker">
							<option<?php if (!isset($_GET['pt'])) echo ' selected';?> value="<?php echo esc_url(remove_query_arg('pt')); ?>">All</option>
							<option<?php if (isset($_GET['pt']) && 'post,page' == $_GET['pt']) echo ' selected';?> value="<?php echo esc_url(add_query_arg('pt', 'post,page')); ?>">Articles &amp; Pages</option>
							<option<?php if (isset($_GET['pt']) && 'recipes' == $_GET['pt']) echo ' selected';?> value="<?php echo esc_url(add_query_arg('pt', 'recipes')); ?>">Recipes</option>
							<option<?php if (isset($_GET['pt']) && 'products' == $_GET['pt']) echo ' selected';?> value="<?php echo esc_url(add_query_arg('pt', 'products')); ?>">Products</option>
							<option<?php if (isset($_GET['pt']) && 'apps-books' == $_GET['pt']) echo ' selected';?> value="<?php echo esc_url(add_query_arg('pt', 'apps-books')); ?>">Apps &amp; Books</option>
						</select>
					</div>
				</div>
				<?php if ($count): ?>
				<?php if (16 < $count) {
					$locations = range(0, 11);
					shuffle($locations);
					
					$products = [];
					
					for ($i = 16; $i < $count; $i++) {
						if ('products' == $posts[$i]->post_type) {
							$products[] = $posts[$i];
							
							unset($posts[$i]);
						}
						
						if (4 == count($products)) {
							break;
						}
					}
					
					if ($products) {
						$posts = array_values($posts);
						
						for ($i = 0; $i < count($products); $i++) {
							array_splice($posts, $locations[$i], 0, [
								$products[$i]
							]);
						}
					}
				} ?>
				<div class="ak-post-widgets">
					<?php 
					
						echo do_shortcode('[ajax_load_more preloaded="true" search="'. get_search_query() .'" preloaded_amount="12" seo="true" post_type="' . esc_attr(isset($_GET['pt'])
							? $_GET['pt']
							: 'post,page,recipes,products,apps-books,offers,videos,competitions'
						) . '" posts_per_page="12" pause="true" scroll="false" images_loaded="true" button_label="Load More"]');

						?>
					
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
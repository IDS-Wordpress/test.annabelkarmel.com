<?php
include('wp-load.php');
$firstRun = 0;
if($_REQUEST['firsttime'] == 1)
{
	$firstRun = 1;
}
else
{
	$firstRun = 0;
}

if(in_array($_SERVER['HTTP_HOST'],array('annabelkarmel.com','www.annabelkarmel.com')))
{
	//Live Information
	$apiKey = '6b703758422b20043b78a3589cf739e6-us5';
	$listId = array('US'=>'c85adaca50','UK'=>'84c94cdc76','AU'=>'cbcc7bec3a','INT'=>'efa4919276');
}
else
{
	//IDS Testing					
	$apiKey = 'c6a36793022d1d7b6dd2525022a5fa36-us15';
	$listId = array('US'=>'f4996b1f7b','UK'=>'1ed11cdd55','AU'=>'d7b9de9f23','INT'=>'415ff3e853');
}
	
if($firstRun==1)				
{
	$args = array('meta_key'=>'source','meta_value'=> 'Mobile',);
}
else
{
	$args = array('meta_query' => array(
					'relation' => 'AND',
                    array(
                        'key' => 'source',
                        'value' => 'Mobile',
                        'compare' => '=='
                    ),
                    array(
                        'key' => 'is_pushed_to_mailchimp',
                        'value' => 0,
                        'compare' => '=='
                    )
                )
            );
}

$users = get_users($args);

//echo "<pre>"; print_r($users); echo "</pre>"; die;
if($users)
{
	foreach($users as $user)
	{	
		$user_id = $user->data->ID; 
		$user_email = $user->data->user_email;
		$user_fname = get_user_meta($user_id,'first_name',true);
		$user_lname = get_user_meta($user_id,'last_name',true);
		$country = get_user_meta($user_id,'country',true);
		
		$listUniqueId = $listId['INT'];
						
		if($country=='United States'){
			$listUniqueId = $listId['US'];
		}
		elseif($country=='United Kingdom'){
			$listUniqueId = $listId['UK'];
		}
		elseif($country=='Australia'){
			$listUniqueId = $listId['AU'];
		}
		else{
			$listUniqueId = $listId['INT'];
		}	
		
		$memberId = md5(strtolower($user_email));
		$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
		$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listUniqueId . '/members/' . $memberId;

		$json = json_encode([
			'email_address' => $user_email,
			'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
			'merge_fields'  => [
				'FNAME'     => $user_fname,
				'LNAME'     => $user_lname
			]
		]);
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		// store the status message based on response code
		if ($httpCode == 200) {
			update_user_meta( $user_id, 'is_pushed_to_mailchimp', 1 );
			$msg = 'User Email "'.$user_email.'" successfully subscribed to AnnabelKarmel.<br>';
			
		}
		else
		{
				switch ($httpCode)
				{
					case 214:
						$msg = 'User Email "'.$user_email.'" are already subscribed.<br>';
						break;
					default:
						$msg = 'Some problem occurred, please try again.<br>';
						break;
				}
		}	
		echo $msg;
	}

}
else
{
	echo 'User Email not found to mailchimp sync.<br>';
}
?>
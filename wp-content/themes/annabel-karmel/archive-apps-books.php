<?php global $wp_query; ?>
<?php get_header(); ?>
<?php if (function_exists('bcn_display')): ?>
<section class="breadcrumbs">
	<div class="container">
		<?php bcn_display(); ?>
	</div>
</section>
<?php endif; ?>
<?php echo do_shortcode('[pro_ad_display_adzone id=' . icl_object_id(12497, 'post', false, ICL_LANGUAGE_CODE) . ']'); ?>
<section class="generic-page">
	<div class="container">
		<div class="table">
			<?php $GLOBALS['tc_return_null'] = true; ?>
			<div class="cell">
				<h1><?php echo get_post_type_object('apps-books')->labels->name; ?></h1>
				<?php if (get_field('apps_books_description', 'option')): ?>
				<div class="flexible-content">
					<?php the_field('apps_books_description', 'option'); ?>
				</div>
				<?php endif; ?>
				<?php if (have_posts()): ?>
				<div class="front-page-board">
					<?php while (have_posts()): the_post(); ?>
					<?php if (!$wp_query->current_post): ?>
					<div class="fp-col">
					<?php elseif (!($wp_query->current_post % 3)): ?>
						</div>
					</div>
					<div class="fp-col">
						<div class="fp-small-cards">
					<?php endif; ?>
					<?php if (!($wp_query->current_post % 5)): ?>
					<?php if (5 == $wp_query->current_post): ?>
						</div>
					<?php endif; ?>
					<div class="owl-carousel owl-theme" data-items="1">
						<div class="item">
							<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false, true); ?>
						</div>
					<?php wp_reset_postdata(); ?>
					</div>
					<?php elseif (1 == $wp_query->current_post): ?>
					<div class="fp-small-cards">
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
					<?php else: ?>
						<?php $GLOBALS['Annabel_Karmel']->ak_category_item(null, false); ?>
					<?php endif; ?>
					<?php if ($wp_query->current_post + 1 == $wp_query->post_count): ?>
					</div>
					<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				<div class="flexible-content">
					<div class="feel-inspired">
						<h2>
							Browse <span><?php echo get_post_type_object('apps-books')->labels->name; ?></span>
						</h2>
						<h3>Full list of our <?php echo get_post_type_object('apps-books')->labels->name; ?></h3>
						<span class="bold">FILTER BY:</span>
						<div class="buttons">
							<button class="selected" data-panel="all">All</button>
							<?php foreach (($terms = get_terms([
								'parent' => 0,
								'taxonomy' => 'app-book-category'
							])) as $term): ?>
							<button data-panel="<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
							<?php endforeach; ?>
						</div>
						<?php $the_query = new WP_Query([
							'meta_key' => 'force_popular',
							'meta_value' => 1,
							'post_type' => 'apps-books',
							'posts_per_page' => 3
						]); ?>
						<?php if ($the_query->have_posts()): ?>
						<div class="products">
							<div class="products-all">
								<h3>Best Sellers</h3>
								<div class="clear"></div>
								<div class="ak-post-widgets fi-panel">
									<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
									<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
									<?php endwhile; ?>
								</div>
							</div>
						</div>
						<?php wp_reset_postdata(); ?>
						<?php endif; ?>
						<div class="recipes">
							<div class="recipes-all">
								<div class="ak-post-widgets fi-panel">
									<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="apps-books" posts_per_page="6" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
								</div>
							</div>
							<?php foreach ($terms as $term): ?>
							<div class="recipes-<?php echo $term->slug; ?>">
								<div class="ak-post-widgets fi-panel">
									<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="apps-books" taxonomy="app-book-category" taxonomy_terms="' . $term->slug . '" posts_per_page="6" pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<?php $GLOBALS['tc_return_null'] = false; ?>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
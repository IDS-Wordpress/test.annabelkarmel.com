<?php
/*
Plugin Name: App Admin AK
Plugin URI: https://appadminAK
Description: The ultimate solution to add infinite scroll functionality to your website.
Text Domain: app-adminAK
Author: AB
Author URI: https://xxx
Version: 1.00
License: GPL
Copyright: IDS
*/

//user meta for api order 

function tm_additional_profile_fields( $user ) {

    $id= $user->ID;
    $user_order_cats = (!empty(get_user_meta($user->ID,'user_order_cats',true)))?get_user_meta($user->ID,'user_order_cats',true):array();
    $user_order = get_user_meta($user->ID, 'user_order', true);
    if(!empty($user_order))
              {

                $transaction_id = $user_order[0]['transaction_id'];
                $purchase_token = $user_order[0]['purchase_token'];
                $purchasedCats = array();
                foreach($user_order as $uOrd)
                {
                  $cats = $uOrd['cat_id'];
                  if(!empty($cats))
                  {
                    foreach($cats as $catId)
                    {
                      $purchasedCats[] = $catId;
                    }
                  }
                }
              }

    ?>
    <h3>App Order information</h3>

    <table class="form-table">
     <tr>
       <th><label for="birth-date-day">App Order Data</label></th>
       <td>
         <input type="checkbox" name="order_data" value="yes" <?php if($transaction_id!='') { ?>checked="checked" <?php } ?> id="order_data">
       </td>
     </tr>
     
    </table>
    <?php
}

add_action( 'show_user_profile', 'tm_additional_profile_fields', 10, 1);
add_action( 'edit_user_profile', 'tm_additional_profile_fields', 10, 1);



/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
function tm_save_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
     return false;
    }
    if($_POST['order_data']=='yes'){
       $user_order = get_user_meta($user_id, 'user_order', true);
    if(empty($user_order))
              {
        $transaction_id = '1000000480534801';
      $purchase_token = '1000000480534801';
      $cats = array_map('intval', array('3686'));
      $user_ordered_details = array(array('transaction_date'=>date('d-m-Y H:i:s'),'transaction_id'=>$transaction_id,'purchase_token'=>$purchase_token,'cat_id'=>$cats));
     if($transaction_id!=''){
      update_user_meta( $user_id, 'user_order', $user_ordered_details);
      update_user_meta( $user_id, 'user_order_cats', $cats);
     }
    }
  }
  
     
}

add_action( 'personal_options_update', 'tm_save_profile_fields' );
add_action( 'edit_user_profile_update', 'tm_save_profile_fields' );
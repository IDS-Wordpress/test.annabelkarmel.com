<div class="ak-post-widgets owl-carousel owl-theme" data-items="3">
	<?php if ('manual' == get_sub_field('type')): ?>
	<?php foreach (get_sub_field('recipes') as $post): setup_postdata($GLOBALS['post'] =& $post); ?>
	<div class="item">
		<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
	</div>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php else: ?>
	<?php $the_query = new WP_Query([
		'post_type' => 'recipes',
		'posts_per_page' => get_sub_field('total_number_of_items'),
		'tax_query' => [
			[
				'taxonomy' => get_sub_field('category')->taxonomy,
				'terms' => get_sub_field('category')->term_id
			]
		]
	]); ?>
	<?php if ($the_query->have_posts()): ?>
	<?php while ($the_query->have_posts()): $the_query->the_post(); ?>
	<div class="item">
		<?php $GLOBALS['Annabel_Karmel']->ak_category_item(); ?>
	</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	<?php endif; ?>
</div>
<?php $default_recipe_categories = array_map(function($item) {
	return $item->slug;
}, (is_tax('recipe-category')
	? [
		get_queried_object()
	]
	: (get_sub_field('default_recipe_categories') ?: [])
)); ?>
<div class="recipe-filter">
	<img alt="Recipe Finder" src="<?php echo get_template_directory_uri(); ?>/images/recipe-finder.png">
	<form action="<?php echo site_url('/recipe-results/'); ?>" method="post">
		<div class="accordion">
			<div class="accordion-container">
				<?php if ($categories = get_terms([
					'orderby' => 'term_order',
					'taxonomy' => 'recipe-category'
				])): ?>
				<div>
					<?php if ((isset($_POST['rf_category']) && $_POST['rf_category']) || $default_recipe_categories): ?>
					<a class="open">Age</a>
					<div style="display: block;">
					<?php else: ?>
					<a>Age</a>
					<div>
					<?php endif; ?>
						<button class="all<?php if (!isset($_POST['rf_category']) && !$default_recipe_categories) echo ' selected'; ?>">All</button>
						<?php foreach ($categories as $category): ?>
						<?php if ((isset($_POST['rf_category']) && in_array($category->slug, $_POST['rf_category'])) || ('GET' == $_SERVER['REQUEST_METHOD'] && in_array($category->slug, $default_recipe_categories))): ?>
						<button class="selected"><?php echo $category->name; ?></button>
						<input checked name="rf_category[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php else: ?>
						<button><?php echo $category->name; ?></button>
						<input name="rf_category[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
				<?php foreach (get_terms([
					'parent' => 0,
					'taxonomy' => 'recipe-filter'
				]) as $filter): ?>
				<?php if ($categories = get_terms([
					'child_of' => $filter->term_id,
					'taxonomy' => 'recipe-filter'
				])): usort($categories, function($a, $b) {
					return strcmp($a->name, $b->name);
				}); ?>
				<div>
					<?php if (isset($_POST['rf_' . $filter->slug]) && $_POST['rf_' . $filter->slug]): ?>
					<a class="open"><?php echo $filter->name; ?></a>
					<div style="display: block;">
					<?php else: ?>
					<a><?php echo $filter->name; ?></a>
					<div>
					<?php endif; ?>
						<button class="all<?php if (!isset($_POST['rf_' . $filter->slug])) echo ' selected'; ?>">All</button>
						<?php foreach ($categories as $category): ?>
						<?php if (isset($_POST['rf_' . $filter->slug]) && in_array($category->slug, $_POST['rf_' . $filter->slug])): ?>
						<button class="selected"><?php echo $category->name; ?></button>
						<input checked name="rf_<?php echo $filter->slug; ?>[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php else: ?>
						<button><?php echo $category->name; ?></button>
						<input name="rf_<?php echo $filter->slug; ?>[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; ?>
				<?php if ($categories = get_terms([
					'taxonomy' => 'allergen'
				])): $count = count($categories); ?>
				<div>
					<?php if (isset($_POST['rf_allergen']) && $_POST['rf_allergen']): ?>
					<a class="open">Preferences &amp; Allergies</a>
					<div style="display: block;">
					<?php else: ?>
					<a>Preferences &amp; Allergies</a>
					<div>
					<?php endif; ?>
						<button class="all<?php if (!isset($_POST['rf_allergen'])) echo ' selected'; ?>">All</button>
						<?php foreach ($categories as $k => $category): ?>
						<?php if (isset($_POST['rf_allergen']) && in_array($category->slug, $_POST['rf_allergen'])): ?>
						<button class="selected"><?php echo $category->name; ?></button>
						<input checked name="rf_allergen[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php else: ?>
						<button><?php echo $category->name; ?></button>
						<input name="rf_allergen[]" type="checkbox" value="<?php echo $category->slug; ?>">
						<?php endif; ?>
						<?php if (4 == $count - $k): ?>
						<br>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<input name="rf" type="submit" value="FILTER RECIPES">
	</form>
	<div class="ak-post-widgets fi-panel results">
		<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" seo="true" post_type="recipes" posts_per_page="6" ' . $GLOBALS['Annabel_Karmel']->alm_recipe_finder($default_recipe_categories) . ' pause="true" scroll="false" images_loaded="true" button_label="Load More"]'); ?>
	</div>
</div>